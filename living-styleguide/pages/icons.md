# Icons
Below is the cheatsheet for Budicon Classic webfont.
First, link the "style.css" 
p.s. ignore the demo.css - that's the css for this cheatsheet

`<link rel="stylesheet" href="yourdomain/style.css">`

Then, copy and paste the "fonts" folder into your directory. 
It's best to put it in the same directory level with style.css
To use Budicon webfont simply use `<i>` tag and point the class name from the icon name on the list below.

`<i class="budicon-*iconName*"></i>`

Example:

`<i class="budicon-bacon"></i>`

## Kitchen Icons
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
| <i class="budicon-apron" style="font-size: 30px"></i>     | budicon-apron | **e900** |
| <i class="budicon-bbq" style="font-size: 30px"></i>     | budicon-bbq | **e901** |
| <i class="budicon-blower" style="font-size: 30px"></i>     | budicon-blower | **e902** |
| <i class="budicon-butcher-knife" style="font-size: 30px"></i>     | budicon-butcher-knife | **e903** 
| <i class="budicon-coffee-maker" style="font-size: 30px"></i>     | budicon-coffee-maker | **e904** |
| <i class="budicon-coffee-press" style="font-size: 30px"></i>     | budicon-coffee-press | **e905** |
| <i class="budicon-exhaust" style="font-size: 30px"></i>     | budicon-exhaust | **e906** |
| <i class="budicon-food-cover" style="font-size: 30px"></i>     | budicon-food-cover | **e907** |
| <i class="budicon-fridge" style="font-size: 30px"></i>     | budicon-fridge | **e908** |
| <i class="budicon-hand-mixer" style="font-size: 30px"></i>     | budicon-hand-mixer | **e909** |
| <i class="budicon-heat" style="font-size: 30px"></i>     | budicon-heat | **e90a** |
| <i class="budicon-high-pot" style="font-size: 30px"></i>     | budicon-high-pot | **e90b** |
| <i class="budicon-hot-pot" style="font-size: 30px"></i>     | budicon-hot-pot | **e90c** |
| <i class="budicon-ingredients" style="font-size: 30px"></i>     | budicon-ingredients | **e90d** |
| <i class="budicon-juice-container" style="font-size: 30px"></i>     | budicon-juice-container | **e90e** |
| <i class="budicon-juicer_1" style="font-size: 30px"></i>     | budicon-juicer_1 | **e90f** |
| <i class="budicon-juicer" style="font-size: 30px"></i>     | budicon-juicer | **e910** |
| <i class="budicon-kitchen-glove" style="font-size: 30px"></i>     | budicon-kitchen-glove | **e911** 
| <i class="budicon-kitchen-shaver" style="font-size: 30px"></i>     | budicon-kitchen-shaver | **e912** |
| <i class="budicon-knife" style="font-size: 30px"></i>     | budicon-knife | **e913** |
| <i class="budicon-large-pot" style="font-size: 30px"></i>     | budicon-large-pot | **e914** |
| <i class="budicon-low-pot" style="font-size: 30px"></i>     | budicon-low-pot | **e915** |
| <i class="budicon-measuring-cup" style="font-size: 30px"></i>     | budicon-measuring-cup | **e916** |
| <i class="budicon-microwave" style="font-size: 30px"></i>     | budicon-microwave | **e917** |
| <i class="budicon-mixer" style="font-size: 30px"></i>     | budicon-mixer | **e918** |
| <i class="budicon-outdoor-bbq" style="font-size: 30px"></i>     | budicon-outdoor-bbq | **e919** |
| <i class="budicon-pomodoro" style="font-size: 30px"></i>     | budicon-pomodoro | **e91a** |
| <i class="budicon-rice-cooker" style="font-size: 30px"></i>     | budicon-rice-cooker | **e91b** |
| <i class="budicon-roller" style="font-size: 30px"></i>     | budicon-roller | **e91c** |
| <i class="budicon-sauce-pan-hot" style="font-size: 30px"></i>     | budicon-sauce-pan-hot | **e91d** |
| <i class="budicon-sauce-pan" style="font-size: 30px"></i>     | budicon-sauce-pan | **e91e** |
| <i class="budicon-spatula" style="font-size: 30px"></i>     | budicon-spatula | **e91f** |
| <i class="budicon-stand-mixer" style="font-size: 30px"></i>     | budicon-stand-mixer | **e920** |
| <i class="budicon-stone-oven" style="font-size: 30px"></i>     | budicon-stone-oven | **e921** |
| <i class="budicon-taste" style="font-size: 30px"></i>     | budicon-taste | **e922** |
| <i class="budicon-tea-pot" style="font-size: 30px"></i>     | budicon-tea-pot | **e923** |
| <i class="budicon-temperature-controller" style="font-size: 30px"></i>     | budicon-temperature-controller | **e924** |
| <i class="budicon-tissue" style="font-size: 30px"></i>     | budicon-tissue | **e925** |
| <i class="budicon-toaster" style="font-size: 30px"></i>     | budicon-toaster | **e926** |
| <i class="budicon-trash-bin" style="font-size: 30px"></i>     | budicon-trash-bin | **e927** |
| <i class="budicon-weigher" style="font-size: 30px"></i>     | budicon-weigher | **e928** |

## Weather
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
| <i class="budicon-blowing-wind" style="font-size: 30px"></i>     | budicon-blowing-wind | **e929** |
| <i class="budicon-cloud" style="font-size: 30px"></i>     | budicon-cloud | **e92a** |
| <i class="budicon-cloudy-night" style="font-size: 30px"></i>     | budicon-cloudy-night | **e92b** |
| <i class="budicon-cloudy-day" style="font-size: 30px"></i>     | budicon-cloudy-day | **e92c** |
| <i class="budicon-day" style="font-size: 30px"></i>     | budicon-day | **e92d** |
| <i class="budicon-decrease-temperature" style="font-size: 30px"></i>     | budicon-decrease-temperature | **e92e** |
| <i class="budicon-drizzling-day" style="font-size: 30px"></i>     | budicon-drizzling-day | **e92f** |
| <i class="budicon-blowing-wind" style="font-size: 30px"></i>  | budicon-blowing-wind | **e929** |
| <i class="budicon-cloud"></i> | budicon-cloud | **e92a** |
| <i class="budicon-cloudy-night"></i> | budicon-cloudy-night | **e92b** |
| <i class="budicon-cloudy-day"></i> | budicon-cloudy-day | **e92c** |
| <i class="budicon-day"></i> | budicon-day | **e92d** |
| <i class="budicon-decrease-temperature"></i> | budicon-decrease-temperature | **e92e** |
| <i class="budicon-drizzling-day"></i> | budicon-drizzling-day | **e92f** |
| <i class="budicon-drizzling-night"></i> | budicon-drizzling-night | **e930** |
| <i class="budicon-drizzling"></i> | budicon-drizzling | **e931** |
| <i class="budicon-flake-day"></i> | budicon-flake-day | **e932** |
| <i class="budicon-flake-night"></i> | budicon-flake-night | **e933** |
| <i class="budicon-flake"></i> | budicon-flake | **e934** |
| <i class="budicon-gloomy-day"></i> | budicon-gloomy-day | **e935** |
| <i class="budicon-gloomy-night"></i> | budicon-gloomy-night | **e936** |
| <i class="budicon-gloomy"></i> | budicon-gloomy | **e937** |
| <i class="budicon-half-moon"></i> | budicon-half-moon | **e938** |
| <i class="budicon-heavy-rain-day"></i> | budicon-heavy-rain-day | **e939** |
| <i class="budicon-heavy-rain-night"></i> | budicon-heavy-rain-night | **e93a** |
| <i class="budicon-heavy-rain"></i> | budicon-heavy-rain | **e93b** |
| <i class="budicon-high-temperature"></i> | budicon-high-temperature | **e93c** |
| <i class="budicon-increase-temperature"></i> | budicon-increase-temperature | **e93d** |
| <i class="budicon-lighting-day"></i> | budicon-lighting-day | **e93e** |
| <i class="budicon-lighting-night"></i> | budicon-lighting-night | **e93f** |
| <i class="budicon-lighting"></i> | budicon-lighting | **e940** |
| <i class="budicon-low-temperature"></i> | budicon-low-temperature | **e941** |
| <i class="budicon-medium-temperature"></i> | budicon-medium-temperature | **e942** |
| <i class="budicon-melting-point"></i> | budicon-melting-point | **e943** |
| <i class="budicon-moon-reflection"></i> | budicon-moon-reflection | **e944** |
| <i class="budicon-moonlight"></i> | budicon-moonlight | **e945** |
| <i class="budicon-night"></i> | budicon-night | **e946** |
| <i class="budicon-pouring-day"></i> | budicon-pouring-day | **e947** |
| <i class="budicon-pouring-night"></i> | budicon-pouring-night | **e948** |
| <i class="budicon-pouring"></i> | budicon-pouring | **e949** |
| <i class="budicon-rain"></i> | budicon-rain | **e94a** |
| <i class="budicon-raining-day"></i> | budicon-raining-day | **e94b** |
| <i class="budicon-raining-night"></i> | budicon-raining-night | **e94c** |
| <i class="budicon-snowflakes"></i> | budicon-snowflakes | **e94d** |
| <i class="budicon-snowing-day"></i> | budicon-snowing-day | **e94e** |
| <i class="budicon-snowing-night"></i> | budicon-snowing-night | **e94f** |
| <i class="budicon-snowing"></i> | budicon-snowing | **e950** |
| <i class="budicon-sun-down"></i> | budicon-sun-down | **e951** |
| <i class="budicon-sun-reflection"></i> | budicon-sun-reflection | **e952** |
| <i class="budicon-sun-set"></i> | budicon-sun-set | **e953** |
| <i class="budicon-sun-up"></i> | budicon-sun-up | **e954** |
| <i class="budicon-three-quarter-moon"></i> | budicon-three-quarter-moon | **e955** |
| <i class="budicon-typhoon"></i> | budicon-typhoon | **e956** |
| <i class="budicon-windometer"></i> | budicon-windometer | **e957** |
| <i class="budicon-windy-day"></i> | budicon-windy-day | **e958** |
| <i class="budicon-windy-night"></i> | budicon-windy-night | **e959** |
| <i class="budicon-windy"></i> | budicon-windy | **e95a** |

## Layout
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
| <i class="budicon-bottombar"></i> | budicon-bottombar | **e95b** |
| <i class="budicon-card-stack-view"></i> | budicon-card-stack-view | **e95c** |
| <i class="budicon-collapse-bottom"></i> | budicon-collapse-bottom | **e95d** |
| <i class="budicon-collapse-left"></i> | budicon-collapse-left | **e95e** |
| <i class="budicon-collapse-right"></i> | budicon-collapse-right | **e95f** |
| <i class="budicon-collapse-top"></i> | budicon-collapse-top | **e960** |
| <i class="budicon-feature-block"></i> | budicon-feature-block | **e961** |
| <i class="budicon-inside-element"></i> | budicon-inside-element | **e962** |
| <i class="budicon-layout-third-vertical"></i> | budicon-layout-third-vertical | **e963** |
| <i class="budicon-layout-third-view"></i> | budicon-layout-third-view | **e964** |
| <i class="budicon-layout-two-view"></i> | budicon-layout-two-view | **e965** |
| <i class="budicon-one-third"></i> | budicon-one-third | **e966** |
| <i class="budicon-sidebar-left-content"></i> | budicon-sidebar-left-content | **e967** |
| <i class="budicon-sidebar-left"></i> | budicon-sidebar-left | **e968** |
| <i class="budicon-sidebar-right-content"></i> | budicon-sidebar-right-content | **e969** |
| <i class="budicon-sidebar-right"></i> | budicon-sidebar-right | **e96a** |
| <i class="budicon-third-one"></i> | budicon-third-one | **e96b** |
| <i class="budicon-topbar"></i> | budicon-topbar | **e96c** |
| <i class="budicon-two-grids"></i> | budicon-two-grids | **e96d** |
| <i class="budicon-two-thirds"></i> | budicon-two-thirds | **e96e** |

## Interface
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
| <i class="budicon-alert-sign-a"></i> | budicon-alert-sign-a | **e96f** |
| <i class="budicon-alert-sign"></i> | budicon-alert-sign | **e970** |
| <i class="budicon-app-notification"></i> | budicon-app-notification | **e971** |
| <i class="budicon-appear-password"></i> | budicon-appear-password | **e972** |
| <i class="budicon-block-sign"></i> | budicon-block-sign | **e973** |
| <i class="budicon-bookmark-ribbon"></i> | budicon-bookmark-ribbon | **e974** |
| <i class="budicon-card-slides"></i> | budicon-card-slides | **e975** |
| <i class="budicon-check-ui"></i> | budicon-check-ui | **e976** |
| <i class="budicon-checkboxes"></i> | budicon-checkboxes | **e977** |
| <i class="budicon-cloud-download"></i> | budicon-cloud-download | **e978** |
| <i class="budicon-cloud-upload"></i> | budicon-cloud-upload | **e979** |
| <i class="budicon-cmd-sign"></i> | budicon-cmd-sign | **e97a** |
| <i class="budicon-cog"></i> | budicon-cog | **e97b** |
| <i class="budicon-collapse-list"></i> | budicon-collapse-list | **e97c** |
| <i class="budicon-cross-sign-square"></i> | budicon-cross-sign-square | **e97d** |
| <i class="budicon-cross-sign"></i> | budicon-cross-sign | **e97e** |
| <i class="budicon-cross-ui"></i> | budicon-cross-ui | **e97f** |
| <i class="budicon-dashboard"></i> | budicon-dashboard | **e980** |
| <i class="budicon-dislike"></i> | budicon-dislike | **e981** |
| <i class="budicon-download-queue"></i> | budicon-download-queue | **e982** |
| <i class="budicon-exit-fullscreen"></i> | budicon-exit-fullscreen | **e983** |
| <i class="budicon-expand-list"></i> | budicon-expand-list | **e984** |
| <i class="budicon-expand-view"></i> | budicon-expand-view | **e985** |
| <i class="budicon-filter"></i> | budicon-filter | **e986** |
| <i class="budicon-fullscreen"></i> | budicon-fullscreen | **e987** |
| <i class="budicon-hamburger-ui-a"></i> | budicon-hamburger-ui-a | **e988** |
| <i class="budicon-hamburger-ui-b"></i> | budicon-hamburger-ui-b | **e989** |
| <i class="budicon-hamburger-ui"></i> | budicon-hamburger-ui | **e98a** |
| <i class="budicon-heart"></i> | budicon-heart | **e98b** |
| <i class="budicon-hide-password"></i> | budicon-hide-password | **e98c** |
| <i class="budicon-home-a"></i> | budicon-home-a | **e98d** |
| <i class="budicon-home"></i> | budicon-home | **e98e** |
| <i class="budicon-internal-link"></i> | budicon-internal-link | **e98f** |
| <i class="budicon-kebab-ui"></i> | budicon-kebab-ui | **e990** |
| <i class="budicon-life-buoy"></i> | budicon-life-buoy | **e991** |
| <i class="budicon-like"></i> | budicon-like | **e992** |
| <i class="budicon-loading-sign"></i> | budicon-loading-sign | **e993** |
| <i class="budicon-lock"></i> | budicon-lock | **e994** |
| <i class="budicon-login"></i> | budicon-login | **e995** |
| <i class="budicon-logout"></i> | budicon-logout | **e996** |
| <i class="budicon-meatball-ui"></i> | budicon-meatball-ui | **e997** |
| <i class="budicon-mic-ui-mute"></i> | budicon-mic-ui-mute | **e998** |
| <i class="budicon-mic-ui"></i> | budicon-mic-ui | **e999** |
| <i class="budicon-minimize-view"></i> | budicon-minimize-view | **e99a** |
| <i class="budicon-minus-sign-square"></i> | budicon-minus-sign-square | **e99b** |
| <i class="budicon-minus-sign"></i> | budicon-minus-sign | **e99c** |
| <i class="budicon-minus-ui"></i> | budicon-minus-ui | **e99d** |
| <i class="budicon-monitor-resolution"></i> | budicon-monitor-resolution | **e99e** |
| <i class="budicon-move-to-bottom"></i> | budicon-move-to-bottom | **e99f** |
| <i class="budicon-move-to-top"></i> | budicon-move-to-top | **e9a0** |
| <i class="budicon-notification-a"></i> | budicon-notification-a | **e9a1** |
| <i class="budicon-notification"></i> | budicon-notification | **e9a2** |
| <i class="budicon-outgoing-link"></i> | budicon-outgoing-link | **e9a3** |
| <i class="budicon-plus-sign-square"></i> | budicon-plus-sign-square | **e9a4** |
| <i class="budicon-plus-sign"></i> | budicon-plus-sign | **e9a5** |
| <i class="budicon-plus-ui"></i> | budicon-plus-ui | **e9a6** |
| <i class="budicon-power-sign"></i> | budicon-power-sign | **e9a7** |
| <i class="budicon-previouse-queue"></i> | budicon-previouse-queue | **e9a8** |
| <i class="budicon-print-a"></i> | budicon-print-a | **e9a9** |
| <i class="budicon-print"></i> | budicon-print | **e9aa** |
| <i class="budicon-radar"></i> | budicon-radar | **e9ab** |
| <i class="budicon-refresh-ui"></i> | budicon-refresh-ui | **e9ac** |
| <i class="budicon-reload-ui"></i> | budicon-reload-ui | **e9ad** |
| <i class="budicon-reply-a"></i> | budicon-reply-a | **e9ae** |
| <i class="budicon-reply-all"></i> | budicon-reply-all | **e9af** |
| <i class="budicon-reply"></i> | budicon-reply | **e9b0** |
| <i class="budicon-search-a"></i> | budicon-search-a | **e9b1** |
| <i class="budicon-search-cross-a"></i> | budicon-search-cross-a | **e9b2** |
| <i class="budicon-search-cross"></i> | budicon-search-cross | **e9b3** |
| <i class="budicon-search-list"></i> | budicon-search-list | **e9b4** |
| <i class="budicon-search-minus-a"></i> | budicon-search-minus-a | **e9b5** |
| <i class="budicon-search-minus"></i> | budicon-search-minus | **e9b6** |
| <i class="budicon-search-plus-a"></i> | budicon-search-plus-a | **e9b7** |
| <i class="budicon-search-plus"></i> | budicon-search-plus | **e9b8** |
| <i class="budicon-search-tick-a"></i> | budicon-search-tick-a | **e9b9** |
| <i class="budicon-search-tick"></i> | budicon-search-tick | **e9ba** |
| <i class="budicon-search-view-minus"></i> | budicon-search-view-minus | **e9bb** |
| <i class="budicon-search-view-plus"></i> | budicon-search-view-plus | **e9bc** |
| <i class="budicon-search-view"></i> | budicon-search-view | **e9bd** |
| <i class="budicon-search"></i> | budicon-search | **e9be** |
| <i class="budicon-share"></i> | budicon-share | **e9bf** |
| <i class="budicon-shortcut-list"></i> | budicon-shortcut-list | **e9c0** |
| <i class="budicon-star"></i> | budicon-star | **e9c1** |
| <i class="budicon-stars"></i> | budicon-stars | **e9c2** |
| <i class="budicon-tick-sign-square"></i> | budicon-tick-sign-square | **e9c3** |
| <i class="budicon-tick-sign"></i> | budicon-tick-sign | **e9c4** |
| <i class="budicon-trash-cancel"></i> | budicon-trash-cancel | **e9c5** |
| <i class="budicon-trash-ui"></i> | budicon-trash-ui | **e9c6** |
| <i class="budicon-trash"></i> | budicon-trash | **e9c7** |
| <i class="budicon-ui-compose-a"></i> | budicon-ui-compose-a | **e9c8** |
| <i class="budicon-ui-compose"></i> | budicon-ui-compose | **e9c9** |
| <i class="budicon-ui-flag-a"></i> | budicon-ui-flag-a | **e9ca** |
| <i class="budicon-ui-flag-b"></i> | budicon-ui-flag-b | **e9cb** |
| <i class="budicon-ui-flag"></i> | budicon-ui-flag | **e9cc** |
| <i class="budicon-ui-speed"></i> | budicon-ui-speed | **e9cd** |
| <i class="budicon-unlock"></i> | budicon-unlock | **e9ce** |
| <i class="budicon-upload-queue"></i> | budicon-upload-queue | **e9cf** |
| <i class="budicon-uploading-ui"></i> | budicon-uploading-ui | **e9d0** |
| <i class="budicon-view"></i> | budicon-view | **e9d1** |
| <i class="budicon-wide-fullscreen"></i> | budicon-wide-fullscreen | **e9d2** |

## Writing
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
| <i class="budicon-alignment-center"></i> | budicon-alignment-center | **e9d3** |
| <i class="budicon-alignment-left"></i> | budicon-alignment-left | **e9d4** |
| <i class="budicon-alignment-right"></i> | budicon-alignment-right | **e9d5** |
| <i class="budicon-anchor-link"></i> | budicon-anchor-link | **e9d6** |
| <i class="budicon-ascending-order"></i> | budicon-ascending-order | **e9d7** |
| <i class="budicon-asterisk"></i> | budicon-asterisk | **e9d8** |
| <i class="budicon-change-order"></i> | budicon-change-order | **e9d9** |
| <i class="budicon-check-done"></i> | budicon-check-done | **e9da** |
| <i class="budicon-columns"></i> | budicon-columns | **e9db** |
| <i class="budicon-compose"></i> | budicon-compose | **e9dc** |
| <i class="budicon-descending-order"></i> | budicon-descending-order | **e9dd** |
| <i class="budicon-digital-keyboard"></i> | budicon-digital-keyboard | **e9de** |
| <i class="budicon-document-page"></i> | budicon-document-page | **e9df** |
| <i class="budicon-forward-page"></i> | budicon-forward-page | **e9e0** |
| <i class="budicon-front-page"></i> | budicon-front-page | **e9e1** |
| <i class="budicon-grid-view"></i> | budicon-grid-view | **e9e2** |
| <i class="budicon-image-caption"></i> | budicon-image-caption | **e9e3** |
| <i class="budicon-indent-left"></i> | budicon-indent-left | **e9e4** |
| <i class="budicon-indent-right"></i> | budicon-indent-right | **e9e5** |
| <i class="budicon-justify-center"></i> | budicon-justify-center | **e9e6** |
| <i class="budicon-justify-left"></i> | budicon-justify-left | **e9e7** |
| <i class="budicon-justify-right"></i> | budicon-justify-right | **e9e8** |
| <i class="budicon-keyboard-appear"></i> | budicon-keyboard-appear | **e9e9** |
| <i class="budicon-keyboard-down"></i> | budicon-keyboard-down | **e9ea** |
| <i class="budicon-media-player-page"></i> | budicon-media-player-page | **e9eb** |
| <i class="budicon-page-blank"></i> | budicon-page-blank | **e9ec** |
| <i class="budicon-page-block"></i> | budicon-page-block | **e9ed** |
| <i class="budicon-page-cross"></i> | budicon-page-cross | **e9ee**|
|<i class="budicon-page-lock"></i> | budicon-page-lock | **e9ef**|
|<i class="budicon-page-mark"></i> | budicon-page-mark | **e9f0**|
|<i class="budicon-page-minus"></i> | budicon-page-minus | **e9f1**|
|<i class="budicon-page-plus"></i> | budicon-page-plus | **e9f2**|
|<i class="budicon-page-tick"></i> | budicon-page-tick | **e9f3**|
|<i class="budicon-pen-add"></i> | budicon-pen-add | **e9f4**|
|<i class="budicon-pen-cross"></i> | budicon-pen-cross | **e9f5**|
|<i class="budicon-pen-minus"></i> | budicon-pen-minus | **e9f6**|
|<i class="budicon-pen-tick"></i> | budicon-pen-tick | **e9f7**|
|<i class="budicon-pen-writing"></i> | budicon-pen-writing | **e9f8**|
|<i class="budicon-pen"></i> | budicon-pen | **e9f9**|
|<i class="budicon-pencil-writing"></i> | budicon-pencil-writing | **e9fa**|
|<i class="budicon-reading-list"></i> | budicon-reading-list | **e9fb**|
|<i class="budicon-redo"></i> | budicon-redo | **e9fc**|
|<i class="budicon-search-page"></i> | budicon-search-page | **e9fd**|
|<i class="budicon-text-box"></i> | budicon-text-box | **e9fe**|
|<i class="budicon-undo"></i> | budicon-undo | **e9ff**|
|<i class="budicon-vertical-align-bottom"></i> | budicon-vertical-align-bottom | **ea00**|
|<i class="budicon-vertical-align-top"></i> | budicon-vertical-align-top | **ea01**|
|<i class="budicon-vertical-allign-middle"></i> | budicon-vertical-allign-middle | **ea02**|
|<i class="budicon-writing-link"></i> | budicon-writing-link | **ea03**|

## Medical
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-ambulance"></i> | budicon-ambulance | **ea04**|
|<i class="budicon-baby"></i> | budicon-baby | **ea05**|
|<i class="budicon-bandage"></i> | budicon-bandage | **ea06**|
|<i class="budicon-blood-donation"></i> | budicon-blood-donation | **ea07**|
|<i class="budicon-blood-transfusion"></i> | budicon-blood-transfusion | **ea08**|
|<i class="budicon-blood-type"></i> | budicon-blood-type | **ea09**|
|<i class="budicon-blood"></i> | budicon-blood | **ea0a**|
|<i class="budicon-body-weigher"></i> | budicon-body-weigher | **ea0b**|
|<i class="budicon-cardio-record"></i> | budicon-cardio-record | **ea0c**|
|<i class="budicon-dialysis"></i> | budicon-dialysis | **ea0d**|
|<i class="budicon-dna"></i> | budicon-dna | **ea0e**|
|<i class="budicon-eye-chart"></i> | budicon-eye-chart | **ea0f**|
|<i class="budicon-eye-sight"></i> | budicon-eye-sight | **ea10**|
|<i class="budicon-handicap"></i> | budicon-handicap | **ea11**|
|<i class="budicon-hearing"></i> | budicon-hearing | **ea12**|
|<i class="budicon-heart-monitor"></i> | budicon-heart-monitor | **ea13**|
|<i class="budicon-heart-rate"></i> | budicon-heart-rate | **ea14**|
|<i class="budicon-hiv-ribbon"></i> | budicon-hiv-ribbon | **ea15**|
|<i class="budicon-hospital-sign"></i> | budicon-hospital-sign | **ea16**|
|<i class="budicon-hospital"></i> | budicon-hospital | **ea17**|
|<i class="budicon-infusion"></i> | budicon-infusion | **ea18**|
|<i class="budicon-injection"></i> | budicon-injection | **ea19**|
|<i class="budicon-lab-liquid"></i> | budicon-lab-liquid | **ea1a**|
|<i class="budicon-lab-test"></i> | budicon-lab-test | **ea1b**|
|<i class="budicon-lab"></i> | budicon-lab | **ea1c**|
|<i class="budicon-medical-assistance"></i> | budicon-medical-assistance | **ea1d**|
|<i class="budicon-medical-bag"></i> | budicon-medical-bag | **ea1e**|
|<i class="budicon-medical-case"></i> | budicon-medical-case | **ea1f**|
|<i class="budicon-medical-liquid"></i> | budicon-medical-liquid | **ea20**|
|<i class="budicon-medical-note"></i> | budicon-medical-note | **ea21**|
|<i class="budicon-medical-record"></i> | budicon-medical-record | **ea22**|
|<i class="budicon-medicine"></i> | budicon-medicine | **ea23**|
|<i class="budicon-online-medical"></i> | budicon-online-medical | **ea24**|
|<i class="budicon-pills"></i> | budicon-pills | **ea25**|
|<i class="budicon-potions"></i> | budicon-potions | **ea26**|
|<i class="budicon-scale"></i> | budicon-scale | **ea27**|
|<i class="budicon-sperm"></i> | budicon-sperm | **ea28**|
|<i class="budicon-sthethoscope"></i> | budicon-sthethoscope | **ea29**|
|<i class="budicon-themometer"></i> | budicon-themometer | **ea2a**|
|<i class="budicon-x-ray"></i> | budicon-x-ray | **ea2b**|

## Photo and Video
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-aperture"></i> | budicon-aperture | **ea2c**|
|<i class="budicon-area-focus"></i> | budicon-area-focus | **ea2d**|
|<i class="budicon-brightness-a"></i> | budicon-brightness-a | **ea2e**|
|<i class="budicon-brightness"></i> | budicon-brightness | **ea2f**|
|<i class="budicon-camera-scan"></i> | budicon-camera-scan | **ea30**|
|<i class="budicon-camera-timer"></i> | budicon-camera-timer | **ea31**|
|<i class="budicon-camera-wifi"></i> | budicon-camera-wifi | **ea32**|
|<i class="budicon-camera"></i> | budicon-camera | **ea33**|
|<i class="budicon-cityscape"></i> | budicon-cityscape | **ea34**|
|<i class="budicon-crop"></i> | budicon-crop | **ea35**|
|<i class="budicon-dslr-menu"></i> | budicon-dslr-menu | **ea36**|
|<i class="budicon-exposure-compensation"></i> | budicon-exposure-compensation | **ea37**|
|<i class="budicon-exposure"></i> | budicon-exposure | **ea38**|
|<i class="budicon-film-clip"></i> | budicon-film-clip | **ea39**|
|<i class="budicon-flashlight"></i> | budicon-flashlight | **ea3a**|
|<i class="budicon-flip-horizontal"></i> | budicon-flip-horizontal | **ea3b**|
|<i class="budicon-flip-vertical"></i> | budicon-flip-vertical | **ea3c**|
|<i class="budicon-focus-point"></i> | budicon-focus-point | **ea3d**|
|<i class="budicon-grid"></i> | budicon-grid | **ea3e**|
|<i class="budicon-half-exposure"></i> | budicon-half-exposure | **ea3f**|
|<i class="budicon-landscape"></i> | budicon-landscape | **ea40**|
|<i class="budicon-live-video"></i> | budicon-live-video | **ea41**|
|<i class="budicon-pocket-camera"></i> | budicon-pocket-camera | **ea42**|
|<i class="budicon-polaroid-stack"></i> | budicon-polaroid-stack | **ea43**|
|<i class="budicon-polaroid"></i> | budicon-polaroid | **ea44**|
|<i class="budicon-red-eye-detection"></i> | budicon-red-eye-detection | **ea45**|
|<i class="budicon-roll-film"></i> | budicon-roll-film | **ea46**|
|<i class="budicon-rotate-camera"></i> | budicon-rotate-camera | **ea47**|
|<i class="budicon-rotate-image"></i> | budicon-rotate-image | **ea48**|
|<i class="budicon-sharpen"></i> | budicon-sharpen | **ea49**|
|<i class="budicon-single-point-focus"></i> | budicon-single-point-focus | **ea4a**|
|<i class="budicon-slideshow-photo"></i> | budicon-slideshow-photo | **ea4b**|
|<i class="budicon-smile-detection"></i> | budicon-smile-detection | **ea4c**|
|<i class="budicon-video-cam-a"></i> | budicon-video-cam-a | **ea4d**|
|<i class="budicon-video-cam"></i> | budicon-video-cam | **ea4e**|
|<i class="budicon-video-recorder"></i> | budicon-video-recorder | **ea4f**|
|<i class="budicon-vintage-camera"></i> | budicon-vintage-camera | **ea50**|

## Design
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-bottom-aligned"></i> | budicon-bottom-aligned | **ea51**|
|<i class="budicon-center-aligned"></i> | budicon-center-aligned | **ea52**|
|<i class="budicon-clipboard"></i> | budicon-clipboard | **ea53**|
|<i class="budicon-design-compass"></i> | budicon-design-compass | **ea54**|
|<i class="budicon-doodle"></i> | budicon-doodle | **ea55**|
|<i class="budicon-eraser"></i> | budicon-eraser | **ea56**|
|<i class="budicon-horizontal-distribute"></i> | budicon-horizontal-distribute | **ea57**|
|<i class="budicon-left-aligned"></i> | budicon-left-aligned | **ea58**|
|<i class="budicon-middle-aligned"></i> | budicon-middle-aligned | **ea59**|
|<i class="budicon-prototype"></i> | budicon-prototype | **ea5a**|
|<i class="budicon-right-aligned"></i> | budicon-right-aligned | **ea5b**|
|<i class="budicon-ruler"></i> | budicon-ruler | **ea5c**|
|<i class="budicon-top-aligned"></i> | budicon-top-aligned | **ea5d**|
|<i class="budicon-vector-clipboard"></i> | budicon-vector-clipboard | **ea5e**|
|<i class="budicon-vector-editing"></i> | budicon-vector-editing | **ea5f**|
|<i class="budicon-vertical-distribute"></i> | budicon-vertical-distribute | **ea60**|

## Space
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-alien"></i> | budicon-alien | **ea61**|
|<i class="budicon-astronout"></i> | budicon-astronout | **ea62**|
|<i class="budicon-comet"></i> | budicon-comet | **ea63**|
|<i class="budicon-earth"></i> | budicon-earth | **ea64**|
|<i class="budicon-galaxy-hole"></i> | budicon-galaxy-hole | **ea65**|
|<i class="budicon-galaxy"></i> | budicon-galaxy | **ea66**|
|<i class="budicon-jupiter"></i> | budicon-jupiter | **ea67**|
|<i class="budicon-lunar-flag"></i> | budicon-lunar-flag | **ea68**|
|<i class="budicon-mars"></i> | budicon-mars | **ea69**|
|<i class="budicon-observatory"></i> | budicon-observatory | **ea6a**|
|<i class="budicon-radar2"></i> | budicon-radar2 | **ea6b**|
|<i class="budicon-raygun"></i> | budicon-raygun | **ea6c**|
|<i class="budicon-research"></i> | budicon-research | **ea6d**|
|<i class="budicon-robot"></i> | budicon-robot | **ea6e**|
|<i class="budicon-rocket"></i> | budicon-rocket | **ea6f**|
|<i class="budicon-satellite"></i> | budicon-satellite | **ea70**|
|<i class="budicon-saturn"></i> | budicon-saturn | **ea71**|
|<i class="budicon-science"></i> | budicon-science | **ea72**|
|<i class="budicon-space-helmet"></i> | budicon-space-helmet | **ea73**|
|<i class="budicon-space-house"></i> | budicon-space-house | **ea74**|
|<i class="budicon-space-shuttle"></i> | budicon-space-shuttle | **ea75**|
|<i class="budicon-spaceship"></i> | budicon-spaceship | **ea76**|
|<i class="budicon-telescope"></i> | budicon-telescope | **ea77**|
|<i class="budicon-ufo"></i> | budicon-ufo | **ea78**|

## Task
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-search-task"></i> | budicon-search-task | **ea79**|
|<i class="budicon-task-alert"></i> | budicon-task-alert | **ea7a**|
|<i class="budicon-task-block"></i> | budicon-task-block | **ea7b**|
|<i class="budicon-task-cross"></i> | budicon-task-cross | **ea7c**|
|<i class="budicon-task-download"></i> | budicon-task-download | **ea7d**|
|<i class="budicon-task-favorite"></i> | budicon-task-favorite | **ea7e**|
|<i class="budicon-task-information"></i> | budicon-task-information | **ea7f**|
|<i class="budicon-task-list"></i> | budicon-task-list | **ea80**|
|<i class="budicon-task-lock"></i> | budicon-task-lock | **ea81**|
|<i class="budicon-task-love"></i> | budicon-task-love | **ea82**|
|<i class="budicon-task-minus"></i> | budicon-task-minus | **ea83**|
|<i class="budicon-task-plus"></i> | budicon-task-plus | **ea84**|
|<i class="budicon-task-question"></i> | budicon-task-question | **ea85**|
|<i class="budicon-task-scheduled"></i> | budicon-task-scheduled | **ea86**|
|<i class="budicon-task-shared"></i> | budicon-task-shared | **ea87**|
|<i class="budicon-task-tick"></i> | budicon-task-tick | **ea88**|
|<i class="budicon-task-upload"></i> | budicon-task-upload | **ea89**|
|<i class="budicon-task"></i> | budicon-task | **ea8a**|

## Technology
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-d-glasses"></i> | budicon-d-glasses | **ea8b**|
|<i class="budicon-airdrop"></i> | budicon-airdrop | **ea8c**|
|<i class="budicon-android-landscape"></i> | budicon-android-landscape | **ea8d**|
|<i class="budicon-android-wifi"></i> | budicon-android-wifi | **ea8e**|
|<i class="budicon-android"></i> | budicon-android | **ea8f**|
|<i class="budicon-bluetooth-circle"></i> | budicon-bluetooth-circle | **ea90**|
|<i class="budicon-bluetooth"></i> | budicon-bluetooth | **ea91**|
|<i class="budicon-browser-cross"></i> | budicon-browser-cross | **ea92**|
|<i class="budicon-browser-minus"></i> | budicon-browser-minus | **ea93**|
|<i class="budicon-browser-plus"></i> | budicon-browser-plus | **ea94**|
|<i class="budicon-browser-tick"></i> | budicon-browser-tick | **ea95**|
|<i class="budicon-browser"></i> | budicon-browser | **ea96**|
|<i class="budicon-cable-cord"></i> | budicon-cable-cord | **ea97**|
|<i class="budicon-cardboard"></i> | budicon-cardboard | **ea98**|
|<i class="budicon-charging"></i> | budicon-charging | **ea99**|
|<i class="budicon-chrome-cast"></i> | budicon-chrome-cast | **ea9a**|
|<i class="budicon-cloud-server"></i> | budicon-cloud-server | **ea9b**|
|<i class="budicon-desktop-smartphone"></i> | budicon-desktop-smartphone | **ea9c**|
|<i class="budicon-desktop-wifi"></i> | budicon-desktop-wifi | **ea9d**|
|<i class="budicon-desktop"></i> | budicon-desktop | **ea9e**|
|<i class="budicon-development-bug"></i> | budicon-development-bug | **ea9f**|
|<i class="budicon-drone"></i> | budicon-drone | **eaa0**|
|<i class="budicon-dvd-i-cable"></i> | budicon-dvd-i-cable | **eaa1**|
|<i class="budicon-flashlight2"></i> | budicon-flashlight2 | **eaa2**|
|<i class="budicon-game-console"></i> | budicon-game-console | **eaa3**|
|<i class="budicon-gameboy"></i> | budicon-gameboy | **eaa4**|
|<i class="budicon-gaming-mouse"></i> | budicon-gaming-mouse | **eaa5**|
|<i class="budicon-google-phone-landscape"></i> | budicon-google-phone-landscape | **eaa6**|
|<i class="budicon-google-phone"></i> | budicon-google-phone | **eaa7**|
|<i class="budicon-hdmi-cable"></i> | budicon-hdmi-cable | **eaa8**|
|<i class="budicon-hosting-server"></i> | budicon-hosting-server | **eaa9**|
|<i class="budicon-house-cloud"></i> | budicon-house-cloud | **eaaa**|
|<i class="budicon-house-network"></i> | budicon-house-network | **eaab**|
|<i class="budicon-internet-network"></i> | budicon-internet-network | **eaac**|
|<i class="budicon-iphone-landscape"></i> | budicon-iphone-landscape | **eaad**|
|<i class="budicon-iphone-wifi"></i> | budicon-iphone-wifi | **eaae**|
|<i class="budicon-iphone"></i> | budicon-iphone | **eaaf**|
|<i class="budicon-keyboard-a"></i> | budicon-keyboard-a | **eab0**|
|<i class="budicon-keyboard"></i> | budicon-keyboard | **eab1**|
|<i class="budicon-laptop-smartphone"></i> | budicon-laptop-smartphone | **eab2**|
|<i class="budicon-laptop-wifi"></i> | budicon-laptop-wifi | **eab3**|
|<i class="budicon-laptop"></i> | budicon-laptop | **eab4**|
|<i class="budicon-magic-mouse"></i> | budicon-magic-mouse | **eab5**|
|<i class="budicon-magnet"></i> | budicon-magnet | **eab6**|
|<i class="budicon-mobile-earphone"></i> | budicon-mobile-earphone | **eab7**|
|<i class="budicon-mobile-photo-scan"></i> | budicon-mobile-photo-scan | **eab8**|
|<i class="budicon-modem-externder"></i> | budicon-modem-externder | **eab9**|
|<i class="budicon-modem"></i> | budicon-modem | **eaba**|
|<i class="budicon-nintendo-console"></i> | budicon-nintendo-console | **eabb**|
|<i class="budicon-processor-chip"></i> | budicon-processor-chip | **eabc**|
|<i class="budicon-projector"></i> | budicon-projector | **eabd**|
|<i class="budicon-remote-control"></i> | budicon-remote-control | **eabe**|
|<i class="budicon-search-browser"></i> | budicon-search-browser | **eabf**|
|<i class="budicon-security-guard"></i> | budicon-security-guard | **eac0**|
|<i class="budicon-signal-tower"></i> | budicon-signal-tower | **eac1**|
|<i class="budicon-television"></i> | budicon-television | **eac2**|
|<i class="budicon-usb-cable"></i> | budicon-usb-cable | **eac3**|
|<i class="budicon-webcam"></i> | budicon-webcam | **eac4**|
|<i class="budicon-world-connection"></i> | budicon-world-connection | **eac5**|
|<i class="budicon-world-network"></i> | budicon-world-network | **eac6**|

## Notes
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-note-alert"></i> | budicon-note-alert | **eac7**|
|<i class="budicon-note-block"></i> | budicon-note-block | **eac8**|
|<i class="budicon-note-cross"></i> | budicon-note-cross | **eac9**|
|<i class="budicon-note-download"></i> | budicon-note-download | **eaca**|
|<i class="budicon-note-favorite"></i> | budicon-note-favorite | **eacb**|
|<i class="budicon-note-information"></i> | budicon-note-information | **eacc**|
|<i class="budicon-note-lock"></i> | budicon-note-lock | **eacd**|
|<i class="budicon-note-love"></i> | budicon-note-love | **eace**|
|<i class="budicon-note-minus"></i> | budicon-note-minus | **eacf**|
|<i class="budicon-note-plus"></i> | budicon-note-plus | **ead0**|
|<i class="budicon-note-quesrion"></i> | budicon-note-quesrion | **ead1**|
|<i class="budicon-note-search"></i> | budicon-note-search | **ead2**|
|<i class="budicon-note-tick"></i> | budicon-note-tick | **ead3**|
|<i class="budicon-note-upload"></i> | budicon-note-upload | **ead4**|
|<i class="budicon-note"></i> | budicon-note | **ead5**|
|<i class="budicon-notes-alert"></i> | budicon-notes-alert | **ead6**|
|<i class="budicon-notes-blank"></i> | budicon-notes-blank | **ead7**|
|<i class="budicon-notes-block"></i> | budicon-notes-block | **ead8**|
|<i class="budicon-notes-cross"></i> | budicon-notes-cross | **ead9**|
|<i class="budicon-notes-download"></i> | budicon-notes-download | **eada**|
|<i class="budicon-notes-favorite"></i> | budicon-notes-favorite | **eadb**|
|<i class="budicon-notes-information"></i> | budicon-notes-information | **eadc**|
|<i class="budicon-notes-list-a"></i> | budicon-notes-list-a | **eadd**|
|<i class="budicon-notes-list"></i> | budicon-notes-list | **eade**|
|<i class="budicon-notes-lock"></i> | budicon-notes-lock | **eadf**|
|<i class="budicon-notes-love"></i> | budicon-notes-love | **eae0**|
|<i class="budicon-notes-minus"></i> | budicon-notes-minus | **eae1**|
|<i class="budicon-notes-plus"></i> | budicon-notes-plus | **eae2**|
|<i class="budicon-notes-question"></i> | budicon-notes-question | **eae3**|
|<i class="budicon-notes-search"></i> | budicon-notes-search | **eae4**|
|<i class="budicon-notes-tick"></i> | budicon-notes-tick | **eae5**|
|<i class="budicon-notes-upload"></i> | budicon-notes-upload | **eae6**|
|<i class="budicon-scheduled-note"></i> | budicon-scheduled-note | **eae7**|
|<i class="budicon-scheduled-notes"></i> | budicon-scheduled-notes | **eae8**|
|<i class="budicon-shared-note"></i> | budicon-shared-note | **eae9**|
|<i class="budicon-shared-notes"></i> | budicon-shared-notes | **eaea**|

## Communication
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-announcement"></i> | budicon-announcement | **eaeb**|
|<i class="budicon-at-symbol"></i> | budicon-at-symbol | **eaec**|
|<i class="budicon-broadcast"></i> | budicon-broadcast | **eaed**|
|<i class="budicon-call"></i> | budicon-call | **eaee**|
|<i class="budicon-chat-alert"></i> | budicon-chat-alert | **eaef**|
|<i class="budicon-chat-block"></i> | budicon-chat-block | **eaf0**|
|<i class="budicon-chat-cross"></i> | budicon-chat-cross | **eaf1**|
|<i class="budicon-chat-download"></i> | budicon-chat-download | **eaf2**|
|<i class="budicon-chat-favorite"></i> | budicon-chat-favorite | **eaf3**|
|<i class="budicon-chat-information"></i> | budicon-chat-information | **eaf4**|
|<i class="budicon-chat-lock"></i> | budicon-chat-lock | **eaf5**|
|<i class="budicon-chat-love"></i> | budicon-chat-love | **eaf6**|
|<i class="budicon-chat-minus"></i> | budicon-chat-minus | **eaf7**|
|<i class="budicon-chat-plus"></i> | budicon-chat-plus | **eaf8**|
|<i class="budicon-chat-question"></i> | budicon-chat-question | **eaf9**|
|<i class="budicon-chat-scheduled"></i> | budicon-chat-scheduled | **eafa**|
|<i class="budicon-chat-thread"></i> | budicon-chat-thread | **eafb**|
|<i class="budicon-chat-tick"></i> | budicon-chat-tick | **eafc**|
|<i class="budicon-chat-upload"></i> | budicon-chat-upload | **eafd**|
|<i class="budicon-chat"></i> | budicon-chat | **eafe**|
|<i class="budicon-connection"></i> | budicon-connection | **eaff**|
|<i class="budicon-conversation"></i> | budicon-conversation | **eb00**|
|<i class="budicon-dropped-cap"></i> | budicon-dropped-cap | **eb01**|
|<i class="budicon-email-alert"></i> | budicon-email-alert | **eb02**|
|<i class="budicon-email-block"></i> | budicon-email-block | **eb03**|
|<i class="budicon-email-cross"></i> | budicon-email-cross | **eb04**|
|<i class="budicon-email-download"></i> | budicon-email-download | **eb05**|
|<i class="budicon-email-favorite"></i> | budicon-email-favorite | **eb06**|
|<i class="budicon-email-forward"></i> | budicon-email-forward | **eb07**|
|<i class="budicon-email-front-envelope"></i> | budicon-email-front-envelope | **eb08**|
|<i class="budicon-email-information"></i> | budicon-email-information | **eb09**|
|<i class="budicon-email-lock"></i> | budicon-email-lock | **eb0a**|
|<i class="budicon-email-love"></i> | budicon-email-love | **eb0b**|
|<i class="budicon-email-minus"></i> | budicon-email-minus | **eb0c**|
|<i class="budicon-email-plus"></i> | budicon-email-plus | **eb0d**|
|<i class="budicon-email-tick"></i> | budicon-email-tick | **eb0e**|
|<i class="budicon-email-undo"></i> | budicon-email-undo | **eb0f**|
|<i class="budicon-email-upload"></i> | budicon-email-upload | **eb10**|
|<i class="budicon-email"></i> | budicon-email | **eb11**|
|<i class="budicon-happy-chat"></i> | budicon-happy-chat | **eb12**|
|<i class="budicon-happy-sticker"></i> | budicon-happy-sticker | **eb13**|
|<i class="budicon-incoming-call"></i> | budicon-incoming-call | **eb14**|
|<i class="budicon-international-call"></i> | budicon-international-call | **eb15**|
|<i class="budicon-mailbox"></i> | budicon-mailbox | **eb16**|
|<i class="budicon-mobile-chat"></i> | budicon-mobile-chat | **eb17**|
|<i class="budicon-mobile-email"></i> | budicon-mobile-email | **eb18**|
|<i class="budicon-numpad"></i> | budicon-numpad | **eb19**|
|<i class="budicon-outgoing-call"></i> | budicon-outgoing-call | **eb1a**|
|<i class="budicon-phone-block"></i> | budicon-phone-block | **eb1b**|
|<i class="budicon-phone-connection"></i> | budicon-phone-connection | **eb1c**|
|<i class="budicon-phone-directed"></i> | budicon-phone-directed | **eb1d**|
|<i class="budicon-phone-forward"></i> | budicon-phone-forward | **eb1e**|
|<i class="budicon-phone-hold"></i> | budicon-phone-hold | **eb1f**|
|<i class="budicon-phone-lock"></i> | budicon-phone-lock | **eb20**|
|<i class="budicon-phone-love"></i> | budicon-phone-love | **eb21**|
|<i class="budicon-phone-voicemail"></i> | budicon-phone-voicemail | **eb22**|
|<i class="budicon-phone"></i> | budicon-phone | **eb23**|
|<i class="budicon-quill-pen"></i> | budicon-quill-pen | **eb24**|
|<i class="budicon-sad-chat"></i> | budicon-sad-chat | **eb25**|
|<i class="budicon-sad-sticker"></i> | budicon-sad-sticker | **eb26**|
|<i class="budicon-search-chat"></i> | budicon-search-chat | **eb27**|
|<i class="budicon-search-email"></i> | budicon-search-email | **eb28**|
|<i class="budicon-sending-message"></i> | budicon-sending-message | **eb29**|
|<i class="budicon-shared-chat"></i> | budicon-shared-chat | **eb2a**|
|<i class="budicon-shared-email"></i> | budicon-shared-email | **eb2b**|
|<i class="budicon-signal-source"></i> | budicon-signal-source | **eb2c**|
|<i class="budicon-signal"></i> | budicon-signal | **eb2d**|
|<i class="budicon-sms"></i> | budicon-sms | **eb2e**|
|<i class="budicon-spam"></i> | budicon-spam | **eb2f**|
|<i class="budicon-stickers"></i> | budicon-stickers | **eb30**|
|<i class="budicon-typing-chat"></i> | budicon-typing-chat | **eb31**|
|<i class="budicon-video-chat"></i> | budicon-video-chat | **eb32**|
|<i class="budicon-voice-mail"></i> | budicon-voice-mail | **eb33**|
|<i class="budicon-web-chat"></i> | budicon-web-chat | **eb34**|
|<i class="budicon-web-sms"></i> | budicon-web-sms | **eb35**|

## Kids
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-abc-blocks"></i> | budicon-abc-blocks | **eb36**|
|<i class="budicon-baby-bottle"></i> | budicon-baby-bottle | **eb37**|
|<i class="budicon-baby-dot"></i> | budicon-baby-dot | **eb38**|
|<i class="budicon-baby-jumper"></i> | budicon-baby-jumper | **eb39**|
|<i class="budicon-balloon"></i> | budicon-balloon | **eb3a**|
|<i class="budicon-birthday-cake"></i> | budicon-birthday-cake | **eb3b**|
|<i class="budicon-birthday-party"></i> | budicon-birthday-party | **eb3c**|
|<i class="budicon-castle-toy"></i> | budicon-castle-toy | **eb3d**|
|<i class="budicon-diaper"></i> | budicon-diaper | **eb3e**|
|<i class="budicon-girl-clothes"></i> | budicon-girl-clothes | **eb3f**|
|<i class="budicon-gun-toy"></i> | budicon-gun-toy | **eb40**|
|<i class="budicon-kid-bottle-love"></i> | budicon-kid-bottle-love | **eb41**|
|<i class="budicon-kid-bottle"></i> | budicon-kid-bottle | **eb42**|
|<i class="budicon-kite"></i> | budicon-kite | **eb43**|
|<i class="budicon-shape-blocks"></i> | budicon-shape-blocks | **eb44**|
|<i class="budicon-teddy-bear"></i> | budicon-teddy-bear | **eb45**|
|<i class="budicon-thermometer"></i> | budicon-thermometer | **eb46**|
|<i class="budicon-train-toy"></i> | budicon-train-toy | **eb47**|

## Business
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-aluminum-bag"></i> | budicon-aluminum-bag | **eb48**|
|<i class="budicon-brown-envelope"></i> | budicon-brown-envelope | **eb49**|
|<i class="budicon-business-checklist"></i> | budicon-business-checklist | **eb4a**|
|<i class="budicon-business-growth"></i> | budicon-business-growth | **eb4b**|
|<i class="budicon-business-note"></i> | budicon-business-note | **eb4c**|
|<i class="budicon-business-performance"></i> | budicon-business-performance | **eb4d**|
|<i class="budicon-business-plan"></i> | budicon-business-plan | **eb4e**|
|<i class="budicon-business-strategy"></i> | budicon-business-strategy | **eb4f**|
|<i class="budicon-business-target"></i> | budicon-business-target | **eb50**|
|<i class="budicon-calculator"></i> | budicon-calculator | **eb51**|
|<i class="budicon-cheque"></i> | budicon-cheque | **eb52**|
|<i class="budicon-deal"></i> | budicon-deal | **eb53**|
|<i class="budicon-duct-tape"></i> | budicon-duct-tape | **eb54**|
|<i class="budicon-file-storage"></i> | budicon-file-storage | **eb55**|
|<i class="budicon-filing"></i> | budicon-filing | **eb56**|
|<i class="budicon-office-bag-a"></i> | budicon-office-bag-a | **eb57**|
|<i class="budicon-office-bag-b"></i> | budicon-office-bag-b | **eb58**|
|<i class="budicon-office-bag-c"></i> | budicon-office-bag-c | **eb59**|
|<i class="budicon-office-bag"></i> | budicon-office-bag | **eb5a**|
|<i class="budicon-office-stationary"></i> | budicon-office-stationary | **eb5b**|
|<i class="budicon-organization"></i> | budicon-organization | **eb5c**|
|<i class="budicon-photocopy-machine"></i> | budicon-photocopy-machine | **eb5d**|
|<i class="budicon-presentation-deck"></i> | budicon-presentation-deck | **eb5e**|
|<i class="budicon-presentation"></i> | budicon-presentation | **eb5f**|
|<i class="budicon-project-folder"></i> | budicon-project-folder | **eb60**|
|<i class="budicon-safe-deposit"></i> | budicon-safe-deposit | **eb61**|
|<i class="budicon-shredder"></i> | budicon-shredder | **eb62**|
|<i class="budicon-speech"></i> | budicon-speech | **eb63**|
|<i class="budicon-stapler"></i> | budicon-stapler | **eb64**|
|<i class="budicon-tie"></i> | budicon-tie | **eb65**|

## Furniture
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-air-conditioner"></i> | budicon-air-conditioner | **eb66**|
|<i class="budicon-armchair"></i> | budicon-armchair | **eb67**|
|<i class="budicon-bathup"></i> | budicon-bathup | **eb68**|
|<i class="budicon-bed-lamp"></i> | budicon-bed-lamp | **eb69**|
|<i class="budicon-bed-side-a"></i> | budicon-bed-side-a | **eb6a**|
|<i class="budicon-bed-side-table"></i> | budicon-bed-side-table | **eb6b**|
|<i class="budicon-bed-side"></i> | budicon-bed-side | **eb6c**|
|<i class="budicon-bed"></i> | budicon-bed | **eb6d**|
|<i class="budicon-big-dresser"></i> | budicon-big-dresser | **eb6e**|
|<i class="budicon-book-cabinet"></i> | budicon-book-cabinet | **eb6f**|
|<i class="budicon-cabinet"></i> | budicon-cabinet | **eb70**|
|<i class="budicon-candle"></i> | budicon-candle | **eb71**|
|<i class="budicon-chest"></i> | budicon-chest | **eb72**|
|<i class="budicon-closet"></i> | budicon-closet | **eb73**|
|<i class="budicon-dining-chair"></i> | budicon-dining-chair | **eb74**|
|<i class="budicon-door"></i> | budicon-door | **eb75**|
|<i class="budicon-dresser-table"></i> | budicon-dresser-table | **eb76**|
|<i class="budicon-dresser"></i> | budicon-dresser | **eb77**|
|<i class="budicon-dryer"></i> | budicon-dryer | **eb78**|
|<i class="budicon-fireplace"></i> | budicon-fireplace | **eb79**|
|<i class="budicon-flower"></i> | budicon-flower | **eb7a**|
|<i class="budicon-futon"></i> | budicon-futon | **eb7b**|
|<i class="budicon-hanging-lamp"></i> | budicon-hanging-lamp | **eb7c**|
|<i class="budicon-home-alert"></i> | budicon-home-alert | **eb7d**|
|<i class="budicon-home-safe"></i> | budicon-home-safe | **eb7e**|
|<i class="budicon-hutch"></i> | budicon-hutch | **eb7f**|
|<i class="budicon-indoor-plant"></i> | budicon-indoor-plant | **eb80**|
|<i class="budicon-indoor-seat"></i> | budicon-indoor-seat | **eb81**|
|<i class="budicon-lamp"></i> | budicon-lamp | **eb82**|
|<i class="budicon-lightbulb"></i> | budicon-lightbulb | **eb83**|
|<i class="budicon-long-desk"></i> | budicon-long-desk | **eb84**|
|<i class="budicon-low-cabinet"></i> | budicon-low-cabinet | **eb85**|
|<i class="budicon-open-door"></i> | budicon-open-door | **eb86**|
|<i class="budicon-outdoor-seat"></i> | budicon-outdoor-seat | **eb87**|
|<i class="budicon-plant"></i> | budicon-plant | **eb88**|
|<i class="budicon-rocking-chair"></i> | budicon-rocking-chair | **eb89**|
|<i class="budicon-round-armchair"></i> | budicon-round-armchair | **eb8a**|
|<i class="budicon-round-chair"></i> | budicon-round-chair | **eb8b**|
|<i class="budicon-showcase-desk"></i> | budicon-showcase-desk | **eb8c**|
|<i class="budicon-shower"></i> | budicon-shower | **eb8d**|
|<i class="budicon-sink"></i> | budicon-sink | **eb8e**|
|<i class="budicon-smart-lamp"></i> | budicon-smart-lamp | **eb8f**|
|<i class="budicon-sofa"></i> | budicon-sofa | **eb90**|
|<i class="budicon-standing-lamp"></i> | budicon-standing-lamp | **eb91**|
|<i class="budicon-storage-desk"></i> | budicon-storage-desk | **eb92**|
|<i class="budicon-sunflower"></i> | budicon-sunflower | **eb93**|
|<i class="budicon-table-lamp"></i> | budicon-table-lamp | **eb94**|
|<i class="budicon-tv-desk"></i> | budicon-tv-desk | **eb95**|
|<i class="budicon-vintage-bed-lamp"></i> | budicon-vintage-bed-lamp | **eb96**|
|<i class="budicon-vintage-chest"></i> | budicon-vintage-chest | **eb97**|
|<i class="budicon-vintage-futon"></i> | budicon-vintage-futon | **eb98**|
|<i class="budicon-vintage-sofa"></i> | budicon-vintage-sofa | **eb99**|
|<i class="budicon-wardrobe"></i> | budicon-wardrobe | **eb9a**|
|<i class="budicon-washer"></i> | budicon-washer | **eb9b**|
|<i class="budicon-windsor-chair"></i> | budicon-windsor-chair | **eb9c**|
|<i class="budicon-wingback-chair"></i> | budicon-wingback-chair | **eb9d**|
|<i class="budicon-work-desk"></i> | budicon-work-desk | **eb9e**|

## Sports
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-archery"></i> | budicon-archery | **eb9f**|
|<i class="budicon-badminton"></i> | budicon-badminton | **eba0**|
|<i class="budicon-baseball-a"></i> | budicon-baseball-a | **eba1**|
|<i class="budicon-baseball"></i> | budicon-baseball | **eba2**|
|<i class="budicon-basketball-field"></i> | budicon-basketball-field | **eba3**|
|<i class="budicon-basketball-ring"></i> | budicon-basketball-ring | **eba4**|
|<i class="budicon-basketball"></i> | budicon-basketball | **eba5**|
|<i class="budicon-boxing"></i> | budicon-boxing | **eba6**|
|<i class="budicon-canoe"></i> | budicon-canoe | **eba7**|
|<i class="budicon-champion"></i> | budicon-champion | **eba8**|
|<i class="budicon-commentator"></i> | budicon-commentator | **eba9**|
|<i class="budicon-crickets"></i> | budicon-crickets | **ebaa**|
|<i class="budicon-diving"></i> | budicon-diving | **ebab**|
|<i class="budicon-drinking-bottles"></i> | budicon-drinking-bottles | **ebac**|
|<i class="budicon-dumbell"></i> | budicon-dumbell | **ebad**|
|<i class="budicon-energy-drink"></i> | budicon-energy-drink | **ebae**|
|<i class="budicon-energy-pill"></i> | budicon-energy-pill | **ebaf**|
|<i class="budicon-fencing"></i> | budicon-fencing | **ebb0**|
|<i class="budicon-football-field"></i> | budicon-football-field | **ebb1**|
|<i class="budicon-football"></i> | budicon-football | **ebb2**|
|<i class="budicon-formation"></i> | budicon-formation | **ebb3**|
|<i class="budicon-golf"></i> | budicon-golf | **ebb4**|
|<i class="budicon-hockey"></i> | budicon-hockey | **ebb5**|
|<i class="budicon-injury"></i> | budicon-injury | **ebb6**|
|<i class="budicon-king"></i> | budicon-king | **ebb7**|
|<i class="budicon-live-sport"></i> | budicon-live-sport | **ebb8**|
|<i class="budicon-medication"></i> | budicon-medication | **ebb9**|
|<i class="budicon-nfl"></i> | budicon-nfl | **ebba**|
|<i class="budicon-olympic"></i> | budicon-olympic | **ebbb**|
|<i class="budicon-ping-pong"></i> | budicon-ping-pong | **ebbc**|
|<i class="budicon-racing-flag"></i> | budicon-racing-flag | **ebbd**|
|<i class="budicon-referee"></i> | budicon-referee | **ebbe**|
|<i class="budicon-runner-outfit"></i> | budicon-runner-outfit | **ebbf**|
|<i class="budicon-sneakers"></i> | budicon-sneakers | **ebc0**|
|<i class="budicon-soccer-ball"></i> | budicon-soccer-ball | **ebc1**|
|<i class="budicon-speed"></i> | budicon-speed | **ebc2**|
|<i class="budicon-spikes"></i> | budicon-spikes | **ebc3**|
|<i class="budicon-sport-card"></i> | budicon-sport-card | **ebc4**|
|<i class="budicon-sumo"></i> | budicon-sumo | **ebc5**|
|<i class="budicon-team-strategy"></i> | budicon-team-strategy | **ebc6**|
|<i class="budicon-tennis-ball"></i> | budicon-tennis-ball | **ebc7**|
|<i class="budicon-trophy"></i> | budicon-trophy | **ebc8**|
|<i class="budicon-uniform"></i> | budicon-uniform | **ebc9**|
|<i class="budicon-voley-ball"></i> | budicon-voley-ball | **ebca**|
|<i class="budicon-volley-ball"></i> | budicon-volley-ball | **ebcb**|
|<i class="budicon-white-flag"></i> | budicon-white-flag | **ebcc**|

## Food
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-apple"></i> | budicon-apple | **ebcd**|
|<i class="budicon-avocado"></i> | budicon-avocado | **ebce**|
|<i class="budicon-bacon"></i> | budicon-bacon | **ebcf**|
|<i class="budicon-basil"></i> | budicon-basil | **ebd0**|
|<i class="budicon-beer"></i> | budicon-beer | **ebd1**|
|<i class="budicon-bottle-water"></i> | budicon-bottle-water | **ebd2**|
|<i class="budicon-bowl"></i> | budicon-bowl | **ebd3**|
|<i class="budicon-bubble-tea"></i> | budicon-bubble-tea | **ebd4**|
|<i class="budicon-carrot"></i> | budicon-carrot | **ebd5**|
|<i class="budicon-cheers"></i> | budicon-cheers | **ebd6**|
|<i class="budicon-cherry"></i> | budicon-cherry | **ebd7**|
|<i class="budicon-chinese-food-a"></i> | budicon-chinese-food-a | **ebd8**|
|<i class="budicon-chinese-food"></i> | budicon-chinese-food | **ebd9**|
|<i class="budicon-cocktail"></i> | budicon-cocktail | **ebda**|
|<i class="budicon-coffee-cup"></i> | budicon-coffee-cup | **ebdb**|
|<i class="budicon-corn"></i> | budicon-corn | **ebdc**|
|<i class="budicon-croissant"></i> | budicon-croissant | **ebdd**|
|<i class="budicon-cupcake"></i> | budicon-cupcake | **ebde**|
|<i class="budicon-donut"></i> | budicon-donut | **ebdf**|
|<i class="budicon-door-board"></i> | budicon-door-board | **ebe0**|
|<i class="budicon-egg"></i> | budicon-egg | **ebe1**|
|<i class="budicon-fork-knife"></i> | budicon-fork-knife | **ebe2**|
|<i class="budicon-fork-spoon"></i> | budicon-fork-spoon | **ebe3**|
|<i class="budicon-french-fries"></i> | budicon-french-fries | **ebe4**|
|<i class="budicon-grapes"></i> | budicon-grapes | **ebe5**|
|<i class="budicon-hamburger"></i> | budicon-hamburger | **ebe6**|
|<i class="budicon-honey"></i> | budicon-honey | **ebe7**|
|<i class="budicon-hot-drink"></i> | budicon-hot-drink | **ebe8**|
|<i class="budicon-hot-tea"></i> | budicon-hot-tea | **ebe9**|
|<i class="budicon-hotdog"></i> | budicon-hotdog | **ebea**|
|<i class="budicon-ice-cream-cone"></i> | budicon-ice-cream-cone | **ebeb**|
|<i class="budicon-ice-cream-stick"></i> | budicon-ice-cream-stick | **ebec**|
|<i class="budicon-jam"></i> | budicon-jam | **ebed**|
|<i class="budicon-ketchup-bottle"></i> | budicon-ketchup-bottle | **ebee**|
|<i class="budicon-lemon"></i> | budicon-lemon | **ebef**|
|<i class="budicon-lettuce"></i> | budicon-lettuce | **ebf0**|
|<i class="budicon-mango"></i> | budicon-mango | **ebf1**|
|<i class="budicon-mayonnaise"></i> | budicon-mayonnaise | **ebf2**|
|<i class="budicon-meal"></i> | budicon-meal | **ebf3**|
|<i class="budicon-milk-bottle"></i> | budicon-milk-bottle | **ebf4**|
|<i class="budicon-milk"></i> | budicon-milk | **ebf5**|
|<i class="budicon-noodle-bowl"></i> | budicon-noodle-bowl | **ebf6**|
|<i class="budicon-onigiri"></i> | budicon-onigiri | **ebf7**|
|<i class="budicon-onion"></i> | budicon-onion | **ebf8**|
|<i class="budicon-organic-drink"></i> | budicon-organic-drink | **ebf9**|
|<i class="budicon-pepper"></i> | budicon-pepper | **ebfa**|
|<i class="budicon-pineapple"></i> | budicon-pineapple | **ebfb**|
|<i class="budicon-pizza"></i> | budicon-pizza | **ebfc**|
|<i class="budicon-pumpkin"></i> | budicon-pumpkin | **ebfd**|
|<i class="budicon-ramen"></i> | budicon-ramen | **ebfe**|
|<i class="budicon-salt"></i> | budicon-salt | **ebff**|
|<i class="budicon-sandwich"></i> | budicon-sandwich | **ec00**|
|<i class="budicon-sauce-tube"></i> | budicon-sauce-tube | **ec01**|
|<i class="budicon-sauce"></i> | budicon-sauce | **ec02**|
|<i class="budicon-soda-can"></i> | budicon-soda-can | **ec03**|
|<i class="budicon-soup-bowl"></i> | budicon-soup-bowl | **ec04**|
|<i class="budicon-soy-sauce"></i> | budicon-soy-sauce | **ec05**|
|<i class="budicon-steak"></i> | budicon-steak | **ec06**|
|<i class="budicon-strawberry"></i> | budicon-strawberry | **ec07**|
|<i class="budicon-sushi"></i> | budicon-sushi | **ec08**|
|<i class="budicon-taco"></i> | budicon-taco | **ec09**|
|<i class="budicon-tea-cup"></i> | budicon-tea-cup | **ec0a**|
|<i class="budicon-toast"></i> | budicon-toast | **ec0b**|
|<i class="budicon-turkey"></i> | budicon-turkey | **ec0c**|
|<i class="budicon-utensil"></i> | budicon-utensil | **ec0d**|
|<i class="budicon-vegetarian"></i> | budicon-vegetarian | **ec0e**|
|<i class="budicon-watermelon"></i> | budicon-watermelon | **ec0f**|
|<i class="budicon-white-bread"></i> | budicon-white-bread | **ec10**|
|<i class="budicon-wine-bottle"></i> | budicon-wine-bottle | **ec11**|
|<i class="budicon-wine-glass"></i> | budicon-wine-glass | **ec12**|
|<i class="budicon-wine"></i> | budicon-wine | **ec13**|

## eCommerce
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-backpack"></i> | budicon-backpack | **ec14**|
|<i class="budicon-barcode"></i> | budicon-barcode | **ec15**|
|<i class="budicon-bill-cross"></i> | budicon-bill-cross | **ec16**|
|<i class="budicon-bill-download"></i> | budicon-bill-download | **ec17**|
|<i class="budicon-bill-lock"></i> | budicon-bill-lock | **ec18**|
|<i class="budicon-bill-minus"></i> | budicon-bill-minus | **ec19**|
|<i class="budicon-bill-plus"></i> | budicon-bill-plus | **ec1a**|
|<i class="budicon-bill-tick"></i> | budicon-bill-tick | **ec1b**|
|<i class="budicon-bill-upload"></i> | budicon-bill-upload | **ec1c**|
|<i class="budicon-bill"></i> | budicon-bill | **ec1d**|
|<i class="budicon-bow-tie"></i> | budicon-bow-tie | **ec1e**|
|<i class="budicon-cart-cross"></i> | budicon-cart-cross | **ec1f**|
|<i class="budicon-cart-download"></i> | budicon-cart-download | **ec20**|
|<i class="budicon-cart-lock"></i> | budicon-cart-lock | **ec21**|
|<i class="budicon-cart-love"></i> | budicon-cart-love | **ec22**|
|<i class="budicon-cart-minus"></i> | budicon-cart-minus | **ec23**|
|<i class="budicon-cart-plus"></i> | budicon-cart-plus | **ec24**|
|<i class="budicon-cart-search"></i> | budicon-cart-search | **ec25**|
|<i class="budicon-cart-tick"></i> | budicon-cart-tick | **ec26**|
|<i class="budicon-cart-upload"></i> | budicon-cart-upload | **ec27**|
|<i class="budicon-cart"></i> | budicon-cart | **ec28**|
|<i class="budicon-credit-card-a"></i> | budicon-credit-card-a | **ec29**|
|<i class="budicon-credit-card"></i> | budicon-credit-card | **ec2a**|
|<i class="budicon-digital-download"></i> | budicon-digital-download | **ec2b**|
|<i class="budicon-digital-upload"></i> | budicon-digital-upload | **ec2c**|
|<i class="budicon-discount-coupon"></i> | budicon-discount-coupon | **ec2d**|
|<i class="budicon-dollar-paper-a"></i> | budicon-dollar-paper-a | **ec2e**|
|<i class="budicon-dollar-paper"></i> | budicon-dollar-paper | **ec2f**|
|<i class="budicon-express"></i> | budicon-express | **ec30**|
|<i class="budicon-hiking-backpack"></i> | budicon-hiking-backpack | **ec31**|
|<i class="budicon-invoice"></i> | budicon-invoice | **ec32**|
|<i class="budicon-jewelry"></i> | budicon-jewelry | **ec33**|
|<i class="budicon-lipstick"></i> | budicon-lipstick | **ec34**|
|<i class="budicon-long-pant"></i> | budicon-long-pant | **ec35**|
|<i class="budicon-market-sign"></i> | budicon-market-sign | **ec36**|
|<i class="budicon-market"></i> | budicon-market | **ec37**|
|<i class="budicon-mastercard"></i> | budicon-mastercard | **ec38**|
|<i class="budicon-membership-card"></i> | budicon-membership-card | **ec39**|
|<i class="budicon-messenger-bag"></i> | budicon-messenger-bag | **ec3a**|
|<i class="budicon-mobile-barcode"></i> | budicon-mobile-barcode | **ec3b**|
|<i class="budicon-money-a"></i> | budicon-money-a | **ec3c**|
|<i class="budicon-money"></i> | budicon-money | **ec3d**|
|<i class="budicon-packaging"></i> | budicon-packaging | **ec3e**|
|<i class="budicon-parcel"></i> | budicon-parcel | **ec3f**|
|<i class="budicon-price-tag"></i> | budicon-price-tag | **ec40**|
|<i class="budicon-purse"></i> | budicon-purse | **ec41**|
|<i class="budicon-receipt"></i> | budicon-receipt | **ec42**|
|<i class="budicon-search-bill"></i> | budicon-search-bill | **ec43**|
|<i class="budicon-shipping"></i> | budicon-shipping | **ec44**|
|<i class="budicon-shirt"></i> | budicon-shirt | **ec45**|
|<i class="budicon-shopping-bag"></i> | budicon-shopping-bag | **ec46**|
|<i class="budicon-shopping-bags"></i> | budicon-shopping-bags | **ec47**|
|<i class="budicon-short-pant"></i> | budicon-short-pant | **ec48**|
|<i class="budicon-socks"></i> | budicon-socks | **ec49**|
|<i class="budicon-tag-cross"></i> | budicon-tag-cross | **ec4a**|
|<i class="budicon-tag-minus"></i> | budicon-tag-minus | **ec4b**|
|<i class="budicon-tag-plus"></i> | budicon-tag-plus | **ec4c**|
|<i class="budicon-tag-search"></i> | budicon-tag-search | **ec4d**|
|<i class="budicon-tag-tick"></i> | budicon-tag-tick | **ec4e**|
|<i class="budicon-tag"></i> | budicon-tag | **ec4f**|
|<i class="budicon-underpant"></i> | budicon-underpant | **ec50**|
|<i class="budicon-upside-sign"></i> | budicon-upside-sign | **ec51**|
|<i class="budicon-wallet"></i> | budicon-wallet | **ec52**|
|<i class="budicon-wishlist"></i> | budicon-wishlist | **ec53**|

## Folders
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-branch-folders"></i> | budicon-branch-folders | **ec54**|
|<i class="budicon-folder-alert"></i> | budicon-folder-alert | **ec55**|
|<i class="budicon-folder-back"></i> | budicon-folder-back | **ec56**|
|<i class="budicon-folder-block"></i> | budicon-folder-block | **ec57**|
|<i class="budicon-folder-cross"></i> | budicon-folder-cross | **ec58**|
|<i class="budicon-folder-download"></i> | budicon-folder-download | **ec59**|
|<i class="budicon-folder-favorite"></i> | budicon-folder-favorite | **ec5a**|
|<i class="budicon-folder-information"></i> | budicon-folder-information | **ec5b**|
|<i class="budicon-folder-lock"></i> | budicon-folder-lock | **ec5c**|
|<i class="budicon-folder-love"></i> | budicon-folder-love | **ec5d**|
|<i class="budicon-folder-minus"></i> | budicon-folder-minus | **ec5e**|
|<i class="budicon-folder-plus"></i> | budicon-folder-plus | **ec5f**|
|<i class="budicon-folder-question"></i> | budicon-folder-question | **ec60**|
|<i class="budicon-folder-sync"></i> | budicon-folder-sync | **ec61**|
|<i class="budicon-folder-tick"></i> | budicon-folder-tick | **ec62**|
|<i class="budicon-folder-upload"></i> | budicon-folder-upload | **ec63**|
|<i class="budicon-folder"></i> | budicon-folder | **ec64**|
|<i class="budicon-scheduled-folder"></i> | budicon-scheduled-folder | **ec65**|
|<i class="budicon-search-folder"></i> | budicon-search-folder | **ec66**|
|<i class="budicon-shared-folder"></i> | budicon-shared-folder | **ec67**|
|<i class="budicon-stack-folders"></i> | budicon-stack-folders | **ec68**|

## Arrows
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-all-directions"></i> | budicon-all-directions | **ec69**|
|<i class="budicon-arrow-bottom-a"></i> | budicon-arrow-bottom-a | **ec6a**|
|<i class="budicon-arrow-bottom-circle"></i> | budicon-arrow-bottom-circle | **ec6b**|
|<i class="budicon-arrow-bottom-left-a"></i> | budicon-arrow-bottom-left-a | **ec6c**|
|<i class="budicon-arrow-bottom-left-circle"></i> | budicon-arrow-bottom-left-circle | **ec6d**|
|<i class="budicon-arrow-bottom-left"></i> | budicon-arrow-bottom-left | **ec6e**|
|<i class="budicon-arrow-bottom-right-a"></i> | budicon-arrow-bottom-right-a | **ec6f**|
|<i class="budicon-arrow-bottom-right-circle"></i> | budicon-arrow-bottom-right-circle | **ec70**|
|<i class="budicon-arrow-bottom-right"></i> | budicon-arrow-bottom-right | **ec71**|
|<i class="budicon-arrow-bottom"></i> | budicon-arrow-bottom | **ec72**|
|<i class="budicon-arrow-left-a"></i> | budicon-arrow-left-a | **ec73**|
|<i class="budicon-arrow-left-circle"></i> | budicon-arrow-left-circle | **ec74**|
|<i class="budicon-arrow-left-right-a"></i> | budicon-arrow-left-right-a | **ec75**|
|<i class="budicon-arrow-left-right-circle"></i> | budicon-arrow-left-right-circle | **ec76**|
|<i class="budicon-arrow-left-right"></i> | budicon-arrow-left-right | **ec77**|
|<i class="budicon-arrow-left"></i> | budicon-arrow-left | **ec78**|
|<i class="budicon-arrow-right-a"></i> | budicon-arrow-right-a | **ec79**|
|<i class="budicon-arrow-right-circle"></i> | budicon-arrow-right-circle | **ec7a**|
|<i class="budicon-arrow-right"></i> | budicon-arrow-right | **ec7b**|
|<i class="budicon-arrow-top-a"></i> | budicon-arrow-top-a | **ec7c**|
|<i class="budicon-arrow-top-circle"></i> | budicon-arrow-top-circle | **ec7d**|
|<i class="budicon-arrow-top-left-a"></i> | budicon-arrow-top-left-a | **ec7e**|
|<i class="budicon-arrow-top-left-circle"></i> | budicon-arrow-top-left-circle | **ec7f**|
|<i class="budicon-arrow-top-left"></i> | budicon-arrow-top-left | **ec80**|
|<i class="budicon-arrow-top-right-a"></i> | budicon-arrow-top-right-a | **ec81**|
|<i class="budicon-arrow-top-right-circle"></i> | budicon-arrow-top-right-circle | **ec82**|
|<i class="budicon-arrow-top-right"></i> | budicon-arrow-top-right | **ec83**|
|<i class="budicon-arrow-top"></i> | budicon-arrow-top | **ec84**|
|<i class="budicon-arrow-turn-bottom"></i> | budicon-arrow-turn-bottom | **ec85**|
|<i class="budicon-arrow-turn-left"></i> | budicon-arrow-turn-left | **ec86**|
|<i class="budicon-arrow-turn-right"></i> | budicon-arrow-turn-right | **ec87**|
|<i class="budicon-arrow-turn-up"></i> | budicon-arrow-turn-up | **ec88**|
|<i class="budicon-arrow-up-down-a"></i> | budicon-arrow-up-down-a | **ec89**|
|<i class="budicon-arrow-up-down-circle"></i> | budicon-arrow-up-down-circle | **ec8a**|
|<i class="budicon-arrow-up-down"></i> | budicon-arrow-up-down | **ec8b**|
|<i class="budicon-backline-arrow"></i> | budicon-backline-arrow | **ec8c**|
|<i class="budicon-bring-list-down"></i> | budicon-bring-list-down | **ec8d**|
|<i class="budicon-bring-list-up"></i> | budicon-bring-list-up | **ec8e**|
|<i class="budicon-chevron-bottom-a"></i> | budicon-chevron-bottom-a | **ec8f**|
|<i class="budicon-chevron-bottom-circle_1"></i> | budicon-chevron-bottom-circle_1 | **ec90**|
|<i class="budicon-chevron-bottom-circle"></i> | budicon-chevron-bottom-circle | **ec91**|
|<i class="budicon-chevron-bottom"></i> | budicon-chevron-bottom | **ec92**|
|<i class="budicon-chevron-left-a"></i> | budicon-chevron-left-a | **ec93**|
|<i class="budicon-chevron-left-circle_1"></i> | budicon-chevron-left-circle_1 | **ec94**|
|<i class="budicon-chevron-left-circle"></i> | budicon-chevron-left-circle | **ec95**|
|<i class="budicon-chevron-left"></i> | budicon-chevron-left | **ec96**|
|<i class="budicon-chevron-right-a"></i> | budicon-chevron-right-a | **ec97**|
|<i class="budicon-chevron-right-circle_1"></i> | budicon-chevron-right-circle_1 | **ec98**|
|<i class="budicon-chevron-right-circle"></i> | budicon-chevron-right-circle | **ec99**|
|<i class="budicon-chevron-right"></i> | budicon-chevron-right | **ec9a**|
|<i class="budicon-chevron-top-a"></i> | budicon-chevron-top-a | **ec9b**|
|<i class="budicon-chevron-top-circle_1"></i> | budicon-chevron-top-circle_1 | **ec9c**|
|<i class="budicon-chevron-top-circle"></i> | budicon-chevron-top-circle | **ec9d**|
|<i class="budicon-chevron-top"></i> | budicon-chevron-top | **ec9e**|
|<i class="budicon-console-arrow"></i> | budicon-console-arrow | **ec9f**|
|<i class="budicon-full-loop-arrow"></i> | budicon-full-loop-arrow | **eca0**|
|<i class="budicon-intersected-arrow"></i> | budicon-intersected-arrow | **eca1**|
|<i class="budicon-inward-pointing"></i> | budicon-inward-pointing | **eca2**|
|<i class="budicon-looping-arrow"></i> | budicon-looping-arrow | **eca3**|
|<i class="budicon-outward-pointing"></i> | budicon-outward-pointing | **eca4**|

## Music
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-airpods-a"></i> | budicon-airpods-a | **eca5**|
|<i class="budicon-airpods"></i> | budicon-airpods | **eca6**|
|<i class="budicon-albums"></i> | budicon-albums | **eca7**|
|<i class="budicon-analog-radio"></i> | budicon-analog-radio | **eca8**|
|<i class="budicon-attachment-music"></i> | budicon-attachment-music | **eca9**|
|<i class="budicon-backward-sign"></i> | budicon-backward-sign | **ecaa**|
|<i class="budicon-backward"></i> | budicon-backward | **ecab**|
|<i class="budicon-bluetooth-earphone"></i> | budicon-bluetooth-earphone | **ecac**|
|<i class="budicon-boom-mic-mute"></i> | budicon-boom-mic-mute | **ecad**|
|<i class="budicon-boom-mic"></i> | budicon-boom-mic | **ecae**|
|<i class="budicon-boombox"></i> | budicon-boombox | **ecaf**|
|<i class="budicon-cassette"></i> | budicon-cassette | **ecb0**|
|<i class="budicon-cd-case"></i> | budicon-cd-case | **ecb1**|
|<i class="budicon-cd"></i> | budicon-cd | **ecb2**|
|<i class="budicon-earphone"></i> | budicon-earphone | **ecb3**|
|<i class="budicon-eject"></i> | budicon-eject | **ecb4**|
|<i class="budicon-equalizer-a"></i> | budicon-equalizer-a | **ecb5**|
|<i class="budicon-equalizer"></i> | budicon-equalizer | **ecb6**|
|<i class="budicon-forward-sign"></i> | budicon-forward-sign | **ecb7**|
|<i class="budicon-forward"></i> | budicon-forward | **ecb8**|
|<i class="budicon-headphone-eq"></i> | budicon-headphone-eq | **ecb9**|
|<i class="budicon-headphones-a"></i> | budicon-headphones-a | **ecba**|
|<i class="budicon-headphones"></i> | budicon-headphones | **ecbb**|
|<i class="budicon-in-ear-headphones"></i> | budicon-in-ear-headphones | **ecbc**|
|<i class="budicon-ipod"></i> | budicon-ipod | **ecbd**|
|<i class="budicon-melody"></i> | budicon-melody | **ecbe**|
|<i class="budicon-microphone-wireless"></i> | budicon-microphone-wireless | **ecbf**|
|<i class="budicon-music-album"></i> | budicon-music-album | **ecc0**|
|<i class="budicon-music-file"></i> | budicon-music-file | **ecc1**|
|<i class="budicon-music-setting"></i> | budicon-music-setting | **ecc2**|
|<i class="budicon-next-sign"></i> | budicon-next-sign | **ecc3**|
|<i class="budicon-next-song"></i> | budicon-next-song | **ecc4**|
|<i class="budicon-pause-sign-a"></i> | budicon-pause-sign-a | **ecc5**|
|<i class="budicon-pause-sign"></i> | budicon-pause-sign | **ecc6**|
|<i class="budicon-pause"></i> | budicon-pause | **ecc7**|
|<i class="budicon-piano"></i> | budicon-piano | **ecc8**|
|<i class="budicon-play-loop"></i> | budicon-play-loop | **ecc9**|
|<i class="budicon-play-sign"></i> | budicon-play-sign | **ecca**|
|<i class="budicon-play"></i> | budicon-play | **eccb**|
|<i class="budicon-playlist-a"></i> | budicon-playlist-a | **eccc**|
|<i class="budicon-playlist-b"></i> | budicon-playlist-b | **eccd**|
|<i class="budicon-playlist"></i> | budicon-playlist | **ecce**|
|<i class="budicon-previous-sign"></i> | budicon-previous-sign | **eccf**|
|<i class="budicon-previous-song"></i> | budicon-previous-song | **ecd0**|
|<i class="budicon-radio"></i> | budicon-radio | **ecd1**|
|<i class="budicon-record-sign"></i> | budicon-record-sign | **ecd2**|
|<i class="budicon-repeat-album"></i> | budicon-repeat-album | **ecd3**|
|<i class="budicon-repeat-one"></i> | budicon-repeat-one | **ecd4**|
|<i class="budicon-repeat"></i> | budicon-repeat | **ecd5**|
|<i class="budicon-rock"></i> | budicon-rock | **ecd6**|
|<i class="budicon-shuffle"></i> | budicon-shuffle | **ecd7**|
|<i class="budicon-song-note-tail"></i> | budicon-song-note-tail | **ecd8**|
|<i class="budicon-song-note"></i> | budicon-song-note | **ecd9**|
|<i class="budicon-song-notes-a"></i> | budicon-song-notes-a | **ecda**|
|<i class="budicon-song-notes"></i> | budicon-song-notes | **ecdb**|
|<i class="budicon-sound"></i> | budicon-sound | **ecdc**|
|<i class="budicon-speaker"></i> | budicon-speaker | **ecdd**|
|<i class="budicon-stop-sign"></i> | budicon-stop-sign | **ecde**|
|<i class="budicon-stop"></i> | budicon-stop | **ecdf**|
|<i class="budicon-vintage-microphone-big"></i> | budicon-vintage-microphone-big | **ece0**|
|<i class="budicon-vintage-microphone"></i> | budicon-vintage-microphone | **ece1**|
|<i class="budicon-vol-high"></i> | budicon-vol-high | **ece2**|
|<i class="budicon-vol-low"></i> | budicon-vol-low | **ece3**|
|<i class="budicon-vol-med"></i> | budicon-vol-med | **ece4**|
|<i class="budicon-vol-mute"></i> | budicon-vol-mute | **ece5**|
|<i class="budicon-volume-low"></i> | budicon-volume-low | **ece6**|
|<i class="budicon-volume-mute-a"></i> | budicon-volume-mute-a | **ece7**|
|<i class="budicon-volume-mute"></i> | budicon-volume-mute | **ece8**|
|<i class="budicon-volume-strong"></i> | budicon-volume-strong | **ece9**|
|<i class="budicon-vynil"></i> | budicon-vynil | **ecea**|
|<i class="budicon-woofer"></i> | budicon-woofer | **eceb**|

## Smart Watches
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-smart-watch-add"></i> | budicon-smart-watch-add | **ecec**|
|<i class="budicon-smart-watch-bell"></i> | budicon-smart-watch-bell | **eced**|
|<i class="budicon-smart-watch-block"></i> | budicon-smart-watch-block | **ecee**|
|<i class="budicon-smart-watch-call"></i> | budicon-smart-watch-call | **ecef**|
|<i class="budicon-smart-watch-chat"></i> | budicon-smart-watch-chat | **ecf0**|
|<i class="budicon-smart-watch-dialog"></i> | budicon-smart-watch-dialog | **ecf1**|
|<i class="budicon-smart-watch-download"></i> | budicon-smart-watch-download | **ecf2**|
|<i class="budicon-smart-watch-finish"></i> | budicon-smart-watch-finish | **ecf3**|
|<i class="budicon-smart-watch-location"></i> | budicon-smart-watch-location | **ecf4**|
|<i class="budicon-smart-watch-lock"></i> | budicon-smart-watch-lock | **ecf5**|
|<i class="budicon-smart-watch-love"></i> | budicon-smart-watch-love | **ecf6**|
|<i class="budicon-smart-watch-mail"></i> | budicon-smart-watch-mail | **ecf7**|
|<i class="budicon-smart-watch-mic"></i> | budicon-smart-watch-mic | **ecf8**|
|<i class="budicon-smart-watch-music"></i> | budicon-smart-watch-music | **ecf9**|
|<i class="budicon-smart-watch-power"></i> | budicon-smart-watch-power | **ecfa**|
|<i class="budicon-smart-watch-progress"></i> | budicon-smart-watch-progress | **ecfb**|
|<i class="budicon-smart-watch-rate"></i> | budicon-smart-watch-rate | **ecfc**|
|<i class="budicon-smart-watch-rss"></i> | budicon-smart-watch-rss | **ecfd**|
|<i class="budicon-smart-watch-sad"></i> | budicon-smart-watch-sad | **ecfe**|
|<i class="budicon-smart-watch-search"></i> | budicon-smart-watch-search | **ecff**|
|<i class="budicon-smart-watch-smile"></i> | budicon-smart-watch-smile | **ed00**|
|<i class="budicon-smart-watch-statistic"></i> | budicon-smart-watch-statistic | **ed01**|
|<i class="budicon-smart-watch-upload"></i> | budicon-smart-watch-upload | **ed02**|
|<i class="budicon-smart-watch-user"></i> | budicon-smart-watch-user | **ed03**|
|<i class="budicon-smart-watch-video"></i> | budicon-smart-watch-video | **ed04**|
|<i class="budicon-smart-watch-volume"></i> | budicon-smart-watch-volume | **ed05**|
|<i class="budicon-smart-watch"></i> | budicon-smart-watch | **ed06**|

## Date and Time
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-alarm"></i> | budicon-alarm | **ed07**|
|<i class="budicon-alert"></i> | budicon-alert | **ed08**|
|<i class="budicon-backward-time"></i> | budicon-backward-time | **ed09**|
|<i class="budicon-bell"></i> | budicon-bell | **ed0a**|
|<i class="budicon-calendar-alert"></i> | budicon-calendar-alert | **ed0b**|
|<i class="budicon-calendar-block"></i> | budicon-calendar-block | **ed0c**|
|<i class="budicon-calendar-cross"></i> | budicon-calendar-cross | **ed0d**|
|<i class="budicon-calendar-dating"></i> | budicon-calendar-dating | **ed0e**|
|<i class="budicon-calendar-download"></i> | budicon-calendar-download | **ed0f**|
|<i class="budicon-calendar-favorite"></i> | budicon-calendar-favorite | **ed10**|
|<i class="budicon-calendar-information"></i> | budicon-calendar-information | **ed11**|
|<i class="budicon-calendar-lock"></i> | budicon-calendar-lock | **ed12**|
|<i class="budicon-calendar-minus"></i> | budicon-calendar-minus | **ed13**|
|<i class="budicon-calendar-plus"></i> | budicon-calendar-plus | **ed14**|
|<i class="budicon-calendar-tick"></i> | budicon-calendar-tick | **ed15**|
|<i class="budicon-calendar-upload"></i> | budicon-calendar-upload | **ed16**|
|<i class="budicon-calendar"></i> | budicon-calendar | **ed17**|
|<i class="budicon-dashboard2"></i> | budicon-dashboard2 | **ed18**|
|<i class="budicon-events"></i> | budicon-events | **ed19**|
|<i class="budicon-fast"></i> | budicon-fast | **ed1a**|
|<i class="budicon-find-event"></i> | budicon-find-event | **ed1b**|
|<i class="budicon-forward-time"></i> | budicon-forward-time | **ed1c**|
|<i class="budicon-hourglass"></i> | budicon-hourglass | **ed1d**|
|<i class="budicon-normal-speed"></i> | budicon-normal-speed | **ed1e**|
|<i class="budicon-schedule"></i> | budicon-schedule | **ed1f**|
|<i class="budicon-share-calendar"></i> | budicon-share-calendar | **ed20**|
|<i class="budicon-slow"></i> | budicon-slow | **ed21**|
|<i class="budicon-snooze"></i> | budicon-snooze | **ed22**|
|<i class="budicon-stopwatch-digital"></i> | budicon-stopwatch-digital | **ed23**|
|<i class="budicon-stopwatch-half"></i> | budicon-stopwatch-half | **ed24**|
|<i class="budicon-stopwatch"></i> | budicon-stopwatch | **ed25**|
|<i class="budicon-time"></i> | budicon-time | **ed26**|
|<i class="budicon-timer"></i> | budicon-timer | **ed27**|
|<i class="budicon-tomorrow"></i> | budicon-tomorrow | **ed28**|
|<i class="budicon-vintage-wall-clock"></i> | budicon-vintage-wall-clock | **ed29**|
|<i class="budicon-wall-clock"></i> | budicon-wall-clock | **ed2a**|
|<i class="budicon-watch"></i> | budicon-watch | **ed2b**|
|<i class="budicon-yesterday"></i> | budicon-yesterday | **ed2c**|

## Files
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-attachment-file"></i> | budicon-attachment-file | **ed2d**|
|<i class="budicon-attachment"></i> | budicon-attachment | **ed2e**|
|<i class="budicon-book"></i> | budicon-book | **ed2f**|
|<i class="budicon-booklet"></i> | budicon-booklet | **ed30**|
|<i class="budicon-bookmark-a"></i> | budicon-bookmark-a | **ed31**|
|<i class="budicon-bookmark"></i> | budicon-bookmark | **ed32**|
|<i class="budicon-box-file-alert"></i> | budicon-box-file-alert | **ed33**|
|<i class="budicon-box-file-block"></i> | budicon-box-file-block | **ed34**|
|<i class="budicon-box-file-cross"></i> | budicon-box-file-cross | **ed35**|
|<i class="budicon-box-file-download"></i> | budicon-box-file-download | **ed36**|
|<i class="budicon-box-file-favorite"></i> | budicon-box-file-favorite | **ed37**|
|<i class="budicon-box-file-heart"></i> | budicon-box-file-heart | **ed38**|
|<i class="budicon-box-file-index"></i> | budicon-box-file-index | **ed39**|
|<i class="budicon-box-file-information"></i> | budicon-box-file-information | **ed3a**|
|<i class="budicon-box-file-lock"></i> | budicon-box-file-lock | **ed3b**|
|<i class="budicon-box-file-minus"></i> | budicon-box-file-minus | **ed3c**|
|<i class="budicon-box-file-plus"></i> | budicon-box-file-plus | **ed3d**|
|<i class="budicon-box-file-search"></i> | budicon-box-file-search | **ed3e**|
|<i class="budicon-box-file-tick"></i> | budicon-box-file-tick | **ed3f**|
|<i class="budicon-box-file-upload"></i> | budicon-box-file-upload | **ed40**|
|<i class="budicon-box-file"></i> | budicon-box-file | **ed41**|
|<i class="budicon-compressed-file"></i> | budicon-compressed-file | **ed42**|
|<i class="budicon-doc-attachment"></i> | budicon-doc-attachment | **ed43**|
|<i class="budicon-doc-marker"></i> | budicon-doc-marker | **ed44**|
|<i class="budicon-file-alert"></i> | budicon-file-alert | **ed45**|
|<i class="budicon-file-blank"></i> | budicon-file-blank | **ed46**|
|<i class="budicon-file-block"></i> | budicon-file-block | **ed47**|
|<i class="budicon-file-cross"></i> | budicon-file-cross | **ed48**|
|<i class="budicon-file-download"></i> | budicon-file-download | **ed49**|
|<i class="budicon-file-favorite"></i> | budicon-file-favorite | **ed4a**|
|<i class="budicon-file-heart"></i> | budicon-file-heart | **ed4b**|
|<i class="budicon-file-information"></i> | budicon-file-information | **ed4c**|
|<i class="budicon-file-lock"></i> | budicon-file-lock | **ed4d**|
|<i class="budicon-file-minus"></i> | budicon-file-minus | **ed4e**|
|<i class="budicon-file-plus"></i> | budicon-file-plus | **ed4f**|
|<i class="budicon-file-question"></i> | budicon-file-question | **ed50**|
|<i class="budicon-file-tick"></i> | budicon-file-tick | **ed51**|
|<i class="budicon-file-upload"></i> | budicon-file-upload | **ed52**|
|<i class="budicon-file"></i> | budicon-file | **ed53**|
|<i class="budicon-files-alert"></i> | budicon-files-alert | **ed54**|
|<i class="budicon-files-block"></i> | budicon-files-block | **ed55**|
|<i class="budicon-files-cross"></i> | budicon-files-cross | **ed56**|
|<i class="budicon-files-download"></i> | budicon-files-download | **ed57**|
|<i class="budicon-files-favorite"></i> | budicon-files-favorite | **ed58**|
|<i class="budicon-files-heart"></i> | budicon-files-heart | **ed59**|
|<i class="budicon-files-information"></i> | budicon-files-information | **ed5a**|
|<i class="budicon-files-lock"></i> | budicon-files-lock | **ed5b**|
|<i class="budicon-files-minus"></i> | budicon-files-minus | **ed5c**|
|<i class="budicon-files-plus"></i> | budicon-files-plus | **ed5d**|
|<i class="budicon-files-search"></i> | budicon-files-search | **ed5e**|
|<i class="budicon-files-tick"></i> | budicon-files-tick | **ed5f**|
|<i class="budicon-files-upload"></i> | budicon-files-upload | **ed60**|
|<i class="budicon-folded-page"></i> | budicon-folded-page | **ed61**|
|<i class="budicon-headline"></i> | budicon-headline | **ed62**|
|<i class="budicon-image-file"></i> | budicon-image-file | **ed63**|
|<i class="budicon-landscape-page"></i> | budicon-landscape-page | **ed64**|
|<i class="budicon-legal-file"></i> | budicon-legal-file | **ed65**|
|<i class="budicon-newspaper"></i> | budicon-newspaper | **ed66**|
|<i class="budicon-notepad"></i> | budicon-notepad | **ed67**|
|<i class="budicon-opened-book"></i> | budicon-opened-book | **ed68**|
|<i class="budicon-project-file-alert"></i> | budicon-project-file-alert | **ed69**|
|<i class="budicon-project-file-block"></i> | budicon-project-file-block | **ed6a**|
|<i class="budicon-project-file-cross"></i> | budicon-project-file-cross | **ed6b**|
|<i class="budicon-project-file-download"></i> | budicon-project-file-download | **ed6c**|
|<i class="budicon-project-file-favorite"></i> | budicon-project-file-favorite | **ed6d**|
|<i class="budicon-project-file-heart"></i> | budicon-project-file-heart | **ed6e**|
|<i class="budicon-project-file-information"></i> | budicon-project-file-information | **ed6f**|
|<i class="budicon-project-file-lock"></i> | budicon-project-file-lock | **ed70**|
|<i class="budicon-project-file-minus"></i> | budicon-project-file-minus | **ed71**|
|<i class="budicon-project-file-plus"></i> | budicon-project-file-plus | **ed72**|
|<i class="budicon-project-file-search"></i> | budicon-project-file-search | **ed73**|
|<i class="budicon-project-file-tick"></i> | budicon-project-file-tick | **ed74**|
|<i class="budicon-project-file-upload"></i> | budicon-project-file-upload | **ed75**|
|<i class="budicon-project-file"></i> | budicon-project-file | **ed76**|
|<i class="budicon-scan-page"></i> | budicon-scan-page | **ed77**|
|<i class="budicon-scheduled-box-file"></i> | budicon-scheduled-box-file | **ed78**|
|<i class="budicon-scheduled-file"></i> | budicon-scheduled-file | **ed79**|
|<i class="budicon-scheduled-files"></i> | budicon-scheduled-files | **ed7a**|
|<i class="budicon-scheduled-project-file"></i> | budicon-scheduled-project-file | **ed7b**|
|<i class="budicon-search-file"></i> | budicon-search-file | **ed7c**|
|<i class="budicon-shared-box-file"></i> | budicon-shared-box-file | **ed7d**|
|<i class="budicon-shared-file"></i> | budicon-shared-file | **ed7e**|
|<i class="budicon-shared-files"></i> | budicon-shared-files | **ed7f**|
|<i class="budicon-shared-project-file"></i> | budicon-shared-project-file | **ed80**|
|<i class="budicon-single-page"></i> | budicon-single-page | **ed81**|
|<i class="budicon-two-page"></i> | budicon-two-page | **ed82**|
|<i class="budicon-zip-file"></i> | budicon-zip-file | **ed83**|

## Finance
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-atm"></i> | budicon-atm | **ed84**|
|<i class="budicon-attachment-dollar"></i> | budicon-attachment-dollar | **ed85**|
|<i class="budicon-bad-trends"></i> | budicon-bad-trends | **ed86**|
|<i class="budicon-bank-a"></i> | budicon-bank-a | **ed87**|
|<i class="budicon-bubble-chart"></i> | budicon-bubble-chart | **ed88**|
|<i class="budicon-cash-dollar"></i> | budicon-cash-dollar | **ed89**|
|<i class="budicon-cash"></i> | budicon-cash | **ed8a**|
|<i class="budicon-dollar-sign"></i> | budicon-dollar-sign | **ed8b**|
|<i class="budicon-donation"></i> | budicon-donation | **ed8c**|
|<i class="budicon-donut-chart"></i> | budicon-donut-chart | **ed8d**|
|<i class="budicon-euro"></i> | budicon-euro | **ed8e**|
|<i class="budicon-finance-policy"></i> | budicon-finance-policy | **ed8f**|
|<i class="budicon-finance-project"></i> | budicon-finance-project | **ed90**|
|<i class="budicon-good-trends"></i> | budicon-good-trends | **ed91**|
|<i class="budicon-graphic-moderate-a"></i> | budicon-graphic-moderate-a | **ed92**|
|<i class="budicon-graphic-moderate"></i> | budicon-graphic-moderate | **ed93**|
|<i class="budicon-graphic-up-a"></i> | budicon-graphic-up-a | **ed94**|
|<i class="budicon-graphic-up"></i> | budicon-graphic-up | **ed95**|
|<i class="budicon-horizontal-graphic"></i> | budicon-horizontal-graphic | **ed96**|
|<i class="budicon-line-chart"></i> | budicon-line-chart | **ed97**|
|<i class="budicon-mobile-banking"></i> | budicon-mobile-banking | **ed98**|
|<i class="budicon-mobile-payment"></i> | budicon-mobile-payment | **ed99**|
|<i class="budicon-online-stock"></i> | budicon-online-stock | **ed9a**|
|<i class="budicon-performance-board-a"></i> | budicon-performance-board-a | **ed9b**|
|<i class="budicon-performance-board-minus"></i> | budicon-performance-board-minus | **ed9c**|
|<i class="budicon-performance-board-plus"></i> | budicon-performance-board-plus | **ed9d**|
|<i class="budicon-performance-board-tick"></i> | budicon-performance-board-tick | **ed9e**|
|<i class="budicon-performance-board"></i> | budicon-performance-board | **ed9f**|
|<i class="budicon-pie-chart-a"></i> | budicon-pie-chart-a | **eda0**|
|<i class="budicon-pie-chart"></i> | budicon-pie-chart | **eda1**|
|<i class="budicon-portfolio"></i> | budicon-portfolio | **eda2**|
|<i class="budicon-poundsterling"></i> | budicon-poundsterling | **eda3**|
|<i class="budicon-presentation-slide-a"></i> | budicon-presentation-slide-a | **eda4**|
|<i class="budicon-presentation-slide"></i> | budicon-presentation-slide | **eda5**|
|<i class="budicon-refund-dollar"></i> | budicon-refund-dollar | **eda6**|
|<i class="budicon-refund"></i> | budicon-refund | **eda7**|
|<i class="budicon-safe-case"></i> | budicon-safe-case | **eda8**|
|<i class="budicon-savings"></i> | budicon-savings | **eda9**|
|<i class="budicon-send-dollar"></i> | budicon-send-dollar | **edaa**|
|<i class="budicon-send-money"></i> | budicon-send-money | **edab**|
|<i class="budicon-web-banking"></i> | budicon-web-banking | **edac**|
|<i class="budicon-withdraw-dollar"></i> | budicon-withdraw-dollar | **edad**|
|<i class="budicon-withdraw-money"></i> | budicon-withdraw-money | **edae**|
|<i class="budicon-yen"></i> | budicon-yen | **edaf**|

## Buildings
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-apartment-a"></i> | budicon-apartment-a | **edb0**|
|<i class="budicon-apartment"></i> | budicon-apartment | **edb1**|
|<i class="budicon-bank"></i> | budicon-bank | **edb2**|
|<i class="budicon-barn"></i> | budicon-barn | **edb3**|
|<i class="budicon-business-area"></i> | budicon-business-area | **edb4**|
|<i class="budicon-church"></i> | budicon-church | **edb5**|
|<i class="budicon-city"></i> | budicon-city | **edb6**|
|<i class="budicon-garage"></i> | budicon-garage | **edb7**|
|<i class="budicon-golden-bridge"></i> | budicon-golden-bridge | **edb8**|
|<i class="budicon-government"></i> | budicon-government | **edb9**|
|<i class="budicon-high-building"></i> | budicon-high-building | **edba**|
|<i class="budicon-hotel-tower"></i> | budicon-hotel-tower | **edbb**|
|<i class="budicon-house-a"></i> | budicon-house-a | **edbc**|
|<i class="budicon-house"></i> | budicon-house | **edbd**|
|<i class="budicon-lighthouse"></i> | budicon-lighthouse | **edbe**|
|<i class="budicon-log-house"></i> | budicon-log-house | **edbf**|
|<i class="budicon-mall"></i> | budicon-mall | **edc0**|
|<i class="budicon-mansion"></i> | budicon-mansion | **edc1**|
|<i class="budicon-modern-office"></i> | budicon-modern-office | **edc2**|
|<i class="budicon-school"></i> | budicon-school | **edc3**|

## Users
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-female-user-block"></i> | budicon-female-user-block | **edc4**|
|<i class="budicon-female-user-cross"></i> | budicon-female-user-cross | **edc5**|
|<i class="budicon-female-user-favorite"></i> | budicon-female-user-favorite | **edc6**|
|<i class="budicon-female-user-information"></i> | budicon-female-user-information | **edc7**|
|<i class="budicon-female-user-lock"></i> | budicon-female-user-lock | **edc8**|
|<i class="budicon-female-user-minus"></i> | budicon-female-user-minus | **edc9**|
|<i class="budicon-female-user-plus"></i> | budicon-female-user-plus | **edca**|
|<i class="budicon-female-user-search"></i> | budicon-female-user-search | **edcb**|
|<i class="budicon-female-user-tick"></i> | budicon-female-user-tick | **edcc**|
|<i class="budicon-female-user"></i> | budicon-female-user | **edcd**|
|<i class="budicon-female"></i> | budicon-female | **edce**|
|<i class="budicon-gender"></i> | budicon-gender | **edcf**|
|<i class="budicon-group"></i> | budicon-group | **edd0**|
|<i class="budicon-male"></i> | budicon-male | **edd1**|
|<i class="budicon-password-lock"></i> | budicon-password-lock | **edd2**|
|<i class="budicon-password-unlock"></i> | budicon-password-unlock | **edd3**|
|<i class="budicon-profile-picture"></i> | budicon-profile-picture | **edd4**|
|<i class="budicon-shared-user"></i> | budicon-shared-user | **edd5**|
|<i class="budicon-user-a"></i> | budicon-user-a | **edd6**|
|<i class="budicon-user-b"></i> | budicon-user-b | **edd7**|
|<i class="budicon-user-block-a"></i> | budicon-user-block-a | **edd8**|
|<i class="budicon-user-block-b"></i> | budicon-user-block-b | **edd9**|
|<i class="budicon-user-block-c"></i> | budicon-user-block-c | **edda**|
|<i class="budicon-user-block"></i> | budicon-user-block | **eddb**|
|<i class="budicon-user-c"></i> | budicon-user-c | **eddc**|
|<i class="budicon-user-card-a"></i> | budicon-user-card-a | **eddd**|
|<i class="budicon-user-card"></i> | budicon-user-card | **edde**|
|<i class="budicon-user-cross-a"></i> | budicon-user-cross-a | **eddf**|
|<i class="budicon-user-cross-b"></i> | budicon-user-cross-b | **ede0**|
|<i class="budicon-user-cross-c"></i> | budicon-user-cross-c | **ede1**|
|<i class="budicon-user-cross"></i> | budicon-user-cross | **ede2**|
|<i class="budicon-user-favorite-a"></i> | budicon-user-favorite-a | **ede3**|
|<i class="budicon-user-favorite-b"></i> | budicon-user-favorite-b | **ede4**|
|<i class="budicon-user-favorite-c"></i> | budicon-user-favorite-c | **ede5**|
|<i class="budicon-user-favorite"></i> | budicon-user-favorite | **ede6**|
|<i class="budicon-user-information-a"></i> | budicon-user-information-a | **ede7**|
|<i class="budicon-user-information-b"></i> | budicon-user-information-b | **ede8**|
|<i class="budicon-user-information-c"></i> | budicon-user-information-c | **ede9**|
|<i class="budicon-user-information"></i> | budicon-user-information | **edea**|
|<i class="budicon-user-list-a"></i> | budicon-user-list-a | **edeb**|
|<i class="budicon-user-list-b"></i> | budicon-user-list-b | **edec**|
|<i class="budicon-user-list-c"></i> | budicon-user-list-c | **eded**|
|<i class="budicon-user-list-d"></i> | budicon-user-list-d | **edee**|
|<i class="budicon-user-lock-a"></i> | budicon-user-lock-a | **edef**|
|<i class="budicon-user-lock-b"></i> | budicon-user-lock-b | **edf0**|
|<i class="budicon-user-lock-c"></i> | budicon-user-lock-c | **edf1**|
|<i class="budicon-user-lock"></i> | budicon-user-lock | **edf2**|
|<i class="budicon-user-minus-a"></i> | budicon-user-minus-a | **edf3**|
|<i class="budicon-user-minus-b"></i> | budicon-user-minus-b | **edf4**|
|<i class="budicon-user-minus-c"></i> | budicon-user-minus-c | **edf5**|
|<i class="budicon-user-minus"></i> | budicon-user-minus | **edf6**|
|<i class="budicon-user-plus-a"></i> | budicon-user-plus-a | **edf7**|
|<i class="budicon-user-plus-b"></i> | budicon-user-plus-b | **edf8**|
|<i class="budicon-user-plus-c"></i> | budicon-user-plus-c | **edf9**|
|<i class="budicon-user-plus"></i> | budicon-user-plus | **edfa**|
|<i class="budicon-user-search-a"></i> | budicon-user-search-a | **edfb**|
|<i class="budicon-user-search-b"></i> | budicon-user-search-b | **edfc**|
|<i class="budicon-user-search-c"></i> | budicon-user-search-c | **edfd**|
|<i class="budicon-user-search"></i> | budicon-user-search | **edfe**|
|<i class="budicon-user-tick-a"></i> | budicon-user-tick-a | **edff**|
|<i class="budicon-user-tick-b"></i> | budicon-user-tick-b | **ee00**|
|<i class="budicon-user-tick-c"></i> | budicon-user-tick-c | **ee01**|
|<i class="budicon-user-tick"></i> | budicon-user-tick | **ee02**|
|<i class="budicon-user"></i> | budicon-user | **ee03**|

## Travel
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-air-craft"></i> | budicon-air-craft | **ee04**|
|<i class="budicon-airplane"></i> | budicon-airplane | **ee05**|
|<i class="budicon-arrival"></i> | budicon-arrival | **ee06**|
|<i class="budicon-bag-trolley"></i> | budicon-bag-trolley | **ee07**|
|<i class="budicon-beach"></i> | budicon-beach | **ee08**|
|<i class="budicon-bicycle"></i> | budicon-bicycle | **ee09**|
|<i class="budicon-big-motorcycles"></i> | budicon-big-motorcycles | **ee0a**|
|<i class="budicon-bus-front"></i> | budicon-bus-front | **ee0b**|
|<i class="budicon-bus"></i> | budicon-bus | **ee0c**|
|<i class="budicon-cabin-bag"></i> | budicon-cabin-bag | **ee0d**|
|<i class="budicon-cable-car"></i> | budicon-cable-car | **ee0e**|
|<i class="budicon-camping"></i> | budicon-camping | **ee0f**|
|<i class="budicon-car-front"></i> | budicon-car-front | **ee10**|
|<i class="budicon-compass-a"></i> | budicon-compass-a | **ee11**|
|<i class="budicon-compass-b"></i> | budicon-compass-b | **ee12**|
|<i class="budicon-compass"></i> | budicon-compass | **ee13**|
|<i class="budicon-cruise"></i> | budicon-cruise | **ee14**|
|<i class="budicon-departure"></i> | budicon-departure | **ee15**|
|<i class="budicon-direction"></i> | budicon-direction | **ee16**|
|<i class="budicon-electric-cord"></i> | budicon-electric-cord | **ee17**|
|<i class="budicon-electric-station"></i> | budicon-electric-station | **ee18**|
|<i class="budicon-first-aid"></i> | budicon-first-aid | **ee19**|
|<i class="budicon-fishing-boat"></i> | budicon-fishing-boat | **ee1a**|
|<i class="budicon-footstep"></i> | budicon-footstep | **ee1b**|
|<i class="budicon-forest"></i> | budicon-forest | **ee1c**|
|<i class="budicon-fuel-tank"></i> | budicon-fuel-tank | **ee1d**|
|<i class="budicon-gas-station"></i> | budicon-gas-station | **ee1e**|
|<i class="budicon-globe"></i> | budicon-globe | **ee1f**|
|<i class="budicon-helicopter"></i> | budicon-helicopter | **ee20**|
|<i class="budicon-hiking"></i> | budicon-hiking | **ee21**|
|<i class="budicon-hotel"></i> | budicon-hotel | **ee22**|
|<i class="budicon-international-travel"></i> | budicon-international-travel | **ee23**|
|<i class="budicon-iron"></i> | budicon-iron | **ee24**|
|<i class="budicon-jet-ski"></i> | budicon-jet-ski | **ee25**|
|<i class="budicon-location-area"></i> | budicon-location-area | **ee26**|
|<i class="budicon-location-finished"></i> | budicon-location-finished | **ee27**|
|<i class="budicon-location-pin-love"></i> | budicon-location-pin-love | **ee28**|
|<i class="budicon-location-pin-minus"></i> | budicon-location-pin-minus | **ee29**|
|<i class="budicon-location-pin-plus"></i> | budicon-location-pin-plus | **ee2a**|
|<i class="budicon-location-pin-tick"></i> | budicon-location-pin-tick | **ee2b**|
|<i class="budicon-location-pin"></i> | budicon-location-pin | **ee2c**|
|<i class="budicon-location"></i> | budicon-location | **ee2d**|
|<i class="budicon-luggage"></i> | budicon-luggage | **ee2e**|
|<i class="budicon-map"></i> | budicon-map | **ee2f**|
|<i class="budicon-motorcycle"></i> | budicon-motorcycle | **ee30**|
|<i class="budicon-multiple-destinations"></i> | budicon-multiple-destinations | **ee31**|
|<i class="budicon-navigation"></i> | budicon-navigation | **ee32**|
|<i class="budicon-parking-meter"></i> | budicon-parking-meter | **ee33**|
|<i class="budicon-passport"></i> | budicon-passport | **ee34**|
|<i class="budicon-plugs-a"></i> | budicon-plugs-a | **ee35**|
|<i class="budicon-plugs"></i> | budicon-plugs | **ee36**|
|<i class="budicon-pocket-knife"></i> | budicon-pocket-knife | **ee37**|
|<i class="budicon-power-cord"></i> | budicon-power-cord | **ee38**|
|<i class="budicon-propeller-airplane"></i> | budicon-propeller-airplane | **ee39**|
|<i class="budicon-sailing-boat"></i> | budicon-sailing-boat | **ee3a**|
|<i class="budicon-sandals"></i> | budicon-sandals | **ee3b**|
|<i class="budicon-sedan-front"></i> | budicon-sedan-front | **ee3c**|
|<i class="budicon-sedan"></i> | budicon-sedan | **ee3d**|
|<i class="budicon-self-navigation"></i> | budicon-self-navigation | **ee3e**|
|<i class="budicon-sightseeing"></i> | budicon-sightseeing | **ee3f**|
|<i class="budicon-small-bike"></i> | budicon-small-bike | **ee40**|
|<i class="budicon-sun"></i> | budicon-sun | **ee41**|
|<i class="budicon-sunblock"></i> | budicon-sunblock | **ee42**|
|<i class="budicon-surf"></i> | budicon-surf | **ee43**|
|<i class="budicon-taxi"></i> | budicon-taxi | **ee44**|
|<i class="budicon-tracks"></i> | budicon-tracks | **ee45**|
|<i class="budicon-traffic-light"></i> | budicon-traffic-light | **ee46**|
|<i class="budicon-train-front"></i> | budicon-train-front | **ee47**|
|<i class="budicon-travel-bag"></i> | budicon-travel-bag | **ee48**|
|<i class="budicon-travel-case"></i> | budicon-travel-case | **ee49**|
|<i class="budicon-travel-ticket"></i> | budicon-travel-ticket | **ee4a**|
|<i class="budicon-van"></i> | budicon-van | **ee4b**|
|<i class="budicon-vespa"></i> | budicon-vespa | **ee4c**|
|<i class="budicon-vintage-car"></i> | budicon-vintage-car | **ee4d**|

## Animals
| Icon        | Class           | Address  |
| ------------- |:-------------:| -----:|
|<i class="budicon-bear"></i> | budicon-bear | **ee4e**|
|<i class="budicon-beaver"></i> | budicon-beaver | **ee4f**|
|<i class="budicon-cat-front"></i> | budicon-cat-front | **ee50**|
|<i class="budicon-chicken"></i> | budicon-chicken | **ee51**|
|<i class="budicon-clamp"></i> | budicon-clamp | **ee52**|
|<i class="budicon-clown-fish"></i> | budicon-clown-fish | **ee53**|
|<i class="budicon-cow-a"></i> | budicon-cow-a | **ee54**|
|<i class="budicon-cow"></i> | budicon-cow | **ee55**|
|<i class="budicon-crab"></i> | budicon-crab | **ee56**|
|<i class="budicon-dog-front"></i> | budicon-dog-front | **ee57**|
|<i class="budicon-dog"></i> | budicon-dog | **ee58**|
|<i class="budicon-duck"></i> | budicon-duck | **ee59**|
|<i class="budicon-fish"></i> | budicon-fish | **ee5a**|
|<i class="budicon-fox"></i> | budicon-fox | **ee5b**|
|<i class="budicon-frog"></i> | budicon-frog | **ee5c**|
|<i class="budicon-gorilla"></i> | budicon-gorilla | **ee5d**|
|<i class="budicon-hedgehog"></i> | budicon-hedgehog | **ee5e**|
|<i class="budicon-hippo"></i> | budicon-hippo | **ee5f**|
|<i class="budicon-husky"></i> | budicon-husky | **ee60**|
|<i class="budicon-jellyfish"></i> | budicon-jellyfish | **ee61**|
|<i class="budicon-lion"></i> | budicon-lion | **ee62**|
|<i class="budicon-monkey"></i> | budicon-monkey | **ee63**|
|<i class="budicon-mouse-front"></i> | budicon-mouse-front | **ee64**|
|<i class="budicon-mouse"></i> | budicon-mouse | **ee65**|
|<i class="budicon-otter"></i> | budicon-otter | **ee66**|
|<i class="budicon-panda"></i> | budicon-panda | **ee67**|
|<i class="budicon-panther"></i> | budicon-panther | **ee68**|
|<i class="budicon-pig"></i> | budicon-pig | **ee69**|
|<i class="budicon-polar-bear"></i> | budicon-polar-bear | **ee6a**|
|<i class="budicon-rabbit"></i> | budicon-rabbit | **ee6b**|
|<i class="budicon-sardine"></i> | budicon-sardine | **ee6c**|
|<i class="budicon-seal"></i> | budicon-seal | **ee6d**|
|<i class="budicon-shark"></i> | budicon-shark | **ee6e**|
|<i class="budicon-shrimp"></i> | budicon-shrimp | **ee6f**|
|<i class="budicon-sloth"></i> | budicon-sloth | **ee70**|
|<i class="budicon-snail"></i> | budicon-snail | **ee71**|
|<i class="budicon-squirrel"></i> | budicon-squirrel | **ee72**|
|<i class="budicon-sympanse"></i> | budicon-sympanse | **ee73**|
|<i class="budicon-tiger"></i> | budicon-tiger | **ee74**|
|<i class="budicon-whale"></i> | budicon-whale | **ee75** |