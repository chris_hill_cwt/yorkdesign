# Layout


All York software should follow a basic structure. This keeps the experience the same for the users across different projects. Having a consistent design flow across projects also makes it easier for a user to get up and running.

The layout of York software has a few basic elements:

![alt text](app/images/basiclayout.jpg "Logo Title Text 1")

![alt text](app/images/basiclayoutdefinitions.jpg "Logo Title Text 1")

## Global Navigation
__View:__ Always visible - fixed to top 

The Global Navigation is a global element and house both the application logo and the Global Navigation links. It should be accessible throughout the application. Generally these are parts of the app specific to the user as opposed to sections specific to the function of the app.

Some examples of common Global Navigation links are:

- Logout
- Settings
- User Profle
- Help
- Notifications
- Messages
- Todos

## App Navigation
__View:__ Usually fixed, but can be collapsed to icons only view 

The App Navigation is your appication's main navigation. Major categories should appear here. If there are sub-navigation, use the Primary View Navigation for these.


## Primay View Navigation
__View:__ Shown only when there are more than one primary views.  

Once a major category is selected in the App Navigation, you might have several secondary navigation links the user can go to under that category. Primary View Navigation can be an array of icons, a dropdown menu or a list of links.

## Primary View
__View:__ Shows the page's primary view, the one that solves the user's primary problem on this view

This section is where the primary, parent information for the view lives. Clicking on elements inside the Primary View can change the content of the Secondary View.

The main purpose of the Primary View is to solve the main problem the user faces at this link. The Secondary View is for supplimental information to help solve the primary problem.

For example, clicking on a claim number in the primary view might show that claim in the secondary view.

## Secondary View
__View:__ Shows the page's secondary view

This section is optional. If your app will never need the secondary view, it can be eliminated and the PagePrimary can be full width. Typically this view will show information or actions related to the Primary View. If there is nothing to show here in the current view, use an empty state design pattern. See "Empty State"

## Global View
__View:__ Shows view for selected global nav item

If a user selects one of the right-hand global nav items, this slide-out panel will display that item's view. By default it's usually hidden.