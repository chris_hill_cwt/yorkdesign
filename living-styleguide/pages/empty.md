# Empty states
#### Empty states occur when an item’s content can’t be shown.

## Usage
A list that doesn’t contain any items, or a search that doesn’t display any results, are examples of empty states. Although these states aren’t typical, they should be designed to prevent user confusion.

## Displaying empty states ###
The most basic empty state displays a non-interactive image and a text tagline.

Use an image that:

- Is subtle and neutral with respect to the background
- Conveys the purpose and potential of the app in a lively way, such as your app's icon

Include a tagline that:

- Has a positive tone
- Is consistent with your brand
- Conveys the purpose of the app without appearing to be actionable

## Avoiding completely empty states ### 
There are several situations in which you can provide users with alternatives to truly empty states.

__Starter content__

The most compelling way for new users to learn and get excited about your app is by using it. Consider providing starter content that will allow users to explore your app right away.

Recommendations:

- Use content that has broad appeal and demonstrates primary features
- Give users the ability to delete and replace this content
- If possible, provide content that's personalized

__Educational content__

If the purpose of the screen isn't easily conveyed through an image and a tagline, consider showing educational content instead.

Recommendations:

- Help users understand what they'll be able to do on this screen once it has content
- Make it possible to dismiss or skip this content
- Keep it brief

__Best match__

If nothing exactly matches the user's query, are there any results for a query spelled slightly differently? If so, then show the results, as they may help a user find what they're after.

With this approach, clearly convey in a heading above the results that this content shouldn't be mistaken for a match to actual query results.