# Help & Tooltips
#### Help content provides answers to user’s questions and concerns.

## Usage
Help content provides answers to common user questions about your app. Users can send comments, report bugs, and ask questions that are not already answered by the help content.

## Placement
Make it easy for users to find help content in your app.

Help content can be made accessible from various points in your app’s navigation, with options listed below. It is usually placed in the navigation drawer (or overflow menu) under the label “Help” or “Send feedback.”

### App bar
To make it easier for users to find general assistance, place a Help icon in the topnav bar.

## Behavior
### Relevant help topics
Help content should be relevant to the user’s current location in an app. For example, if a user is viewing their account information, the help content displayed should include information about accounts.

### Immediate access
Take the user directly to help content upon selecting “Help” in your app.

### Unsolicited help
Occasionally, help content appears when users interact with an app for the first time, even though the user has not requested help. By offering unsolicited help, users don’t have to search the “Help” menu for answers on how to use new features. This kind of help often appears:

- To promote feature discovery, when a feature can’t easily be activated from a menu or button
- When a new gesture is introduced that wasn’t in the app before

This type of help should be limited to new features and gesture education, as it could be interruptive and distracting.

## Writing
Help content lets users find answers to questions or problems that are unique to your app.

Consider these guidelines when writing help content:

### Give key information
Keep explanations as short as possible. Avoid giving details that aren’t relevant to typical usage. Answer one question or concern at a time.

### Make it easy to read
Make help content easy to read by formatting text with bold headings, lists, tables, and space between paragraphs as needed.

In particular, when referring to elements that users need to select, such as buttons or links, bold the label names when referring to them in help content.

### Use simple language
Avoid using technical terms in help content where possible.

### Show images
When providing step-by-step instructions, show relevant images or icons to explain what the user needs to do.