# Colors
#### York employs a muted color scheme with shocks of color where the user's attention should be directed.

## Usage
York's app color palette comprises primary and accent colors that can be used for primary and secondary actions. They’ve been designed to work harmoniously with each other. 


## App Chrome
The primary color scheme of York apps are blue-grey chrome elements on a white background. These muted colors keeps the user's focus on the content and makes room for using color to highlight important information and push the user towards primary actions


<div class="color-tag light" style="background-color: #3c4659;">
<span class="group" style="font-size: 22px;margin-bottom: 60px;">Slate 700</span>
<div class="details">
<span class="hex" style="font-size: 22px;">#3c4659</span>
</div>
</div>
<div class="color-tag light" style="background-color: #ffffff;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 60px; color: #2b2a28">White</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #2b2a28">#ffffff</span>
</div>
</div>
<br/><br/>
<div class="color-tag light" style="background-color: #5f6f8f;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 25px; color: #ffffff">Slate 500</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #ffffff">#5f6f8f</span>
</div>
</div>
<div class="color-tag light" style="background-color: #ebf0f3;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 25px; color: #2b2a28">Slate 50</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #2b2a28">#ebf0f3</span>
</div>
</div>
<br/><br/>
<div class="color-tag light" style="background-color: #252c38;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 0px; color: #fffff">Slate 900</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #ffffff">#252c38</span>
</div>
</div>
<div class="color-tag light" style="background-color: #2d3642;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 0px; color: #fffff">Slate 800</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #ffffff">#2d3642</span>
</div>
</div>
<div class="color-tag light" style="background-color: #4c5a72;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 0px; color: #fffff">Slate 600</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #ffffff">#4c5a72</span>
</div>
</div>

<div class="color-tag light" style="background-color: #7789a8;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 0px; color: #fffff">Slate 400</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #ffffff">#7789a8</span>
</div>
</div>
<div class="color-tag light" style="background-color: #8da1be;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 0px; color: #fffff">Slate 300</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #ffffff">#8da1be</span>
</div>
</div>
<div class="color-tag light" style="background-color: #bac9dc;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 0px; color: #2b2a28">Slate 200</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #2b2a28">#bac9dc</span>
</div>
</div>
<div class="color-tag light" style="background-color: #d6dfe9;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 0px; color: #2b2a28">Slate 100</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #2b2a28">#d6dfe9</span>
</div>
</div>



## Primary Actions / Highlight
Use York Orange to bring the user's attention to something important in the UI, or to point them towards an action you'd like them to take.


<div class="color-tag light" style="background-color: #f28531;">
<span class="group" style="font-size: 22px;margin-bottom: 60px;">York Orange</span>
<div class="details">
<span class="hex" style="font-size: 22px;">#f28531</span>
</div>
</div>


## A Secondary Actions / Highlight
Use this color for secondary actions in the UI.

<div class="module">
<div class="color-tag light" style="background-color: #6f89a4;">
<span class="group" style="font-size: 22px;">Medium Blue</span>
<div class="details">
<span class="hex" style="font-size: 22px;">#6f89a4</span>
</div>
</div>
<div class="color-tag light" style="background-color: #bed4e9;">
<span class="group" style="font-size: 22px;margin-bottom: 0;">Light Blue</span>
<div class="details">
<span class="hex">#bed4e9</span>
</div>
</div>