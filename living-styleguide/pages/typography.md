# Typography

## Georgia

##### Font stack
For both mobile and web properties, the font stack should specify `Georgia, Times, Times New Roman, serif`. This font is primarily used for headings.

##### Glyphs
<div style="font-size: 32px; line-height: 1.4; font-family: Georgia, Times, Times New Roman, serif">
ABCDEFGHIJKLMNOPQRSŠTUVWXYZŽ
<br/>
abcdefghijklmnopqrsštuvwxyzž
<br/>
ÂÊÔâêô
<br/>
1234567890
<br/>
‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*</div>

##### Headers
# Header 1: Experienced Claims and Risk Management Specialists
## Header 2: Experienced Claims and Risk Management Specialists
### Header 3: Experienced Claims and Risk Management Specialists 
#### Header 4: Experienced Claims and Risk Management Specialists
##### Header 5: Experienced Claims and Risk Management Specialists
<hr/>

## Source Sans Pro

##### Font stack
For both mobile and web properties, the font stack should specify `'Source Sans Pro', sans-serif`.

#####  Glyphs
<div style="font-size: 32px;line-height: 1.4; font-family: 'Source Sans Pro', sans-serif">
ABCDEFGHIJKLMNOPQRSŠTUVWXYZŽ
<br/>
abcdefghijklmnopqrsštuvwxyzž
<br/>
ÂÊÔâêô
<br/>
1234567890
<br/>
‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*</div>

##### Paragraph
I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at.

##### Blockquote
Use the class `blockquote` on a paragraph to render: `<p class="blockquote"></p>`

<blockquote>
<p class="blockquote">
"I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at."
<br>
<br>
<small>- Kanye West
</small>
</p>
</blockquote>

##### Paragraph Styles

### Muted Text

`<p class="text-muted"></p>`
<p class="text-muted">
I will be the leader of a company that ends up being worth billions of dollars, because I got the answers...
</p>

### Primary Text

`<p class="text-primary"></p>`
<p class="text-primary">
I will be the leader of a company that ends up being worth billions of dollars, because I got the answers...
</p>

### Info Text

`<p class="text-info"></p>`
<p class="text-info">
I will be the leader of a company that ends up being worth billions of dollars, because I got the answers...
</p>

### Success Text

`<p class="text-success"></p>`
<p class="text-success">
I will be the leader of a company that ends up being worth billions of dollars, because I got the answers... 
</p>

### Warning Text

`<p class="text-warning"></p>`
<p class="text-warning">
I will be the leader of a company that ends up being worth billions of dollars, because I got the answers...
</p>

### Danger Text

`<p class="text-danger"></p>`
<p class="text-danger">
I will be the leader of a company that ends up being worth billions of dollars, because I got the answers... 
</p>

