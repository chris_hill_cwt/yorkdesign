# Introduction

## This is the beta version of the York Software Style Kit

__**The latest beta version of the york style sheets, fonts and icons can be found**__ [here](https://s3.amazonaws.com/yorksingularity/YorkStyleGuide.zip)

This York Software Style Kit should be referenced when building all applications within York. It's important that each software project we produce has some consistent design principles and patterns.

This document lays out the baseline design standards for all York software. It can be should be referenced throughout the development of your project.

These standards will work with both vanilla and Bootstrap projects. The styles, fonts and icons are designed to be used across both types of front end.

This is still in Beta. If any bugs or issues arise, please contact Schonne Eldridge (schonne.eldridge@careworkstech.com)

York's Style Guide assets is a responsive Bootstrap 4 kit that can depreciate to a non-Bootstrap project.

York's Style Guide will help you create a clean and simple web project that is a perfect fit for today's flat design. It is built using the 12 column grid system, with components designed to fit together perfectly. It makes use of bold colours, beautiful typography, clear photography and spacious arrangements.

### Restyled Components ###
Here is the list of Bootstrap 4 components that were restyled in York Style Kit:

- Buttons
- Inputs
- Select
- Textarea
- Dropdown
- Progress Bars
- Navbars
- Pagination
- Labels
- Typography
- Modal

### New Components ###

Besides giving the existing Bootstrap elements a new look, we added new ones, so that the interface and consistent and homogenous.

Going through them, we added:

- Checkboxes
- Radio Buttons
- Budicons Icons
