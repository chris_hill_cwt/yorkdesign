We added extra classes that can help you better customise the look. You can use regular buttons, rounded corners buttons or plain simple buttons.

Button groups and disabled state all work like they are supposed to.

Icons are part of the budicon icon font included as part of York's Style Kit.

Please note: there are no rounded buttons in the Style Kit. Putting in the `btn-round` class will default to square corners.