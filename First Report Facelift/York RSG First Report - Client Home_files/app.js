﻿angular
    .module('firstReport', ['ui.bootstrap', 'angularSpinner', 'smart-table'])

    .controller('lookup', function ($scope, $uibModal, $compile) {

        // we must re-compile the angular directives bound in the ASP.net UpdatePanel
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
            var elem = angular.element(document.getElementById("clientHomeReportOptions"));
            $compile(elem.children())($scope);
            $scope.$apply();
        });

        $scope.employees = {};

        $scope.open = function () {

            if ($('.ddlEmployerLocations').val() == "") {
                $('.rfvEmployerLocations').css('visibility', 'visible');
                return;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'employeeSearch.html',
                controller: 'EmployeeLookupModal',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    employees: function () {
                        return $scope.employees;
                    }
                }
            });
        };

    })

    .controller('EmployeeLookupModal', function ($scope, $http, $uibModal, $uibModalInstance, employees, usSpinnerService) {

        $scope.employees = employees;
        $scope.searchType = 'empnum'; // default button-group selection
        $scope.searchQuery = '';
        $scope.selectedCss = '';

        $scope.displayedCollection = [].concat($scope.employees);

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.createReport = function () {
            $('.spinner-overlay').show();
            $scope.startSpin();
            $('.report-button').click();
        };

        $scope.startSpin = function () {
            usSpinnerService.spin('spinner');
        }
        $scope.stopSpin = function () {
            usSpinnerService.stop('spinner');
        }

        $scope.searchEmployees = function () {

            $('.spinner-overlay').show();
            $scope.startSpin();

            var query = '';
            switch($scope.searchType)
            {
                case "lname":
                    query = '&lname=' + $scope.searchQuery;
                    break;
                case "empnum":
                    query = '&empnum=' + $scope.searchQuery;
                    break;
                case "ssn":
                    query = '&ssn=' + $scope.searchQuery;
                    break;
            }

            $http.get('/SearchDataExchange.aspx?clientNo=7399' + query, { cache: false })
                .success(function (data) {
                    $scope.employees = data.results;
                    $('.spinner-overlay').hide();
                    $scope.stopSpin();
                });
        };

        // select a row in the employee list table
        $scope.selectEmployee = function (empnum) {

            $('.selected').removeClass('selected');
            $('#' + empnum).addClass('selected');
            $('.create-report').prop('disabled', '');

            // jquery string end matching
            $('input[id$=hfEmpId]').val(empnum);
        };

        $scope.openMissing = function () {

            $scope.cancel();

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'employeeMissing.html',
                controller: 'EmployeeMissingModal',
                size: 'med',
                backdrop: 'static'
            });

        };
    })

    .controller('EmployeeMissingModal', function ($scope, $uibModalInstance, usSpinnerService) {

        $('input[id$=hfEmpId]').val('0');

        $scope.startSpin = function () {
            usSpinnerService.spin('spinner');
        }
        $scope.stopSpin = function () {
            usSpinnerService.stop('spinner');
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.createReportAnyway = function () {
            $('.spinner-overlay').show();
            $scope.startSpin();
            $('.report-button').click();
        };
    })

    // used to format strings in title case
    .filter('titleCase', function () {
        return function (input) {
            input = input || '';
            return input.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
        };
    })

    // used to capture the enter key and perform an action on the input element
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });
 
                    event.preventDefault();
                }
            });
        };
    });