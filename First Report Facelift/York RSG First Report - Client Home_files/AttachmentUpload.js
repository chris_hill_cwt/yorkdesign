﻿// MS 01/24/2012: We should probably combine this with textPopUp but for now we will keep them separate to get the differences worked out.

//0 means invisible; 1 means visible; implemented for testing, but may be needed for certain instances
var overlayStatus = 0;
var textBoxField3 = null;
var textBoxFieldLabel3 = null;
var submitting = false;

//function bUpload_Click()
//{
//    if (ValidAttachment())
//    {
//        var xhr = new XMLHttpRequest();
//        var data = new FormData();
//        var files = $("#FileUpload1").get(0).files;

//        for (var i = 0; i < files.length; i++) {
//            data.append(files[i].name, files[i]);
//        }

//        data.append("UploadFormId", $('#UploadForm_ID').val());
//        data.append("ClaimId", $('#Claim_ID').val());
//        data.append("ClaimNumber", $('#ClaimNumber').val());
//        data.append("Comments", $('#taComments').val());
//        data.append("AttachmentCategoryId", $('#ddlAttachmentCategory').val());

//        xhr.upload.addEventListener("progress", function (evt) {
//            if (evt.lengthComputable) {
//                var progress = Math.round(evt.loaded * 100 / evt.total);

//                $("#Progressbar").progressbar("value", progress);
//            }
//        });

//        //xhr.upload.addEventListener("load", function (evt)
//        //                                    {
//        //                                        $('#uploadMessage').text('Done!');
//        //                                    });
//        xhr.upload.addEventListener("error", function (evt) {
//            $('#uploadMessage').text('Oh no!');
//        });
//        xhr.upload.addEventListener("abort", function (evt) {
//            $('#uploadMessage').text('Cancelled!');
//        });

//        xhr.open("POST", "UploadHandler.ashx");
//        xhr.send(data);

//        $("#Progressbar").progressbar(
//                            {
//                                max: 100,
//                                change: function (evt, ui) {
//                                    $(".ProgressLabel").text($("#Progressbar").progressbar("value") + "%");
//                                },
//                                complete: function (evt, ui) {
//                                    $(".ProgressLabel").text("Complete!");
//                                }
//                            });

//        //// No longer needed now that this is converted to standard function.
//        //evt.preventDefault();

//        if (xhr.readyState === xhr.DONE) {
//            // Do these work here?
//            window.alert('Status: ' + xhr.status);
//            window.alert('Response: ' + xhr.response);
//            window.alert('Response Text: ' + xhr.responseText);
//        }
//    }
//    else
//    {
//                                            return false;
//    }
//}

function ValidAttachment()
{
    var maxFileSizeInMb = 50;
    var maxFileSizeInBytes = (maxFileSizeInMb * 1024) * 1024;
    var fileUpload = $('#FileUpload1');

    if (fileUpload.val() == '')
    {
        return false;
    }
    else if (submitting == false) {
        if (fileUpload[0].files[0].size < maxFileSizeInBytes) {
            // Can't disable the submit button or we get an error. We'll track a variable instead.
            //$('#bUpload').prop('disabled', true);
            submitting = true;
            $('#uploadMessage').text('');
            return true;
        }
        else {
            $('#uploadMessage').text('Files must be smaller than ' + maxFileSizeInMb + ' MB!');
            return false;
        }
    }
    else
    {
        // If we make it here it was a duplicate click.
        return false;
    }

    //return true;
}

function ShowUploadOverlay(Opacity)
{

    if (overlayStatus == 0)
    {
        hideSelectBoxes3();

        $("#overlayBackground").css({ "opacity": Opacity });
        $("#overlayBackground").fadeIn("slow");
        $("#UploadOverlay").fadeIn("slow");

        overlayStatus = 1;

        CenterOverlay("#UploadOverlay");
    }
}

function HideUploadOverlay()
{
    //disables popup only if it is visible
    if (overlayStatus == 1)
    {
        $("#overlayBackground").fadeOut("slow");
        $("#UploadOverlay").fadeOut("slow");

        //document.getElementById("uploadMessage").innerHTML = "";

        showSelectBoxes3();

        overlayStatus = 0;
    }
}

function showPopup3(url, opacity, div, obj)
{
	//loads popup from external html file only if it is invisible.
	//use the load callback function to ensure that the resource is loaded before updating it's css.
    if (overlayStatus == 0)
    {
        $(div).load(url, function (responseText, textStatus, XMLHttpRequest)
        {
            if (textStatus == "success")
            {
				hideSelectBoxes2();

				$("#overlayBackground").css({ "opacity": opacity });
				$("#overlayBackground").fadeIn("slow");
				$("#UploadOverlay").fadeIn("slow");

				overlayStatus = 1;
				centerPopup2();

				//iterate through textarea controls to find popup due to ie bug preventing use of getElementById
				var tags = document.getElementsByTagName("textarea");

				for (var i = 0; i < tags.length; i++)
                {
					var status = tags[i].getAttribute("id");

					if (status == "multiLineTextBox") { var textAreaField = tags[i]; }
				}

				//step back through DOM to retrieve previous object (label span)
	            var prevObj = obj;

				do prevObj = prevObj.previousSibling;
				while (prevObj && prevObj.nodeType != 1);

				textBoxFieldLabel2 = prevObj;

				//update popup label with control label
				document.getElementById('textAreaLabel').innerHTML = textBoxFieldLabel2.innerHTML;

				textBoxField2 = obj;
				textAreaField.focus();
				textAreaField.value = textBoxField2.value;
            }

            if (textStatus == "error")
            {
				//In the event the resource cannot load, display help and error message
                ret = confirm('An error prevented the update of the requested field. Please choose save incomplete to save your progress and contact the administrator.');

                if (ret == true)
                {
					hidePopup3();
				}
	            else
                {
					hidePopup3();
				}
			}
		})
		//$(div).load(url);
	}
}

function hidePopup3()
{
	//disables popup only if it is visible
    if (popupStatus2 == 1)
    {
		$("#overlayBackground").fadeOut("slow");
		$("#UploadOverlay").fadeOut("slow");

		showSelectBoxes2();

		popupStatus2 = 0;
	}
}

function CenterOverlay(Div)
{
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $(Div).height();
	var popupWidth = $(Div).width();

	//centering
	$(Div).css(
    {
		"position": "absolute",
		"top": windowHeight / 2 - popupHeight / 2,
		"left": windowWidth / 2 - popupWidth / 2
	});
}

function transferText3()
{
	//iterate through textarea controls to find popup due to ie bug preventing use of getElementById
    var tags = document.getElementsByTagName("textarea");

    for (var i = 0; i < tags.length; i++)
    {
        var status = tags[i].getAttribute("id");

        if (status == "multiLineTextBox") { var textAreaField = tags[i]; }
    }

	//transfer text and set focus back to original control
	textBoxField2.focus();
	textBoxField2.value = textAreaField.value;
	textBoxField2.title = textAreaField.value;

	//cleanup popup window and variables
	hidePopup2();

	textBoxField2 = null;
	document.getElementById('textAreaLabel').innerHTML = null;
}

function hideSelectBoxes3()
{
    if ((navigator.appVersion.indexOf("MSIE 6.") != -1))
    {
		// Hide Select Boxes for IE6  
        var x = document.getElementsByTagName("select");

        for (i = 0; i < x.length; i++)
        {
			x[i].style.display = "none";
		}
	}
}

function showSelectBoxes3()
{
    if ((navigator.appVersion.indexOf("MSIE 6.") != -1))
    {
		// Show Select Boxes for IE6  
        var x = document.getElementsByTagName("select");

        for (i = 0; i < x.length; i++)
        {
			x[i].style.display = "inline";
		}
	}
}