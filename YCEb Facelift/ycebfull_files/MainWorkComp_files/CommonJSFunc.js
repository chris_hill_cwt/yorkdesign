	//LISTS:
	var Occupation = ""; var JobCode=''; var EmplStatus=''; var SettlType='';
	var NCCI_ClassCode = ''; var NCCI_Source_Of_Injury_List = ''; var NCCI_Nature_Of_Injury_List = ''; var WC_LossCov = '';
	var Loss_Cause = new Array();var strLoss_Cause = ''; 
	var WC_Minor_LossType='';var WC_PmntSuspReason='';
	//
	var IAdjFlag_Original=IAdjFlag;  IAdjFlag=0;
	var PrFlag_Original = PrFlag;   PrFlag = 0;
	var prodliabFlag_Original = prodliabFlag; prodliabFlag = 0;
	var Required_custom_FLD_Msg=''; // message & flag for required custom fields
	var COUNTY=''; //List of counties loaded to browser
	var ModDuty_To_=0; // only one record in Mod Duties can be empty string
	var STAT_Hist='';var ADJ_Hist='';var COV_Hist='';var MLT_Hist='';var RATE_Hist='';var BRCH_Hist='';var OPEN_Hist='';
	var BenTechName='';var ModDutyType_List='';
	 // lists complex  claims codes
	var Complex_ReferralReason=''; var Complex_IssueType=''; var SLA_GA_LIST='';var ComplexAnalyst_LIST='';//list of employess of the complex branches
	//Flag for new reserves to be approved:
	var IR_appr_new=0;var ER_appr_new=0;var MR_appr_new=0; var MC_appr_flag='';var reserveChangeReason = '';

	var HisResTrnsact = ''; var arrHisRes = new Array(); var AllResTrnsact = '';
	var Branch_Original = Branch;
    var SetDiary;
    
    var Cov_Full = Cov;  // full list of coverage codes

    var ClmOriginal = (function() {
        if (typeof (this.OccurrenceInjuryData) != "object") {
            this.OccurrenceInjuryData = {};
        }

        this.CustomLists_ = {}; objectExtend(CustomLists, CustomLists_);

        if (typeof (this.Doctors) != "object") {
            this.Doctors = {                        //Settings: function(screen, id, oncancel) { },
                screen: 'List',
                doctorId: 0,
                claimMpnId: 0,
                cancel: function() { }
            };
        }


        if (typeof (this.Vehicles) != "object") {
            this.Vehicles = {
                screen: 'List',
                vehicleId: 0,  // policeReportId: 0,
                cancel: function() { }   // this func defined as function CloseMe(pass){}  in ClaimVehicles.aspx 
            };
        }

        //        if (typeof (this.EDIData) != "object") {
        //            this.EDIData = { release: 0 };
        //            this.EDIData.release = EDIData_XTRCT();
        //        }

        if (typeof (this.Aigrm_AccDesc_List) != "object" && (SCREEN == "AIGRM_Rental" || SCREEN == "AIGRM")) {
            this.Aigrm_AccDesc_List = { lob: Unit,
                code: AccDsc,
                id: AccDsc_Id,
                optionsList: '',
                refresh: function(lob, code, id, listReload) {
                    var retval = '';
                    if (typeof (lob) === 'string') {
                        retval = AIGRM_ACCDESC_XTRCT(lob, code, id, listReload);
                    }
                    this.optionsList = retval;
                    return;
                }
            }
        }

        if (typeof (this.ClaimType) != "object") {
            var _isProperty = (Unit == 'CO' || Unit == 'HO' || Unit == 'IM' || Unit == 'OM' || Unit == 'CR') ? 1 : 0;
            this.ClaimType = {
                LOB : Unit,
                CovCod: CovCod,
                isProperty: _isProperty
            }
        }
    
        if (typeof (this.odgReserves) != "object") {
            this.odgReserves = {
                data: null,
                populateScreen: function() {
                    if (this.data != null) {
                        if (this.data.hasOwnProperty('ir')) {
                            $('#odgIR', frInfo.document).html(this.data.ir);
                        }
                        if (this.data.hasOwnProperty('mr')) {
                            $('#odgMR', frInfo.document).html(this.data.mr);
                        }
                        if (this.data.hasOwnProperty('er')) {
                            $('#odgER', frInfo.document).html(this.data.er);
                        }
                        if (this.data.hasOwnProperty('total')) {
                            $('#odgTotal', frInfo.document).html(this.data.total);
                        }
                        if (this.data.hasOwnProperty('lostDays')) {
                            $('#odgLostDays', frInfo.document).html(this.data.lostDays);
                        }
                    }
                    return;
                }
            }
        }

        if (typeof (this.DynamicElements) != "object") {
            this.DynamicElements = {
                screens: 'PIP,Property,ThirdPartyDeductible',
                displayedFields: [],
                hideScreen: function(_screen) {
                    if (typeof this.displayedFields[_screen] != 'undefined') {
                        this.displayedFields[_screen].length = 0;
                    }
                },
                hideField: function(_screen, _field) {
                    var index = this.isFieldDisplayed(_screen, _field);
                    if (index > -1) {
                        delete this.displayedFields[_screen][index];
                    }
                },
                displayedField: function(_screen, _field, _value) {
                    if (this.isFieldDisplayed(_screen, _field) < 0) {

                        var _o = {
                            screen: _screen,
                            field: _field,
                            value: _value
                        }
                        if (typeof this.displayedFields[_screen] == 'undefined') {
                            this.displayedFields[_screen] = [];
                        }
                        this.displayedFields[_screen].push(_o);
                    }
                },
                isFieldDisplayed: function(_screen, _field) {
                    var index;
                    var _o;
                    var retval = -1;

                    if (typeof this.displayedFields[_screen] != 'undefined') {
                        var _c = this.displayedFields[_screen].length;
                        for (index = 0; index < _c; ++index) {
                            if (typeof this.displayedFields[_screen][index] == 'object') {
                                _o = this.displayedFields[_screen][index];
                                if (_o.screen == _screen && _o.field == _field) {
                                    retval = index;
                                    break;
                                }
                            }
                        }
                    }
                    return retval;
                }
            };
        };
        return ({ AccDate: AccDate,
            PolNo: PolNo,
            EffDT: EffDate,
            ExpDT: ExpDate,
            CovCod: CovCod,
            ClaimType : this.ClaimType,
            OccurrenceInjuryData: this.OccurrenceInjuryData,
            Doctors: this.Doctors,
            Vehicles: this.Vehicles,
            CustomLists: this.CustomLists_,
            //            EDIData: this.EDIData,
            odgReserves: this.odgReserves,
            Aigrm_AccDesc_List: this.Aigrm_AccDesc_List,
            DynamicElements: this.DynamicElements,
            HistLinks: {},
            ChildWindows: {},
            HR: {
                id: -1,
                Employee: {
                    ssNum: '',
                    lName: '',
                    fFName: '',
                    mi: '',
                    cAddr1: '',
                    cAddr2: '',
                    cCity: '',
                    cState: '',
                    cZip: '',
                    cPhone: '',
                    workPhone: '',
                    eMail: '',
                    dob: '',
                    sex: ''
                },
                Employment: {
                    employeeId: '',
                    dept: '',
                    jobTitle: '',
                    emplStatusCode: 0,
                    weekWage: 0,
                    weeklyHours: 0,
                    hireDate: '',
                    terminateJobDate: '',
                    employerLocId: '',
                    employerNo: '',
                    employerTaxId: '',
                    HRSuperRecId: 0,
                    Loc: -1,
                    LocTxtCod: '',
                    customJS: null
                }
            }
        });
    } ()
    );
    
    var CountryRegions =
        ({
            countryList: '',
            stateList: '',
            stateLegacyList: '',          
            refreshStateList:function(countryCode){
                var lst  = ''
                lst = stateListHtmlGet(countryCode, 0, '');
                return(lst);
            },
            init: function() {
                this.countryList = '';
                for (var c in Countries) {
                    if (Countries[c].hasOwnProperty('country')) {
                        this.countryList += '<option value="' + Countries[c].code + '">' + Countries[c].country + '</option>';
                    }
                };
                this.stateList = stateListHtmlGet(LossCountry, LossStateId, State);
                this.stateLegacyList = StatesCodes;  //include on FormPopulation.asp
                
                return this;
            }
        }).init();


        function stateListHtmlGet(countryCode, stateId, stateCode) {
            var RetVal
            RetVal = "<option value='' stateId=0 aigrm=0"
            if (stateId == 0 && stateCode == '') {
                RetVal += "  selected ";
            }
            RetVal += ">NOT SPECIFIED</option>";
            var regions  = new Array();
            var aigrmScreen = (SCREEN.indexOf("AIGRM") > -1) ? 1 : 0;
            var aigrm = 0;

            if (countryCode != '') {
                if (Countries[countryCode].regions.length < 2) {
                    RetVal = "<option value='' stateId='" + Countries[countryCode].regions[0].id + "' selected>NOT SPECIFIED</option>";
                } else {
                    regions = Countries[countryCode].regions;
                    var c = 0
                    while (c < regions.length) {
                        if (Countries[countryCode].regions[c].active == 'Y' || Countries[countryCode].regions[c].code == stateCode || Countries[countryCode].regions[c].id == stateId) {   // current state (even if inactive should appear on the screen)
                            aigrm = (aigrmScreen == 0) ? 0 : Countries[countryCode].regions[c].aigrm;
                            RetVal += "<option value='" + Countries[countryCode].regions[c].code + "' aigrm=" + aigrm + " stateId='" + Countries[countryCode].regions[c].id + "'"
                            if (State === Countries[countryCode].regions[c].code || State === aigrm || LossStateId === Countries[countryCode].regions[c].id) {
                               RetVal += " selected " 
                            }
                             RetVal += ">" + Countries[countryCode].regions[c].desc + "</option>";
                            c++
                        }    
                    }
                }
            }
            return(RetVal);
        }
    
    // FUNCTION PROPERTIES:
    Settlement_Get.count = 0; 

	//custom Object - ReOpenFileDiary
	function objOpenFileDiary(message, status) { 
	    this.message = message;
	    this.status = status;
	    this.SetOpenFileDiary = SetOpenFileDiary;
	}

	function SetOpenFileDiary() {
	        var d2 = new Date;
	        var indS = frGeneral.document.frmGeneral.cboSuper.selectedIndex;
	        var adjBranch =GetBranch('Adj');
	        var url = "ReOpenFileDiary.asp?T=" + d2.getTime() + "&ClmNo=" + ClaimNumber + "&ADJ=" + escape(frGeneral.document.frmGeneral.cboAdjuster.value) + "&Super=" + frGeneral.document.frmGeneral.cboSuper.options[indS].id + "&STATUS=" + SetDiary.status + "&ClientNumber=" + ClientNumber + "&User=" + User + "&adjBranch=" + adjBranch + "&Note=" + escape(SetDiary.message);
	        var rRetVal = window.showModalDialog(url, "Diary", "dialogHeight:70px;dialogwidth:650px;dialogTop:190;dialogLeft:90;resizable:yes;help:no;status:no")

	        if (rRetVal != '') {
	            alert('Diary has not been saved; Please add it manually and notify IT Dept');
	        }
	}
    //////////////  //////////   /////
	
	function Cancel(){
			mmRunApp("../startup/EmptyPage.asp", window.self)
		}

	function Off_Adj_Populate(){		
		if(Company==""){
			return(true);
		}else{			
			frGeneral.document.frmGeneral.cboCompany.value=Company;	
			OfficeListOfCompanyPopulate(Company);
			AdjustersPopulate();

			return(true);
		}		
	}

	function MyOfficeName(Company_pass){		
		if (MyTitle !="claim"){
			alert ("To change the Company correctly, please, press button 'General' to open screen 'General Information'")
			frGeneral.document.frmGeneral.cboCompany.value=Company
			return(true)
		}		
		OfficeListOfCompanyPopulate(Company_pass);
	}

	function MyLineBus(b_pass){	
		var lUnit=Unit;

		//if (typeof (frGeneral.document.frmGeneral.cboOffice) == "undefined") {
		//    alert('Please select Company and Control Branch')
		//    return (false);
		//}


		if (MyTitle !="claim"){
			alert ("To change the Line of Business correctly, please, press button 'General' to open screen 'General Information'")
			frGeneral.document.frmGeneral.cbolLineBus.value=Unit;
			return(true);
		}
		
		if(parseFloat(IR_appr)!=0 || parseFloat(ER_appr)!=0 || parseFloat(MR_appr)!=0){
			alert("Line of Business can't be changed until reserves pending approval will be approved.")
			frGeneral.document.frmGeneral.cbolLineBus.value=Unit;
			return(true);
		}
		
//		if(ROLEUPNUM=='1270' && MainFlag=='Edit'){
//			if(ROLEUPNUM_1270_VERIFY()==false){
//				alert('You have attempted to change the Line of Business field which is locked.\nPlease contact Client Services to modify this field.')
//				frGeneral.document.frmGeneral.cbolLineBus.value=Unit;
//				return(true);
//			}
//		}
		//for "WC" Line of business - Medical reserves should be zero out as soon as "WC" as changed to something else
		if(Unit=="WC" && frGeneral.document.frmGeneral.cbolLineBus.value!="WC"){
			try{
				if(frInfo.document.frmClaim.txtMedical.value!=0){
					alert('You have attempted to change the Line of Business from WC to ' + frGeneral.document.frmGeneral.cbolLineBus.value + '.\nIn order to do that please enter "0" in the Medical Reserve field.')
					frGeneral.document.frmGeneral.cbolLineBus.value=Unit;
					frInfo.document.frmClaim.txtMedical.focus();
					return(true);
				}
			}catch(e){
			}
		} 

		Unit=b_pass.substring(0, 2)
		if (LineOfBus_Cov(Unit) == false) {
		    frGeneral.document.frmGeneral.cbolLineBus.value = lUnit;
		    return (true);
		};
		AdjustersPopulate();

		//re-load the LOB-specific lists:
        CustomCause_List_XTRCT(Unit, '', '')

        if (SCREEN == "AIGRM_Rental" || SCREEN == "AIGRM") {
            ClmOriginal.Aigrm_AccDesc_List.refresh(Unit, 0, 0, true);
        }
		
		//refresh Claim General screen to add/remove Medical reserves
		if(lUnit=="WC" || frGeneral.document.frmGeneral.cbolLineBus.value=="WC"){
			Var_Save("claim");
			Variable_Populate("claim");	
			Display("claim");
		}

		//text of the row Extra options depends on fiollowing values: HO, CO, IM, OM, PL
		if (lUnit == "PL" || frGeneral.document.frmGeneral.cbolLineBus.value == 'PL' || lUnit == "HO" || frGeneral.document.frmGeneral.cbolLineBus.value == 'HO' || lUnit == "CO" || frGeneral.document.frmGeneral.cbolLineBus.value == 'CO' || lUnit == "CR" || frGeneral.document.frmGeneral.cbolLineBus.value == 'CR' || lUnit == "IM" || frGeneral.document.frmGeneral.cbolLineBus.value == 'IM' || lUnit == "OM" || frGeneral.document.frmGeneral.cbolLineBus.value == 'OM') {
			try{
				frInfo.document.getElementById('ExtrOptns').innerHTML="";
				var str=''
				if(MainFlag!='Edit'){
					str=Populate_claim_0();
				}else{
					str=Populate_claim_1();
				}				
				frInfo.document.getElementById('ExtrOptns').innerHTML=str;

			}catch(e){}
		}       
}

//LINE OF BUSINESS -- COVCODE:  PROCEDURE
function LineOfBus_Cov_Populate(){
	if (frGeneral.document.frmGeneral.cbolLineBus.value==""){
		return true;
	} else{
		LineOfBus_Cov(frGeneral.document.frmGeneral.cbolLineBus.value)
	}
}

function LineOfBus_Cov(LOB){
    //PROPERTY SCREEN - 'HO', 'CO', 'IM', 'OM  // 'WC'
	Check_Reserve_MinorCodes_Flag(LOB);
	//*******************	
	var Cov_Code; var Cov_Desc; var Loss_Type; var MyParty; var Cov_Value='';
	var counter=0;	
	frInfo.document.frmClaim.cboCov.options.length=0;
	var re=eval("/" + LOB + "\\^(\\d+)__([-\\w.,()&#\\/ ]+)\\^([Y|N])\\^(\\w*)\\^/gi");
	var foundArr=re.exec(Cov);
	var oOption=frInfo.document.createElement("OPTION");
	oOption.text="";
	oOption.value="";
	frInfo.document.frmClaim.cboCov.add(oOption);

	if (LOB == 'GL' && foundArr == null) { return(false)};   // PolicyCC is not for selected Line of business 
		
	while(foundArr !=null){
		Cov_Code=RegExp.$1; Cov_Desc=RegExp.$2; Loss_Type=RegExp.$3; MyParty=RegExp.$4
		counter++ 
		
		oOption=frInfo.document.createElement("OPTION");
		oOption.text=Cov_Code + '__' + Cov_Desc;
		oOption.value=Loss_Type + '~' + MyParty; //value of the cboCov - [Type of Loss]~[Party]; [Type of Loss]:Y-'BI'; N-'PD';[Party]: 3rd;1st; empty string
		frInfo.document.frmClaim.cboCov.add(oOption);	
		if(CovCod==(Cov_Code + '__' + Cov_Desc)){
			oOption.selected=true;	
			Cov_Value=oOption.value
		}			
		foundArr=re.exec(Cov)
	}		
	//BR_: EDIT COVERAGE LIST
	switch(ClientNumber){
		case "2094":
			BR_EditLists(ClientNumber, 'frInfo.document.frmClaim.cboCov', '/^10_|^11_|^12_|^13_|^14_|^15_|^18_|^20_|^110_|^111_|^112_|^113_|^114_|^115_|^118_|^120_/i', 'text')
			break;
		case "2266":
			BR_EditLists(ClientNumber, 'frInfo.document.frmClaim.cboCov', '/^10_|^11_|^12_|^13_|^14_|^15_|^18_|^19_|^20_|^22_|^110_|^111_|^112_|^113_|^114_|^115_|^118_|^119_|^120_/i', 'text')
			break;
	}
	//BR_EditLists(ClientNumber, 'frInfo.document.frmClaim.cboCov', '/^10_|^11_|^12_|^13_|^14_|^15_|^18_|^20_|^110_|^111_|^112_|^113_|^114_|^115_|^118_|^120_/i', 'text')
	if(counter==0){   //IF LINE OF BUSINESS HAS NO COVERAGE LIST, then - default 'GL'
		LineOfBus_Cov('GL');		
	}
	
	MyTypeOfLoss('',Cov_Value);
}

	function MyTypeOfLoss(caller, pass){
	    var Loss_Type = ''; var TYPE_Loss_prev = TYPE_Loss;
	    var MyParty = ''; 

		var re=/^(\d*)_/gi
		var IndxSelected=frInfo.document.frmClaim.cboCov.options.selectedIndex;
		var CovText = frInfo.document.frmClaim.cboCov.options[IndxSelected].text;
		var _CovCod = '';
		if (CovText.length > 0) {
		    re.exec(CovText);
		    _CovCod = RegExp.$1;
		}
    
        //validate loss type change
        if (Unit == 'WC' && MainFlag == 'Edit') {
            if (Validate_WC_LossType_Change(CovText, CovCod) == false) {
                for(var i=0; i<frInfo.document.frmClaim.cboCov.length; i++){
                    if (frInfo.document.frmClaim.cboCov.options[i].text == CovCod) {
                        frInfo.document.frmClaim.cboCov.options[i].selected = true;
                        return;
                    }
                }
            }
        }

        MyParty = pass.substr(2);	
		Loss_Type=pass.substr(0,1);

		Loss_Type = TYPE_Loss_Set(Loss_Type, CovText);  //
        
		var cI = I
		Index(Unit); //Index(MyPolTyp);
		if(cI!=I){
			frIndex.document.open();
			frIndex.document.write(I);
			frIndex.document.close();
		}
		
		//CLAIM PARTY:
		if(MyParty==''){			
			try{
			    // frGeneral.document.getElementById('ClamParty').innerHTML = "<select name='ClmParty' onchange='parent.frInfo.document.getElementById(\"TPD\").innerHTML=(this.value==\"3rd\" || parent._CovCod == 20 || parent._CovCod == 120 || parent._CovCod ==3 || parent._CovCod ==14  || parent._CovCod ==114) ? parent.Populate_ThirdPartyDeductible_Span() : \"\";'><option value=''>Party?</option><option value='1st'>1st party</option><option value='3rd'>3rd party</option></select>"
			    frGeneral.document.getElementById('ClamParty').innerHTML = "<select name='ClmParty' onchange='parent.Party_Change(this.value, parent._CovCod);'><option value=''>Party?</option><option value='1st'>1st party</option><option value='3rd'>3rd party</option></select>"
				frGeneral.document.frmGeneral.ClmParty.value=trim(ClmParty);
			}catch(e){}
		}else{//specific claim party of this LOB:
			try{
			    frGeneral.document.getElementById('ClamParty').innerHTML = "<B><input value='" + MyParty + " party' name='ClmParty' type='text' style='width:65px' class=textReadOnly></B>"

			}catch(e){}
		}

	    Party_Change(MyParty, _CovCod);

	    if (Unit == 'AL' || Unit == 'PA' || Unit == 'WC' || Unit == 'GL' || Unit == 'LL' || Unit == 'PL') {
	        Check_PIPcode(_CovCod, MyParty)	 //AUTO Coverage - PIP  VERIFICATION  // for WorkComp - Limit  is claim -specific excess level
	    } else {
	        try {
	            ClmOriginal.DynamicElements.hideScreen('PIP');
	        } catch (e) { alert(e.message); }
        }

		if(IndxSelected>0){
			frInfo.document.frmClaim.cboCov.options[IndxSelected].selected=true;
					
			if(Polstruct=='A'){	//Polstruct "A": AdminPolicy
				Populate_PolCov_Data()
			}
		}

		if (caller == 'cboCov') {
		    CustomCause_List_XTRCT("", _CovCod, "");
		}

		if (TYPE_Loss_prev != TYPE_Loss) {
		    try {
		        frInfo.document.getElementById("InjuList").innerHTML = '<select name="cboINJTYP">' + ((TYPE_Loss == 'PD') ? DamageTypeList : InjTypeList) + '</select>';
		        frInfo.document.getElementById("Inju").innerHTML = (TYPE_Loss == 'PD') ? "Damage Type:" : "Injury Type:";
		    } catch (e) { };

		    try { frInfo.document.frmClaim.cboINJTYP.value = InjTyp; } catch (e) { };

		    try {
		        frInfo.document.getElementById("PobCodList").innerHTML = '<select name="cboPOBCOD">' + ((TYPE_Loss == 'PD') ? PobCodPDList : PobCodBIList) + '</select>';
		        frInfo.document.getElementById("PobCod").innerHTML = (TYPE_Loss == 'PD') ? "Peril Type:" : "Body part:";
		    } catch (e) { };

		    try { frInfo.document.frmClaim.cboPOBCOD.value = PobCod; } catch (e) { };
		}
	}

	function TYPE_Loss_Set(BI, CovText) {
	    var TYPE_Loss_current = TYPE_Loss;
	    switch (BI) {
	        case 'Y': TYPE_Loss = 'BI'; break;
	        case 'N': TYPE_Loss = 'PD'; break;
	    }

	    //RowBI:
	    if (TYPE_Loss_current != TYPE_Loss) {
	        try {
	            if (frInfo.document.getElementById('RowBI').innerHTML.length > 0) {
	                var r = Populate_claim_BI(CovText);
	                frInfo.document.getElementById('RowBI').innerHTML = r;
	            }
	        } catch (e) { }
	        Index(Unit); //Index(MyPolTyp);
	        
	        frIndex.document.open();
	        frIndex.document.write(I);
	        frIndex.document.close();
	    }
	    return (BI);
    }

    
    function Party_Change(MyParty, _CovCod) {
        // populate third party deductible screen:
        if (MainFlag == 'Edit') {
            if (MyParty == '3rd' || _CovCod == 20 || _CovCod == 120 || _CovCod == 3 || _CovCod == 14 || _CovCod == 114) {
                    frInfo.document.getElementById('TPD').innerHTML = Populate_ThirdPartyDeductible_Span();

                    ClmOriginal.DynamicElements.displayedField('ThirdPartyDeductible', 'TP_Deductible', TP_Deductible);
                    ClmOriginal.DynamicElements.displayedField('ThirdPartyDeductible', 'TP_Deductible_Amount', TP_Deductible_Amount);
            } else {
                    frInfo.document.getElementById('TPD').innerHTML = '';
                    ClmOriginal.DynamicElements.hideScreen('ThirdPartyDeductible');
            }
        }
        // preserve value of the MyParty
        if (frGeneral.document.frmGeneral.iParty.value == '' && MyParty != '') {
            frGeneral.document.frmGeneral.iParty.value = MyParty;
        }
        
        //check exampts for adjuster licensing rules:
        if (frGeneral.document.frmGeneral.iParty.value != MyParty && MyParty != '') {
            //AdjustersPopulate();
            AdjusterListOfCompanyPopulate(false);
            frGeneral.document.frmGeneral.iParty.value = MyParty;
        }
    }

function BranchTitle_Get(){
	try{
		if(frGeneral.document.frmGeneral.cboOffice.value!=Branch){            
            var re = eval("/" + Branch_Original + "/gi");
			if(re.test(BranchList)==true || UserBranch=='113' || UserBranch==Branch){
				Branch=frGeneral.document.frmGeneral.cboOffice.value;
			}else{	
				alert('You are not authorized to change Control Branch.')
				frGeneral.document.frmGeneral.cboOffice.value=Branch; //only users from Control Branch or Client Services  may change Control Branch
			}
		}
		var BrhIx=frGeneral.document.frmGeneral.cboOffice.selectedIndex
		var BrahchTitle=frGeneral.document.frmGeneral.cboOffice[BrhIx].title
		frGeneral.document.getElementById("ContBrh").title=BrahchTitle
	}catch(e){	
	}
}

function AsBrh_Get(){
	try{
		var BrhIx=frGeneral.document.frmGeneral.cboOffice.selectedIndex
		var BrahchTitle=frGeneral.document.frmGeneral.cboOffice[BrhIx].title
		frGeneral.document.getElementById("ContBrh").title=BrahchTitle 
	}catch(e){	
	}
}

//	PROD MANAGER && HOME OFFICE DIARY && Field Executives
	function ROLEUPNUM_1270_VERIFY(){
	    return (true);
	}
//Loss Address get:
function LossAddressByZip(){
    if(ValZip(frInfo.document.frmClaim.LossZip_incr.value, State)==false){
		alert("US Zip code should be entered in the format ##### or #####-####");
		frInfo.document.frmClaim.LossZip_incr.value="";
		frInfo.document.frmClaim.LossZip_incr.focus();
	}
	var MyZip=frInfo.document.frmClaim.LossZip_incr.value.slice(0,5);
	var re=/\d{5}/gi
	if(re.test(MyZip)){
		if(frGeneral.document.frmGeneral.cboLossState.value==''){
			alert('Please specify Loss State')
		}else{
			if(frInfo.document.all['cboCounty'].options.length<3){
				CountyList_Get("GET THE LIST");
			}
			var b = LossAddr_ByZip_XTRCT(MyZip)
			if(b!=''){
				s=b.split("^");			
				if(s[2]!=frGeneral.document.frmGeneral.cboLossState.value){
					alert('Please check your information:\nEntered zip is in ' + s[2] +  '\nLoss State:' + frGeneral.document.frmGeneral.cboLossState.value);
				}else{
					frInfo.document.all['txtLossCity'].value=s[0];	
					frInfo.document.all['cboCounty'].value=s[1];
				}
			}
		}
	}
}
//RESERVES
function DisplayReserve(pass){
	try{
		if(pass==''){return(0)}
	}catch(e){
		return(0);	
	}finally{
		pass='' + pass
	}
		
	var re				
	re=/,/g
	pass=pass.replace(re,"")
		
	re=/\$/g
	pass=pass.replace(re,"")

	pass = parseFloat(pass)
	pass = format(pass, 2)
	return(pass)
}

function MyCatCode(pass){Corp='pass';}

function RadioBoxValue(name_pass){
	if(Lien=='N'){Lien='Y'}else{Lien='N'}
}

//PROCEDURE TO CLOSE FILE:
//************************
		function CloseFileRequest(){
			var entry=confirm("CLAIM " + ClaimNumber + " IS ABOUT TO BE CLOSED.\nIT WILL ZERO OUT ALL RESERVES AND REMOVE ALL HOT LIST ITEMS FOR THIS CLAIM.\nWOULD YOU LIKE TO PROCEED?")
				if(entry){
					NewWnd=window.open("../CloseFile/CloseFileProc.asp?ClmNo=" + ClaimNumber + "&User=" + User + "",'CloseFile','status,height=901,width=901,top=9000,left=9000');
					return(true);
				}else{
					return(false);
				}			
		}
		
		function CloseFileResponse(Er){
			if(Er==''){
				IR_Disp=0
				ER_Disp=0
				MR_Disp=0
				Status='C'
				if(MyTitle=='claim'){
					if(IR!='' && IR!=0){try{frInfo.document.frmClaim.txtIndemnity.value=0}catch(e){}}
					if(MR!='' && MR!=0){try{frInfo.document.frmClaim.txtMedical.value=0}catch(e){}}
					if(ER!='' && ER!=0){try{frInfo.document.frmClaim.txtExpense.value=0}catch(e){}}	
				}
				alert("Claim has been successfully closed")
				//RUN SUBMIT:
				mysubmit();
			}else{				
				IR_Disp=DisplayReserve(IR)	
				ER_Disp=DisplayReserve(ER)	
				MR_Disp=DisplayReserve(MR)			
				if(MyTitle=='claim'){
					if(IR!='' && IR!=0){frInfo.document.frmClaim.txtIndemnity.value=commafy(DisplayReserve(IR))}
					if(MR!='' && MR!=0){frInfo.document.frmClaim.txtMedical.value=commafy(DisplayReserve(MR))}
					if(ER!='' && ER!=0){frInfo.document.frmClaim.txtExpense.value=commafy(DisplayReserve(ER))}			 
				}
				alert(Er); return(false);
			}			
		}
//***********************
//PROCEDURE TO SAVE OPEN CLAIM DIARY:
		function OpenFileDiary(pass, status) {
			var entry=confirm(pass);
				if(entry){
				    SetDiary = null;
				    SetDiary = new objOpenFileDiary(pass, status);
				    return (true);
				} else {
                    return(false);
				}
		}

//verification of status changing (procesing on submit)
function SubmitStatusChanging(){
	//VERIFICATION OF THE `INCIDENT` STATUS
		if(MainFlag=="NewClaim"){
			if((IR_Disp==0 || IR_Disp=='') && (MR_Disp==0 || MR_Disp=='')){
				//WorkComp
				if(Unit=='WC' || frGeneral.document.frmGeneral.cbolLineBus.value=='WC'){
					if(frGeneral.document.frmGeneral.cboAdjuster.value!=0 && frGeneral.document.frmGeneral.cboAdjuster.value!=''){
						if(ER_Disp > 0){
							alert("To set up claim as In-Progress file the Expense Reserve must be zero.")
							return (false);
						}else{
							alert("This claim will be set up as In-Progress 'P' claim.\nDiary will be created for the adjuster on this file.")
							Status="P"
							frGeneral.document.frmGeneral.txtStatus.value="P"
						}
					}else{
						var resp=confirm("This claim will be set up as Incident only.\nNo diary will be created for this incident.")
						if(resp){
							// expense reserve of `INCIDENT` claims must be `0`
							if(ER_Disp > 0){
								alert("To set up claim as Incident the Expense Reserve must be zero.")
								return (false);
							}
							Status="I"
							frGeneral.document.frmGeneral.txtStatus.value="I"
								return (true);						
						}else{
							alert("To open a claim Indemnity (Medical) Reserve must be at least $1")	
							return (false);
						}
					}					
				}else{
					//Non-WorkComp
					var resp=confirm("This claim will be set up as Incident only.\nNo diary will be created for this incident.")
					if(resp){
						// expense reserve of `INCIDENT` claims must be `0`
						if(ER_Disp > 0){
							alert("To set up claim as Incident the Expense Reserve must be zero.")
							return (false);
						}
						Status="I"
						frGeneral.document.frmGeneral.txtStatus.value="I"
							return (true);						
					}else{
						alert("To open a claim Indemnity (Medical) Reserve must be at least $1")	
						return (false);
					}
				}
			}else{
				//new claim is opened and IR or MR greater then zero
				return(true)
			}
		}

	//!!!!!!!!!!!
	//EDITING!!!!
	// expense reserve of `INCIDENT` claims must be `0`
	if(Status=="I"){
		if((IR_Disp==0 || IR_Disp=='') && (MR_Disp==0 || MR_Disp=='')){
			if(ER_Disp > 0){
				alert("The file with Status 'I' must have Expense Reserve equal zero.")
				frGeneral.document.frmGeneral.txtNewStatus.value=''; //incident
				return (false);	
			}
		
			//WorkComp
			if(Unit=='WC' || frGeneral.document.frmGeneral.cbolLineBus.value=='WC'){
				if(frGeneral.document.frmGeneral.cboAdjuster.value!=0 && frGeneral.document.frmGeneral.cboAdjuster.value!=''){
					if(OpenFileDiary("Adjuster has been specified, this will set the file as In-Progress",'I')==true){
						Status="P"
						frGeneral.document.frmGeneral.txtStatus.value="P";
						return(true); 
					}else{				
						frGeneral.document.frmGeneral.cboAdjuster.value=0; //Incidents don't have Adjuster and Assist Adjuster
						frGeneral.document.frmGeneral.cboAsAdjuster.value=0;
						frGeneral.document.frmGeneral.txtNewStatus.value=''; //incident
						return(false);
					}	
				}
			}
		}
		
		if(IR_Disp>0){
			if(OpenFileDiary("Loss Reserve is greater then zero, this will open the File.",'I')==true){	
				Status="O"
				frGeneral.document.frmGeneral.txtStatus.value="O";
				return(true); 
			}else{
                ReserveWorksheet_OutputCancel();	
				if(MyTitle=='claim'){
				    frInfo.document.frmClaim.txtIndemnity.value = commafy(DisplayReserve(IR));
				    if (typeof frInfo.document.frmClaim.txtMedical != 'undefined') {
				        frInfo.document.frmClaim.txtMedical.value = commafy(DisplayReserve(MR));
				    }
				}
				//IR_Disp=DisplayReserve(IR);	            
				frGeneral.document.frmGeneral.txtNewStatus.value=''; //incident
				return(false);
			}				
		}
		if(MR_Disp>0){
			if(OpenFileDiary("Medical Reserve is greater then zero, this will open the File.",'I')==true){		
				Status="O"
				frGeneral.document.frmGeneral.txtStatus.value="O"
				return(true); 
			}else{
                ReserveWorksheet_OutputCancel();		
				if(MyTitle=='claim'){
				    frInfo.document.frmClaim.txtIndemnity.value = commafy(DisplayReserve(IR));
					frInfo.document.frmClaim.txtMedical.value=commafy(DisplayReserve(MR));
				}
				//MR_Disp=DisplayReserve(MR)	            
				frGeneral.document.frmGeneral.txtNewStatus.value=''; //incident
				return (false);
			}				
		}
	}
	//change status Pending
	if (Status == "P") {
	    if ((IR_Disp == 0 || IR_Disp == '') && (MR_Disp == 0 || MR_Disp == '')) {
	        if (ER_Disp > 0) {
	            alert("The file with Status 'P' must have Expense Reserve equal zero.")
	            frGeneral.document.frmGeneral.txtNewStatus.value = ''; //incident
	            return (false);
	        }
	    }
	
		if(IR_Disp>0 || MR_Disp>0){
			//if(confirm("Reserve is greater then zero, this will open the File.")){	
		    if (OpenFileDiary("Reserve is greater then zero, this will open the File.", 'P') == true) {	
				Status="O"
				frGeneral.document.frmGeneral.txtStatus.value="O";
				return(true); //return(true) invokes mysubmit() - it will be invoked after
			}else{
                ReserveWorksheet_OutputCancel();		
				if(MyTitle=='claim'){
				    frInfo.document.frmClaim.txtIndemnity.value = commafy(DisplayReserve(IR));
				    if (typeof frInfo.document.frmClaim.txtMedical != 'undefined') {
				        frInfo.document.frmClaim.txtMedical.value = commafy(DisplayReserve(MR));
				    }
				}
				//IR_Disp = DisplayReserve(IR);
				//MR_Disp = DisplayReserve(MR);				
				frGeneral.document.frmGeneral.txtNewStatus.value=''; //incident
				return(false);
			}				
		}else{
			//change from InProgress to Incident
			if(frGeneral.document.frmGeneral.cboAdjuster.value==0 && frGeneral.document.frmGeneral.cboAdjuster.value==''){
				if(confirm("Adjuster on the file is not specified\nThis will change status to I (Incident).\nAll Hot List items will be marked as completed.")){	
					Status="I"
					frGeneral.document.frmGeneral.txtStatus.value="I";
					return(true); //return(true) invokes mysubmit() - it will be invoked after
				}else{
					frGeneral.document.frmGeneral.txtNewStatus.value=''; //incident
					return(false);
				}
			}				
		}
	}

	var IPmnts_info_local = DisplayReserve(IPmnts_info);
	var MPmnts_info_local = DisplayReserve(MPmnts_info);
	var EPmnts_info_local = DisplayReserve(EPmnts_info);
	
    // to handle stranfe exceptions when payments <0:
	IPmnts_info_local = (parseFloat(IPmnts_info_local) < 0) ? 0 : IPmnts_info_local
	MPmnts_info_local = (parseFloat(MPmnts_info_local) < 0) ? 0 : MPmnts_info_local
	EPmnts_info_local = (parseFloat(EPmnts_info_local) < 0) ? 0 : EPmnts_info_local

	//FILE IS CURRENTLY CLOSED
	if (Status == "C") {
	    if ((IR_Disp == 0 || IR_Disp == '') && (MR_Disp == 0 || MR_Disp == '')) {
	        if (ER_Disp > 0) {
	            alert("The file with Status 'C' must have Expense Reserve equal zero.")
	            frGeneral.document.frmGeneral.txtNewStatus.value = ''; 
	            return (false);
	        }
	    }

	    if ((IR_Disp - IPmnts_info_local) > 0 && IR_Disp > 0) {
	        if ((ER_Disp - EPmnts_info_local) < 0) {
	            alert("You are initializing claim reopening but expense payments are not covered by expense reserves.\nPlease adjust expense reserves");
	            frGeneral.document.frmGeneral.txtNewStatus.value = '';
	            return (false);
	        }

	        if ((MR_Disp - MPmnts_info_local) < 0) {
	            alert("You are initializing claim reopening but medical payments are not covered by medical reserves.\nPlease adjust medical reserves");
	            frGeneral.document.frmGeneral.txtNewStatus.value = '';
	            return (false);
	        }
	        
	        
	        //save re-open diary: 
	        if (OpenFileDiary("The File is currently closed;\nYou are initializing claim reopening as the Loss Reserve is now greater than zero.", 'C') == true) {				
				return(true);
			}else{
                ReserveWorksheet_OutputCancel();
				if(MyTitle=='claim'){
				    frInfo.document.frmClaim.txtIndemnity.value = commafy(DisplayReserve(IR));
				    if (typeof frInfo.document.frmClaim.txtMedical != 'undefined') {
				        frInfo.document.frmClaim.txtMedical.value = commafy(DisplayReserve(MR));
				    }
				}
				//IR_Disp=DisplayReserve(IR)
				frGeneral.document.frmGeneral.txtNewStatus.value = '';
				return(false);
			}				
		}

		if ((MR_Disp - MPmnts_info_local) > 0 && MR_Disp > 0) {
		    if ((ER_Disp - EPmnts_info_local) < 0) {
		        alert("You are initializing claim reopening but expense payments are not covered by expense reserves.\nPlease adjust expense reserves");
		        frGeneral.document.frmGeneral.txtNewStatus.value = '';
		        return (false);
		    }

		    if ((IR_Disp - IPmnts_info_local) < 0) {
		        alert("You are initializing claim reopening. Indemnity(loss) reserves have to exceed indemnity(loss) payments.\nPlease adjust indemnity(loss) reserves");
		        frGeneral.document.frmGeneral.txtNewStatus.value = '';
		        return (false);
		    }
		
		    //save re-open diary:
		    if (OpenFileDiary("The File is currently closed;\nYou are initializing claim reopening as the Medical Reserve is now greater than zero.", 'C') == true) {
				return(true);
			}else{
                ReserveWorksheet_OutputCancel();	
				if(MyTitle=='claim'){
				    frInfo.document.frmClaim.txtIndemnity.value = commafy(DisplayReserve(IR));
					frInfo.document.frmClaim.txtMedical.value=commafy(DisplayReserve(MR));
				}
	            //MR_Disp=DisplayReserve(MR)
	            frGeneral.document.frmGeneral.txtNewStatus.value = '';	            
				return (false);
			}				
		}		
	}
	//FILE IS CURRENTLY OPEN
	if (Status == "O") {
		var Ipass=(IR_Disp=="") ? 0 : format(IR_Disp,2)
		var Mpass=(MR_Disp=="") ? 0 : format(MR_Disp,2)
		var Epass=(ER_Disp=="") ? 0 : format(ER_Disp,2)

		if(((parseFloat(Ipass) < parseFloat(IPmnts_info_local)) && parseFloat(Ipass)>0) ||
			((parseFloat(Mpass) < parseFloat(MPmnts_info_local)) && parseFloat(Mpass) > 0) ||
			(parseFloat(Mpass) ==0  && parseFloat(Ipass) ==0) ||
			(parseFloat(Mpass) < parseFloat(MPmnts_info_local) && parseFloat(Ipass) < parseFloat(IPmnts_info_local))) {
		    //(parseFloat(Mpass) <= parseFloat(MPmnts_info_local) && parseFloat(Ipass) <= parseFloat(IPmnts_info_local))
			if(confirm("Reserves are exhausted!\n\n" +
								"Indemnity: Gross Reserves $" + commafy(format(Ipass,2)) + "; Payments $" + IPmnts_info + "\n" +
								"Medical:   Gross Reserves $" + commafy(format(Mpass,2)) + "; Payments $" + MPmnts_info + "\n\n" +
								"Would you like to close the file?")){
								
				if(REC_STATUS==''){//RECOVERY STATUS VERIFICATION					
					alert('To close the file - Recovery Possibilities should be marked.')
					return(false);
				}else{									
					if(CloseFileRequest()==false){	
						return(false);
					}else{
						frGeneral.document.frmGeneral.txtNewStatus.value='C';  
						return(false);		//  SUBMIT will be executed from func. CloseFileResponse
					}
				}
			}else{
				alert("Please balance financials")
				return(false);
			}		
		}

		if (IR_Disp > 0 || MR_Disp > 0 || IR_appr > 0 || MR_appr_new > 0){
			if(parseFloat(Epass) < parseFloat(EPmnts_info_local)){
			    if (ClientNumber != 1688 && ClientNumber != 1689) {    //ClientNumber!=2094 && ClientNumber!=2155 && 
					alert("Expense payments exceed reserves!\n\n" +
									"Gross Reserves $" + commafy(format(Epass,2)) + "; Payments $" + EPmnts_info + "\n" +
									"Please balance financials");
					return(false);
				}
			}
		}
	}
}

function CheckLimit(ResVal, ResType){
  var RetVal = '';  //true;
  if(AutoReserve=='Y' && ResType=='IR'){
	return(RetVal);
  }

  if (Split.client == 'Y' && Split.claim != 'Y') {
      RetVal = "Client settings require claim percentage split release. Reserves can't be changed at this time."
      return (RetVal);
  }

  if (PolicyCC_AllowChangeReserve() == false) {
      RetVal = "You are assigning a new policy to the claim. Reserves can't be changed at this time. "
      //alert("You are assigning a new policy to the claim. Reserves can't be changed at this time. ")
      //RetVal = false;
      return (RetVal);
  }
  
  if (Status == "C") {
      if (CanChangeSuper() == false) {
          if (parseFloat(LIMIT_IND, 10) > 0 || parseFloat(LIMIT_MED, 10) > 0 || parseFloat(LIMIT_MED, 10) > 0) {
              alert("You have no rights to reopen the claim\nAll reserve changes will be saved for approval once you select 'Update'")
              LIMIT_IND = 0;
              LIMIT_MED = 0;
              LIMIT_EXP = 0;
          }
      }
  }
  
  if(isNaN(ResVal)==true){        
//        alert("Incorrect value: " + ResVal);
//        RetVal = false;
        RetVal = "Incorrect reserve value: " + ResVal
  }else{	
	 try{
		switch(ResType){
		    case "IR":
		        if (parseFloat(LIMIT_IND) < parseFloat(ResVal)) {
		            //RetVal=false; 
		            if (MainFlag == "NewClaim") {
		                //alert("The entered loss reserve exceeds your limit. Amount=" + commafy(ResVal) + " ; Limit=" + commafy(LIMIT_IND) + "")
		                RetVal = "The entered loss reserve exceeds your limit. Amount=" + commafy(ResVal) + " ; Limit=" + commafy(LIMIT_IND) + "";
		            } else {
		                IR_appr = parseFloat(ResVal);
		                try { frInfo.document.all['MRC_tobe_appr'].innerHTML = Display_RMC_tobe_approved(''); } catch (e) { }
		                //RetVal = true; //09/27/2007
		                RetVal = '';
		                IR_appr_new = 1
		            }
		        } else {
		            if (IR_appr > 0) {    //&& Reserve_MinorCodes_Flag==0
		                IR_appr = 0;
		                IR_appr_new = 0
		                try { frInfo.document.all['MRC_tobe_appr'].innerHTML = Display_RMC_tobe_approved(''); } catch (e) { };
		            }
		        }
		        break;
			case "ER":
				if(parseFloat(LIMIT_EXP)< parseFloat(ResVal)){
					RetVal=false; 
					if(MainFlag=="NewClaim"){
					    RetVal = "The entered expense reserve exceeds your limit. Amount=" + commafy(ResVal) + " ; Limit=" + commafy(LIMIT_EXP) + ""
						//alert("The entered expense reserve exceeds your limit. Amount=" + commafy(ResVal) + " ; Limit=" + commafy(LIMIT_EXP) + "")
					}else{
						ER_appr=parseFloat(ResVal);
						ER_appr_new++;
						//RetVal=true; //09/27/2007				
						RetVal = "";
						try{frInfo.document.all['MRC_tobe_appr'].innerHTML= Display_RMC_tobe_approved('');}catch(e){}
					}
				}else{
					if(ER_appr>0){
						ER_appr=0;
						ER_appr_new--;
						try{frInfo.document.all['MRC_tobe_appr'].innerHTML= Display_RMC_tobe_approved('');}catch(e){};
					}
				}
				break;				
			case "MR":		
				if(parseFloat(LIMIT_MED)< parseFloat(ResVal)){
					RetVal=false;
					if(MainFlag=="NewClaim"){
					    //alert("The entered medical reserve exceeds your limit. Amount=" + commafy(ResVal) + " ; Limit=" + commafy(LIMIT_MED) + "")
					    RetVal = "The entered medical reserve exceeds your limit. Amount=" + commafy(ResVal) + " ; Limit=" + commafy(LIMIT_MED) + "";
					}else{
						MR_appr=parseFloat(ResVal);
						MR_appr_new++;
						//RetVal=true; //09/27/2007					
						RetVal='';
						try{frInfo.document.all['MRC_tobe_appr'].innerHTML= Display_RMC_tobe_approved('');}catch(e){}	
					}
				}else{
					if(MR_appr>0){
						MR_appr=0;
						MR_appr_new--;
						try{frInfo.document.all['MRC_tobe_appr'].innerHTML= Display_RMC_tobe_approved('');}catch(e){};
					}
				}
				break;				
			default:
			    //alert('Unable to check your Reserve Limits - please contact IT dept.'); RetVal = false;
			    RetVal = 'Unable to check your Reserve Limits - please contact IT dept.'
		}
	 }catch(e){
		//alert('Unable to check your Reserve Limits - please contact IT dept.')
	    RetVal = 'Unable to check your Reserve Limits - please contact IT dept.'
	 }
  }
  return (RetVal);
}

function ClntPolCC_ReserveChangeAllowed() {
    var retval = true;
    if (MainFlag == 'Edit') {
        if (polCCreq === 'Y') {
            if (PolicyCC.hasOwnProperty('id')) {   //indicator that CC policy is assigned!
                if (PolicyCC.Active.charAt(0) != 'Y') {
                    retval = false;
                }
            } else {
                retval = false;
            }

            if (!retval) { alert("Client settings necessitate the claim's link to an active policy.\nReserve change is blocked until this requirement is met.") };
        }
    }
    return (retval);
}

    function MyReserveValidate(type, curRes, passRes, curPmnts, runAlert) {
        var message = '';  // type: 'IR' / 'ER' / 'MR'

        //IF PolicyCC is changed on a claim  - do not allow to change reserves:
        if (!ClntPolCC_ReserveChangeAllowed()) {
//            message = "Client settings necessitate the claim's link to an active policy.\nReserve change is blocked until this requirement is met."
//            if (runAlert) { alert(message) };
            return (message);
        }

        //IF PolicyCC is changed on a claim  - do not allow to change reserves:
        if (PolicyCC_AllowChangeReserve() == false) {
            message = "You are assigning a new policy to the claim. Reserves can't be changed at this time.";
            if (runAlert) { alert(message) };
            return (message);
        }

        var re = /^(\d{1,3},?(\d{3},?)*\d{3}(\.\d{0,2})?|\d{1,3}(\.\d{0,2})?|\.\d{1,2}?)$/
        if (re.test(passRes) == false) {
            message = 'Incorrect entry:' + passRes + '\n Please enter reserve in the following format: ###,###.##'
            if (runAlert) { alert(message) };
            return (message);
        }

        //RESERVE CHANGES FOR LEXINGTON:
        if ((ROLEUPNUM == '1270' || ROLEUPNUM == '1700') && parseFloat(passRes) > 149999.99) {
            if (ROLEUPNUM_1270_VERIFY() == false) {
                message = 'You have attempted to post a reserve in excess of your authority limits for this program.\nPlease contact Client Services to set this reserve.';
                if (runAlert) { alert(message) };
                return (message);
            }
        }

        if (parseFloat(passRes) < parseFloat(decommafy(curPmnts)) && parseFloat(passRes) != 0) {
            message = "Gross reserve can't be less then the corresponding payments: $" + commafy(format(passRes, 2)) + " < $" + curPmnts + "."
            if (runAlert) { alert(message) };
            return (message);
        }

        if (parseFloat(passRes) < 0) {
            message = "The amount of the reserve can not be negative."
            if (runAlert) { alert(message) };
            return (message);
        }
        
        return (message);
    }


    function MyIndReserve(Ipass, validateWithAlerts, checkUserLimits) {
        var m = '';
        var Res = commafy(IR_Disp);

	    //Data validation
	    if (Ipass == '') {
	        Ipass = 0
	    } else {
	        Ipass = parseFloat(decommafy(Ipass))
	        Ipass = format(Ipass, 2)
	    }

	    if (validateWithAlerts) {    //(validateWithAlerts !== null) {
	        var IPmnts_info_local = DisplayReserve(IPmnts_info)
	        m = MyReserveValidate('IR', Res, Ipass, IPmnts_info_local, validateWithAlerts);
	    }
    	

	    if (m != '') {
	        try { frInfo.document.frmClaim.txtIndemnity.focus(); } catch (e) { }
	    } else {
	        if (checkUserLimits) {
	            if (format(parseFloat(IR_Disp), 2) != format(parseFloat(Ipass), 2)) {
	                message = CheckLimit(Ipass, 'IR');
	                if (message != '') {
	                    if (validateWithAlerts) { alert(message) };
	                    return (Res);
	                }
	            }
	        }
            IR_Disp = format(parseFloat(Ipass), 2);
            Res = commafy(IR_Disp);
	    }
		return(Res)	
	}

	function MyMedReserve(Mpass, validateWithAlerts, checkUserLimits) {
	    var m = '';
	    var Res = commafy(MR_Disp);
	   
		//Data validation
		if (Mpass == '') {
		    Mpass = 0
		} else {
		    Mpass = parseFloat(decommafy(Mpass))
		    Mpass = format(Mpass, 2)
		}
	
		if (validateWithAlerts !== null) {
		    var MPmnts_info_local = DisplayReserve(MPmnts_info);
		    m = MyReserveValidate('MR', Res, Mpass, MPmnts_info_local, validateWithAlerts);
		}

		if (m != '') {
		    try { frInfo.document.frmClaim.txtMedical.focus(); } catch (e) { }
		} else {
		    if (checkUserLimits) {
		        if (format(parseFloat(MR_Disp), 2) != format(parseFloat(Mpass), 2)) {
		            message = CheckLimit(Mpass, 'MR');
		            if (message != '') {
		                if (validateWithAlerts) { alert(message) };
		                return (Res);
		            }
		        }
		    }		    
		    MR_Disp = format(parseFloat(Mpass), 2);
		    Res = commafy(MR_Disp);
		}

		return(Res)	
	}

	function MyExpReserve(Epass, validateWithAlerts, checkUserLimits) {
	    var Res = commafy(ER_Disp);
        var m = '';

	    //Data validation
	    if (Epass == '') {
	        Epass = 0
	    } else {
	        Epass = parseFloat(decommafy(Epass))
	        Epass = format(Epass, 2)
	    }

	    if (validateWithAlerts !== null) {
	        var EPmnts_info_local = DisplayReserve(EPmnts_info);
	        m = MyReserveValidate('ER', Res, Epass, EPmnts_info_local, validateWithAlerts);
	    }

	    if (m != '') {
	        try { frInfo.document.frmClaim.txtExpense.focus(); } catch (e) { }
	    } else {
	        if (checkUserLimits) {
	            if (format(parseFloat(ER_Disp), 2) != format(parseFloat(Epass), 2)) {
	                message = CheckLimit(Epass, 'ER');
	                if (message != '') {
	                    if (validateWithAlerts) { alert(message) };
	                    return (Res);
	                }
	            }
	        }	    
	        ER_Disp = format(parseFloat(Epass), 2);
	        Res = commafy(ER_Disp);
	    }

	    return (Res)	
	}	


	function MyStatus(Spass, PolTyp){
	   var MPmnts_info_local=DisplayReserve(MPmnts_info)
	   var IPmnts_info_local=DisplayReserve(IPmnts_info)	
	   
		if (MainFlag=="NewClaim"){ 
			alert ("To open the File Loss Reserve or Medical Reserve must be at least $1.")
			return (Status);
		}
		
		if(Status=="I"){
			alert ("To open the File Loss Reserve or Medical Reserve must be at least $1.")
			return (Status);
		}
		
		if(Status=="C"){
			var IA=IPmnts_info_local + 1
			var MA=MPmnts_info_local + 1
			
	    	if(PolTyp=='WC'){
				alert ("To reopen the File Loss Reserve must be at least " + IA + " dol. or Medical Reserve must be at least " + MA + " dol.")
			}else{
				alert ("To reopen the File Loss Reserve must be at least " + IA + " dol.")
			}			
			return (Status);
		}
		
		if(Status=="O"){
			if(MR_Disp>0 && IR_Disp>0){
				if(PolTyp=='WC'){
					alert ("To Close the File  Loss and Medical Reserves must be equal zero.")
				}else{
					alert ("To Close the File  Loss Reserve must be equal zero.")
				}			
				return (Status);
			}
			
			if(IR_Disp>0){
				alert ("To Close the File  Indemnity Reserve must be equal zero.")				
				return (Status);			
			}
			
			if(MR_Disp>0){
				alert ("To Close the File Medical Reserve must be equal zero.")				
				return (Status);			
			}				
		}		
	}


	function CheckNewStatus() {
		var MPmnts_info_local=(MPmnts_info=="") ? 0 : DisplayReserve(MPmnts_info)
		var IPmnts_info_local=(IPmnts_info=="") ? 0 : DisplayReserve(IPmnts_info)

		var Ipass=(IR_Disp=="") ? 0 : IR_Disp
		var Mpass=(MR_Disp=="") ? 0 : MR_Disp
		
		if (MainFlag=="NewClaim"){ 
			//OPENING:
			if(Ipass==0 && Mpass==0){
				if(Unit=='WC' || frGeneral.document.frmGeneral.cbolLineBus.value=='WC'){
					if(frGeneral.document.frmGeneral.cboAdjuster.value!=''){
						frGeneral.document.frmGeneral.txtNewStatus.value='P'; //In-Progress
					}else{
						frGeneral.document.frmGeneral.txtNewStatus.value='I'; //incident
					}
				}else{
					frGeneral.document.frmGeneral.txtNewStatus.value='I'; //incident
				}
			}else{
				frGeneral.document.frmGeneral.txtNewStatus.value='O';
			}		
		}else{	
			//EDITING:
			if(Unit=='WC' || frGeneral.document.frmGeneral.cbolLineBus.value=='WC'){
				if(Status=="I"){					
					if(frGeneral.document.frmGeneral.cboAdjuster.value!=0 && frGeneral.document.frmGeneral.cboAdjuster.value!=''){
						if((Ipass - IPmnts_info_local)>0 || (Mpass - MPmnts_info_local)>0){
							frGeneral.document.frmGeneral.txtNewStatus.value='O';
						}else{
							frGeneral.document.frmGeneral.txtNewStatus.value='P'; //In-Progress
						}
					}else{
						if((Ipass - IPmnts_info_local)>0 || (Mpass - MPmnts_info_local)>0){
							alert("You have entered reserves. This will open this claim.\nPlease specify an adjuster.")
							// I will perform final validation in  Validate_general()
						}
					}
				}
				
				if(Status=="C"){
					if((Ipass - IPmnts_info_local)>0 || (Mpass - MPmnts_info_local)>0){
						frGeneral.document.frmGeneral.txtNewStatus.value='O';
					}
				}
				
				if(Status=="P"){
					if(frGeneral.document.frmGeneral.cboAdjuster.value!=0 && frGeneral.document.frmGeneral.cboAdjuster.value!=''){
						if((Ipass - IPmnts_info_local)>0 || (Mpass - MPmnts_info_local)>0){
							frGeneral.document.frmGeneral.txtNewStatus.value='O';
						}
					}else{
						if((Ipass - IPmnts_info_local)>0 || (Mpass - MPmnts_info_local)>0){
							alert("You have entered reserves. This will open this claim.\nPlease specify an adjuster.")
						}else{
							frGeneral.document.frmGeneral.txtNewStatus.value='I';
						}
					}
				}
			}else{
				if(Status=="C" || Status=="I"){
					if((Ipass - IPmnts_info_local)>0 || (Mpass - MPmnts_info_local)>0){
						frGeneral.document.frmGeneral.txtNewStatus.value='O';
					}
				}
			}
			//opened:
			if(Status=="O"){
				if(Ipass==0  && Mpass==0){
					frGeneral.document.frmGeneral.txtNewStatus.value='C';	
				}
			}
		}
	}

	function CheckClientsIncurredApprovalLimit() {
	    var retval = true;
	    var m = 'The Incurred exceeds client�s approval limit $' + DisplayReserve(IncurredApprovalLimit) + '.\nClient authorization is required.\n\n';

	    if (IncurredApprovalLimit > 0 && Unit == 'WC') {
	        if( (parseFloat(IR_Disp) + parseFloat(MR_Disp) + parseFloat(ER_Disp))  >  (parseFloat(IR) + parseFloat(MR) + parseFloat(ER)) ){ 
	            if ((parseFloat(IR_Disp) + parseFloat(MR_Disp) + parseFloat(ER_Disp)) > parseFloat(IncurredApprovalLimit)) {
	                if (!CanChangeSuper()) {
	                    retval = false;
	                    m += "Reserve changes will be routed to the approval queue"
	                    alert(m);
	                } else {
	                    m += "[Ok] - proceed with the reserve change\n" + "[Cancel] -  route reserve changes to the approval queue";
	                    if (!confirm(m)) {
	                        retval = false;
	                    }
	                }	            
	            }
	        }
	    }
	    return (retval);
	}

	function CheckReservesAgainstPmnts(){
	    var RetVal = '';	
		
		var IPmnts_info_local=DisplayReserve(IPmnts_info);
		var MPmnts_info_local=DisplayReserve(MPmnts_info);
		var EPmnts_info_local = DisplayReserve(EPmnts_info);

		IR_Disp = format(IR_Disp, 2);
		MR_Disp = format(MR_Disp, 2);
		ER_Disp = format(ER_Disp, 2);
	//Indemnity:
		if(Status=="C"){
			if(IR_Disp!=0){		
				if((parseFloat(IR_Disp)-parseFloat(IPmnts_info_local))<=0){
					alert("To reopen the File - the amount of the Loss Reserve must exceed $ " + IPmnts_info_local + "") 
					try{frInfo.document.frmClaim.txtIndemnity.focus();}catch(e){};
					try{Display('rmc');}catch(e){};
					RetVal='IR';
				}
			}			
		}

		if(Status=="O"){
			if(IR_Disp!=0){	
				if((IR_Disp - IPmnts_info_local)<0){
					alert("The amount of the Gross Loss Reserve must exceed $" + IPmnts_info_local  +  ".\n To close the file the amount of the reserve must be equal to zero.")
					try{frInfo.document.frmClaim.txtIndemnity.focus();}catch(e){}
					try{Display('rmc');}catch(e){};
					RetVal='IR';
				}
			}
		}		
		
	//Medical:		
		if(Status=="C"){			//reserve was increased but not sufficiently
			if(MR_Disp!=0){
				if((parseFloat(MR_Disp) - parseFloat(MPmnts_info_local))<=0){
					alert("To reopen the file - the amount of the Medical Reserve  must exceed $" + MPmnts_info_local + ".")
					try{frInfo.document.frmClaim.txtMedical.focus();}catch(e){}	
					RetVal='MR';
				}
			}
		}
		
		if(Status=="O"){
			if(MR_Disp!=0){
				if((MR_Disp - MPmnts_info_local)<0){					
					alert("The amount of the Gross Medical reserve must exceed $" + MPmnts_info_local +  ".\n To close the file the amount of the reserve must be equal to zero.")
					try{frInfo.document.frmClaim.txtMedical.focus();}catch(e){}	
					RetVal='MR';
				}
			}
		}
	
	//Expense:		
		if(ER_Disp!=0){
			if((parseFloat(ER_Disp) - parseFloat(EPmnts_info_local))<0){				// - TotalBills_info_local
				alert("The amount of the Expense Reserve must exceed $" + EPmnts_info_local +  ".")
				try{frInfo.document.frmClaim.txtExpense.focus();}catch(e){};				
				RetVal='ER';
			}
		}
		
    ///////////////////////
	// MINOR RESERVE CODES:
		var IR_MC = ''; var IR_MC_2 = 0

		if (RSV_MC_NEW.length > 0) {
		    ///////////////////////////////
		    //newly entered minor reserves:
		    if (RSV_MC_NEW[1][1] == 0 && RSV_MC_NEW[1][2] > 0) {
		        alert('Undetermined portion of the indemnity reserve must be zero.')
		        RetVal = 'MC';

		    } else {

		        for (c1 = 1; c1 < RSV_MC_NEW.length; c1++) {
		            IR_MC_2 = parseFloat(IR_MC_2) + parseFloat(RSV_MC_NEW[c1][2]);

		            // if claim is open - minor reserves and payments should be balanced:
		            if (parseFloat(IR_Disp) > 0 || parseFloat(MR_Disp) > 0) {
		                if (parseFloat(RSV_MC_NEW[c1][2]) < parseFloat(RSV_MC_NEW[c1][3]) && RSV_MC_NEW[c1][1] > 0) { //Undetermined portion of the indemnity reserve should not be subject of verification;
		                    alert("Payments exceed reserves!\n\n" +
									"Loss " + RSV_MC_NEW[c1][4] + " reserves: $" + format(RSV_MC_NEW[c1][2], 2) + "; payments: $" + format(RSV_MC_NEW[c1][3], 2) + ";" +
									"\n\nPlease balance financials");

		                    RetVal = 'MC';
		                }
		            }
		        }

		        if (format(parseFloat(IR_MC_2), 2) != format(parseFloat(IR_Disp), 2)) {
		            alert("Total of minor reserves $" + format(IR_MC_2, 2) + " is not equal to Gross Loss reserve $" + format(IR_Disp, 2) + "\n Please balance financials;")

		            RetVal = 'MC';  //minor code
		        }
		    }
		} else if (RSV_MC.length > 1) {
		    /////////////////////////
		    //current minor reserves:
		    if (RSV_MC[1][1] == 0 && RSV_MC[1][2] > 0) {
		        alert('Undetermined portion of the indemnity reserve must be zero.')
		        RetVal = 'MC';
		    } else {
		        for (c1 = 1; c1 < RSV_MC.length; c1++) {
		            IR_MC_2 = parseFloat(IR_MC_2) + parseFloat(RSV_MC[c1][2]);

		            // if claim is open - minor reserves and payments should be balanced:
		            if (parseFloat(IR_Disp) > 0 || parseFloat(MR_Disp) > 0) {
		                if (parseFloat(RSV_MC[c1][2]) < parseFloat(RSV_MC[c1][3]) && RSV_MC[c1][1] > 0) {
		                    alert("Payments exceed reserves!\n\n" +
									        "Loss " + RSV_MC[c1][4] + " reserves: $" + format(RSV_MC[c1][2], 2) + "; payments: $" + format(RSV_MC[c1][3], 2) + ";" +
									        "\n\nPlease balance financials");

		                    RetVal = 'MC';
		                }
		            }
		        }
		    }

		    if (format(parseFloat(IR_MC_2), 2) != format(parseFloat(IR_Disp), 2)) {
		        alert("Total of minor reserves $" + format(IR_MC_2, 2) + " is not equal to Gross Loss reserve $" + format(IR_Disp, 2) + "\n Please balance financials;")

		        RetVal = 'MC';  //minor code
		    }
		}
		 
		return(RetVal);
	}
	
	function LocationLabel(val){
		////claims that have associated files hae to be Full Adjustment files only;
		//if(typeof(Claim_Assigns)!="undefined"){
		//	if(Claim_Assigns.length>0){
		//		if(Claim_Assigns[0].clmno_assist!=ClaimNumber  && val!='F'){
		//			alert("This claim has associated assignments; its type can be 'Full Adjustment' only;");
		//			frGeneral.document.frmGeneral.cboAsgmtType.value=Assignt;
		//			return;
		//		}
		//	}
	    //}
	    if (!ClaimAssignmentValidate(val)) {
	        if (Assignt == '') {
	            frGeneral.document.frmGeneral.cboAsgmtType.selectedIndex = -1;
	        }else{
	            frGeneral.document.frmGeneral.cboAsgmtType.value = Assignt;
	        }
	        return;
	    }

		if (MyTitle !="claim"){
			alert ("To change the Assignment, please, press button 'General' to open screen 'General Information'")
			frGeneral.document.frmGeneral.cboAsgmtType.value=Assignt;			
		}else{		

			try {
			    Assignt = val;	
			}catch(e){}
		}
		return;
	}
//************************* PRINT RESERVE CHANGES FORM ********************

	function DisplayReserveChanges() {
	//function DisplayReserveChanges(pass, StringToAdjNotes) {	
		var pr2='';
		//	pr2=" <table cellPadding=2 cellSpacing=2 style=color:" + NoteColor + " width=100% border=1 frame=below rules=none>"
		//	+"<Caption><B>RESERVE CHANGE SUMMARY" + "\t" + "</B></Caption>"			
		//	+"<col width=15%><col width=23%><col width=22%><col width=20%><col width=20%>"	
		//	+"<tr>"
		//		+"<td></td>"
		//		+"<td><U>" + "\t" + "Previous Gross Reserve" + "\t" + "</U></td>"
		//		if(StringToAdjNotes==0){	
		//		pr2 +="<td><U>" + "\t" + "Previous Net Reserve" + "\t" + "</U></td>"
		//		}
		//		pr2 +="<td><U>" + "\t" + "New Gross Reserve" + "\t" + "</U></td>"
		//		if(StringToAdjNotes==0){	
		//		pr2 +="<td><U>" + "\t" + "New Net Reserve" + "\t" + "</U></td>"
		//		}
		//		if(StringToAdjNotes==1){
		//		pr2 +="<td><U>" + "\t" + "Change" + "\t" + "</U></td>"
		//		}
		//	pr2 +="</tr>"
		//	+"<tr>"				
		//		+"<td><B>- Indemnity:" + "\t" + "</B></td>"
		//		//+"<td><B>____________Indemnity:" + "\t" + "</B></td>"				
		//		+"<td align=middle>" + "\t" +  dollarize(DisplayReserve(IR)) + "\t" + "</td>"
		//		if(StringToAdjNotes==0){
		//		pr2 +="<td align=middle>" + "\t" + dollarize(DisplayReserve(IR_info)) + "\t" + "</td>"
		//		}
		//		pr2 +="<td align=middle>" + "\t" + dollarize(IR_Disp) + "\t" + "</td>"
		//		if(StringToAdjNotes==0){
		//			if(IR_Disp==0){
		//			//str= ln + "$0.00"
		//			pr2 +="<td align=middle>" + "\t" + "$0.00" + "\t" + "</td>"
		//			}else{
		//			pr2 +="<td align=middle>" + "\t" + dollarize(IR_Disp - DisplayReserve(IPmnts_info)) + "\t" + "</td>"	
		//			}
		//		}
		//		if(StringToAdjNotes==1){
		//		pr2 +="<td align=middle>" + "\t" + dollarize((parseFloat(IR_Disp) - parseFloat(IR))) + "\t" + "</td>"
		//		}
		//	pr2 +="</tr>"
		//	pr2 +=pass //minor indemnity codes			
		//	if(DisplayReserve(MPmnts_info)!=0 || DisplayReserve(MR)!=0 || DisplayReserve(MR_Disp)!=0){
		//	pr2 +="<tr>"				
		//		+"<td><B>- Medical:" + "\t" + "</B></td>"				
		//		+"<td align=middle>" + "\t" + dollarize(DisplayReserve(MR)) + "\t" + "</td>"	
		//		if(StringToAdjNotes==0){		
		//		pr2 +="<td align=middle>" + "\t" + dollarize(DisplayReserve(MR_info)) + "\t" + "</td>"
		//		}
		//		pr2 +="<td align=middle>" + "\t" + dollarize(MR_Disp) + "\t" + "</td>"
		//		if(StringToAdjNotes==0){		
		//			if(MR_Disp==0){
		//			pr2 +="<td align=middle>" + "\t" +  "$0.00" + "\t" + "</td>"
		//			}else{
		//			pr2 +="<td align=middle>" + "\t" + dollarize(MR_Disp - DisplayReserve(MPmnts_info)) + "\t" + "</td>"	
		//			}
		//		}
		//		if(StringToAdjNotes==1){
		//		pr2 +="<td align=middle>" + "\t" + dollarize((parseFloat(MR_Disp) - parseFloat(MR))) + "\t" + "</td>"
		//		}
		//	pr2 +="</tr>"
		//	}
		//	pr2 +="<tr>"				
		//		+"<td><B>- Expense:" + "\t" + "</B></td>"				
		//		+"<td align=middle>" + "\t" + dollarize(DisplayReserve(ER)) + "\t" + "</td>"
		//		if(StringToAdjNotes==0){			
		//		pr2 +="<td align=middle>" + "\t" + dollarize(DisplayReserve(ER_info)) + "\t" + "</td>"
		//		}
		//		pr2 +="<td align=middle>" + "\t" + dollarize(ER_Disp) + "\t" + "</td>"			
		//		if(StringToAdjNotes==0){	
		//			if(ER_Disp==0){
		//			pr2 +="<td align=middle>" + "\t" + "$0.00" + "\t" + "</td>"
		//			}else{
		//			pr2 +="<td align=middle>" + "\t" + dollarize(ER_Disp - DisplayReserve(EPmnts_info)) + "\t" + "</td>"	
		//			}
		//		}
		//		if(StringToAdjNotes==1){
		//		pr2 +="<td align=middle>" + "\t" + dollarize((parseFloat(ER_Disp) - parseFloat(ER))) + "\t" + "</td>"
		//		}
		//	pr2 += "</tr>"
        //    if (typeof reserveChangeReason == 'string') {
        //        if(reserveChangeReason != '') {
		//	        pr2 += "<tr>"
		//	             + "<td><B>Reason:</B></td>"
		//	             + "<td colspan=4>" + reserveChangeReason + "</td>"	
		//	        pr2 += "</tr>"   
		//	    }
		//	}
		//	pr2 += "</table>"			

			if (typeof reserveChangeReason == 'string') {
			    if (reserveChangeReason != '') {
			        pr2 = reserveChangeReason;
			    }
			}
			
        return (pr2);
	}

	// generic positive number decimal formatting function
	function format(expr, decplaces){
		if(isNaN(parseInt(expr))){
			return(expr);
		}else{
			try{
				var str="" + Math.round (eval(expr) * Math.pow(10,decplaces))	
				while (str.length <=decplaces){
					str="0" + str
				}
				var decpoint=str.length - decplaces
				return str.substring(0,decpoint) + "." + str.substring(decpoint,str.length);
			}catch(e){
				alert('Invalid value:' + expr)
				return(expr);
			}
		}
	}

	
	// turn incoming expression into a dollar value
	function dollarize(expr){	
		expr=format(expr,2) + ""
		expr=commafy(expr);				
		return("$" + expr);
	}

	function MyAccDate(DT_pass){
		var M_DT=""

		if(DT_pass=="" || DT_pass==0){
			M_DT=""
			return (M_DT)
		}
		M_DT=DT_pass

		//Check the Policy Period	
		var MyAccDT= MyDateFormat(M_DT)
		var MyEffDT
		//RETRODT POLICIES FILE:
		if(RETRODT !=''){
			MyEffDT=RETRODT
		}else{
			MyEffDT=MyDateFormat(frGeneral.document.frmGeneral.txtEffDT.value)
		}
		
		var MyExpDT= MyDateFormat(frGeneral.document.frmGeneral.txtExpDT.value,1)
		
		if (DateComp(MyEffDT, MyAccDT)<0){
			alert("The Date of Loss is not in the Policy Date's interval.")
			frGeneral.document.frmGeneral.txtAccDT.focus();
			return(M_DT);	
		}
		
		if (DateComp(MyAccDT, MyExpDT,1)<0){
			alert("The Date of Loss is not in the Policy Date's interval.")
			frGeneral.document.frmGeneral.txtAccDT.focus();
			return(M_DT);	
		}
		
		return (M_DT);
	}

function TransmitToIntegra(){
	var RetVal='';
	if(SCREEN=="Commerce_2" || SCREEN=="Commerce"){
		if(frInfo.document.frmClaim.txtInsZip.value==''){
			alert('Please enter Insureds Zip code.\nThis is required information for the loss estimation system "Integra"');
			frInfo.document.frmClaim.chkToIntegra.checked=false;
			frGeneral.document.frmGeneral.ToIntegra.value='';
			return;
		}
	}
		
	if(frInfo.document.frmClaim.chkToIntegra.checked){
		frGeneral.document.frmGeneral.ToIntegra.value='NEW';
	}else{
		frGeneral.document.frmGeneral.ToIntegra.value='';
	}
}

function FindCheckDetails(ChkNo,Amount){
	var i=1
	
	if (CD.length==0 || ChkNo==0){
		return(0)
	}
	while (i<CD.length){
		if(CD[i][1]==ChkNo && CD[i][6]==Amount){ //DisplayReserve()
			return(i)
		}
		i++	
	}
	return(0)
}

function DisplaySubjectInsurance(){
	MyTitle="sji"	
	var r=1; //SubjectI['1']={'Art/Antique','Art','N'}
	var q=""
	q +="<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
	+"<Title>sji</Title><Form name='frmClaim'>"
	+"<table id='chkdet' cellPadding=2 cellSpacing=2 border=0 width=450px>"
	+"<Caption vAlign=top><B>SUBJECTS OF INSURANCE</B></Caption>"
	+"<table id='SubjI' cellPadding=2 cellSpacing=2 border=0>"
	while(r < SubjectI.length){
		q +="<tr>"
		+"<td>" + SubjectI[r][0] + "</td>"
		+"<td style='width:10px'><input type='checkbox' name='chkSbjI" + r + "' value='' "
		if(SubjectI[r][1]=="Y"){q +=" checked "}
		q+=" onClick='if(this.checked){parent.SubjectI["+ r +"][1]=\"Y\"}else{parent.SubjectI["+ r +"][1]=\"N\"}'> </td>"
		try{
		q+="<td>" + SubjectI[r+1][0] + "</td>"
		+"<td style='width:10px'><input type='checkbox' name='chkSbjI" + (r+1) + "' value='' "
		if(SubjectI[r+1][1]=="Y"){q +=" checked "}
		q+=" onClick='if(this.checked){parent.SubjectI["+ (r+1) + "][1]=\"Y\"}else{parent.SubjectI["+ (r+1) +"][1]=\"N\"}'> </td>"
		}catch(e){}
		+"</TR>"
	 r=r+2
	}
	q +="</table>"
	+"<BR>"	
	+"<table cellPadding=1 cellSpacing=1 border=0 width=450>"
	+"<tr><td align=middle>"
	+"<input type='button' name='cmdBack' value='BACK'  style='width:60px' class=BottonScreen onclick='parent.MyTitle=\"claim\";parent.Display(\"claim\")'>"
	+"</td></tr>"	
	+"</table>"
	+"</form>"
	
	frInfo.document.open();
	frInfo.document.write(q);
	frInfo.document.close();
}

function DisplayCheckDetails(i, fltr) {
    var payType_ = '';
    var q = ""
    q += "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
      + "<Title>listloc</Title><Form name='frmClaim'>"
      + "<table id='chkdet' cellPadding=2 cellSpacing=2 border=0 width=50%>"
      + "<caption vAlign=top><B>Financial Details:</B></caption>"
      + "<caption vAlign=bottom><A name='Print' href='javascript:parent.PrintScreen(\"chkdet\")'>PRINT SCREEN</A></caption>"
      + "<Col width=90><Col width=140><Col=width=90><Col width=140><Col width=90><Col width=140>"
      + "<tr>"
      + "<td id='Clmnt'>Claimant:</td>"
      + "<td id='Clmnt' colspan=5 style='COLOR:blue'>" + CName + "</td>"
      + "</tr>"
      + "<tr>"
      + "<td id='ChTp'>Type:</td>"
      + "<td id='ChTp' style='COLOR: blue'>"
    switch (CD[i][23].substr(0, 1)) {
        case "1":
            payType_ = "IMPREST via " + CD[i][23].slice(1);
            break;
        case "3":
            payType_ = "RECORD ONLY";
            break;
        case "4":
            payType_ = "CSO/LMS";
            break;
        default:
            "-"
    }
    //switch (CD[i][23]) {
    //    case "1b":
    //        q += " via ACH1"; break;
    //    case "1c":
    //        q += " via debit card"; break;
    //    default:
    //        ""
    //}
    q += payType_
    q += "</td>"
      + "<td id='Chek'>Trans#:</td>"
      + "<td id='Chek' style='COLOR: blue'>" + CD[i][1] + "</td>"
      + "<td id='BillTo'>Bill To:</td>"
      + "<td id='BillTo' style='COLOR: blue'>" + CD[i][2] + "</td>"
      + "</tr>"
      + "<tr>"
      + "<td id='Amnt'>Amount:</td>"
      + "<td id='Amnt' style='COLOR: blue'>" + CD[i][6] + "</td>"
      + "<td id='ChkD'>Trans Date:</td>"
      + "<td id='ChkD' style='COLOR: blue'>" + CD[i][5] + "</td>"
      + "<td id='Clea'>Cleared:</td>"
      + "<td id='Clea' style='COLOR:blue'>" + CD[i][16] + "</td>"
      + "</tr>"
      + "<tr>"
      + "<td id='PTyp'>PayTyp:</td>"
      + "<td id='PTyp' style='COLOR: blue'>" + CD[i][3] + "</td>"
      + "<td id='Deta'>Detail:</td>"
      + "<td id='Deta' style='COLOR: blue'>" + CD[i][4] + "</td>"
      + "<td id='Paee'>TaxID:</td>"
      + "<td id='Paee' style='COLOR: blue'>" + CD[i][14] + "</td>"
      + "</tr>"
      + "<tr>"
      + "<td id='Paee'>Payee:</td>"
      + "<td id='Paee' colspan=5 style='COLOR:blue'>" + CD[i][15] + "</td>"
      + "</tr>"
      + "<tr>"
      + "<td id='Mail'>Mail:</td>"
      + "<td id='Mail' colspan=5 style='COLOR:blue'>" + CD[i][7] + " " + CD[i][8] + " " + CD[i][9] + ", " + CD[i][10] + " " + CD[i][11] + "</td>"
      + "</tr>"
      + "<tr>"
      + "<td id='MFor'>For:</td>"
      + "<td id='MFor' colspan=5 style='COLOR:blue'>" + CD[i][12] + "</td>"
      + "</tr>"
    if (CD[i][23].substr(0, 1) == '1') {
        q += "<tr>"
            + "<td id='MaiD'>Mailed:</td>"
            + "<td id='MaiD' style='COLOR: blue'>" + CD[i][13] + "</td>"
            + "<td id='RDat'>Reim. Date:</td>"
            + "<td id='RDat' style='COLOR: blue'>" + CD[i][17] + "</td>"
            + "<td id='RAmt'>Reim. Amt:</td>"
            + "<td id='RAmt' style='COLOR: blue'>" + CD[i][18] + "</td>"
            + "</tr>"
    }
    q += "<tr>"
      + "<td id='Reri'>Period From:</td>"
      + "<td id='Reri' style='COLOR: blue'>" + CD[i][21] + "</td>"
      + "<td id='Reri'>Period To:</td>"
      + "<td id='Reri' style='COLOR: blue'>" + CD[i][22] + "</td>"
      + "<td id='ReriEscheatLbl' >Escheat date:</td>"
      + "<td id='ReriEscheat' style='color: blue'>" + CD[i][24] + "</td>"
      + "</tr>"
    q += "<tr>"
    if (CD[i][19].length > 0) {
        q += "<td id='void' colspan=3 style='COLOR: red'><B>VOIDED ON " + CD[i][19] + "</B></td>"
    } else {
        q += "<td colspan=3></td>"
    }
    if (CD[i][6] < 0) {
        q += "<td id='void' colspan=2 style='COLOR: red'><B>VOID ADJUSTMENT</B></td>"
    } else {
        q += "<td id='void' colspan=2></td>"
    }
    if (CD[i][23].slice(1).toLowerCase() != 'check' && CD[i][23].substr(0, 1) == '1') {
        q += "<td><span  style='color:blue' onclick='parent.LookupWindow_ExternalTransaction_History(" + CD[i][0] + ",\"" + payType_ + "\"," + CD[i][1] + ")'><u> History::Log </u></span></td>"
    }
    if (CD[i][20] == "503") {   
        q += "<td  colspan=2><span id='MedMarijInfo' style='color:blue;cursor:hand' onclick='parent.MedMarijuana_XTRCT(" + CD[i][0] + ",1)'><u>Med marijuana purchase info</u></span></td>"
    }
    q += "</tr>"
      + "</table><BR>"
      + "<table  cellPadding=1 cellSpacing=1 border=0 width=450>"
      + "<tr>"
      + "<td align=middle>"
      + "<input type='button' name='cmdBack' value='BACK'  style='width:60px' class=BottonScreen "
      + " onclick='parent.BackToFinDetails(\"" + fltr + "\",\"MyChkDetails" + i + "\");'"
      + ">"
    //    // FOR WC ONLY LINK ALLOWING SPLIT:
    if (UserRole.indexOf('EditFile') != -1 && CanChangeSuper()) {
        //if (!isNaN(CD[i][1])) {
        var Split_IsAllowed = Split_IsAllowed_XTRCT(CD[i][0]);

        if (Split_IsAllowed == 'Y') {
            //if (parseFloat(decommafy(CD[i][6].replace('$', '')), 10) > 0) {  // positive records:
            //if (CD[i][19] == '') {  // No Void date               
            q += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    + "<input type='button' name='cmdSplit' value='SPLIT'  style='width:60px' class=BottonScreen "
                    + " onclick='parent.Split_XTRCT(" + CD[i][23].substr(0,1) + "," + CD[i][1] + "," + decommafy(CD[i][6].replace('$', '')) + ",\"" + CD[i][3] + "\","
                    + "" + CD[i][20] + ",\"" + CD[i][4] + "\",\"" + CD[i][5] + "\",\"" + CD[i][21] + "\",\"" + CD[i][22] + "\"," + CD[i][0] + "," + CD[i][2] + ");' >"
            //  }
            //}
        }
        if (Split_IsAllowed == 'C') {
            q += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    + "<input type='button' name='cmdSplitCancel' value='CANCEL SPLIT'  style='width:90px' class=BottonScreen "
                    + " onclick='parent.SplitCancel_XTRCT(" + CD[i][0] + "," + CD[i][2] + ");' >"
        }
        // }
    }
    q += "</td></tr>"
      + " <TR height=30px><TD></TD></TR>"
      + "</table>"
      + "</form>"

    frInfo.document.open();
    frInfo.document.write(q);
    frInfo.document.close();   
}


	function BackToFinDetails(fltr, MyChkDetailsRow){
	//	Display("fin");		
	//	setTimeout('ViewHideFinDetails("P","' + fltr + '");',15)
        ViewHideFinDetails("P", fltr);
		setTimeout('ScrollToTheLine("FinDetails","' + MyChkDetailsRow + '");',180)
	}
	

	function CheckBoxTransfer(){
		if(Locked.indexOf('Edit')>-1 && ClmUserRole!='CLT'){
			if(Transfer==''){
				var entry=confirm("Transferring should only be done to indicate that York is no longer handling the claim.\nFor example: when a file is sent back to the carrier.\nTransferred claims may retain an `Open` status, but will no longer appier on any pending list.\nAre you sure you want to transfer this claim?")
				if(entry){
					Transfer='T'
				}else{
					Transfer=''
					frInfo.document.frmClaim.chkTransfer.checked=false
				}				
			}else{
				Transfer=''
			}
		}				
	}

	function Populate_Severity(pd_rating){
		if(JurState=='TX' || JurState=='CA'){
			if(isNaN(pd_rating)==false){
				if(pd_rating==0){
					frInfo.document.frmClaim.cboNCCI_SEV.value= 5;
				}else if(pd_rating>0 && pd_rating<25){
					frInfo.document.frmClaim.cboNCCI_SEV.value= 4;
				}else if(pd_rating>24 && pd_rating<100){
					frInfo.document.frmClaim.cboNCCI_SEV.value= 3;
				}
			}
		}
	}

//UPDATING VARIABLES:	
	function Var_Save(t_pass) {
	    var RetVal = true;

	    Company = frGeneral.document.frmGeneral.cboCompany.value;
	    Branch = frGeneral.document.frmGeneral.cboOffice.value;
	    MyOffice = frGeneral.document.frmGeneral.cboOffice.value;
	    Unit = frGeneral.document.frmGeneral.cbolLineBus.value;
	    MyAdj = frGeneral.document.frmGeneral.cboAdjuster.value;
	    MyAsAdj = frGeneral.document.frmGeneral.cboAsAdjuster.value	
	    State = frGeneral.document.frmGeneral.cboLossState.value;
	    IPolNo = SingleQuote(frGeneral.document.frmGeneral.txtPolNo.value);
	    IEffDT = frGeneral.document.frmGeneral.txtEffDT.value;
	    IExpDT = frGeneral.document.frmGeneral.txtExpDT.value;

	    if (t_pass == "claim") { Var_Save_CLM() }
	    else if (t_pass == "workcomp") { Var_Save_WorkComp(); }
	    else if (t_pass == "workcomp_ca") { Var_Save_workcomp_ca(); }
	    else if (t_pass == "vehicle") { RetVal = Var_Save_Veh(); }
	    else if (t_pass == "claimant") { Var_Save_Claimant(); }
	    else if (t_pass == "law") { Var_Save_Law(); }
	    else if (t_pass == "doc") { RetVal = Var_Save_Doc(); }
	    else if (t_pass == "rec") { Var_Save_Rec(); }
	    else if (t_pass == "iadj") { Var_Save_IAdj(); }
	    else if (t_pass == "prodliab") { Var_Save_prodliab(); }
	    else if (t_pass == "producer") { Var_Save_Pr(); }
	    else if (t_pass == "ysi") { Var_Save_YSI(); }
	    else if (t_pass == "complex") { Var_Save_Complex(); }
	    else if (t_pass == "custom") { RetVal = Var_Save_Custom(); }
	    else if (t_pass == "injury") { Var_Save_injury(); }
	    else if (t_pass == "polInfo") { RetVal = Var_Save_polInfo(); }  // FAJUA
	    else if (t_pass == "polCCinfo") { RetVal = Var_Save_polCCinfo(); }
	    else if (t_pass == "rmc") { RetVal = Var_Save_rmc(); }  
	    else if (t_pass == "lossLoc") { RetVal = Var_Save_lossLoc(frInfo.document.frmClaim); }

	    return (RetVal);
	}

	function Var_Save_ClaimDetails() {
        
	    CltClmNo = SingleQuote(frInfo.document.frmClaim.txtClientsNum.value)
	    ThirdNum = SingleQuote(frInfo.document.frmClaim.txt3PartyNo.value);
	    CarClmNo = SingleQuote(frInfo.document.frmClaim.txtCarrierClmno.value);

	    Desc = trim(SingleQuote(frInfo.document.frmClaim.txtLossDescr.value));
	    Desc2 = trim(SingleQuote(frInfo.document.frmClaim.txtLossDescr2.value));
	    Desc3 = trim(SingleQuote(frInfo.document.frmClaim.txtLossDescr3.value));

	    return;
	}


	function Var_Save_rmc() {
	    var retVal_rmc = true;
        if (MainFlag == 'Edit') {
	        retVal_rmc = GenericCustomFieldsSave();
	    }
	    return (retVal_rmc);
    }

	function Var_Save_Complex(){
		//cmpxAnal=frInfo.document.frmClaim.cboComplexAnalyst.value;
		cmpxReferralDate=frInfo.document.frmClaim.txtReferralDate.value
		cmpxReferralReason=SingleQuote(frInfo.document.frmClaim.cboReferralReason.value);
		cmpxFirstReport=frInfo.document.frmClaim.txtDateOfFirstReport.value;
		cmpxVSONumber=SingleQuote(frInfo.document.frmClaim.txtVSONumber.value);		
		cmpxIssueType=frInfo.document.frmClaim.cboTypeOfIssue.value;
		//cmpxVSResponse=   //// cmpxSLA  
		cmpxVSAnal=SingleQuote(frInfo.document.frmClaim.txtVSAnalyst.value);
		cmpxVSDate=frInfo.document.frmClaim.txtDateVSResponse.value;
		cmpxSLA= SingleQuote(frInfo.document.frmClaim.cboSLAGA.value);		
		cmpxNote=SingleQuote(frInfo.document.frmClaim.txtNote.value);
	}

	function Var_Save_Veh() {
	    var RetVal = true;

	    if (typeof (ClmOriginal.Vehicles) != 'undefined') {
	        if (ClmOriginal.Vehicles.screen == 'Details') {
	            RetVal = ClmOriginal.Vehicles.cancel();
	        }
	    }
	    return (RetVal);
	}	


	function Var_Save_Doc() {
	    var RetVal = true;
	    if (typeof (ClmOriginal.Doctors) != 'undefined') {
	        if (ClmOriginal.Doctors.screen == 'Details') {
	            RetVal = ClmOriginal.Doctors.cancel();
	        }
	    }
	
	    return (RetVal);
	}
	
	
	function Var_Save_Law(){
			 Ltype=frInfo.document.frmClaim.cboLawTyp.value
			 LName = SingleQuote(frInfo.document.frmClaim.txtLawName.value)

			 LLName = SingleQuote(frInfo.document.frmClaim.txtLawLName.value)
			 LFName = SingleQuote(frInfo.document.frmClaim.txtLawFName.value)

			 LstateId = frInfo.document.frmClaim.LawStateId.value;
			 LstateDesc = frInfo.document.frmClaim.txtLawState.value; 
			 LCountryId = frInfo.document.frmClaim.LawCountryId.value;
			 LCountry =frInfo.document.frmClaim.txtCountry.value;

			 Laddr1=SingleQuote(frInfo.document.frmClaim.txtLawAddress.value)
			 Laddr2=SingleQuote(frInfo.document.frmClaim.txtLawAddress2.value)
			 Lcity=SingleQuote(frInfo.document.frmClaim.txtLawCity.value)
			 Lstate=frInfo.document.frmClaim.cboLawState.value
			 Lzip=frInfo.document.frmClaim.txtLawZip.value;
			 Lphone=frInfo.document.frmClaim.txtLawPhone.value;
			 LtaxId=frInfo.document.frmClaim.txtLawTaxID.value;
			 Lfax=frInfo.document.frmClaim.txtLawFax.value;
			 LEMail=SingleQuote(frInfo.document.frmClaim.LEMAIL.value);
			 LContact = SingleQuote(frInfo.document.frmClaim.LCONTACT.value);
			 represent_notice_date = frInfo.document.frmClaim.represent_notice_date.value; 
	}

	function Var_Save_Claimant(){
	    // ReportType=frInfo.document.frmClaim.cboCIDRepTyp.value;
             SSNum = (typeof (frInfo.document.frmClaim.txtClmtSSN.value) == 'undefined') ? SSNum : frInfo.document.frmClaim.txtClmtSSN.value;
			 Caddr1=SingleQuote(frInfo.document.frmClaim.txtClmtAddress.value);
			 Caddr2=SingleQuote(frInfo.document.frmClaim.txtClmtAddress2.value);
			 Ccity=SingleQuote(frInfo.document.frmClaim.txtClmtCity.value);
			 Cstate = frInfo.document.frmClaim.cboClmtState.value;

			 Cstate = frInfo.document.frmClaim.cboClmtState.value;
			 CstateId = frInfo.document.frmClaim.ClmtStateId.value;
			 CstateDesc  = frInfo.document.frmClaim.txtClmtState.value;
			 
			 CCountryId = frInfo.document.frmClaim.ClmtCountryId.value;
			 CCountry = frInfo.document.frmClaim.txtCountry.value;     
			 
			 Czip = frInfo.document.frmClaim.txtClmtZip.value;
			 
			 Cphone=frInfo.document.frmClaim.txtClmtPhone.value;
			 Cworkphone=frInfo.document.frmClaim.txtClmtBusPhone.value;
			 DOB=frInfo.document.frmClaim.txtClmtDOB.value;
			 Sex=frInfo.document.frmClaim.cboClmtSex.value;
			 CEMail=SingleQuote(frInfo.document.frmClaim.CEMAIL.value);
			 CContact=SingleQuote(frInfo.document.frmClaim.CCONTACT.value);
			 CLanguage = SingleQuote(frInfo.document.frmClaim.CLanguage.value);
			 
			 DODEATH = frInfo.document.frmClaim.txtDODEATH.value;
			 CLICENSENO = frInfo.document.frmClaim.CLICENSENO.value;
			 CLICSTATE = frInfo.document.frmClaim.CLICSTATE.value;
			 MaidenName = SingleQuote(frInfo.document.frmClaim.txtMaidenName.value);

			 if (MainFlag == 'Edit') {
			     Auth_SSN_Release = (typeof (frInfo.document.frmClaim.Auth_SSN_Release)=="undefined" ) ? "" : frInfo.document.frmClaim.Auth_SSN_Release.value;
			     HICN = (typeof (frInfo.document.frmClaim.HICN) == "undefined") ? "" : SingleQuote(frInfo.document.frmClaim.HICN.value);
			     if (DOB != '') {
			        Auth_Age_Release = 6
			     } else {
			        Auth_Age_Release = (typeof frInfo.document.frmClaim.Auth_Age_Release == "undefined") ? 0 : frInfo.document.frmClaim.Auth_Age_Release.value;
			     }
			 }
	}

	function Var_Save_WorkComp(){
		try{
			hours_sun=frInfo.document.frmClaim.hours_sun.value; 
			hours_mon=frInfo.document.frmClaim.hours_mon.value;  
			hours_tue=frInfo.document.frmClaim.hours_tue.value;
			hours_wed=frInfo.document.frmClaim.hours_wed.value;
			hours_thu=frInfo.document.frmClaim.hours_thu.value;
			hours_fri=frInfo.document.frmClaim.hours_fri.value;
			hours_sat=frInfo.document.frmClaim.hours_sat.value;
		
			WeeklyHours=frInfo.document.frmClaim.txtWC_Hours.value;
			EmplDutyDescr=frInfo.document.frmClaim.EmplDutyDescr.value;
		
			time_began_work=frInfo.document.frmClaim.time_began_work.value;
			began_am_pm=frInfo.document.frmClaim.began_am_pm.value;
		
			//WeeklyRate=frInfo.document.frmClaim.txtWC_Rate.value;
			DateOfHire=frInfo.document.frmClaim.txtWC_Hire.value;
			DateInitia=frInfo.document.frmClaim.txtWC_D_InitDisab.value;
			DateReport = frInfo.document.frmClaim.txtWC_D_Report.value;
			TerminateJobDate = frInfo.document.frmClaim.txtTerminateJobDate.value;

			Marital=frInfo.document.frmClaim.cboWC_MStatus.value;
			Dependents=frInfo.document.frmClaim.txtWC_DependNo.value;
			WeekWage=frInfo.document.frmClaim.txtWC_WeeklyWage.value;
			JurState=frInfo.document.frmClaim.cboWC_JuriState.value;
			//Occup=frInfo.document.frmClaim.cboWC_Occup.value;
			OccupID = (PolicyCC.hasOwnProperty('id')) ? frInfo.document.frmClaim.cboWC_Occup.value : OccupID;
			JobTitle=Quote(frInfo.document.frmClaim.txtJobTitle.value);
			wage_method=frInfo.document.frmClaim.cboWage_Method.value;
			EmplSTATE=frInfo.document.frmClaim.cboWC_EmplState.value;
			
			
			//InjOnEmplP
			TempTotalRATE=frInfo.document.frmClaim.txtWC_TTRate.value;
			TempPartialRATE=frInfo.document.frmClaim.txtWC_TPRate.value;
			PermTotalRATE=frInfo.document.frmClaim.txtWC_PTRate.value;
			VocRehabRATE = frInfo.document.frmClaim.txtWC_VRRate.value;
			
			StartJobDate=frInfo.document.frmClaim.txtStartJobDate.value;

			LastWorkDay = frInfo.document.frmClaim.txtWC_LastWorkDay.value;
			CurrentLastWorkDay = frInfo.document.frmClaim.txtWC_CurrentLastWorkDay.value;
			
			FirstLostTimeDay=frInfo.document.frmClaim.txtWC_FirstLostTimeDay.value;			
			EmplStatusCod=frInfo.document.frmClaim.cboEmplStatus.value;
			JobCodeCod=frInfo.document.frmClaim.cboWC_JobCode.value;
			//ClassificationCode=frInfo.document.frmClaim.cboWC_Class.value;
			employee_id = SingleQuote(frInfo.document.frmClaim.employee_id.value);
			injury_dept = SingleQuote(frInfo.document.frmClaim.injury_dept.value);
			if (ConcurEMPLMNT == 'Y') {
			    ConcurWAGE = frInfo.document.frmClaim.txtWC_ConcurWAGE.value;
			    InsuredWAGE = frInfo.document.frmClaim.txtWC_InsuredWAGE.value;
			}
		}catch(e){}		
	}

	function Var_Save_injury() {
	    try {	    
	        if (JurState != frInfo.document.frmClaim.cboWC_JuriState.value) {
	            JurState = frInfo.document.frmClaim.cboWC_JuriState.value; InjuryFlag = 1;
	        }
	        if (Corp != frInfo.document.frmClaim.cboCatCode.value) {
	            Corp = frInfo.document.frmClaim.cboCatCode.value; InjuryFlag = 1;
	        }
	        if (Fatal != frInfo.document.frmClaim.cboFatal.value) {
	            Fatal = frInfo.document.frmClaim.cboFatal.value; InjuryFlag = 1;
	        }
	        if (PDRating != frInfo.document.frmClaim.PDRating.value) {
	            PDRating = frInfo.document.frmClaim.PDRating.value; InjuryFlag = 1;
	        }
	        if (BodyPartCod != frInfo.document.frmClaim.cboAnatomy.value) {
	            BodyPartCod = frInfo.document.frmClaim.cboAnatomy.value; InjuryFlag = 1;
	        }
	        if (Injuries != SingleQuote(frInfo.document.frmClaim.txtInjuries.value)) {
	            Injuries = SingleQuote(frInfo.document.frmClaim.txtInjuries.value); InjuryFlag = 1;
	        }
	        if (NCCI_SOI != frInfo.document.frmClaim.cboNCCI_SOI.value) {
	            NCCI_SOI = frInfo.document.frmClaim.cboNCCI_SOI.value; InjuryFlag = 1;
	        }
	        if (NCCI_NOI != frInfo.document.frmClaim.cboNCCI_NOI.value) {
	            NCCI_NOI = frInfo.document.frmClaim.cboNCCI_NOI.value; InjuryFlag = 1;
	        }
	        if (DateReport != frInfo.document.frmClaim.txtWC_D_Report.value) {
	            DateReport = frInfo.document.frmClaim.txtWC_D_Report.value;  InjuryFlag = 1;
	        }
	        if (TerminateJobDate != frInfo.document.frmClaim.txtTerminateJobDate.value) {
	            TerminateJobDate = frInfo.document.frmClaim.txtTerminateJobDate.value; InjuryFlag = 1;
	        }	        
	        if (PreExistDisability != frInfo.document.frmClaim.cboPreExistDisability.value) {
	            PreExistDisability = frInfo.document.frmClaim.cboPreExistDisability.value; InjuryFlag = 1;
	        }	        
	    } catch (e) { }
	}

	function Var_Save_workcomp_ca() {
	    try {
	        if (EmplZip == '' && InjOnEmplP == 'Y') {
	            EmplZip = (typeof (set_loss_zip()) == "undefined") ? "" : set_loss_zip();
	        }
	    } catch (e) { }
	    //----
	    try {		
		    FormProvideDate=frInfo.document.frmClaim.FormProvideDate.value;
		    FormReceiveDate = frInfo.document.frmClaim.FormReceiveDate.value;
		    FormReceiveDateTPA = frInfo.document.frmClaim.FormReceiveDateTPA.value;
		    
		    PDRating =frInfo.document.frmClaim.PDRating.value;
    		
		    NCCI_SOI=frInfo.document.frmClaim.cboNCCI_SOI.value;
		    NCCI_NOI=frInfo.document.frmClaim.cboNCCI_NOI.value;
		    NCCI_SEV=frInfo.document.frmClaim.cboNCCI_SEV.value;
    		
		    EmplZip=frInfo.document.frmClaim.txtEmplZip.value;	

    //claims table:
		   // NCCICod=frInfo.document.frmClaim.cboWC_ClassCode.value;
    //workcomp table:			
		    CLASSCode=frInfo.document.frmClaim.cboWC_ClassCode.value;
    			
		    Minor_LossType=frInfo.document.frmClaim.cboMinor_LossType.value;

		    DateReturn = frInfo.document.frmClaim.txtWC_D_Returned.value;
		    datereturn_current = frInfo.document.frmClaim.txtWC_D_Returned_current.value;
		    
		    DateMaxMedImpr=frInfo.document.frmClaim.txtWC_DateMaxMedImpr.value;	
		    FirstTREAT=frInfo.document.frmClaim.txtWC_D_FTreat.value;
    		
		    PreExistDisability=frInfo.document.frmClaim.cboPreExistDisability.value;
		    AOE=frInfo.document.frmClaim.cboADE_CDE.value;
		    InjOnEmplP=frInfo.document.frmClaim.cboWC_InjOnEmplP.value; 			
		    wc_board_fraud=frInfo.document.frmClaim.cboBoardFraud.value;
		    initialTreatmentType = frInfo.document.frmClaim.cboInitialTreatment.value;
		    
		    LossCovCode = frInfo.document.frmClaim.cboWC_LossCov.value;
		    if (cumulative_trauma == 'Y'){
		        cumulative_trauma_date = frInfo.document.frmClaim.cumulative_trauma_date.value;
		        cumulative_trauma_last_date = frInfo.document.frmClaim.cumulative_trauma_last_date.value;
		    }
			if (RTWSameEmpl =='Y') {
			    return_position = SingleQuote(frInfo.document.frmClaim.return_position.value);
			}
			ReleasedRTW = frInfo.document.frmClaim.txtReleasedRTW.value;

			if (typeof (frInfo.document.getElementById("cboFM_type")) != 'undefined') {
			    future_medical_type = frInfo.document.getElementById("cboFM_type").value;
			} 
		}catch(e){}
	}	
	

	function Var_Save_Rec(){
		SNAME=SingleQuote(frInfo.document.frmClaim.txtSName.value)
		SADDR1=SingleQuote(frInfo.document.frmClaim.txtSAddress1.value)
		SADDR2=SingleQuote(frInfo.document.frmClaim.txtSAddress2.value)
		SCITY=SingleQuote(frInfo.document.frmClaim.txtSCity.value)
		SSTATE=SingleQuote(frInfo.document.frmClaim.cboSState.value)
		SZIP=SingleQuote(frInfo.document.frmClaim.txtSZip.value)
		SPHONE=SingleQuote(frInfo.document.frmClaim.txtSPhone.value)
		SFAX=SingleQuote(frInfo.document.frmClaim.txtSFax.value)
		SEMAIL=SingleQuote(frInfo.document.frmClaim.txtSEmail.value)
		ACCONTACT=SingleQuote(frInfo.document.frmClaim.txtAContact.value)
		ACADDR1=SingleQuote(frInfo.document.frmClaim.txtAAddress1.value)
		ACADDR2=SingleQuote(frInfo.document.frmClaim.txtAAddress2.value)
		ACCITY=SingleQuote(frInfo.document.frmClaim.txtACity.value)
		ACSTATE=SingleQuote(frInfo.document.frmClaim.cboAState.value)
		ACZIP=SingleQuote(frInfo.document.frmClaim.txtAZip.value)
		ACPHONE=SingleQuote(frInfo.document.frmClaim.txtAPhone.value)
		ACFAX=SingleQuote(frInfo.document.frmClaim.txtAFax.value)
		ACEMAIL=SingleQuote(frInfo.document.frmClaim.txtAEmail.value)
		ACCLMNO=SingleQuote(frInfo.document.frmClaim.txtACClmNo.value)
		RECOVPERCENTAGE=frInfo.document.frmClaim.txtRecovPercent.value
		if(RECOVPERCENTAGE==''){RECOVPERCENTAGE=0}
		SETTLEMENT=frInfo.document.frmClaim.txtRSettlement.value
		if(SETTLEMENT==''){SETTLEMENT=0}		
		RAMOUNT=frInfo.document.frmClaim.txtRAmount.value;
		if(RAMOUNT==''){RAMOUNT=0};
		SYORKAMOUNT=frInfo.document.frmClaim.txtSYorkAmount.value ;
		if(SYORKAMOUNT==''){SYORKAMOUNT=0};
		RECRESERVE=frInfo.document.frmClaim.txtRecReserve.value 
		if(RECRESERVE==''){RECRESERVE=0};
		SNOTES=SingleQuote(frInfo.document.frmClaim.txtSNotes.value);
		RADJNO=frInfo.document.frmClaim.cboRAdjNo.value;
		PursuitAgainst=frInfo.document.frmClaim.cboPursuitAgainst.value;  
		SubrogationType=frInfo.document.frmClaim.cboSubrogationType.value;  
		ReferredTo=frInfo.document.frmClaim.cboReferredTo.value;
		ResolvedBy=frInfo.document.frmClaim.cboResolvedBy.value;
		Disposition = frInfo.document.frmClaim.cboDisposition.value;

		StatuteDate = frInfo.document.frmClaim.StatuteDate.value;
		
		SStateID = frInfo.document.frmClaim.SStateID.value;
		SCountryId = frInfo.document.frmClaim.SCountryId.value;
		SCountry = frInfo.document.frmClaim.SCountry.value;
		SStateDesc = frInfo.document.frmClaim.SStateDesc.value;

		ACStateID = frInfo.document.frmClaim.ACStateID.value;
		ACCountryId = frInfo.document.frmClaim.ACCountryId.value;
		ACCountry = frInfo.document.frmClaim.ACCountry.value;
		ACStateDesc = frInfo.document.frmClaim.ACStateDesc.value;
	}

	function Var_Save_IAdj(){			 
			if(IAdjName!=frInfo.document.frmClaim.txtIAdjName.value){
				IAdjName=frInfo.document.frmClaim.txtIAdjName.value;IAdjFlag=1;
			}
			if (IAdjAddr1 != frInfo.document.frmClaim.txtIAdjAddr1.value) {
			    if (frInfo.document.frmClaim.txtIAdjAddr1.value.length > 50) {
			        alert('Ind.Adjuster screen:\n\nThe length of the address (line 1) is ' + frInfo.document.frmClaim.txtIAdjAddr1.value.length + '\nIt exceeds allowed 50 characters;\nPlease adjust the address')
			    }
				IAdjAddr1=frInfo.document.frmClaim.txtIAdjAddr1.value;IAdjFlag=1;
			}
			if (IAdjAddr2 != frInfo.document.frmClaim.txtIAdjAddr2.value) {
			    if (frInfo.document.frmClaim.txtIAdjAddr2.value.length > 50) {
			        alert('Ind.Adjuster screen:\n\nThe length of the address (line 2) is ' + frInfo.document.frmClaim.txtIAdjAddr2.value.length + '\nIt exceeds allowed 50 characters;\nPlease adjust the address')
			    }
				IAdjAddr2=frInfo.document.frmClaim.txtIAdjAddr2.value;IAdjFlag=1;
			}
			if(IAdjCity!=frInfo.document.frmClaim.txtIAdjCity.value){
				IAdjCity=frInfo.document.frmClaim.txtIAdjCity.value;IAdjFlag=1;
			}
			if(IAdjState!=frInfo.document.frmClaim.cboIAdjState.value){
			    IAdjState = frInfo.document.frmClaim.cboIAdjState.value; IAdjFlag = 1;

			    IstateDesc = frInfo.document.frmClaim.txtIAdjState.value;  
			    IAdjstateId = frInfo.document.frmClaim.IAdjStateId.value;
			    IAdjCountryId = frInfo.document.frmClaim.IAdjCountryId.value;
			}
			if(IAdjZip!=frInfo.document.frmClaim.txtIAdjZip.value){
				IAdjZip=frInfo.document.frmClaim.txtIAdjZip.value;IAdjFlag=1;
			}
			if(IAdjTaxID!=frInfo.document.frmClaim.txtIAdjTaxID.value){
				IAdjTaxID=frInfo.document.frmClaim.txtIAdjTaxID.value;IAdjFlag=1;
			}
			if(IAdjEMail!=frInfo.document.frmClaim.txtIAdjEMail.value){
				IAdjEMail=frInfo.document.frmClaim.txtIAdjEMail.value;IAdjFlag=1;
			}
			if(IAdjPhone!=frInfo.document.frmClaim.txtIAdjPhone.value){
				IAdjPhone=frInfo.document.frmClaim.txtIAdjPhone.value;IAdjFlag=1;
			}
			if(IAdjFax!=frInfo.document.frmClaim.txtIAdjFax.value){
				IAdjFax=frInfo.document.frmClaim.txtIAdjFax.value;IAdjFlag=1;
			}			
		}

		function Var_Save_prodliab() {
		    if (plStatus != frInfo.document.frmClaim.plStatus.value) {
		        plStatus = frInfo.document.frmClaim.plStatus.value; prodliabFlag = 1;
		    }
		    if (plMassTort != frInfo.document.frmClaim.plMassTort.value) {
		        plMassTort = frInfo.document.frmClaim.plMassTort.value; prodliabFlag = 1;
		    }
		    if (plManufacturer != frInfo.document.frmClaim.plManufacturer.value) {
		        if (frInfo.document.frmClaim.plManufacturer.value.length > 40) {
		            alert('Product Liability screen:\n\nThe length of the value of the field Manufacturer is ' + frInfo.document.frmClaim.txtIAdjAddr2.value.length + '\nIt exceeds allowed 40 characters;\nPlease adjust it');
		        }
		        plManufacturer = frInfo.document.frmClaim.plManufacturer.value; prodliabFlag = 1;
		    }
		    if (plGenericName != frInfo.document.frmClaim.plGenericName.value) {
		        if (frInfo.document.frmClaim.plGenericName.value.length > 40) {
		            alert('Product Liability screen:\n\nThe length of the value of the field Generic Name is ' + frInfo.document.frmClaim.plGenericName.value.length + '\nIt exceeds allowed 40 characters;\nPlease adjust it');
		        }
		        plGenericName = frInfo.document.frmClaim.plGenericName.value; prodliabFlag = 1;
		    }
		    if (plBrandName != frInfo.document.frmClaim.plBrandName.value) {
		        if (frInfo.document.frmClaim.plBrandName.value.length > 40) {
		            alert('Product Liability screen:\n\nThe length of the value of the field Brand Name is ' + frInfo.document.frmClaim.plBrandName.value.length + '\nIt exceeds allowed 40 characters;\nPlease adjust it');
		        }
		        plBrandName = frInfo.document.frmClaim.plBrandName.value; prodliabFlag = 1;
		    }
		    if (plAllegedHarm != frInfo.document.frmClaim.plAllegedHarm.value) {
		        plAllegedHarm = frInfo.document.frmClaim.plAllegedHarm.value.slice(0,200); prodliabFlag = 1;
		    }
		}


	function Var_Save_Pr(){			 
			if(PrName!=frInfo.document.frmClaim.txtPrName.value){
				PrName=frInfo.document.frmClaim.txtPrName.value;PrFlag=1;
			}
			if(PrAddr1!=frInfo.document.frmClaim.txtPrAddr1.value){		
				PrAddr1=frInfo.document.frmClaim.txtPrAddr1.value;PrFlag=1;
			}
			if(PrAddr2!=frInfo.document.frmClaim.txtPrAddr2.value){
				PrAddr2=frInfo.document.frmClaim.txtPrAddr2.value;PRFlag=1;
			}
			if(PrCity!=frInfo.document.frmClaim.txtPrCity.value){
				PrCity=frInfo.document.frmClaim.txtPrCity.value;PrFlag=1;
			}
			if(PrState!=frInfo.document.frmClaim.cboPrState.value){
			    PrState = frInfo.document.frmClaim.cboPrState.value; PrFlag = 1;

			    PrstateDesc = frInfo.document.frmClaim.PrstateDesc.value;
			    PrstateId = frInfo.document.frmClaim.PrstateId.value;
			    PrCountryId = frInfo.document.frmClaim.PrCountryId.value;
			}
			if(PrZip!=frInfo.document.frmClaim.txtPrZip.value){
				PrZip=frInfo.document.frmClaim.txtPrZip.value;PrFlag=1;
			}
			if(PrPhone!=frInfo.document.frmClaim.txtPrPhone.value){
				PrPhone=frInfo.document.frmClaim.txtPrPhone.value;PrFlag=1;
			}
			if(PrFax!=frInfo.document.frmClaim.txtPrFax.value){
				PrFax=frInfo.document.frmClaim.txtPrFax.value;PrFlag=1;
			}
			if(PrEmail!=frInfo.document.frmClaim.txtPrEmail.value){
				PrEmail=frInfo.document.frmClaim.txtPrEmail.value;PrFlag=1;
			}
		}

	function Var_Save_lossLoc(f) {
	    var retVal = true;

	    var nState = '';
	    var nStateId = 0;
	    var oCountryDesv = State;
	    var lcIdx_ = frGeneral.document.getElementById("cboLossCountry").selectedIndex;


	    var oStateText = (State == '') ? frGeneral.document.frmGeneral.cboLossCountry[lcIdx_].text + " "  : State;
	    var nStateText = (f.txtLossState.value == '') ? f.txtLossStateDesc.value : f.txtLossState.value;

        if (State != f.txtLossState.value || LossStateId != f.txtLossStateId.value) {
            if (!confirm('You have changed loss state from "' + oStateText + '"  to  "' + nStateText + '".\nThis will redirect you to the tab "General".\n\nWould you like to proceed?')) {
                //alert('Please adjust loss state.\nExpected value:' + State)
                return (false);
            } else {               
                nState = f.txtLossState.value;
                nStateId = f.txtLossStateId.value;
            }
	    }

        if (Unit =='WC' && !LossLocation_Zip_validate(f.txtLossZip.value)) {
            if (!confirm('Value of the Employer Loss Site Zip (Injury tab) will be changed from ' + EmplZip + ' to ' + f.txtLossZip.value + '\nWould you like to proceed?')) {
                return (false);
            }
        }

	    try{ 
	        LossAddr1 = f.txtLossAddr1.value;
	        LossAddr2 = f.txtLossAddr2.value; 
	        LossCity = f.txtLossCity.value;
	        LossZip = f.txtLossZip.value;
	        LocDesc = f.txtLossLoc.value;

	        EmplZip = (Unit =='WC' && f.txtLossZip.value == '') ? EmplZip : f.txtLossZip.value;
	    } catch (e) {
	        alert(e.message);
	        return (false);
	    }

	    if (nState != '' || nStateId != 0) {
	        if (frInfo.document.title != 'claim') {	            
	            setTimeout("Display('claim')", 50);
	        }

	        if (frGeneral.document.getElementById("cboLossCountry").value != f.txtLossCountryId.value) {
	            frGeneral.document.getElementById("cboLossCountry").value = f.txtLossCountryId.value;
	            $("#cboLossCountry", frGeneral.document).trigger("change");
	        }

	        State = nState;  StateId = nStateId;

	        if (nState != '') {
	            frGeneral.document.getElementById('cboLossState').value = nState;
	        }else{
	            $('#cboLossState', frGeneral.document).find('option[stateId="' + nStateId + '"]').attr("selected", "selected");
	        }

	        $('#cboLossState', frGeneral.document).trigger("change");
	    }

	    return (retVal);
	}

	function Var_Save_Custom(){
	    var retVal = true;
        var c = 1; var Label; 

		Required_custom_FLD_Msg='';
		while(c<=CUSTOM_FIELDS){			
			try{
				if(eval('frInfo.document.frmClaim.' + 'T' + c + '.value=="restricted"')){
				}else{				
					eval('T' + c + '=frInfo.document.frmClaim.' + 'T' + c + '.value');
					//validate required fields:
					if(trim(eval('frInfo.document.frmClaim.' + 'T' + c + '.value'))=='' && eval('frInfo.document.frmClaim.' + 'T' + c + '.id')=='Y'){
						Label=frInfo.document.getElementById("Label_T" + c + "").innerText
						Required_custom_FLD_Msg = Required_custom_FLD_Msg + 'Field [' + Label + '] has to be filled out.' + '\n';
						retVal = false;
					}
				}
			}catch(e){
				 eval('T' + c + '=""')
					if(eval('frInfo.document.frmClaim.' + 'T' + c + '.id')=='Y'){
						Label=frInfo.document.getElementById("Label_T" + c + "").innerText
						Required_custom_FLD_Msg = Required_custom_FLD_Msg + 'Field [' + Label + '] has to be filled out.' + '\n';
						retVal = false;
					}					
			}
			c++
		}
	    // save Generic Custom Field:
		var genRetVal = true;
        if(MainFlag == 'Edit'){
            genRetVal  =  GenericCustomFieldsSave();
        }
        if (Required_custom_FLD_Msg.length > 0) { alert(Required_custom_FLD_Msg) };

        if (genRetVal != retVal) {
            retVal = false;
        }
        return (retVal);
	}

	function Var_Save_polCCinfo() {
	    var PolNo_= PolicyCC.PolNo;
	    var EffDT_='';
	    var ExpDT_ = '';
	    var strLoc = '';

	    //validate coverage data selection:	            
	    if (PolicyCC.hasOwnProperty('id')) {   //indicator that CC policy is assigned!
	        //If Policy is Inactive  - it can't be selected.
	        if (PolicyCC.Active.charAt(0)!='Y') {
	            alert('Selected policy is not active.\nPlease either activate the policy or select the active one');
	            return (false);
	        }

	        if(PolicyCC.LOC.length>0){
	            var l = ',0,';
		        try {
		            l = ',' + EditLoc[1][0] + ',';		
		        } catch (e) {}

		        strLoc = ',' + PolicyCC.LOC.join(",") + ',';
		        var re = eval('/' + l + '/gi');
			    
			    var foundArr = re.exec(strLoc);
			    if (foundArr == null) {	// list of acceptable licences for the LOB and State
			        alert('Selected policy is not associated with ' + LocLegend + ' (location) on this claim ');
			        return(false);
			    }				
			}
	        
	        if (PolicyCC.Type == 'AL') {
	            if (typeof (PolicyCC.VEH) != "undefined") {
	                if (PolicyCC.VEH[0] == null) {
	                    alert('Please select Policy Vehicle ')
	                    return (false);
	                }
	            } else {
	                alert('Please select Policy Vehicle ')
	                return (false);
	            }
	        }

	        // scheduled auto:
	        if (PolicyCC.Type == 'AL' && PolicyCC.Shdl == 1) {
	            if (PolicyCC.VEH[0].hasOwnProperty('COV')) {
	                if (PolicyCC.VEH[0].COV[0] == null) {
	                    alert('Please select Policy Coverage ')
	                    return (false);
	                }
	            }
	        }

	        // non-scheduled auto:  //  casualty
	        if ((PolicyCC.Type == 'AL' && PolicyCC.Shdl == 0) || PolicyCC.Type == 'CA') {
	            if (typeof (PolicyCC.COV) != "undefined") {
	                if (PolicyCC.COV[0] == null) {
	                    alert('Please select Policy Coverage ')
	                    return (false);
	                }
	            }
	        }

            // on claim level keep Policy period:
	        EffDT_ = PolicyCC.Eff;
	        ExpDT_ = PolicyCC.Exp;

	        // assign values:
	        if (SelfInsuredPrefix != 'Y') {  // for self insured property
	            frGeneral.document.frmGeneral.txtPolNo.value = PolNo_;
	            frGeneral.document.frmGeneral.txtEffDT.value = MyDateFormat(trim(EffDT_));
	            frGeneral.document.frmGeneral.txtExpDT.value = MyDateFormat(trim(ExpDT_));
	        }

	    }
        
        Cov = Populate_polCCcov();         

       return(true);
    }

////////////////////////////////////
//COLLECTING DATA TO SEND ON SERVER:
function DataForServer(){
    var f = ''
        // close all child windoes:
       if (typeof ClmOriginal.ChildWindows != 'undefined' && ClmOriginal.ChildWindows != null) {
           for (var _oCW in ClmOriginal.ChildWindows) {
               if (typeof  _oCW == 'string') {
                    try {
                        if (!ClmOriginal.ChildWindows[_oCW].closed) {
                            ClmOriginal.ChildWindows[_oCW].close();
                        }
                    } catch (e) {}
               }
            }
        } 

		f +="<html><body>"
		  + "<form name='frmMyClaim' action='../openers/" + SCREEN + ".asp' method='post' target='_parent'>"
          //+ "<input name=ContentId Type='hidden' value='" + ContentId + "'>"
		  + "<input name='LossCause' Type='hidden' value=" + LossCause + ">"
		  + "<input name='LossStateId' Type='hidden' value=" +  $('#cboLossState', frGeneral.document).find('option:selected').attr('stateId') + ">"

		  + "<input name='LossAddr1' Type='hidden' value='" + SingleQuote(LossAddr1) + "'>"
          + "<input name='LossAddr2' Type='hidden' value='" + SingleQuote(LossAddr2) + "'>"
          + "<input name='LossCity' Type='hidden' value='" + SingleQuote(LossCity) + "'>"
          + "<input name='LossZip' Type='hidden' value='" + SingleQuote(LossZip) + "'>"

		//PolicyCC Assignment:
	    var PolicyCC_ID_ = 0;
		var PolicyCC_Veh_ = 0;  
		var PolicyCC_Drvr_ = 0; 
		var PolicyCC_Prop_ = 0;

		var CovCod_ = CovCod.substring(0, (CovCod.indexOf('_')));

		if (PolicyCC.hasOwnProperty('id')) {
		    PolicyCC_ID_ = PolicyCC.id;
		    if (PolicyCC.hasOwnProperty('Retro')) {
		        if (PolicyCC.Retro.length > 0  && PolicyCC.hasOwnProperty('EffNoRetro')) {  // claim-made policy
                    if(MyDate(PolicyCC.EffNoRetro, 1) != '') {      // valid date
                        if (DateComp(MyDateFormat(ReportDate), MyDateFormat(trim(PolicyCC.EffNoRetro), 2) , 1) == 1 || DateComp(MyDateFormat(trim(PolicyCC.Exp), 2), MyDateFormat(ReportDate), 1) == 1) {   //  ReportDate < PolicyCC.EffNoRetro  ||
                            alert('This claim is linked to the claim-made policy.\nPolicy loss reporting period is  [' + MyDateFormat(trim(PolicyCC.EffNoRetro), 2) + ' - ' + MyDateFormat(trim(PolicyCC.Exp), 2) + ']\nReport date [' + MyDateFormat(ReportDate) + '] is our of that period.');
                            return;
                        }
                    }
		        }		        
            }

            if (PolicyCC.hasOwnProperty('VEH') && PolicyCC.VEH != null) {
		        if (typeof (PolicyCC.VEH[0]) != "undefined") {
		            PolicyCC_Veh_ = PolicyCC.VEH[0].ID;
		        }
		    }
		    if (PolicyCC.hasOwnProperty('DRVR') && PolicyCC.DRVR != null) {
		        if (typeof (PolicyCC.DRVR[0]) != "undefined") {
		            PolicyCC_Drvr_ = PolicyCC.DRVR[0].ID;
		        }
		    }
		    if (PolicyCC.hasOwnProperty('PROP') && PolicyCC.PROP!=null) {
		        if (typeof (PolicyCC.PROP[0]) != "undefined") {
		            PolicyCC_Prop_ = PolicyCC.PROP[0].ID;
		        }
		    }

            if (PolicyCC.Type == 'WC') {
		        if (PolicyCC.hasOwnProperty('OccCodeStates')) {
		            if (typeof (PolicyCC.OccCodeStates) == "string") {
		                var PolicyCC_OccCodeStates_ = PolicyCC.OccCodeStates;
		                if (trim(PolicyCC_OccCodeStates_) != '') {
		                    if (PolicyCC_OccCodeStates_.indexOf(JurState + ',') != -1){
		                        if (OccupID == 0 || OccupID == '' || OccupID == 'GET ANOTHER') {
		                            if (frInfo.document.title != 'workcomp') {
		                                setTimeout("Display('workcomp')", 100);
		                            }
		                            alert("Please specify policy specific State/NCCI Occup Code");
		                            try {
		                                setTimeout("frInfo.document.all['lblOccup'].style.fontWeight='bold'", 200);
		                                setTimeout("frInfo.document.all['lblOccup'].style.color='red'", 200);
		                            } catch (e) { }
		                            return;
                                }
		                    }
                        }
		            }
		        }
		    }

            if (PolicyCC.Type == 'AL' || PolicyCC.Type == 'CA') {
                if (PolicyCC.hasOwnProperty('VEH')) {
                    if (PolicyCC.VEH.length == 0) {
                        if (!confirm('Policy vehicle is not selected\n\nPress [Ok] to proceed\nPress [Cancel] to stop submission')) {
                            if (frInfo.document.title != "polCCinfo") {
                                setTimeout("Display('polCCinfo')", 100);
                                return;
                            }
                        }
                    }
                }
		        // scheduled auto:
		        if (PolicyCC.Type == 'AL' && PolicyCC.Shdl == 1) {
	                if (PolicyCC.VEH[0].hasOwnProperty('COV')) {
	                    if (PolicyCC.VEH[0].COV[0] == null) {
	                        alert('Please select Policy Coverage ')
	                        return;
	                    }
	                }
		        }

		        if ((PolicyCC.Type == 'AL' && PolicyCC.Shdl == 0) || PolicyCC.Type == 'CA') {
		            if (PolicyCC.COV.length > 0) {
		                if (PolicyCC.COV[0] == null) {
		                    alert('Please select Policy Coverage');
		                    return;
		                } else {

		                    if (CovCod_ != PolicyCC.COV[0].Cd) {
		                        if (frInfo.document.title != 'claim') {
		                            setTimeout("Display('claim')", 100);
		                        }
		                        alert('Selected Coverage from the policy does not match the chosen Coverage on the claim.  Please update the claim`s Coverage type');
		                        try {
		                            setTimeout("frInfo.document.all['CovlL'].style.color='red'", 100);
		                            setTimeout("frInfo.document.frmClaim.cboCov.focus()", 100);
		                        } catch (e) { }

		                        return;
		                    }


		                }
		            }
		        }	    
		    }
		}
	    f += "<input name=PolicyCC_ID Type='hidden' value='" + PolicyCC_ID_ + "'>"
		      + "<input name=PolicyCC_Veh Type='hidden' value='" + PolicyCC_Veh_ + "'>"
		      + "<input name=PolicyCC_Drvr Type='hidden' value='" + PolicyCC_Drvr_ + "'>"
		      + "<input name=PolicyCC_Prop Type='hidden' value='" + PolicyCC_Prop_  + "'>"
 		
		//ticket 104199:
		if (frGeneral.document.frmGeneral.cboAdjuster.value.toUpperCase() == frGeneral.document.frmGeneral.cboSuper.value.toUpperCase()) {
		    alert('The same person cannot be Adjuster and Supervisor on the same claim')
		    return;
		}

		if (InjuryFlag == 1) {		    
		    frButtons.document.frmButtons.WorkcompFlag.value = InjuryFlag;
		}
		
		if (frButtons.document.frmButtons.WorkcompFlag.value==1) {
			if(NCCI_SOI.length==0){NCCI_SOI=0}
			if(NCCI_NOI.length==0){NCCI_NOI=0}
			if(NCCI_SEV.length==0){NCCI_SEV=0}

        // Optional default mutipart data elements:

		f += "" //Input name=txtWC_Rate Type='hidden' value='" + WeeklyRate + "'>"		    
		  +  "<input name=cboNCCI Type='hidden' value='" + NCCICod + "'>"
		  +  "<input name=txtWC_Hire Type='hidden' value='" + DateOfHire + "'>"
		  +  "<input name=txtWC_D_InitDisab Type='hidden' value='" + DateInitia + "'>"
		  + "<input name=txtWC_D_Report Type='hidden' value='" + DateReport + "'>"
          + "<input name=TerminateJobDate Type='hidden' value='" + TerminateJobDate + "'>"
		
		  + "<input name=txtWC_D_Returned Type='hidden' value='" + DateReturn + "'>"
          + "<input name='datereturn_current' Type='hidden' value='" + datereturn_current + "'>"
          
		  +  "<input name=cboWC_MStatus Type='hidden' value='" + Marital + "'>"
		  +  "<input name=txtWC_DependNo Type='hidden' value='" + SingleQuote(Dependents) + "'>"
		  +  "<input name=txtWC_WeeklyWage Type='hidden' value='" + WeekWage + "'>"
		  +  "<input name=cboWC_JuriState Type='hidden' value='" + JurState + "'>"
		  +  "<input name=txtWC_DateMaxMedImpr Type='hidden' value='" + DateMaxMedImpr + "'>"
		//+  "<input name=cboWC_Occup Type='hidden' value='" + Occup + "'>"
		  + "<input name=cboWC_Occup Type='hidden' value=" + OccupID + ">"		
		  +  "<input name=cboNCCI_SOI Type='hidden' value='" + NCCI_SOI + "'>"
		  +  "<input name=cboNCCI_NOI Type='hidden' value='" + NCCI_NOI + "'>"
		  + "<input name=cboNCCI_SEV Type='hidden' value='" + NCCI_SEV + "'>"

          + "<input name=cboCLASSCode type='hidden' value='" + CLASSCode + "'>"
		  
		  +  "<input name=txtJobTitle Type='hidden' value='" + SingleQuote(JobTitle) + "'>"
		  +  "<input name=StartJobDate Type='hidden' value='" +  StartJobDate + "'>"
		  + "<input name=wage_method Type='hidden' value=" + wage_method + ">"
		  + "<input name=employee_id Type='hidden' value='" + SingleQuote(employee_id) + "'>"
		  + "<input name=injury_dept Type='hidden' value='" + SingleQuote(injury_dept) + "'>"
		 
		  +  "<input name=FirstTREAT Type='hidden' value='" + FirstTREAT + "'>"
		  +  "<input name=EmplSTATE Type='hidden' value='" + EmplSTATE + "'>"
		  + "<input name=InjOnEmplP Type='hidden' value='" + InjOnEmplP + "'>"
		  
		  +  "<input name=TTRATE Type='hidden' value='" + TempTotalRATE + "'>"
		  +  "<input name=TPRATE Type='hidden' value='" + TempPartialRATE + "'>"
		  +  "<input name=PTRATE Type='hidden' value='" + PermTotalRATE + "'>"
		  + "<input name=VRRATE Type='hidden' value='" + VocRehabRATE + "'>"
		  
		  +  "<input name=MDuty Type='hidden' value='" + MODIFIEDUTY + "'>"
		  +  "<input name=CCE Type='hidden' value='" + ConcurEMPLMNT + "'>"
		  +  "<input name=cceWAGE Type='hidden' value='" + ConcurWAGE + "'>"
		  + "<input name=InsWAGE Type='hidden' value='" + InsuredWAGE + "'>"
		 
          + "<input name=cumulative_trauma_date Type='hidden' value='" + cumulative_trauma_date + "'>"
          + "<input name=cumulative_trauma_last_date Type='hidden' value='" + cumulative_trauma_last_date + "'>"

		  + "<input name=LastWorkDay Type='hidden' value='" + LastWorkDay + "'>"
          + "<input name=CurrentLastWorkDay Type='hidden' value='" + CurrentLastWorkDay + "'>"
		  
		  +  "<input name=FirstLostTimeDay Type='hidden' value='" + FirstLostTimeDay + "'>"		  
		  +  "<input name=EmplStatusCod Type='hidden' value='" + EmplStatusCod + "'>"		   
		  +  "<input name=JobCodeCod Type='hidden' value='" + JobCodeCod + "'>"
		  +  "<input name=ClassificationCode Type='hidden' value='" + ClassificationCode + "'>"		   
		  +  "<input name=EmplZip Type='hidden' value='" + EmplZip + "'>"
		  
		  + "<input name=PmntSuspension Type='hidden' value=" + PmntSuspension + ">"
		  + "<input name=SalaryCont Type='hidden' value='" + SalaryCont + "'>"
		  + "<input name=RTWSameEmpl Type='hidden' value='" + RTWSameEmpl + "'>"
		  + "<input name=ReleasedRTW Type='hidden' value='" + ReleasedRTW + "'>"
		
		  
		  if (RTWSameEmpl =='Y') {
		    f += "<input name=return_position Type='hidden' value='" + SingleQuote(return_position) + "'>"
		  }else{
		    f += "<input name=return_position Type='hidden' value=''>"
		  }
		  
		  f +="<input name=InjDayPaid Type='hidden' value='" + InjDayPaid + "'>"
          
		  
		  +  "<input name=FormProvideDate Type='hidden' value='" + FormProvideDate + "'>"
		  + "<input name=FormReceiveDate Type='hidden' value='" + FormReceiveDate + "'>"
          + "<input name=FormReceiveDateTPA Type='hidden' value='" + FormReceiveDateTPA + "'>"
		  
		  +  "<input name=PDRating Type='hidden' value='" + PDRating + "'>"
		  +  "<input name=wc_board_fraud Type='hidden' value='" + wc_board_fraud + "'>"
          +  "<input name=initialTreatmentType  Type='hidden' value='" +  initialTreatmentType + "'>"
		  
		  +  "<input name=hours_sun Type='hidden' value='" + hours_sun + "'>"
		  +  "<input name=hours_mon Type='hidden' value='" + hours_mon + "'>"
		  +  "<input name=hours_tue Type='hidden' value='" + hours_tue + "'>"
		  +  "<input name=hours_wed Type='hidden' value='" + hours_wed + "'>"
		  +  "<input name=hours_thu Type='hidden' value='" + hours_thu + "'>"		  
		  +  "<input name=hours_fri Type='hidden' value='" + hours_fri + "'>"
		  +  "<input name=hours_sat Type='hidden' value='" + hours_sat + "'>"
			if(weekly_hours_calculate()!=0){
				if(weekly_hours_calculate()!=parseFloat(WeeklyHours)){
					alert('Please check working hours.\nWeekly and Daily hours do not match.')
					return;
				}
			}
		 f+= "<input name=WeeklyHours Type='hidden' value='" + WeeklyHours + "'>"
		  +  "<input name=EmplDutyDescr Type='hidden' value='" + SingleQuote(EmplDutyDescr) + "'>"
		  +  "<input name=Minor_LossType Type='hidden' value='" + Minor_LossType + "'>"			  		  			  	  			  
		  +  "<input name=time_began_work Type='hidden' value='" + time_began_work + "'>"
		  +  "<input name=began_am_pm Type='hidden' value='" + began_am_pm + "'>"
		  +  "<input name=AOE Type='hidden' value='" + AOE + "'>"
		  +  "<input name=PreExistDisability Type='hidden' value='" + PreExistDisability + "'>"
		  +  "<input name=LossCovCode Type='hidden' value='" + LossCovCode + "'>"
		  
		  +  "<input name=irregular Type='hidden' value='" + irregular + "'>"
		  +  "<input name=life_pension Type='hidden' value='" + life_pension + "'>"
		  +  "<input name=comp_with_liability Type='hidden' value='" + comp_with_liability + "'>"
		  + "<input name=life_medical Type='hidden' value='" + life_medical + "'>"
		 if (life_medical != 'Y') {
		     f += "<input name='future_medical_type' Type='hidden' value=''>"
		 } else {
		     f += "<input name='future_medical_type' Type='hidden' value='" + future_medical_type + "'>"
		 }
		  f+= ''
		  +  "<input name=cumulative_trauma Type='hidden' value='" + cumulative_trauma + "'>"		
		  +  "<input name=joint_coverage Type='hidden' value='" + joint_coverage + "'>"
		  +  "<input name=safety_officer Type='hidden' value='" + safety_officer + "'>"
		  +  "<input name=other_worker_injured Type='hidden' value='" + other_worker_injured + "'>"			    		  
		  +  "<input name='unable_work_oneday' Type='hidden' value='" + unable_work_oneday + "'>"
		  +  "<input name='surgery' Type='hidden' value='" + surgery + "'>"
		  
		  +  "<input name='CA_132A' Type='hidden' value='" + CA_132A + "'>"
		  +  "<input name='Serious_Willful' Type='hidden' value='" + Serious_Willful + "'>"
		  
		  +  "<input name=social_security_offset Type='hidden' value='" + social_security_offset + "'>"
		  +  "<input name=pension_plan_offset Type='hidden' value='" + pension_plan_offset + "'>"
		  +  "<input name=unemployment_offset Type='hidden' value='" + unemployment_offset + "'>"
		  +  "<input name=special_fund_offset Type='hidden' value='" + special_fund_offset + "'>"
		  + "<input name=other_benefit_offset Type='hidden' value='" + other_benefit_offset + "'>"

		  + "<input name=business_owner Type='hidden' value='" + business_owner + "'>"
	      + "<input name=safety_provided Type='hidden' value='" + safety_provided + "'>"
	      + "<input name=safety_used Type='hidden' value='" + safety_used + "'>"
	      + "<input name=drug_test	Type='hidden' value='" + drug_test + "'>"
		}

		if (frButtons.document.frmButtons.ClaimantFlag.value == 1) {
		f +="<input name=cboCIDRepTyp Type='hidden' value=''>"
		  +  "<input name=txtClmtSSN Type='hidden' value='" + SSNum + "'>"
		  +  "<input name=txtClmtAddress Type='hidden' value='" + SingleQuote(Caddr1) + "'>"
		  + "<input name=txtClmtAddress2 Type='hidden' value='" + SingleQuote(Caddr2) + "'>"

		if (Ccity.length > 18) {
		    Ccity = Ccity.substr(0, 18);
		    alert('Due to the temporary system limitation the Claimant (Loss Address) city value will be saved as [' + Ccity + ']')
		}

		f += "<input name=txtClmtCity Type='hidden' value='" + SingleQuote(Ccity) + "'>"
		  + "<input name=cboClmtState Type='hidden' value='" + Cstate + "'>"
          + "<input name=CstateId Type='hidden' value='" + CstateId + "'>"          
		  +  "<input name=txtClmtZip Type='hidden`' value='" + Czip + "'>"
		  +  "<input name=txtClmtPhone Type='hidden' value='" + Cphone + "'>"
		  +  "<input name=txtClmtBusPhone Type='hidden' value='" + Cworkphone + "'>"
		  +  "<input name=txtClmtDOB Type='hidden' value='" + DOB + "'>"
		  +  "<input name=cboClmtSex Type='hidden' value='" + Sex + "'>"
		  +  "<input name=CEMAIL Type='hidden' value='" + CEMail + "'>"
		  +  "<input name=CCONTACT Type='hidden' value='" + CContact + "'>"
		  +  "<input name=CPerson Type='hidden' value='" + CPerson + "'>"
		  +  "<input name=CFName Type='hidden' value='" + CFName + "'>"
		  +  "<input name=CLName Type='hidden' value='" + CLName + "'>"
		  +  "<input name=CMI type='hidden' value='" + CMI + "'>"		
		  +  "<input name=CLanguage type='hidden' value='" + CLanguage + "'>"
		  + "<input name=DODEATH type='hidden' value='" + DODEATH + "'>"
    	  + "<input name=CLICENSENO type='hidden' value='" + CLICENSENO + "'>"
		  + "<input name=CLICSTATE type='hidden' value='" + CLICSTATE + "'>"
		  + "<input name=CCITIZEN type='hidden' value='" + CCITIZEN + "'>"
            + "<input name=Auth_Med_Release type='hidden' value='" + Auth_Med_Release + "'>"
            + "<input name=Auth_SSN_Release type='hidden' value='" + Auth_SSN_Release + "'>"
            + "<input name=MEDICARE_BENEFICIARY type='hidden' value='" + MEDICARE_BENEFICIARY + "'>"
            + "<input name='MaidenName' Type='hidden' value='" + SingleQuote(MaidenName) + "'>"
            + "<input name=HICN type='hidden' value='" + HICN + "'>"
            + "<input name=Auth_Age_Release type='hidden' value=" + Auth_Age_Release + ">"

            + "<input name=DemographicsSupervisorRecId type='hidden' value=" + HRSuperRecId + ">"  
		} 

		if(frButtons.document.frmButtons.ComplexFlag.value==1){		
		f += "<input name=txtComplexAnalyst Type='hidden' value='0'>"   //ticket 120928
		  +  "<input name=txtReferralDate Type='hidden' value='" + cmpxReferralDate + "'>"		  
		  +  "<input name=txtDateOfFirstReport Type='hidden' value='" + cmpxFirstReport + "'>"		  
		  +  "<input name=txtDateVSResponse Type='hidden' value='" + cmpxVSDate + "'>"
		  +  "<input name=cboReferralReason  Type='hidden' value='" + cmpxReferralReason + "'>"		  
		  +  "<input name=txtVSONumber Type='hidden' value='" + cmpxVSONumber + "'>"		  
		  +  "<input name=txtVSAnalyst Type='hidden' value='" + cmpxVSAnal + "'>"
		  +  "<input name=cboTypeOfIssue Type='hidden' value='" + cmpxIssueType + "'>"		  
		  +  "<input name=radioVSResponse Type='hidden' value='" + cmpxVSResponse + "'>"		  
		  +  "<input name=radioSLAHandling Type='hidden' value='" + SLA  + "'>"		  
		  +  "<input name=cboSLAGA Type='hidden' value='" + cmpxSLA + "'>"		  
		  +  "<input name=txtNote Type='hidden' value='" + cmpxNote + "'>"		  		  
		  +  "<input name=ComplexDeControl Type='hidden' value=''>"
		} 

		if(frButtons.document.frmButtons.LawFlag.value==1){	
		f +="<input name=cboLawTyp Type='hidden' value='" + Ltype + "'>"
		  + "<input name=txtLawName Type='hidden' value='" + SingleQuote(LName) + "'>"

          + "<input name=txtLawLName Type='hidden' value='" + SingleQuote(LLName) + "'>"
		  + "<input name=txtLawFName Type='hidden' value='" + SingleQuote(LFName) + "'>"
		  
		  +  "<input name=txtLawAddress Type='hidden' value='" + SingleQuote(Laddr1) + "'>"
		  +  "<input name=txtLawAddress2 Type='hidden' value='" + SingleQuote(Laddr2) + "'>"

		
		f += "<input name=txtLawCity Type='hidden' value='" + SingleQuote(Lcity) + "'>"
		  +  "<input name=cboLawState Type='hidden' value='" + Lstate + "'>"
		  +  "<input name=LstateId Type='hidden' value='" + LstateId + "'>"
		  
		  +  "<input name=txtLawZip Type='hidden' value='" + Lzip + "'>"
		  +  "<input name=txtLawPhone Type='hidden' value='" + Lphone + "'>"
		  +  "<input name=txtLawTaxID Type='hidden' value='" + LtaxId + "'>"
		  +  "<input name=txtLawFax Type='hidden' value='" + Lfax + "'>"
		  +  "<input name=LEMAIL Type='hidden' value='" + LEMail + "'>"
		  + "<input name=LCONTACT Type='hidden' value='" + LContact + "'>"
		  + "<input name=represent_notice_date Type='hidden' value='" + represent_notice_date + "'>"
		}
		if (MainFlag != 'Edit') {
		    VehicleFlag = (ClmOriginal.Vehicles.vehicleId < 0) ? ClmOriginal.Vehicles.vehicleId : 0;
		}
    	if (MainFlag != 'Edit') {
		    DocFlag = (ClmOriginal.Doctors.doctorId < 0) ? ClmOriginal.Doctors.doctorId : 0
		}

		if(frGeneral.document.frmGeneral.cboAsgmtType.value!='F'  && frGeneral.document.frmGeneral.cboAsgmtType.value!='R'){
			// for non - full adjustment claims  default recovery possibility: "N"
			f +="<input name='REC_STATUS' Type='hidden' value='N'>"			
		}else{
			f +="<input name='REC_STATUS' Type='hidden' value='" + REC_STATUS + "'>"
		}
		if (frButtons.document.frmButtons.RecoveryFlag.value == 1) {
			f +=""
			+  "<input name=txtSNAME type='hidden' value='" + SNAME  + "'>"
			+  "<input name=txtSADDR1 type='hidden' value='" + SADDR1  + "'>"
			+  "<input name=txtSADDR2 type='hidden' value='" + SADDR2  + "'>"
			+  "<input name=txtSCITY type='hidden' value='" + SCITY  + "'>"
			+ "<input name=txtSSTATE type='hidden' value='" + SSTATE + "'>"

			+ "<input name=SStateID type='hidden' value='" + SStateID + "'>"
			
			+  "<input name=txtSZIP type='hidden' value='" + SZIP  + "'>"
			+  "<input name=txtSPHONE type='hidden' value='" + SPHONE  + "'>"
			+  "<input name=txtSFAX type='hidden' value='" + SFAX  + "'>"
			+  "<input name=txtSEMAIL type='hidden' value='" + SEMAIL  + "'>"
			+  "<input name=txtACCONTACT type='hidden' value='" + ACCONTACT  + "'>"
			+  "<input name=txtACADDR1 type='hidden' value='" + ACADDR1  + "'>"
			+  "<input name=txtACADDR2 type='hidden' value='" + ACADDR2  + "'>"
			+  "<input name=txtACCITY type='hidden' value='" + ACCITY  + "'>"
			+ "<input name=txtACSTATE type='hidden' value='" + ACSTATE + "'>"

			+ "<input name=ACStateID type='hidden' value='" + ACStateID + "'>"
			
			+  "<input name=txtACZIP type='hidden' value='" + ACZIP  + "'>"
			+  "<input name=txtACPHONE type='hidden' value='" + ACPHONE  + "'>"
			+  "<input name=txtACFAX type='hidden' value='" + ACFAX  + "'>"
			+  "<input name=txtACEMAIL type='hidden' value='" + ACEMAIL  + "'>"
			+  "<input name=txtACCLMNO type='hidden' value='" + ACCLMNO  + "'>"
			+  "<input name=txtRECOVPERCENTAGE type='hidden' value='" + RECOVPERCENTAGE  + "'>"
			+  "<input name=txtRVEHICLE type='hidden' value='" + RVEHICLE  + "'>"
			+  "<input name=txtSETTLEMENT type='hidden' value='" + SETTLEMENT  + "'>"
			+  "<input name=txtRPAYABLETO type='hidden' value='" + RPAYABLETO + "'>"
			+  "<input name=txtRAMOUNT type='hidden' value='" + RAMOUNT  + "'>"
			+  "<input name=txtSYORKFEE type='hidden' value='" + SYORKFEE + "'>"
			+  "<input name=txtSYORKAMOUNT type='hidden' value='" + SYORKAMOUNT + "'>"
			+  "<input name=txtRECRESERVE type='hidden' value='" + RECRESERVE + "'>"
			+  "<input name=txtSNOTES type='hidden' value='" + SNOTES  + "'>"
			+  "<input name=txtRADJNO type='hidden' value=" + RADJNO  + ">"			
			+  "<input name=cboPursuitAgainst type='hidden' value=" + PursuitAgainst + ">"	
			+  "<input name=cboSubrogationType type='hidden' value=" + SubrogationType + ">"
			+  "<input name=cboReferredTo type='hidden' value=" + ReferredTo + ">"	
			+  "<input name=cboResolvedBy type='hidden' value=" + ResolvedBy + ">"
			+  "<input name=cboDisposition type='hidden' value=" + Disposition + ">"

            + "<input name=StatuteDate type='hidden' value='" + StatuteDate + "'>"	

           	//RECOVERY_ExtraTypes:
			var rc=0; var t='';
			if(RECOVERY_ExtraTypes.length>0){
				while(rc<RECOVERY_ExtraTypes.length){
					t='';
					try{
						t+="<input Name='cboSubroType" + (rc+1) + "' Type='hidden' value=" + RECOVERY_ExtraTypes[rc][0] + ">"
						 + "<input Name='cboRefTo" + (rc+1) + "' Type='hidden' value=" + RECOVERY_ExtraTypes[rc][1] + ">"
					}catch(e){
						t=''; //lower element of the array ExtraRecoveryTypes might not be initialized
					}
					f+=t;
					rc++
				}
			}
			f +="<input Name='RECOVERY_ExtraTypes_C' Type='hidden' value=" + rc + ">"
		}
				
		if(IAdjFlag==1){
			f +=""
			+ "<input name=txtIAdjName Type='hidden' value='" + SingleQuote(IAdjName) + "'>"
			+ "<input name=txtIAdjAddr1 Type='hidden' value='" + SingleQuote(IAdjAddr1) + "'>"
			+ "<input name=txtIAdjAddr2 Type='hidden' value='" + SingleQuote(IAdjAddr2) + "'>"
			+ "<input name=txtIAdjCity Type='hidden' value='" + SingleQuote(IAdjCity) + "'>"
			+ "<input name=txtIAdjState Type='hidden' value='" + IAdjState + "'>"
			+ "<input name=txtIAdjZip Type='hidden' value='" + SingleQuote(IAdjZip) + "'>" 
			+ "<input name=txtIAdjTaxID Type='hidden' value='" + IAdjTaxID + "'>"
			+ "<input name=txtIAdjEMail Type='hidden' value='" + SingleQuote(IAdjEMail) + "'>" 
			+ "<input name=txtIAdjPhone Type='hidden' value='" + SingleQuote(IAdjPhone) + "'>"
			+ "<input name=txtIAdjFax Type='hidden' value='" + SingleQuote(IAdjFax) + "'>"
			+ "<input name=IAdjStateId Type='hidden' value='" + IAdjstateId + "'>"
			
		}

		if (prodliabFlag == 1) {
		    f += ""
			+ "<input name=plStatus  Type='hidden' value='" + plStatus  + "'>"
			+ "<input name=plMassTort Type='hidden' value='" + plMassTort + "'>"
			+ "<input name=plManufacturer Type='hidden' value='" + SingleQuote(plManufacturer) + "'>"
			+ "<input name=plGenericName Type='hidden' value='" + SingleQuote(plGenericName) + "'>"
			+ "<input name=plBrandName Type='hidden' value='" + SingleQuote(plBrandName) + "'>"
			+ "<input name=plAllegedHarm Type='hidden' value='" + SingleQuote(plAllegedHarm) + "'>"
		}

		if(PrFlag==1){
			f +=""
			+ "<input name=txtPrName Type='hidden' value='" + SingleQuote(PrName) + "'>"
			+ "<input name=txtPrAddr1 Type='hidden' value='" + SingleQuote(PrAddr1) + "'>"
			+ "<input name=txtPrAddr2 Type='hidden' value='" + SingleQuote(PrAddr2) + "'>"
			+ "<input name=txtPrCity Type='hidden' value='" + SingleQuote(PrCity) + "'>"
			+ "<input name=txtPrState Type='hidden' value='" + PrState + "'>"

			+ "<input name=PrstateId Type='hidden' value='" + PrstateId + "'>"
			
			+ "<input name=txtPrZip Type='hidden' value='" + SingleQuote(PrZip) + "'>" 
			+ "<input name=txtPrPhone Type='hidden' value='" + PrPhone + "'>"
			+ "<input name=txtPrFax Type='hidden' value='" + SingleQuote(PrFax) + "'>" 
			+ "<input name=txtPrEmail Type='hidden' value='" + SingleQuote(PrEmail) + "'>"			
		}else{
			f +="<input name=txtPrName Type='hidden' value='" + SingleQuote(PrName) + "'>"
		}

		if(frGeneral.document.frmGeneral.cboAsgmtType.value==''){
			alert('Please specify Type of Assignment');
			frGeneral.document.frmGeneral.cboAsgmtType.focus();
			return;
		}else{	
			f+="<input name='AsgmtType' Type='hidden' value='" + frGeneral.document.frmGeneral.cboAsgmtType.value + "'>"
		}
	
	//SPECIAL RULLE  FOR DTAG NEW FILES: 
	if(MainFlag!='Edit'){
		if(ClientNumber==1335 || ClientNumber==1603 || ClientNumber==1349 || ClientNumber==1943){			
			if(CarClmNo.length==0){alert('Carrier Claim Number has to be specified.');return;}
			if(Polstruct=="R"){
				if(AgreementN.length==0){alert('Renters Agreement has to be specified.');return;}
			}
		}
	}else{
		// Edit - specific fields:
		try{
			if(typeof(TP_Deductible)=="undefined" || ClmOriginal.DynamicElements.isFieldDisplayed('ThirdPartyDeductible', 'TP_Deductible') < 0 ){TP_Deductible = ''}
		}catch(e){
			TP_Deductible=''
		}finally{
			f+="<input name=TP_Deductible Type='hidden' value='" + TP_Deductible + "'>"
		}
		
		try {
		    if (typeof (TP_Deductible_Amount) == "undefined" || ClmOriginal.DynamicElements.isFieldDisplayed('ThirdPartyDeductible', 'TP_Deductible_Amount') < 0) { TP_Deductible_Amount = 0}
		} catch (e) {
		    TP_Deductible_Amount = 0
		} finally {
		    f += "<input name=TP_Deductible_Amount Type='hidden' value='" + TP_Deductible_Amount + "'>"
		}
	}
	//custom fields
		if(CUSTOM_FIELDS==0){
			f +="<input name=CUSTOM_FIELDS Type='hidden' value='0'>"
		}else{
			var c=1; var custom_Flag=0;			
			if(Required_custom_FLD_Msg.length > 0){
				alert('CUSTOM FIELDS REQUIREMENTS:\n\n' + Required_custom_FLD_Msg)
				Display('custom');
				return;
			}else{
				Display('custom')
				Var_Save_Custom();

				if(Required_custom_FLD_Msg.length > 0){
					alert('CUSTOM FIELDS REQUIREMENTS:\n\n' + Required_custom_FLD_Msg);					
					return;
				}else{
					f +="<input name=CUSTOM_FIELDS Type='hidden' value='" + CUSTOM_FIELDS + "'>"
					while(c<=CUSTOM_FIELDS){
						f +="<input name=txt" + "T" + c + "  Type='hidden' value='" + SingleQuote(eval('T'+c)) + "'>"
						if(eval('T'+c+'.length')>0){custom_Flag++}
					c++	
					}
				}
			}
		}

		//ARA BR: Policy_Prefix_Matrix
		if(ClientNumber==2094 && CarClmNo==''){
			if(T14=='YES'){
				if(frGeneral.document.frmGeneral.txtPolNo.value.substr(0,3).toUpperCase()!='CAR'){			
					var url='XCall_ARA_POLICY_PREF.asp?ClmNo=' + ClaimNumber + '&P=' + escape(frGeneral.document.frmGeneral.txtPolNo.value) + '&Eff=' + escape(frGeneral.document.frmGeneral.txtEffDT.value) + '&Exp=' + escape(frGeneral.document.frmGeneral.txtExpDT.value) + '&T2=' + escape(T2) + "&T10=" + escape(T10.slice(0,4)) + "&T11=" + escape(T11);
					var xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
					xmlhttp.Open("POST", url, false);
					xmlhttp.Send();
					var RetVal = xmlhttp.responseText;
					RetVal=RetVal.replace(/[*]/g,'\n') 
					xmlhttp = null;
					if(RetVal!=''){
						alert(RetVal)
						return;
					}
				}
			}
		}	
		// *** *** *** *** ***
		//check factored Reserves				
		var iIR=IR_Disp
		if(MainFlag!='Edit'){
				// DeCristo, Diane: 
				//When new losses are received they will be set up with the factored reserves we have used since the inception of the program.
				
				// We will eliminate the Rule that prevents these factored reserves to be changed.� 
				//From this point on all reserves can be changed at any point in the life of the file (even the day after the factor is set).�
				// Anyone with authority to handle the claim can change the reserve (adjuster, supervisor, manager)

			IR_Disp=CheckIRFactor(IR_Disp, IR)
		}else{
			AutoReserve='N';
		}

		//double check reserves:
		var cR=CheckReservesAgainstPmnts()
		if(cR!=''){
			if(cR=='IR' && AutoReserve=='Y'){
				if(confirm('Unable to set up factored reserve value.\nWould you like to set up amount you have entered initially - $' + commafy(iIR) + ' ?')){
					IR_Disp=iIR;
					AutoReserve='N';
					if(CheckReservesAgainstPmnts()!=''){
						return;
					}
				}else{
					return;
				}
			}else{
				return;
			}
		}	
		
    // claim core:
		f += "<input name=txtLossDescr Type='hidden' value='" + SingleQuote(Desc) + "'>"
            + "<input name=txtLossDescr2 Type='hidden' value='" + SingleQuote(Desc2) + "'>"
            + "<input name=txtLossDescr3 Type='hidden' value='" + SingleQuote(Desc3) + "'>"

            + "<input name=txtCarrierClmno Type='hidden' value='" + SingleQuote(CarClmNo) + "'>"
            + "<input name=txtClientsNum Type='hidden' value='" + SingleQuote(CltClmNo) + "'>"
            + "<input name=txt3PartyNo Type='hidden' value='" + SingleQuote(ThirdNum) + "'>"

	//claim party	
		if(frGeneral.document.frmGeneral.ClmParty.value==''){
			alert('Please specify party of the claim.')
			frGeneral.document.frmGeneral.ClmParty.focus();
			return;
		}else{
			f +="<input name=txtClmParty Type='hidden' value=" + frGeneral.document.frmGeneral.ClmParty.value.substr(0,3) + ">"
		}
	//PIP fields
		f +="<input name=cboThreshold Type='hidden' value='" + trim(Threshold) + "'>"
		if (Copay == '' || ClmOriginal.DynamicElements.isFieldDisplayed('PIP', 'Copay') < 0) {
			f +="<input name='txtCopay' Type='hidden' value='-1'>"
		}else{
			f +="<input name='txtCopay' Type='hidden' value=" + decommafy(Copay) + ">"
		}	
		
		if(typeof(MaxCopay)=="undefined"){MaxCopay=0}
		if (MaxCopay == '' || ClmOriginal.DynamicElements.isFieldDisplayed('PIP', 'MaxCopay') < 0) {
			f +="<input name='txtMaxCopay' Type='hidden' value='0'>"
		}else{
			f +="<input name='txtMaxCopay' Type='hidden' value=" + decommafy(MaxCopay) + ">"
		}
        if (Limit == '') {
			f +="<input name='txtLimit' Type='hidden' value='-1'>"
		}else{
			f +="<input name='txtLimit' Type='hidden' value=" + decommafy(Limit) + ">"
		}
        if (ExhaustDate == '' || ClmOriginal.DynamicElements.isFieldDisplayed('PIP', 'ExhaustDate') < 0) {
		    f += "<input name='ExhaustDate' Type='hidden' value=''>"
		} else {
		    f += "<input name='ExhaustDate' Type='hidden' value='" + ExhaustDate + "'>"
		}
		if (PIPCode == '' || ClmOriginal.DynamicElements.isFieldDisplayed('PIP', 'PIPCode') < 0 ) {
			f +="<input name='txtPIPCode' Type='hidden' value=0>"
		}else{
			f +="<input name='txtPIPCode' Type='hidden' value='" + (parseInt(PIPCode.substr(1),10) + parseInt(PIPaddtl,10)) + "'>"
		}
//property fields:
		if(GrossLoss==''){
			f +="<input name='txtGrossLoss' Type='hidden' value='-1'>"
		}else{
			f +="<input name='txtGrossLoss' Type='hidden' value=" + decommafy(GrossLoss) + ">"
		}
        if (Deductible == '' || (ClmOriginal.DynamicElements.isFieldDisplayed('PIP', 'Deductible') < 0 && ClmOriginal.DynamicElements.isFieldDisplayed('Property', 'Deductible') < 0)) {
			f +="<input name='txtDeductible' Type='hidden' value='-1'>"
		}else{
			f +="<input name='txtDeductible' Type='hidden' value=" + decommafy(Deductible) + ">"
		}
	//property field as well as Auto  [COMPREHENSIVE, COLLISION]
		f +="<input name='cboTotalLoss' Type='hidden' value='" + TotalLoss + "'>"		
		if(SCREEN=="AIGRM_Rental" || SCREEN=="AIGRM"){
			if(RepDT==''){
				f +="<input name='txtReportDate' Type='hidden' value=''>"
			}else{
				f +="<input name='txtReportDate' Type='hidden' value='" + RepDT + "'>"
			}	
		}else{
			if(ReportDate==''){
				f +="<input name='txtReportDate' Type='hidden' value=''>"
			}else{
				f +="<input name='txtReportDate' Type='hidden' value='" + ReportDate + "'>"
			}
		}	
		if(ContactDate==''){
			f +="<input name='txtContactDate' Type='hidden' value=''>"
		}else{
			f +="<input name='txtContactDate' Type='hidden' value='" + ContactDate + "'>"
		}
		if(InspectDate==''){
			f +="<input name='txtInspectDate' Type='hidden' value=''>"
		}else{
			f +="<input name='txtInspectDate' Type='hidden' value='" + InspectDate + "'>"
		}
		if(FirstReport==''){
			f +="<input name='txtFirstReport' Type='hidden' value=''>"
		}else{
			f +="<input name='txtFirstReport' Type='hidden' value='" + FirstReport + "'>"
		}
		//property - SubjectInsurance
		if(CovCod.indexOf('THEFT')!=-1){
			var SjI='';var rr=1;
			while(rr < SubjectI.length){
				if(SubjectI[rr][1]=="Y"){
					if(SjI!=''){
						SjI+=',' + rr
					}else{
						SjI+=rr
					}
				}
				rr++
			}
			if(SjI==''){
				alert('Please select at least one Subject of Insurance');
				DisplaySubjectInsurance();
				return;
			}else{
				f +="<input name='SjI' type='hidden' value='" + SjI + "'>"
			}
		}else{
			f +="<input name='SjI' type='hidden' value=''>"
		}
	//supervisor:
		var indS=frGeneral.document.frmGeneral.cboSuper.selectedIndex	
		if(indS==-1){	
			f +="<input name=txtSuper Type='hidden' value=0>"
		}else{
			if(isNaN(parseInt(frGeneral.document.frmGeneral.cboSuper.options[indS].id))){
				f +="<input name=txtSuper Type='hidden' value=0>"
			}else{
				f +="<input name=txtSuper Type='hidden' value=" + frGeneral.document.frmGeneral.cboSuper.options[indS].id + ">"
			}
		}	

	// PL	 --> InsuredsProduct
	if(frGeneral.document.frmGeneral.cbolLineBus.value!='PL'){
		f +="<input name='cboInsProduct' Type='hidden' value=''>"
	}
	//Assist Branch:
		if(MainFlag!='Edit'){
			if(frGeneral.document.frmGeneral.cboAdjuster.value=='PENDING'){
				try{
					f +="<input name='AssistBranch' Type='hidden' value='" + frGeneral.document.frmGeneral.cboAssistBranch.value + "'>"
				}catch(e){
					f +="<input name='AssistBranch' Type='hidden' value=0>"
				}
			}else{
				f +="<input name='AssistBranch' Type='hidden' value=0>"
			}
		}

		f +=DataForServerGeneral();		
    
		f +="<input name='EmpNum' Type='hidden' value=" + EmpNum + ">"	
		//------------ FLAGS:
		+ "<input name='ButtonFlag' Type='hidden' value='" + frButtons.document.frmButtons.ButtonFlag.value + "'>"
		+ "<input name='VehicleFlag' Type='hidden' value='" + VehicleFlag + "'>"  // frButtons.document.frmButtons.VehicleFlag.value 
		+ "<input name='ClaimantFlag' Type='hidden' value='" + frButtons.document.frmButtons.ClaimantFlag.value + "'>"
		+ "<input name='LawFlag' Type='hidden' value='" + frButtons.document.frmButtons.LawFlag.value + "'>"
		+ "<input name='DocFlag' Type='hidden' value='" + DocFlag + "'>"
		+ "<input name='claimMpnId' Type='hidden' value='" + ClmOriginal.Doctors.claimMpnId + "'>"
		
		+ "<input name='WorkcompFlag' Type='hidden' value='" + frButtons.document.frmButtons.WorkcompFlag.value + "'>"
		+ "<input name='JobFlag' Type='hidden' value='" + frButtons.document.frmButtons.JobFlag.value + "'>"
		+ "<input name='InsuredsFlag' Type='hidden' value='" + frButtons.document.frmButtons.InsuredsFlag.value + "'>"
		+ "<input name='RecoveryFlag' Type='hidden' value='" + frButtons.document.frmButtons.RecoveryFlag.value + "'>"
		+ "<input name='IAdjFlag' Type='hidden' value=" + IAdjFlag + ">"
		+ "<input name='prodliabFlag' Type='hidden' value=" + prodliabFlag + ">"
		+ "<input name='InjuryFlag' Type='hidden' value=" + InjuryFlag + ">"  //NON-WC BI claims  - injury data
		+ "<input name='PrFlag' Type='hidden' value=" + PrFlag + ">"
		+ "<input name='ComplexFlag' Type='hidden' value='" + frButtons.document.frmButtons.ComplexFlag.value + "'>"	

		//----------- General hidden fields:
		+ "<input name='ToIntegra' type='hidden' value='" + frGeneral.document.frmGeneral.ToIntegra.value + "'>"
		+ "<input name='JuriState' Type='hidden' value='" + JurState + "'>"
//  /////////////////////
//	//reserve minor codes
        var IR_MC=''; 

		if(RSV_MC_NEW.length>0){		
			if(RSV_MC_NEW[1][1]==0 && RSV_MC_NEW[1][2]>0){
				alert('Undetermined portion of the indemnity reserve must be zero.')
				Display('rmc');
				return;				
			}else{				
				for(c1=1; c1<RSV_MC_NEW.length; c1++){				

					if(parseFloat(RSV_MC_NEW[c1][2])>0){
						IR_MC=IR_MC + RSV_MC_NEW[c1][1] + ',' + format(RSV_MC_NEW[c1][2],2) + '|'
					}else{
						var c2=1;
						while(c2<RSV_MC.length){
							if(RSV_MC[c2][1]==RSV_MC_NEW[c1][1] && RSV_MC[c2][2]>0){
								IR_MC=IR_MC + RSV_MC_NEW[c1][1] + ',' + format(RSV_MC_NEW[c1][2],2) + '|';
								break;
							}
							c2++
						}
		            }           
					
				}								
			}
		}

		if(MainFlag!='Edit'){
			f +="<input name='txtIR_MC' Type='hidden' value='" + IR_MC + "'>"
			  +	"<input name=txtIndemnity Type='hidden' value='" + format(decommafy(IR_Disp),2) + "'>"
			  + "<input name=txtMedical Type='hidden' value='" + format(decommafy(MR_Disp),2) + "'>"
			  + "<input name=txtExpense Type='hidden' value='" + format(decommafy(ER_Disp),2) + "'>"	
		}else{
			//benefit tech
			f+="<input name='BenTech' Type='hidden' value=" + BenTechVal + ">"

			//RESERVES:     
			var ResCngFlag = 0;

			if (SelfInsuredPrefix != 'Y') { // do not verify limits for self-insured 
			    if (PolicyCC.hasOwnProperty('id')) {  //verify on PolicyCC only
			    
			        var Reserves_Limits_Arr = new Array();
			        Reserves_Limits_Arr = Reserves_Limits_Compare(IR_MC)
			        // if Gross Reserve exceeds limits  - all reserves have to ve saved for approval;
			        if (Reserves_Limits_Arr.length > 0) {
			            var c = 0;
			            while (c < Reserves_Limits_Arr.length) {
			                if (Reserves_Limits_Arr[c] == 'IR') {
			                    if (IR_appr == 0 && IR_Disp > 0) {
			                        IR_appr_new++
			                        IR_appr = format(parseFloat(IR_Disp), 2)
			                    }
			                }
			                if (Reserves_Limits_Arr[c] == 'MR') {
			                    if (MR_appr == 0 && MR_Disp > 0) {
			                        MR_appr_new++
			                        MR_appr = format(parseFloat(MR_Disp), 2)
			                    }
			                }
			                if (Reserves_Limits_Arr[c] == 'ER') {
			                    if (ER_appr == 0 && ER_Disp > 0) {
			                        ER_appr_new++
			                        ER_appr = format(parseFloat(ER_Disp), 2)
			                    }
			                }
			                c++
			            }
			        }
			        
			    }
			}
			
			//if at least one reserve category needs to be approved - all changed reserves should be submitted to the approval queue for claims that about to be re-open
			if (Status == 'C') {
			    if ((format(parseFloat(IR_appr, 10),2) != 0 && format(IR_appr_new,2) > 0) || (format(parseFloat(MR_appr),2) != 0 && format(MR_appr_new,2) > 0) || (format(parseFloat(ER_appr),2) != 0 && format(ER_appr_new,2) > 0)) {
			        if (IR_appr == 0 && IR_Disp>0) {
			            IR_appr_new++
			            IR_appr = format(parseFloat(IR_Disp),2)
			        }
			        if (MR_appr == 0 && MR_Disp>0) {
			            MR_appr_new++
			            MR_appr = format(parseFloat(MR_Disp),2)
			        }
			        if (ER_appr == 0 && ER_Disp>0) {
			            ER_appr_new++
			            ER_appr = format(parseFloat(ER_Disp),2)
			        }	
			    }
			}
			////////////////////
			var m = ''; var r_mc_pr = ''; var MCLine;
			//Reserves for approval:
			if(format(parseFloat(IR_appr),2)!=0 && IR_appr_new>0){
				MCLine = MinorReserveCodes_InLine();
				var txt = "Would you like to save Indemnity Reserve " + dollarize(IR_appr) + " for approval?\n\n" 

				if(confirm(txt)){
				    ReserveApproval_XTRCT(0, 'IR', IR_appr, MCLine, '', ReserveSheet.id, '')
				}
				
				IR_Disp = DisplayReserve(IR); IR_MC= ""; 
			}else{
				r_mc_pr=Print_Reserve_MinorCodes(0); 
				if(DisplayReserve(IR)!=DisplayReserve(IR_Disp)){
				    //if(CheckLimit(IR_Disp,'IR')==false){return}
				    m = CheckLimit(IR_Disp, 'IR');
				    if (m != '') {alert(m); return;};
					ResCngFlag=1;
				} 
				if(r_mc_pr !=''){
					ResCngFlag=1;
				}		
			}

			if(format(parseFloat(MR_appr),2)!=0 && MR_appr_new>0){
				if(confirm("Would you like to save Medical Reserve " + dollarize(MR_appr) + " for approval?\n\n" )){
				    ReserveApproval_XTRCT(0, 'MR', MR_appr, '', '', ReserveSheet.id, '')
				}
				MR_Disp = DisplayReserve(MR);
			}else{
				if(DisplayReserve(MR)!=DisplayReserve(MR_Disp)){
				    //if (CheckLimit(MR_Disp, 'MR') == false) { return }
				    m = CheckLimit(MR_Disp, 'MR');
				    if (m != '') { alert(m); return; };
					ResCngFlag=1;
				}
			}
			if(format(parseFloat(ER_appr),2)!=0 && ER_appr_new>0){
				if(confirm("Would you like to save Expense Reserve " + dollarize(ER_appr) + " for approval?\n\n" )){
				    ReserveApproval_XTRCT(0, 'ER', ER_appr, '', '', ReserveSheet.id, '')
				}
				ER_Disp = DisplayReserve(ER);
			}else{
				if(DisplayReserve(ER)!=DisplayReserve(ER_Disp)){
				    //if(CheckLimit(ER_Disp,'ER')==false){return}
				    m = CheckLimit(ER_Disp, 'ER');
				    if (m != '') { alert(m); return; };
					ResCngFlag=1;
				}
			}
		    //  Clients IncurredApprovalLimit verification
			if (ResCngFlag == 1) {
			    if (!CheckClientsIncurredApprovalLimit()) {
			        var m_ = 'The Incurred exceeds client approval limit $' + DisplayReserve(IncurredApprovalLimit) + '. Client authorization is required.';
			        if (parseFloat(IR_Disp) > 0 && parseFloat(IR_Disp) != parseFloat(IR)) {
			            MCLine = MinorReserveCodes_InLine();
			            ReserveApproval_XTRCT(0, 'IR', IR_Disp, MCLine, '', ReserveSheet.id, m_);
			            IR_Disp = DisplayReserve(IR); IR_MC = "";
			        };
			        if (parseFloat(MR_Disp) > 0 && parseFloat(MR_Disp) != parseFloat(MR)) { ReserveApproval_XTRCT(0, 'MR', MR_Disp, '', '', ReserveSheet.id, m_); MR_Disp = DisplayReserve(MR); };
			        if (parseFloat(ER_Disp) > 0 && parseFloat(ER_Disp) != parseFloat(ER)) { ReserveApproval_XTRCT(0, 'ER', ER_Disp, '', '', ReserveSheet.id, m_); ER_Disp = DisplayReserve(ER); };
			        ResCngFlag = 0;
			    }
			}

            // confirm Demograhics data
            if (ClmOriginal.HR.Employment.employerLocId != '' && EditLoc[1][10] != '' &&  ClmOriginal.HR.Employment.employerNo  != -1) { 
                if(ClmOriginal.HR.Employment.employerLocId != EditLoc[1][10]){
                    if (confirm('Currently, the following ' + LocLegend + ' Id is on the claim : [' + EditLoc[1][10] + '].\nAs per client demographic info, it should be : [' + ClmOriginal.HR.Employment.employerLocId + '].\nWould you like to change it?\n\n[Yes] -  press [OK]\t[No] -  press [Cancel]')) {
                        if (frInfo.document.title != 'claim') {
                            setTimeout("Display('claim')", 100);
                            try {
                                setTimeout("frInfo.document.all['LocLegend'].style.color='red';frInfo.document.all['LocLegend'].title='The expected client`s " + LocLegend + " Id : " + ClmOriginal.HR.Employment.employerLocId + "';", 100);
                            } catch (e) { }
                        }		
                        return;
                    }
                 }
            }

            if (ClmOriginal.HR.Employment.Loc != -1 && ClmOriginal.HR.Employment.Loc != EditLoc[1][0]) {
                alert('The demographic data has been retrieved for ' + LocLegend + ' # ' + ClmOriginal.HR.Employment.LocTxtCod + '.\nYou are trying to save ' + LocLegend + ' # ' + EditLoc[1][10] + '.\nPlease refresh the demographic data and proceed with the update.');
                if (frInfo.document.title != 'claimant') {
                    setTimeout("Display('claimant')", 100);
                }
                return (false);
            }
            //  end   Demograhics data confrimation

			//reserves to server:
			if (ResCngFlag == 1 || frGeneral.document.frmGeneral.cboAsgmtType.value != Assignt){
			    //
			    if(!ClntPolCC_ReserveChangeAllowed()){  //2015
                    return (false);	
                }
			    //WORKCOMP VALIDATION WILL BE ENFORCED ONLY FOR RESERVE CHANGES:
			    if (frGeneral.document.frmGeneral.cboAsgmtType.value=='F') {    //!!!!!!!!!!	only FULL ADJUSTMENTS ARE SUBJECT OF VALIDATION
			    	if(Validate_WorkComp()==false){
			    			return (false);	
			    	}
			    }

				// var txt=DisplayReserveChanges(r_mc_pr,0);				
				//var r_mc_pr=Print_Reserve_MinorCodes(1)
			    //var txt_AdjNotes=DisplayReserveChanges(r_mc_pr,1); 
				var txt_AdjNotes = DisplayReserveChanges();
				f +="<input name=txtIndemnity Type='hidden' value='" + format(decommafy(IR_Disp),2) + "'>"
					+"<input name=txtMedical Type='hidden' value='" + format(decommafy(MR_Disp),2) + "'>"
					+"<input name=txtExpense Type='hidden' value='" + format(decommafy(ER_Disp),2) + "'>"
					+"<input name='txtIR_MC' Type='hidden' value='" + IR_MC + "'>"
					+ "<input name='ReserveFlag' Type='hidden' value='Y'>"
                f +=(ResCngFlag == 1) ? "<input name='ReserveSheetId' Type='hidden' value=" + ReserveSheet.id + ">" : "<input name='ReserveSheetId' type='hidden' value=0>"
                f += "<input name='txtToPrint' type='hidden' value=" + '"' + txt_AdjNotes + '"' + ">"
					//+"<textarea style=\"FONT-WEIGHT: bolder; FONT-SIZE: 10pt; FONT-FAMILY: 'MS Sans Serif';\" COLS=680 ROWS=100 name=txtPrintToSave wrap=OFF>" + txt_AdjNotes  + "</textarea>"
			}else{
			    f += "<input name='ReserveFlag' type='hidden' value=''>"
			    f += "<input name='ReserveSheetId' type='hidden' value=0>"			
				f+=	"<input name=txtIndemnity type='hidden' value='" + format(decommafy(IR_Disp),2) + "'>"
					+"<input name=txtMedical type='hidden' value='" + format(decommafy(MR_Disp),2) + "'>"
					+"<input name=txtExpense type='hidden' value='" + format(decommafy(ER_Disp),2) + "'>"
					+ "<input name='txtIR_MC' type='hidden' value='" + IR_MC + "'>"
                    + "<input name='txtToPrint' type='hidden' value=''>"
			}
	
	
			f +=YSIForServer()
		}	
		f +="</PRE></form></body></html>"

		// diary settings notifications:
		if (SetDiary != null) {
		    SetDiary.SetOpenFileDiary();
		}

    	frGeneral.document.open();
		frIndex.document.open(); 
		frButtons.document.open();
		frInfo.document.open();
		frInfo.document.write('Please wait ... processing');

		frPrint.document.open();
		frPrint.document.write(f);
		frPrint.document.close();	
		frPrint.document.frmMyClaim.submit();
	}

//SENDING DATA ON SCREEN:
	function Display(name) {
	    if (name == 'general') {
	        frGeneral.document.open();
	        frGeneral.document.write(general);
	        frGeneral.document.close();

 	        frButtons.document.open();
	        frButtons.document.write(ActionButtons());
	        frButtons.document.close();

	        if (frGeneral.document.getElementById("ClamLossStHlp") != null &&  frGeneral.document.getElementById("cboLossState") != null) {
	            mmCreateHelpSpot(1, "Loss State-related guidelines - click here"   // title
                                , frGeneral.document.getElementById("ClamLossStHlp")       // id of element where to created Help Spot
                                , function (helpId) {
                                    var state = frGeneral.document.getElementById("cboLossState").value; // selected state value of selected
                                    if (state === '') {
                                        alert('Loss State-related guidelines are available for US states only')
                                    } else {
                                        var params = { "state": state };
                                        mmOpenHelp(helpId, params);
                                    }
                                }
                          );
	        }
 
            //delegate click event to frGeneral for Adjuster list
            $("#frmGeneral", frGeneral.document).on("click", "#cboAdjuster", function() {
                                    if ($(this).find("option").length < 4) {
                                        AdjusterList_XTRCT('');
                                        AdjustersPopulate();
                                    }
                                });
             $("#frmGeneral", frGeneral.document).on("click", "#cboAsAdjuster", function () {
                                    if ($(this).find("option").length < 4) {
                                        AdjusterList_XTRCT('');
                                        AsAdjusterListPopulate();
                                    }
                                }); 

	        frGeneral.document.frmGeneral.cbolLineBus.value = Unit;
            if (frGeneral.document.frmGeneral.cboLossState.selectedIndex == -1) {  //frGeneral.document.frmGeneral.cboLossState.value.length == 0 &&            
                frGeneral.document.frmGeneral.cboLossState.value = trim(State);
            };
	        frGeneral.document.frmGeneral.cboAsgmtType.value = Assignt;

            if (MaintanenceClaim == 'Y') {
	            frGeneral.document.frmGeneral.chkMaintanenceClaim.checked=true;
	        }
	        if (UserRole.indexOf('ReopenClaim') == -1 && MainFlag == 'Edit') {
	            frGeneral.document.frmGeneral.chkMaintanenceClaim.disabled = true;
            }
	        // Country settings:
	        $("#cboLossCountry", frGeneral.document).on("change", function(event) {
	            var v = $(this).val();
	            var list = CountryRegions.refreshStateList(v);

	            var ls = frGeneral.document.frmGeneral.cboLossState;
	            ls.length = 0;

	            LossCountry = v;
	            State = '';
	            LossStateId = 0;
	            $('#cboLossState', frGeneral.document).append(list);
	            
	            ls = frGeneral.document.frmGeneral.cboLossState;
	            if ($("#cboLossCountry option:selected", frGeneral.document).val() == 'USA') { $("#ClamLossStLbl", frGeneral.document).text('Loss State:').css({ 'font-weight': 'bold' }); } else { $("#ClamLossStLbl", frGeneral.document).text("Loss Region:").css({ 'font-weight': 'bold' }); };
	            if (ls.options.length == 1) { ls[0].selected = true } else { ls.value = State; };
	            CountyList_Get("GET THE LIST");
                if (v == 'USA' || LossCountry == 'USA' || MainFlag != 'Edit') {
	                AdjustersPopulate();
	            }

	        });

	        MyTitle = "claim"

	        Off_Adj_Populate();
	        Display("claim");
	        CountyList_Get("GET THE LIST");
	        
	        $('#cboLossCountry', frGeneral.document).val(LossCountry);
	        if ($("#cboLossCountry", frGeneral.document).val() == 'USA') { $("#ClamLossStLbl", frGeneral.document).text('Loss State:').css({ 'font-weight': 'bold' }); } else { $("#ClamLossStLbl", frGeneral.document).text("Loss Region:").css({ 'font-weight': 'bold' }); };

	        if (LossStateId != 0) {
	            $('#cboLossState', frGeneral.document).find('option[stateId="' + LossStateId + '"]').attr("selected", "selected");
	        }
	        if (MainFlag != 'Edit') {
	            frButtons.document.frmButtons.ClaimantFlag.value = 1;
	        }
	    } else {
	        if (MyTitle != name) {
	            if (MainFlag != 'Edit') {
	                if (typeof (frGeneral.document.frmGeneral.cboOffice) == "undefined") {
	                    alert('Please select Company and Control Branch')
	                    return (false);
	                }

	            }

	            if (Var_Save(MyTitle) == false) { return } //Current screen; FALSE - failed screen requirements

	            MyTitle = name; 		//frInfo.document.title;

	            var cI = I;
	            Index(Unit); //Index(MyPolTyp);
	            if (cI != I) {
	                frIndex.document.open();
	                frIndex.document.write(I);
	                frIndex.document.close();
	            }

	            Variable_Populate(name); //New screen  // in case of assist claim I need to overwrite  frIndex
	        }
	    }

	    //ACTION BUTTOMS RE-ACTIVATION:
		if (name == "vehicle" || name  == "doc") {
		    try {
		         if (MainFlag == 'Edit') {
		            frButtons.document.frmButtons.cmdEdit.disabled = true;   // block high level action button
		        } else {
		            frButtons.document.frmButtons.cmdOpenNew.disabled = true;
		        }
		    } catch (e) { };
		} else {
		    try {
		        if (MainFlag == 'Edit') {
		            frButtons.document.frmButtons.cmdEdit.disabled = false;    // block high level action button
		        } else {
		            frButtons.document.frmButtons.cmdOpenNew.disabled = false;		             
		        }
		    } catch (e) { };
		    
		    try {
		        if (UserRole.indexOf('EditFile') != -1 || MainFlag != 'Edit') {
		            if (MainFlag == 'Edit') {  //frButtons.document.
		                frButtons.document.frmButtons.cmdEdit.disabled = false;
		            } else {
		                frButtons.document.frmButtons.cmdOpenNew.disabled = false;
		            }
		        }
		        if (ClientNumber == 6892) {
		            frButtons.document.frmButtons.cmdEdit.disabled = true; //Prefix Lockout
		        }
		    } catch (e) { }
        }


		if (name == "claim") {
		    frInfo.document.open();
		    frInfo.document.write(claim);
		    frInfo.document.close();

		    try {
		        if (CanChangeSuper() == false) {
		            frGeneral.document.frmGeneral.cboSuper.disabled = true;
		        }
		    } catch (e) {
		    }

		    try {
		        if (frGeneral.document.frmGeneral.cbolLineBus.value == 'PL')
		        { frInfo.document.frmClaim.cboInsProduct.value = InsProd; }
		    } catch (e) { }

		    try {
		        if (Claim_Assigns.length > 0) {
		            if (Claim_Assigns[0].clmno_assist == ClaimNumber) {
		                //assist files have unchangable type of assignments:
		                frGeneral.document.frmGeneral.cboAsgmtType.disabled = true;
		            }
		        }
		    } catch (e) { }

		    LineOfBus_Cov_Populate();

		    if (frInfo.document.frmClaim.cboJuriState != null) {
		        try {
		            frInfo.document.frmClaim.cboJuriState.value = JurState;
		        } catch (e) { };
		    }

		    // desc
		    try {
		        frInfo.document.getElementById("txtLossDescr").value = Desc;
		        frInfo.document.getElementById("txtLossDescr").maxLength = '80';

		        frInfo.document.getElementById("txtLossDescr2").value = Desc2;
		        frInfo.document.getElementById("txtLossDescr2").maxLength = '80';

		        frInfo.document.getElementById("txtLossDescr3").value = Desc3;
		        frInfo.document.getElementById("txtLossDescr3").maxLength = '100';
		    } catch (e) { };

		    // external claim #s  default settings:
		    try {
		        frInfo.document.frmClaim.txtCarrierClmno.value = CarClmNo;
		        frInfo.document.frmClaim.txtCarrierClmno.maxLength = "20";

		        frInfo.document.frmClaim.txtClientsNum.value = CltClmNo;
		        frInfo.document.frmClaim.txtClientsNum.maxLength = "20";

		        frInfo.document.frmClaim.txt3PartyNo.value = ThirdNum;
		        frInfo.document.frmClaim.txt3PartyNo.maxLength = "25"; 
		       // frInfo.document.frmClaim.txt3PartyNo.disabled = true;
		    } catch (e) { };

		    if (ROLEUPNUM == '1270') {
		        if (ROLEUPNUM_1270_VERIFY() == false && UserBranch != '152') {
		            frInfo.document.frmClaim.txtCarrierClmno.disabled = true;
		        } else {
		            $('#txtCarrierClmno', frInfo.document).on('blur', $(this), CarClmNo_1270);
		        }
		    }

            //
		    switch (SCREEN) {
		        case "AIGRM":
		            frInfo.document.frmClaim.txtClientsNum.maxLength = "15";

		            frInfo.document.frmClaim.cboCPrsn.value = CPerson;
		            try { frInfo.document.frmClaim.cboCause.value = CauseCod; } catch (e) { };

		            if (typeof frInfo.document.frmClaim.cboINJTYP == 'undefined' || frInfo.document.frmClaim.cboINJTYP.value != InjTyp) {
		                try {
		                    frInfo.document.getElementById("InjuList").innerHTML = '<select name="cboINJTYP">' + ((TYPE_Loss == 'PD') ? DamageTypeList : InjTypeList) + '</select>';
		                    frInfo.document.getElementById("Inju").innerHTML = (TYPE_Loss == 'PD') ? "Damage Type:" : "Injury Type:";
		                } catch (e) { };

		                try { frInfo.document.frmClaim.cboINJTYP.value = InjTyp; } catch (e) { };

		            }
		            if (typeof frInfo.document.frmClaim.cboPOBCOD == 'undefined' || frInfo.document.frmClaim.cboPOBCOD.value != PobCod) {
		                try {
		                    frInfo.document.getElementById("PobCodList").innerHTML = '<select name="cboPOBCOD">' + ((TYPE_Loss == 'PD') ? PobCodPDList : PobCodBIList) + '</select>';
		                    frInfo.document.getElementById("PobCod").innerHTML = (TYPE_Loss == 'PD') ? "Peril Type:" : "Body part:";
		                } catch (e) { };

		                try { frInfo.document.frmClaim.cboPOBCOD.value = PobCod; } catch (e) { };
		            }
		            // acc description:
		            if (ClmOriginal.Aigrm_AccDesc_List.optionsList.length < 50) {
		                ClmOriginal.Aigrm_AccDesc_List.refresh(Unit, AccDsc, AccDsc_Id, false);
		            }
		            if (typeof frInfo.document.frmClaim.cboACCDSC == 'undefined' || frInfo.document.frmClaim.cboACCDSC.value != AccDsc) {
		                try {
		                    frInfo.document.getElementById("ADesList").innerHTML = 'Accident Desc:<select name="cboACCDSC" onclick="if(this.options.length<3){this.options.length=0; parent.ClmOriginal.Aigrm_AccDesc_List.refresh(parent.Unit, 0, 0, true)};">' + ClmOriginal.Aigrm_AccDesc_List.optionsList + '</select>';
		                } catch (e) { };

		                try { frInfo.document.frmClaim.cboACCDSC.value = AccDsc; } catch (e) { };
		            }
		            DiableReservePendingApproval();
		            CountyPopulate();
		            if (Unit == 'WC') {
		                frInfo.document.getElementById('3-rdlbl').innerText = 'Juris #:'
		            }

		            try {
		                if (Loss_Cause.length == 0 && Unit != 'WC') {
		                    Loss_Cause_List_XTRCT();
		                } else {
		                    frInfo.document.all['cboLoss_Cause'].value = LossCause;
		                    TYPE_Loss_Set(frInfo.document.frmClaim.cboLoss_Cause.options[frInfo.document.frmClaim.cboLoss_Cause.options.selectedIndex].title, frInfo.document.frmClaim.cboCov.options[frInfo.document.frmClaim.cboCov.options.selectedIndex].text)
		                }
		            } catch (e) { }

		            break
		        case "AIGRM_Rental":
		            frInfo.document.frmClaim.txtClientsNum.maxLength = "15";

		            frInfo.document.frmClaim.cboRentState.value = RState;		           
		            frInfo.document.frmClaim.cboCPrsn.value = CPerson;
		            try { frInfo.document.frmClaim.cboCause.value = CauseCod; } catch (e) { };

		            if (typeof frInfo.document.frmClaim.cboINJTYP == 'undefined' || frInfo.document.frmClaim.cboINJTYP.value != InjTyp) {
		                try {
		                    frInfo.document.getElementById("InjuList").innerHTML = '<select name="cboINJTYP">' + ((TYPE_Loss == 'PD') ? DamageTypeList : InjTypeList) + '</select>';
		                    frInfo.document.getElementById("Inju").innerHTML = (TYPE_Loss == 'PD') ? "Damage Type:" : "Injury Type:";
		                } catch (e) { };

		                try { frInfo.document.frmClaim.cboINJTYP.value = InjTyp; } catch (e) { };
		            }

		            if (typeof frInfo.document.frmClaim.cboPOBCOD == 'undefined' || frInfo.document.frmClaim.cboPOBCOD.value != PobCod) {
		                try {
		                    frInfo.document.getElementById("PobCodList").innerHTML = '<select name="cboPOBCOD">' + ((TYPE_Loss == 'PD') ? PobCodPDList : PobCodBIList) + '</select>';
		                    frInfo.document.getElementById("PobCod").innerHTML = (TYPE_Loss == 'PD') ? "Peril Type:" : "Body part:";
		                } catch (e) { };

		                try { frInfo.document.frmClaim.cboPOBCOD.value = PobCod; } catch (e) { };
		            }

		            // acc description:
		            if (ClmOriginal.Aigrm_AccDesc_List.optionsList.length < 50) {
		                ClmOriginal.Aigrm_AccDesc_List.refresh(Unit, AccDsc, AccDsc_Id, false);
		            }
		            if (typeof frInfo.document.frmClaim.cboACCDSC == 'undefined' || frInfo.document.frmClaim.cboACCDSC.value != AccDsc) {
		                try {
		                    frInfo.document.getElementById("ADesList").innerHTML = 'Accident Desc:<select name="cboACCDSC" onclick="if(this.options.length<3){this.options.length=0; parent.ClmOriginal.Aigrm_AccDesc_List.refresh(parent.Unit, 0, 0, true)};">' + ClmOriginal.Aigrm_AccDesc_List.optionsList + '</select>';
		                } catch (e) { };

		                try { frInfo.document.frmClaim.cboACCDSC.value = AccDsc; } catch (e) { };
		            }
		            //
		            
                    DiableReservePendingApproval();
                    CountyPopulate();
		            try {
		                if (Loss_Cause.length == 0 && Unit != 'WC') {
		                    Loss_Cause_List_XTRCT();
		                } else {
		                    frInfo.document.all['cboLoss_Cause'].value = LossCause;
		                    TYPE_Loss_Set(frInfo.document.frmClaim.cboLoss_Cause.options[frInfo.document.frmClaim.cboLoss_Cause.options.selectedIndex].title, frInfo.document.frmClaim.cboCov.options[frInfo.document.frmClaim.cboCov.options.selectedIndex].text)
		                }
		            } catch (e) { }
		            break
		        case "Commerce":
		            frInfo.document.frmClaim.cboInsState.value = IState;
		            frInfo.document.frmClaim.cboCPrsn.value = CPerson;
		            frInfo.document.frmClaim.cboIPrsn.value = IPerson;
		            if (NCCI != '') { frInfo.document.frmClaim.cboNCCI.value = NCCICod };

		            CountyPopulate();
		            if (Unit == 'WC') {
		                frInfo.document.getElementById('3-rdlbl').innerText = 'Board Case #:'
		            } else {
		                frInfo.document.getElementById('3-rdlbl').innerText = '3-rd Party #:'
		            }
		            try {
		                frInfo.document.frmClaim.cboAnatomy.value = BodyPartCod;
		                //								    frInfo.document.frmClaim.cboNCCI_SOI.value = GL_NCCI_SOI;
		                //								    frInfo.document.frmClaim.cboNCCI_NOI.value = GL_NCCI_NOI;
		            } catch (e) { };
		            //specific rules for Robert Plan:
		            if (frGeneral.document.frmGeneral.txtClntNo.value == 2094 || frGeneral.document.frmGeneral.txtClntNo.value == 11) {
		                frInfo.document.frmClaim.txtCarrierClmno.disabled = true;
		                if (frInfo.document.frmClaim.txtCarrierClmno.value != '') {
		                    frGeneral.document.frmGeneral.txtAccDT.disabled = true;
		                    //frGeneral.document.frmGeneral.txtReportDate.disabled=true;
		                    frGeneral.document.frmGeneral.txtPolNo.disabled = true;
		                    frGeneral.document.frmGeneral.txtEffDT.disabled = true;
		                    frGeneral.document.frmGeneral.txtExpDT.disabled = true;
		                    frGeneral.document.frmGeneral.cbolLineBus.disabled = true;
		                    frGeneral.document.frmGeneral.cboLossState.disabled = true;
		                    frGeneral.document.frmGeneral.cboLossCountry.disabled = true;
		                    frInfo.document.frmClaim.cboCov.disabled = true;
		                }
		            }
		            DiableReservePendingApproval();

		            try {
		                if (Loss_Cause.length == 0 && Unit != 'WC') {
		                    Loss_Cause_List_XTRCT();
		                } else {
		                    frInfo.document.all['cboLoss_Cause'].value = LossCause;
		                    TYPE_Loss_Set(frInfo.document.frmClaim.cboLoss_Cause.options[frInfo.document.frmClaim.cboLoss_Cause.options.selectedIndex].title, frInfo.document.frmClaim.cboCov.options[frInfo.document.frmClaim.cboCov.options.selectedIndex].text)
		                }
		            } catch (e) { }
		            break
		        case "Commerce_2":
		            frInfo.document.frmClaim.cboInsState.value = IState;
		            frInfo.document.frmClaim.cboCPrsn.value = CPerson;
		            frInfo.document.frmClaim.cboIPrsn.value = IPerson;
		            try {frInfo.document.frmClaim.cboCause.value = CauseCod;} catch (e) { };
		            if (NCCI != '') { frInfo.document.frmClaim.cboNCCI.value = NCCICod };

		            try {
		                if (Loss_Cause.length == 0 && Unit != 'WC') {
		                    Loss_Cause_List_XTRCT();
		                } else {
		                    frInfo.document.all['cboLoss_Cause'].value = LossCause;
		                    TYPE_Loss_Set(frInfo.document.frmClaim.cboLoss_Cause.options[frInfo.document.frmClaim.cboLoss_Cause.options.selectedIndex].title, frInfo.document.frmClaim.cboCov.options[frInfo.document.frmClaim.cboCov.options.selectedIndex].text)
		                }
		            } catch (e) { }


		            CountyPopulate();
		            if (Unit == 'WC') {
		                frInfo.document.getElementById('3-rdlbl').innerText = 'Board Case #:'
		                frInfo.document.getElementById('ResvLosslbl').innerText = 'Indemnity:'
		            } else {
		                frInfo.document.getElementById('3-rdlbl').innerText = '3-rd Party #:'
		                frInfo.document.getElementById('ResvLosslbl').innerText = 'Loss:'
		            }
		            //specific rules for Robert Plan:
		            if (frGeneral.document.frmGeneral.txtClntNo.value == 2094 || frGeneral.document.frmGeneral.txtClntNo.value == 11) {
		                frInfo.document.frmClaim.txtCarrierClmno.disabled = true;
		                if (frInfo.document.frmClaim.txtCarrierClmno.value != '') {
		                    frGeneral.document.frmGeneral.txtAccDT.disabled = true;
		                    //frGeneral.document.frmGeneral.txtReportDate.disabled=true;
		                    frGeneral.document.frmGeneral.txtPolNo.disabled = true;
		                    frGeneral.document.frmGeneral.txtEffDT.disabled = true;
		                    frGeneral.document.frmGeneral.txtExpDT.disabled = true;
		                    frGeneral.document.frmGeneral.cbolLineBus.disabled = true;
		                    frGeneral.document.frmGeneral.cboLossState.disabled = true;
		                    frGeneral.document.frmGeneral.cboLossCountry.disabled = true;
		                    frInfo.document.frmClaim.cboCov.disabled = true;
		                }
		            }
		            DiableReservePendingApproval();
		            break;
		        case "MainWorkComp":
		            //frInfo.document.frmClaim.cboInsState.value=IState;
		            frInfo.document.frmClaim.cboCPrsn.value = CPerson;
		            //frInfo.document.frmClaim.cboIPrsn.value=IPerson;
		            if (NCCI != '') { frInfo.document.frmClaim.cboNCCI.value = NCCICod };
		            //LineOfBus_Cov_Populate();								
		            try { frInfo.document.frmClaim.cboCause.value = CauseCod } catch (e) { };
		            try { frInfo.document.frmClaim.cboAnatomy.value = BodyPartCod; } catch (e) { };
		            CountyPopulate();
		            frInfo.document.getElementById('3-rdlbl').innerText = 'Juris #:'
		            if (Unit == 'WC') {
		                frInfo.document.getElementById('ResvLosslbl').innerText = 'Indemnity:'
//		                if (UserRole.indexOf('EDIManagement') == -1) {
//		                    frInfo.document.frmClaim.txt3PartyNo.disabled = true;
//		                }
		            } else {
		                frInfo.document.getElementById('ResvLosslbl').innerText = 'Loss:'
		            }
		            //specific rules for Robert Plan:
		            if (frGeneral.document.frmGeneral.txtClntNo.value == 2094 || frGeneral.document.frmGeneral.txtClntNo.value == 11) {
		                frInfo.document.frmClaim.txtCarrierClmno.disabled = true;
		                if (frInfo.document.frmClaim.txtCarrierClmno.value != '') {
		                    frGeneral.document.frmGeneral.txtAccDT.disabled = true;
		                    //frGeneral.document.frmGeneral.txtReportDate.disabled=true;
		                    frGeneral.document.frmGeneral.txtPolNo.disabled = true;
		                    frGeneral.document.frmGeneral.txtEffDT.disabled = true;
		                    frGeneral.document.frmGeneral.txtExpDT.disabled = true;
		                    frGeneral.document.frmGeneral.cbolLineBus.disabled = true;
		                    frGeneral.document.frmGeneral.cboLossState.disabled = true;
		                    frGeneral.document.frmGeneral.cboLossCountry.disabled = true;
		                    frInfo.document.frmClaim.cboCov.disabled = true;
		                }
		            }
		            DiableReservePendingApproval();

		            if (MainFlag == 'Edit') {
		                if (BenTechName == '') {
		                    c = 0
		                    while (c < frInfo.document.all['cboBenTech'].options.length) {
		                        if (frInfo.document.all['cboBenTech'].options[c].id == BenTechVal) {
		                            frInfo.document.all['cboBenTech'].options[c].selected = true;
		                            BenTechName = frInfo.document.all['cboBenTech'].options[c].value;
		                            break;
		                        }
		                        c++
		                    }
		                } else {
		                    frInfo.document.all['cboBenTech'].value = BenTechName;
		                }

		                if (UserRole.indexOf('EditAssist') == -1) {
		                    frInfo.document.frmClaim.cboBenTech.disabled = true;
		                    frInfo.document.frmClaim.cboBenTech_incr.style.display = "none";
		                    frInfo.document.frmClaim.cboBenTech_incr_btn.style.display = "none";
		                }
		            }


		            try {
		                frInfo.document.getElementById('InjuBodyl').style.fontWeight = 'bold';
		            } catch (e) { }
		            break;
		        case "CommerceWorkComp":
		            frInfo.document.frmClaim.cboCPrsn.value = CPerson;
		            if (NCCI != '') { frInfo.document.frmClaim.cboNCCI.value = NCCICod };
		            //LineOfBus_Cov_Populate();								
		            try { frInfo.document.frmClaim.cboCause.value = CauseCod } catch (e) { };
		            try { frInfo.document.frmClaim.cboAnatomy.value = BodyPartCod; } catch (e) { };
		            CountyPopulate();
		            frInfo.document.getElementById('3-rdlbl').innerText = 'Juris #:'
		            if (Unit == 'WC') {
		                frInfo.document.getElementById('ResvLosslbl').innerText = 'Indemnity:';
//		                if (UserRole.indexOf('EDIManagement') == -1) {
//		                    frInfo.document.frmClaim.txt3PartyNo.disabled = true;
//		                }
		            } else {
		                frInfo.document.getElementById('ResvLosslbl').innerText = 'Loss:'
		            }
		            //specific rules for Robert Plan:
		            if (frGeneral.document.frmGeneral.txtClntNo.value == 2094 || frGeneral.document.frmGeneral.txtClntNo.value == 11) {
		                frInfo.document.frmClaim.txtCarrierClmno.disabled = true;
		                if (frInfo.document.frmClaim.txtCarrierClmno.value != '') {
		                    frGeneral.document.frmGeneral.txtAccDT.disabled = true;
		                    frGeneral.document.frmGeneral.txtPolNo.disabled = true;
		                    frGeneral.document.frmGeneral.txtEffDT.disabled = true;
		                    frGeneral.document.frmGeneral.txtExpDT.disabled = true;
		                    frGeneral.document.frmGeneral.cbolLineBus.disabled = true;
		                    frGeneral.document.frmGeneral.cboLossState.disabled = true;
		                    frGeneral.document.frmGeneral.cboLossCountry.disabled = true;
		                    frInfo.document.frmClaim.cboCov.disabled = true;
		                }
		            }
		            DiableReservePendingApproval();

		            if (MainFlag == 'Edit') {
		                if (BenTechName == '') {
		                    c = 0
		                    while (c < frInfo.document.all['cboBenTech'].options.length) {
		                        if (frInfo.document.all['cboBenTech'].options[c].id == BenTechVal) {
		                            frInfo.document.all['cboBenTech'].options[c].selected = true;
		                            BenTechName = frInfo.document.all['cboBenTech'].options[c].value;
		                            break;
		                        }
		                        c++
		                    }
		                } else {
		                    frInfo.document.all['cboBenTech'].value = BenTechName;
		                }

		                if (UserRole.indexOf('EditAssist') == -1) {
		                    frInfo.document.frmClaim.cboBenTech.disabled = true;
		                    frInfo.document.frmClaim.cboBenTech_incr.style.display = "none";
		                    frInfo.document.frmClaim.cboBenTech_incr_btn.style.display = "none";
		                }
		            }
		            try {
		                frInfo.document.getElementById('InjuBodyl').style.fontWeight = 'bold';
		            } catch (e) { }
		            break;
		        case "AdminPolicy":
		            if (MyPOList.length > 0 && PolInx > -1) {
		                frGeneral.document.frmGeneral.txtPolNo.value = MyPOList[PolInx].PolNo;
		            }
		            frInfo.document.frmClaim.cboInsState.value = IState;
		            frInfo.document.frmClaim.cboCPrsn.value = CPerson;
		            frInfo.document.frmClaim.cboIPrsn.value = IPerson;
		            if (NCCI != '') { frInfo.document.frmClaim.cboNCCI.value = NCCICod };

		            try {
		                if (Loss_Cause.length == 0 && Unit != 'WC') {
		                    Loss_Cause_List_XTRCT();
		                } else {
		                    frInfo.document.all['cboLoss_Cause'].value = LossCause;
		                    TYPE_Loss_Set(frInfo.document.frmClaim.cboLoss_Cause.options[frInfo.document.frmClaim.cboLoss_Cause.options.selectedIndex].title, frInfo.document.frmClaim.cboCov.options[frInfo.document.frmClaim.cboCov.options.selectedIndex].text)
		                }
		            } catch (e) { }

		            CountyPopulate();
		            if (Unit == 'WC') {
		                frInfo.document.getElementById('3-rdlbl').innerText = 'Board Case #:'
		                frInfo.document.getElementById('ResvLosslbl').innerText = 'Indemnity:'
		            } else {
		                frInfo.document.getElementById('3-rdlbl').innerText = '3-rd Party #:'
		                frInfo.document.getElementById('ResvLosslbl').innerText = 'Loss:'
		            }
		            DiableReservePendingApproval();
		            break;
		        case "Rental":
		            frInfo.document.frmClaim.txt3PartyNo.disabled = false;
		            
		            frInfo.document.frmClaim.cboRentState.value = RState;
		            frInfo.document.frmClaim.cboCPrsn.value = CPerson;
		            frInfo.document.frmClaim.cboCause.value = CauseCod;
		            try {
		                if (Loss_Cause.length == 0 && Unit != 'WC') {
		                    Loss_Cause_List_XTRCT();
		                } else {
		                    frInfo.document.all['cboLoss_Cause'].value = LossCause;
		                    TYPE_Loss_Set(frInfo.document.frmClaim.cboLoss_Cause.options[frInfo.document.frmClaim.cboLoss_Cause.options.selectedIndex].title, frInfo.document.frmClaim.cboCov.options[frInfo.document.frmClaim.cboCov.options.selectedIndex].text)
		                }
		            } catch (e) { }

		            CountyPopulate();
		            DiableReservePendingApproval();
		            break
		        case "M_A":
		            //LineOfBus_Cov_Populate();			
		            break;
		        case "Main":
		            frInfo.document.frmClaim.cboCPrsn.value = CPerson;
		            frInfo.document.frmClaim.cboCause.value = CauseCod;
		            if (NCCI != '') { frInfo.document.frmClaim.cboNCCI.value = NCCICod }
		            try {
		                if (Loss_Cause.length == 0 && Unit != 'WC') {
		                    Loss_Cause_List_XTRCT();
		                } else {
		                    frInfo.document.all['cboLoss_Cause'].value = LossCause;
		                    TYPE_Loss_Set(frInfo.document.frmClaim.cboLoss_Cause.options[frInfo.document.frmClaim.cboLoss_Cause.options.selectedIndex].title, frInfo.document.frmClaim.cboCov.options[frInfo.document.frmClaim.cboCov.options.selectedIndex].text)
		                }
		            } catch (e) { }

		            //frInfo.document.frmClaim.txtClaimant.focus();
		            CountyPopulate();
		            if (Unit == 'WC') {
		                frInfo.document.getElementById('3-rdlbl').innerText = 'Board Case #:'
		                frInfo.document.getElementById('ResvLosslbl').innerText = 'Indemnity:'
		            } else {
		                frInfo.document.getElementById('3-rdlbl').innerText = '3-rd Party #:'
		                frInfo.document.getElementById('ResvLosslbl').innerText = 'Loss:'
		            }
		            DiableReservePendingApproval();

		            try {
		                frInfo.document.frmClaim.cboAnatomy.value = BodyPartCod;
		            } catch (e) { };

		            if (ClientNumber == 2232 && UserBranch != '152') {  //ticket 37357
		                frInfo.document.frmClaim.txtCarrierClmno.disabled = true;
		            }
		    }
		}
		else if (name == "workcomp") {
		    frInfo.document.open();
		    frInfo.document.write(workcomp);
		    frInfo.document.close();
		    //frInfo.document.frmClaim.txtWC_Rate.focus();
		    frButtons.document.frmButtons.WorkcompFlag.value = 1
		    frInfo.document.frmClaim.cboWC_JuriState.value = JurState;
		    frInfo.document.frmClaim.cboWC_JobCode.value = JobCodeCod;
		    frInfo.document.frmClaim.cboWC_EmplState.value = EmplSTATE;
		    frInfo.document.frmClaim.cboEmplStatus.value = EmplStatusCod;

		    //frInfo.document.frmClaim.cboWC_Occup.value = Occup;		    
		    frInfo.document.frmClaim.cboWC_Occup.value = OccupID;
		    if (MainFlag == 'Edit') {
		        var c = 0
		        frInfo.document.getElementById("lblWC_Employer").innerHTML = (Employer == "C") ? 'Yes' : 'No';		        
		    }
		    frInfo.document.frmClaim.began_am_pm.value = began_am_pm;

		    var reV = /^(\d*)_/gi
		    var foundArr1 = reV.exec(ClmOriginal.CovCod)
		    var covCod_ = foundArr1[1]

		    if (ClmSalarySetting.SalaryCalcStates.indexOf(JurState) > -1 && covCod_ == 24) {
	            frInfo.document.frmClaim.txtWC_WeeklyWage.disabled = true;
	            if (MainFlag == 'Edit') {
	                frInfo.document.getElementById("spnWageCalculate").innerHTML = "Set";
	                frInfo.document.getElementById("spnWageCalculate").style.textDecoration = "underline";
	                $("#spnWageCalculate", frInfo.document).on("click", function () {
	                    WageCalculation_XTRCT();
	                });
	            }
		    }
		    if (MainFlag == 'Edit') {
		        ClmOriginal.HistLinks[name] = HistLinks_XTRCT(name, ClaimNumber, 'ClmNo');
		        Populate_HistLinks(frInfo.document, ClmOriginal.HistLinks[name]);
		    }
		}
		else if (name == "workcomp_ca") {
		    frInfo.document.open();
		    frInfo.document.write(workcomp_ca);
		    frInfo.document.close();

		    frInfo.document.frmClaim.cboNCCI_SOI.value = NCCI_SOI;
		    frInfo.document.frmClaim.cboNCCI_NOI.value = NCCI_NOI;
		    frInfo.document.frmClaim.cboNCCI_SEV.value = NCCI_SEV;
		    //frInfo.document.frmClaim.cboPmntSuspReason.value=PmntSuspension;
		    frInfo.document.frmClaim.cboWC_ClassCode.value = CLASSCode;
		    frInfo.document.frmClaim.cboMinor_LossType.value = Minor_LossType;

		    if (UserRole.indexOf('MLTBillingIndicator') == -1 && UserRole.indexOf('ReopenClaim') == -1) {
		        if (MLT_BillingCodes.indexOf(Minor_LossType + ',') > -1) {
		            frInfo.document.frmClaim.cboMinor_LossType.disabled = true;
		        }
		    }

		    frInfo.document.frmClaim.cboWC_InjOnEmplP.value = InjOnEmplP;
		    frInfo.document.frmClaim.cboPreExistDisability.value = PreExistDisability;
		    frInfo.document.frmClaim.cboADE_CDE.value = AOE;
		    frInfo.document.frmClaim.cboWC_LossCov.value = LossCovCode;

		    frInfo.document.frmClaim.cboBoardFraud.value = wc_board_fraud;
		    frInfo.document.frmClaim.cboInitialTreatment.value = initialTreatmentType; 
		    //				frInfo.document.frmClaim.cboSettlType.value=settlement_type;
		    frButtons.document.frmButtons.WorkcompFlag.value = 1

		    if (MainFlag == 'Edit') {
		        frInfo.document.frmClaim.chkSalaryContinued.disabled = (ActiveBenTypes == 0) ? false : true;
		        frInfo.document.frmClaim.chkSalaryContinued.title = (ActiveBenTypes == 0) ? "Cheking this box will block activation of the benefit types on [WC Benefit Types] panel" : "Since this claim has active Benefit Types - Employers payment can't be allowed"
		    }

		    c = 0
		    while (c < frInfo.document.all['WC_MoDuty'].length) {
		        if (frInfo.document.all['WC_MoDuty'][c].value == MODIFIEDUTY) {
		            frInfo.document.all['WC_MoDuty'][c].checked = true; break;
		        }
		        c++
		    }
		    c = 0
		    while (c < frInfo.document.all['WC_LostTime'].length) {
		        if (frInfo.document.all['WC_LostTime'][c].value == LOSTTIME) {
		            frInfo.document.all['WC_LostTime'][c].checked = true; break;
		        }
		        c++
		    }
		    c = 0
		    while (c < frInfo.document.all['WC_OSHA'].length) {
		        if (frInfo.document.all['WC_OSHA'][c].value == OSHA) {
		            frInfo.document.all['WC_OSHA'][c].checked = true; break;
		        }
		        c++
		    }
		    c = 0;
		    while (c < frInfo.document.frmClaim.Settlement.length) {
		        frInfo.document.frmClaim.Settlement[c].checked = false;

		        if (frInfo.document.frmClaim.Settlement[c].value == 'Y' && SettlList.length > 0) {
		            frInfo.document.frmClaim.Settlement[c].checked = true;
		        }
		        if (frInfo.document.frmClaim.Settlement[c].value == 'N' && SettlList.length == 0) {
		            frInfo.document.frmClaim.Settlement[c].checked = true;
		        }
		        c++
		    }
		    if (life_medical == 'Y') {
		        try {
		            frInfo.document.getElementById("cboFM_type").value = future_medical_type;
		            frInfo.document.frmClaim.chkLifeMedical.checked = true;
		            if (CanChangeSuper() == false) {
		                frInfo.document.frmClaim.chkLifeMedical.disabled = true;
		            }
		        } catch (e) { }
		    }
		    if (RTWSameEmpl != 'Y') {
		        frInfo.document.getElementById("RTWPosition").style.display = "none";
		    }
		    if (MainFlag == 'Edit') {
		        ClmOriginal.HistLinks[name] = HistLinks_XTRCT(name, ClaimNumber, 'ClmNo');
		        Populate_HistLinks(frInfo.document, ClmOriginal.HistLinks[name]);
		    }
		}
		else if (name == "injury") {
		    frInfo.document.open();
		    frInfo.document.write(injury);
		    frInfo.document.close();
		    
		    frInfo.document.frmClaim.cboNCCI_SOI.value = NCCI_SOI;
		    frInfo.document.frmClaim.cboNCCI_NOI.value = NCCI_NOI;

		    //				frInfo.document.frmClaim.cboSettlType.value = settlement_type;
		    frInfo.document.frmClaim.cboWC_JuriState.value = JurState;
		    frInfo.document.frmClaim.cboAnatomy.value = BodyPartCod;

		    frInfo.document.frmClaim.cboPreExistDisability.value = PreExistDisability;

		    var c = 0;
		    while (c < frInfo.document.frmClaim.Settlement.length) {
		        frInfo.document.frmClaim.Settlement[c].checked = false;

		        if (frInfo.document.frmClaim.Settlement[c].value == 'Y' && SettlList.length > 0) {
		            frInfo.document.frmClaim.Settlement[c].checked = true;
		        }
		        if (frInfo.document.frmClaim.Settlement[c].value == 'N' && SettlList.length == 0) {
		            frInfo.document.frmClaim.Settlement[c].checked = true;
		        }
		        c++
		    }
		}
		else if (name == "vehicle") {
		    var vUrlPath = "";
		    var claimParty = ''; var covCod_ = '';
            var mes_='';

		    try{ claimParty = frGeneral.document.frmGeneral.iParty.value;} catch (e) { };
		    try {
		        var ct = '';
		        if (typeof (frInfo.document.frmClaim.cboCov) == 'undefined') {
		            ct = CovCod;
		        } else {
		            var ind = frInfo.document.frmClaim.cboCov.options.selectedIndex;
		            ct = frInfo.document.frmClaim.cboCov.options[ind].text;
		        }
		        var reV = /^(\d*)_/gi
		        var foundArr1 = reV.exec(ct)
		        covCod_ = foundArr1[1]
		    } catch (e) { };

		    if (covCod_ == '' || covCod_ == null) {
		        alert('Please define coverage');
		        Display('claim');
		        return;
		    }
		    if (PolicyCC.hasOwnProperty('id')) {
		        if (PolicyCC.Type == 'AL') {
		            mes_ = 'Policy vehicle is not defined';
		            if (typeof (PolicyCC.VEH) != "undefined") {
		                if (PolicyCC.VEH[0] != null) {
		                    if (typeof PolicyCC.VEH[0] != 'undefined') {
		                        if (trim(PolicyCC.VEH[0].VIN) != '') {
		                            mes_ = '';  // policy vehicle is defined
		                            vUrlPath = vUrlPath + "&VehicleVINNumber=" + trim(PolicyCC.VEH[0].VIN) + ""
		                        }
		                        if (trim(PolicyCC.VEH[0].Year) != '') {
		                            vUrlPath = vUrlPath + "&VehicleYear=" + trim(PolicyCC.VEH[0].Year) + ""
		                        }
		                        if (trim(PolicyCC.VEH[0].Plate) != '') {
		                            vUrlPath = vUrlPath + "&VehiclePlateNumber=" + trim(PolicyCC.VEH[0].Plate) + ""
		                        }
		                        if (trim(PolicyCC.VEH[0].Make) != '') {
		                            vUrlPath = vUrlPath + "&AutoMake=" + trim(PolicyCC.VEH[0].Make) + ""
		                        }
		                        if (PolicyCC.VEH[0].hasOwnProperty('Model')) {
		                            if (trim(PolicyCC.VEH[0].Make) != '') {
		                                vUrlPath = vUrlPath + "&Model=" + trim(PolicyCC.VEH[0].Make) + ""
		                            }
		                        }
		                    }                 
		                }
		            }
		        }
		    }

		    if (mes_ != '') {
		        alert(mes_);
		        Display('claim');
		        return;
		    }

		    frInfo.document.open();
		    frInfo.document.write(vehicle);
		    frInfo.document.close();

		    var vUrl = "/portal/NET/Vehicles/ClaimVehicles.aspx?lossdate=" + ClmOriginal.AccDate + "&clntno=" + ClientNumber + "&occurrence=" + Occurrence
                + "&claimParty=" + claimParty + "&covCod=" + covCod_ + "&TotalLoss=" + TotalLoss;

		        vUrl += "&Claim_ID="
		        vUrl += (claim_id == 0) ? ClmOriginal.Vehicles.vehicleId : claim_id
		        vUrl += "" + vUrlPath;
                frInfo.document.getElementById('ifrVehicles').src = vUrl;
		}
		else if (name == "claimant") {
		    frInfo.document.open();
		    frInfo.document.write(claimant);
		    frInfo.document.close();
		    
		    frButtons.document.frmButtons.ClaimantFlag.value = 1
		    //frInfo.document.frmClaim.txtClmtAddress.focus();
		    frInfo.document.frmClaim.focus();
		    frInfo.document.frmClaim.cboClmtState.value = Cstate;
		    frInfo.document.frmClaim.cboClmtSex.value = Sex;

		    try {
		        frInfo.document.frmClaim.CLICSTATE.value = CLICSTATE;
		        frInfo.document.frmClaim.CCITIZEN.checked = (CCITIZEN == "Y") ? true : false;
		    } catch (e) { }

		    if (UserRole.indexOf('EditClaimant') == -1 && MainFlag == 'Edit' && Status != 'P' && Status != 'I') {
		        frInfo.document.frmClaim.txtClmtAddress.disabled = true;
		        frInfo.document.frmClaim.txtClmtAddress2.disabled = true;
		        frInfo.document.frmClaim.txtClmtCity.disabled = true;
		        frInfo.document.frmClaim.cboClmtState.disabled = true;
		        frInfo.document.frmClaim.txtClmtZip.disabled = true;
		    }

		    if (typeof frInfo.document.getElementById('chkDebitCardAuth') != 'undefined') {
		            frInfo.document.getElementById('chkDebitCardAuth').style.display = 'none';
                    frInfo.document.getElementById('spnDebitCardAuth').style.display = 'none';
		    }

		    if (Unit == 'WC' || TYPE_Loss == 'BI' || CPerson == 'Y') {
		        if (Unit != 'CO' && Unit != 'HO' && Unit != 'IM' && Unit != 'OM' && Unit != 'CR') {
		            if (CPerson == 'Y') {
                        try {
		                    frInfo.document.getElementById('ClmntGender').style.fontWeight = 'bold';
		                    frInfo.document.getElementById('ClmntDOB').style.fontWeight = 'bold';
		                } catch (e) {
		            };
		                try {
		                    if (EnablePlasticCard != 'A' && EnablePlasticCard != 'P' && EnablePlasticCard != 'Y' && EnablePlasticCard != 'C') {
		                        frInfo.document.getElementById('chkDebitCardAuth').style.display = 'none';
		                        frInfo.document.getElementById('spnDebitCardAuth').style.display = 'none';
		                    } else {
		                        frInfo.document.getElementById('chkDebitCardAuth').style.display = 'inline';
		                        frInfo.document.getElementById('spnDebitCardAuth').style.display = 'inline';
		                        $('#chkDebitCardAuth', frInfo.document).on('click', { }, PlasticCardAuth_XTRCT);

		                        if (EnablePlasticCard == 'A' || EnablePlasticCard == 'P') {
		                            $('#chkDebitCardAuth', frInfo.document).prop("checked", true);
		                    }

		                        if (EnablePlasticCard == 'P' || EnablePlasticCard == 'C') {
		                            frInfo.document.getElementById('chkDebitCardAuth').disabled = true;
		                            frInfo.document.getElementById('chkDebitCardAuth').title = (EnablePlasticCard == 'P') ? 'Pending account setup confirmation' : 'Debit card account has been closed';
		                    };

		                }
		                } catch (e) {
		            };
		        }

		            frInfo.document.getElementById('ClmntSSN').style.fontWeight = 'bold';
		            frInfo.document.getElementById('ClmtAddr1').style.fontWeight = 'bold';
		            frInfo.document.getElementById('ClmtCity').style.fontWeight = 'bold';
		            frInfo.document.getElementById('ClmtState').style.fontWeight = 'bold';
		            frInfo.document.getElementById('ClmtZip').style.fontWeight = 'bold';

		            //ticket  51790 : SS# and DOB of claimant is sensitive information:
		            if (UserRole.indexOf('Confidential') == -1) {
		                if (DOB != '') {
		                    frInfo.document.frmClaim.txtClmtDOB.style.display = "none"
		            }
		                if (SSNum != '') {
		                    frInfo.document.frmClaim.txtClmtSSN.style.display = "none";
		            }
		                if (frInfo.document.getElementById('lkHRdata') != null) {
		                    frInfo.document.getElementById('lkHRdata').style.display = "none";
		            }
		        }
		            if (MainFlag == 'Edit' && CPerson == 'Y') {
		                try {
		                    frInfo.document.frmClaim.Auth_SSN_Release.value = Auth_SSN_Release;
		                } catch (e) {
		            }
		                if (TYPE_Loss == 'BI') {
		                    try {
		                        frInfo.document.frmClaim.Auth_Age_Release.value = Auth_Age_Release;
		                    } catch (e) {
		                }
		            }
		        }
		    }
		    }
		}
		else if (name == "law") {
		    frInfo.document.open();
		    frInfo.document.write(law);
		    frInfo.document.frmClaim.cboLawTyp.focus();
		    frButtons.document.frmButtons.LawFlag.value = 1
		    frInfo.document.frmClaim.cboLawState.value = Lstate;
		    
		    var AddrViewLine = frInfo.document.getElementById('AddrViewLine');
		    if (typeof AddrViewLine != 'undefined') {
		        AddrViewLine.className = 'textReadOnly';
		    }
		}
		else if (name == "doc") {
		    frInfo.document.open();
		    frInfo.document.write(doc);
		    frInfo.document.close();
		}
		else if (name == "fin") {
		    frInfo.document.open();
		    frInfo.document.write(fin);
		    frInfo.document.close();
		}
		else if (name == "res") {
		    frInfo.document.open();
		    frInfo.document.write(res);
		    frInfo.document.close();
		}
		else if (name == "lossLoc") {
		    var lsIdx_ = frGeneral.document.frmGeneral.cboLossState.selectedIndex;
		    var lcIdx_ = frGeneral.document.frmGeneral.cboLossCountry.selectedIndex;

		    //if (lsIdx_ <= 0) {
		    //    alert('Please specify loss state');
		    //    MyTitle = '';
		    //    setTimeout("Display('claim')", 50);		        
		    //} else {
		        frInfo.document.open();
		        frInfo.document.write(lossLoc);
		        frInfo.document.close();

		        frInfo.document.frmClaim.txtLossStateDesc.value = frGeneral.document.frmGeneral.cboLossState[lsIdx_].text;
		        frInfo.document.frmClaim.txtLossCountry.value = frGeneral.document.frmGeneral.cboLossCountry[lcIdx_].text;
		    //}
		}
		else if (name == "rmc") { //reserves minor codes
		    frInfo.document.open();
		    frInfo.document.write(rmc);
		    frInfo.document.close();

		    ClmOriginal.odgReserves.populateScreen();

		    //ReserveSheetId  =0: no reserve sheet;  >0: reserve sheet option is activated;    <0: reserve sheet option is activated but reserve change is disallowed
		    if ((ER_apprID != '' && ER_apprID != 0) || (IR_apprID != '' && IR_apprID != 0) || (IR_apprID != '' && IR_apprID != 0) || ReserveSheet.type == 0) {
		        if (typeof frInfo.document.frmClaim.loadWorksheet === 'object') {
		            frInfo.document.frmClaim.loadWorksheet.style.display = "none";
		            frInfo.document.frmClaim.clearWorksheetOutput.style.display = "none";
		            frInfo.document.frmClaim.emailWorksheet.style.display = "none";
		            frInfo.document.frmClaim.viewSubmittedWorksheet.style.display = "none";
		        }
		    } else {
		        if (ReserveSheet.status < 0) {  //change of the reserves is blocked 
		            if (typeof frInfo.document.frmClaim.loadWorksheet === 'object') {
		                frInfo.document.frmClaim.loadWorksheet.style.display = "none";
		                frInfo.document.frmClaim.clearWorksheetOutput.style.display = "none";
		                frInfo.document.frmClaim.emailWorksheet.style.display = "none";
		                frInfo.document.frmClaim.viewSubmittedWorksheet.style.display = "none";
		            }
		        }
		    }

	        if (ReserveSheet.id != 0) {
	            frInfo.document.getElementById("ResChangeAllowed").innerHTML = "Pending reserve worksheet (#" + ReserveSheet.id + ").";
	            frInfo.document.getElementById("ResChangeAllowed").style.color = 'blue';
	        }
		    
		    if (!ClntPolCC_ReserveChangeAllowed()) {
		        if (typeof frInfo.document.frmClaim.loadWorksheet === 'object') {
		            frInfo.document.frmClaim.loadWorksheet.style.display = "none";
		            frInfo.document.frmClaim.clearWorksheetOutput.style.display = "none";
		            frInfo.document.frmClaim.emailWorksheet.style.display = "none";
		            frInfo.document.frmClaim.viewSubmittedWorksheet.style.display = "none";
 
		            frInfo.document.getElementById("ResChangeAllowed").innerHTML = "Client settings necessitate the claim's link to an active policy.\nReserve change is blocked until this requirement is met.";
		            frInfo.document.getElementById("ResChangeAllowed").style.color = 'red';
		        }
		    }
		    
		    //reserves are requiring approval ||  Reserve have to be changed vua reserve worksheet!
		    try {
		        if ((IR_apprID != '' && IR_apprID != 0) || (ReserveSheet.type != 0)) {
		            var c = 1;
		            while (c < RSV_MC.length) {
		                //t = 'frInfo.document.frmClaim.txt' + RSV_MC[c][1] + '.className="textReadOnly";'
		                frInfo.document.frmClaim['txt' + RSV_MC[c][1]].disabled = true;
		                
		                c++;
		            }
		        }
		    } catch (e) { }
		    try {
		        if ((ER_apprID != '' && ER_apprID != 0) || (ReserveSheet.type != 0)) {
		            //t = 'frInfo.document.frmClaim.txtExpense.className=textReadOnly'
		            t = 'frInfo.document.frmClaim.txtExpense.disabled=true'
		            eval(t);
		        }
		    } catch (e) { }
		    try {
		        if ((IR_apprID != '' && IR_apprID != 0) || (ReserveSheet.type != 0)) {
		            //t = 'frInfo.document.frmClaim.txtMedical.className=textReadOnly'    
		            t = 'frInfo.document.frmClaim.txtMedical.disabled=true'
		            eval(t);
		        }
		    } catch (e) { }

            //15.8 - RMPG:
		    if (MainFlag == 'Edit' && Unit == 'WC') {
		        var special_fund_offset_ = (trim(special_fund_offset) == '') ? 'N' : special_fund_offset
		        var customFieldsProperties = { readOnly: false, contextDocument: frInfo.document };
		        $('#CF_Container_19', frInfo.document).attr("data-cf-Applyto", '[{ "Name": "ClntNo", "Value": "' + ClientNumber + '" }, { "Name": "SpecialFnd", "Value": "' + special_fund_offset_ + '" }]');
		        CustomFields_Initialize("div.Cf_Container", customFieldsProperties);
		        $(".Dynamically_Generated_Label", frInfo.document).css({ 'font-weight': 'normal'}); 

		    }
		}
//		else if (name == "defc") {
//		    frInfo.document.open();
//		    frInfo.document.write(defc);
//		}
		else if (name == "complex") {
		    frButtons.document.frmButtons.ComplexFlag.value = 1;
		    var c = 0
		    frInfo.document.open();
		    frInfo.document.write(complex);
		    frInfo.document.close();
		    //populate radio/selects:
		    c = 0
		    while (c < frInfo.document.frmClaim.radioVSResponse.length) {
		        if (frInfo.document.frmClaim.radioVSResponse[c].value == cmpxVSResponse) {
		            frInfo.document.frmClaim.radioVSResponse[c].checked = true; break;
		        }
		        c++
		    }
		    c = 0
		    while (c < frInfo.document.frmClaim.radioSLAHandling.length) {
		        if (frInfo.document.frmClaim.radioSLAHandling[c].value == SLA) {
		            frInfo.document.frmClaim.radioSLAHandling[c].checked = true; break;
		        }
		        c++
		    }
		    frInfo.document.frmClaim.cboSLAGA.value = cmpxSLA;
		    frInfo.document.frmClaim.cboTypeOfIssue.value = cmpxIssueType;
		    frInfo.document.frmClaim.cboReferralReason.value = cmpxReferralReason;
		}
		else if (name == "request_assistance") {
		    if (trim(State) == '.'){
		        alert('Claim Loss State (Region) is "Other"\nPlease select a valid value');
		        Display('claim');
		        return;
		    }
		    
		    frInfo.document.open();
		    frInfo.document.write(request_assistance);
		    frInfo.document.close();
		    try {
		        frInfo.document.frmClaim.cboReferredTo.value = ReferredTo;
		        frInfo.document.frmClaim.cboReferredTo.disabled = true;
		        frInfo.document.frmClaim.txtSYorkAmount.readOnly = true
		        frInfo.document.frmClaim.cboSState.value = SSTATE;
		        frInfo.document.frmClaim.cboAState.value = ACSTATE;
		        frInfo.document.frmClaim.cboRAdjNo.value = RADJNO;
		        frInfo.document.frmClaim.cboPursuitAgainst.value = PursuitAgainst;
		        frInfo.document.frmClaim.cboSubrogationType.value = SubrogationType;
		        frInfo.document.frmClaim.cboResolvedBy.value = ResolvedBy;
		        frInfo.document.frmClaim.cboDisposition.value = Disposition;
		        frInfo.document.frmClaim.RecTypesNum.value = (RECOVERY_ExtraTypes.length + 1)

		        RecoveryRequiredFields_Mark();
		    } catch (e) { }

		    if (SCREEN == "Rental") {
		        with (frInfo.document.frmClaim) {
		            try { txtSName.value = trim(RFirstName) + ' ' + trim(RLastName); } catch (e) { }
		            try { txtSAddress1.value = RAddr1; } catch (e) { }
		            try { txtSAddress2.value = RAddr2; } catch (e) { }
		            try { txtSCity.value = RCity; } catch (e) { }
		            try { cboSState.value = RState; } catch (e) { }
		            try { txtSZip.value = RZip; } catch (e) { }
		            try { txtSPhone.value = RPhone; } catch (e) { }
		        }
		    }
		}
		else if (name == "assistance") {
		    frInfo.document.open();
		    frInfo.document.write(assistance);
		}
		else if (name == "iadj") {
		    frInfo.document.open();
		    frInfo.document.write(iadj);
		    frInfo.document.frmClaim.cboIAdjState.value = IAdjState;
		}
		else if (name == "prodliab") {
		    frInfo.document.open();
		    frInfo.document.write(prodliab);

		    frInfo.document.frmClaim.plMassTort.value = plMassTort;
		    frInfo.document.frmClaim.plStatus.value = plStatus;
		}
		else if (name == "producer") {
		    frInfo.document.open();
		    frInfo.document.write(producer);
		   // frInfo.document.frmClaim.cboPrState.value = PrState;
		}
		else if (name == 'listloc') {
		    frInfo.document.open();
		    frInfo.document.write(listloc);
		    frInfo.document.close();
		    //frInfo.focus();
		    setTimeout("frInfo.document.frmClaim.txtL_Number.focus();", 100);
		}
		else if (name == 'polInfo') {  //TRUMBLE
		    if (MyPOList.length > 0 && PolInx > -1) {
		        frGeneral.document.frmGeneral.txtPolNo.value = MyPOList[PolInx].PolNo;
		    }
		    frInfo.document.open();
		    frInfo.document.write(polInfo);
		    frInfo.document.close();
		    
		    frInfo.focus();
		}
		else if (name == 'polCCinfo') {  //CC Policy

		    frInfo.document.open();
		    frInfo.document.write(polCCinfo);
		    frInfo.document.close();

		    frInfo.focus();
		}
		else if (name == 'rec') {
		    frInfo.document.open();
		    frInfo.document.write(rec);
		    frInfo.focus();
		    frButtons.document.frmButtons.RecoveryFlag.value = 1;
		    frInfo.document.frmClaim.cboRAdjNo.value = RADJNO;
		    frInfo.document.frmClaim.cboPursuitAgainst.value = PursuitAgainst;
		    frInfo.document.frmClaim.cboSubrogationType.value = SubrogationType;
		    frInfo.document.frmClaim.cboReferredTo.value = ReferredTo;
		    frInfo.document.frmClaim.cboResolvedBy.value = ResolvedBy;
		    frInfo.document.frmClaim.cboDisposition.value = Disposition;

		    frInfo.document.frmClaim.txtSYorkAmount.readOnly = true
		    frInfo.document.frmClaim.RecTypesNum.value = (RECOVERY_ExtraTypes.length + 1)
		    //edititing restictions: closed or opened recovery may be edited only by RoleUpNum 1270 and Branch 153 or Branch 110
		    if (RecovUser == 0) {
		        if (REC_STATUS_Original == 'C' || REC_STATUS_Original == 'O') {
		            frButtons.document.frmButtons.RecoveryFlag.value = 0;
		            var i = 0;
		            while (i < frInfo.document.frmClaim.length) {
		                if (frInfo.document.frmClaim.elements[i].type == 'text' || frInfo.document.frmClaim.elements[i].type == 'textarea') {
		                    frInfo.document.frmClaim.elements[i].readOnly = true;
		                }
		                i++
		            }
		        }
		    }

		    if (REC_STATUS == 'Y' && REC_STATUS_Original != 'Y') { //added 11/25/2007
		        RecoveryRequiredFields_Mark();
		        //ticket:36198  //pre-populate Renters data into responsible party boxes;
		        if (SCREEN == "Rental") {
		            with (frInfo.document.frmClaim) {
		                txtSName.value = trim(RFirstName) + ' ' + trim(RLastName);
		                txtSAddress1.value = RAddr1;
		                txtSAddress2.value = RAddr2;
		                txtSCity.value = RCity;
		                cboSState.value = RState;
		                txtSZip.value = RZip;
		                txtSPhone.value = RPhone;
		            }
		        }
		    }
		    //03.31/2008 historical recovery info is read-only;				
		    try {
		        frInfo.document.frmClaim.cboReferredTo.disabled = true;

		        if (Claim_Assigns.length > 0 && Claim_Assigns[0].clmno_assist == ClaimNumber) {
		        } else {
		            //all recovery records should be available for editing on a master claim if they are handled by external entities and if recovery has been reported before 04.01.2008
		            if (ReferredTo == 4 || ReferredTo == 5 || ReferredTo == 6 || (DateComp(MyDateFormat(RecCreDate), '04/01/2008', 1) == 1)) {
		                frInfo.document.frmClaim.cboReferredTo.disabled = true;
		            } else {
		                frInfo.document.frmClaim.txtSName.disabled = true;
		                frInfo.document.frmClaim.txtSAddress1.disabled = true;
		                frInfo.document.frmClaim.txtSAddress2.disabled = true;
		                frInfo.document.frmClaim.txtSCity.disabled = true;
		                frInfo.document.frmClaim.cboSState.disabled = true;
		                frInfo.document.frmClaim.txtSZip.disabled = true;
		                frInfo.document.frmClaim.txtSPhone.disabled = true;
		                frInfo.document.frmClaim.txtSFax.disabled = true;
		                frInfo.document.frmClaim.txtSEmail.disabled = true;
		                frInfo.document.frmClaim.txtAContact.disabled = true;
		                frInfo.document.frmClaim.txtAAddress1.disabled = true;
		                frInfo.document.frmClaim.txtAAddress2.disabled = true;
		                frInfo.document.frmClaim.txtACity.disabled = true;
		                frInfo.document.frmClaim.cboAState.disabled = true;
		                frInfo.document.frmClaim.txtAZip.disabled = true;
		                frInfo.document.frmClaim.txtAPhone.disabled = true;
		                frInfo.document.frmClaim.txtAFax.disabled = true;
		                frInfo.document.frmClaim.txtAEmail.disabled = true;
		                frInfo.document.frmClaim.txtACClmNo.disabled = true;
		                frInfo.document.frmClaim.txtRecovPercent.disabled = true;
		                frInfo.document.frmClaim.txtRSettlement.disabled = true;
		                frInfo.document.frmClaim.txtRAmount.disabled = true;
		                frInfo.document.frmClaim.txtSYorkAmount.disabled = true;
		                frInfo.document.frmClaim.txtRecReserve.disabled = true;
		                frInfo.document.frmClaim.txtSNotes.disabled = true;
		                frInfo.document.frmClaim.cboRAdjNo.disabled = true;
		                frInfo.document.frmClaim.cboPursuitAgainst.disabled = true;
		                frInfo.document.frmClaim.cboSubrogationType.disabled = true;
		                frInfo.document.frmClaim.cboReferredTo.disabled = true;
		                frInfo.document.frmClaim.cboResolvedBy.disabled = true;
		                frInfo.document.frmClaim.cboDisposition.disabled = true;
		            }
		        }
		    } catch (e) { };
            
            //style:
		    var SAddrViewLine = frInfo.document.getElementById('SAddrViewLine');
		    if (typeof SAddrViewLine != 'undefined') {
		        SAddrViewLine.className = 'textReadOnly';
		    }
		    var AddrViewLine = frInfo.document.getElementById('AddrViewLine');
		    if (typeof AddrViewLine != 'undefined') {
		        AddrViewLine.className = 'textReadOnly'; 
		    }
		}
		else if (name == 'customList') {
		    frInfo.document.open();
		    frInfo.document.write(customList);
		    frInfo.focus();
		}
		else if (name =='prefProvider') {
		    frInfo.document.open();
		    frInfo.document.write(prefProvider);
		    frInfo.focus();
		}
		else if (name == 'custom') {
		    
            frInfo.document.open();
		    frInfo.document.write(custom);
		    frInfo.document.close();

		    frInfo.focus();

		    var str = ''; var c = 1;
		    while (c <= CUSTOM_FIELDS) {
		        str = 'if(frInfo.document.frmClaim.' + 'T' + c + '.value!="restricted"){' +
								'frInfo.document.frmClaim.' + 'T' + c + '.value=' + 'T' + c + ';}'
		        try {
		            eval(str); //value of custom field might not exist
		        } catch (e) {
		            eval('frInfo.document.frmClaim.' + 'T' + c + '.value=""');
		        }
		        c++
		    }
		    if (CarClmNo != '' && frGeneral.document.frmGeneral.txtClntNo.value == 2094) {
		        frInfo.document.frmClaim.T2.disabled = true;
		        try { frInfo.document.frmClaim.T10.disabled = true } catch (e) { }
		        try { frInfo.document.frmClaim.T11.disabled = true } catch (e) { }
		    }
		    try {
		        if (frGeneral.document.frmGeneral.txtClntNo.value == 4001) {
		            frInfo.document.frmClaim.T4.disabled = true;
		            frInfo.document.frmClaim.T6.disabled = true;
		        }
		    } catch (e) { }

		    var _CovCod;

		    try {
		        var re = /^(\d*)_/gi
		        re.exec(CovCod);
		        _CovCod = RegExp.$1;
		    } catch (e) { };

		    var l = (typeof EditLoc[1][0] != 'undefined') ?  EditLoc[1][0] : 0;
		    
		    // initialize Generic Custom Fields:
		    if (MainFlag == 'Edit') {
		        var customFieldsProperties = { readOnly: false, contextDocument: frInfo.document };
		        $('#CF_Container_1', frInfo.document).attr("data-cf-Applyto", '[{ "Name": "ClntNo", "Value": "' + ClientNumber + '" }, { "Name": "ClmStatus", "Value": "' + Status + '" }, { "Name": "Logon", "Value": "' + Logon + '" }, { "Name": "LOB", "Value": "' + Unit + '" }, { "Name": "CovCod", "Value": "' + _CovCod + '" }, { "Name": "LossState", "Value": "' + State + '" }, { "Name": "Loc", "Value": "' + l + '" }]');
		        //CustomFields_Initialize("div.Cf_Container", customFieldsProperties);
		        CustomFields_Initialize2("div.Cf_Container", customFieldsProperties, '', -1);  //_CF_LABEL_POSITION_LEFT, # of cols
		        $(".Dynamically_Generated_Label", frInfo.document).css({ 'font-weight': 'normal' }); 
		    }
		}


        // final Context Help Injection:
//		var script = frInfo.document.createElement('script')
//		script.setAttribute("type", "text/javascript")
//		script.setAttribute("src", "/portal/startup/mmlibrary.js")

//		frInfo.document.appendChild(script);
//		frGeneral.document.appendChild(script);
	}
	
	function LossAddressInsert(){
		if(Polstruct=='F' || Polstruct=='D'){
			try{
			 frInfo.document.frmClaim.txtClmtAddress.value=SingleQuote(IAddr1)
			 frInfo.document.frmClaim.txtClmtAddress2.value=SingleQuote(IAddr2)
			 frInfo.document.frmClaim.txtClmtCity.value=SingleQuote(ICity)
			 frInfo.document.frmClaim.cboClmtState.value=IState 
			 frInfo.document.frmClaim.txtClmtZip.value=IZip
			 frInfo.document.frmClaim.txtClmtPhone.value=IPhone
			 frInfo.document.frmClaim.txtClmtBusPhone.value=IWorkPhone
			}catch(e){}
		}else{
			try{
				if(EditLoc[1][0]>0){
					frInfo.document.frmClaim.txtClmtAddress.value=EditLoc[1][2]
					frInfo.document.frmClaim.txtClmtCity.value=EditLoc[1][3] 
					frInfo.document.frmClaim.cboClmtState.value=EditLoc[1][4]
					frInfo.document.frmClaim.txtClmtZip.value=EditLoc[1][5]
				}
			}catch(e){}
		}
	}

	function IsReservePendingApproval() {
	    var RetVal = { IR: 0, MR: 0, ER: 0 };
	
	    try {
	        if (IR_apprID != '' && IR_apprID != 0) {
	            RetVal.IR = 1;
	        }
	    } catch (e) { }
	    try {
	        if (ER_apprID != '' && ER_apprID != 0) {
	            RetVal.ER = 1;
	        }
	    } catch (e) { }
	    try {
	        if (MR_apprID != '' && MR_apprID != 0) {
	            RetVal.MR = 1;
	        }
	    } catch (e) { }
	    
	    return(RetVal);
	}


	function DiableReservePendingApproval() { // IR_apprID - record ID in the table  Claimreserve_tobe_Approved
	    var _ReserveTypesPendAppr = IsReservePendingApproval();

	    try {
	        if (_ReserveTypesPendAppr.IR == 1) {
					frInfo.document.frmClaim.txtIndemnity.disabled=true
			}
		}catch(e){}
		try{
		    if (_ReserveTypesPendAppr.ER == 1) {
				frInfo.document.frmClaim.txtExpense.disabled=true
			}
		}catch(e){}
		try{
		    if (_ReserveTypesPendAppr.MR == 1) {
				frInfo.document.frmClaim.txtMedical.disabled=true
			}
		}catch(e){}
	}
	
	function ViewReservePendingApprovalWin(resType, apprID, Amount){
		var URL="/portal/ReserveApprove/ReservesView.asp?Option=Show&ClaimNumber=" + ClaimNumber + "&resType=" + resType +  "&Amount=" + Amount +"&rID=" + apprID + "&BranchList=ViewEdit"
		var NewWnd=window.open(URL,'History','status,height=580px,width=770px,top=9,left=9');
	}
	
	function ReturnReservePendingApprovalWin(pass){
		var fi = RefreshFinancial_XTRCT();
		if(fi!=''){
		     try{
			    eval(fi);
			 }catch(e){
		     }

			IR_Disp=DisplayReserve(IR);
			ER_Disp=DisplayReserve(ER);
			MR_Disp = DisplayReserve(MR);
			
			setTimeout("Variable_Populate('claim')", 200);
			setTimeout("Display('claim')", 200);
		}
	}
	
	function LossStateChange(pass){
	    var oState = State;

	    if (typeof pass == 'string') {
	        State = pass;
	    }
	    
	    try { LossStateId = $('#cboLossState', frGeneral.document).find('option:selected').attr('stateId'); } catch (e) { };

		if(MainFlag!='Edit'){
			try{
				if(JurState=='' || typeof(JurState)=='undefined'){
					JurState = State;
				}
			}catch(e){}
		}

		try { AdjustersPopulate(); } catch (e) { };

		try { CountyPopulate() } catch (e) { };

	    try { PIPCodeList_Get(0) } catch (e) { };

		if (State != oState) {		           
		    if (frInfo.document.title != 'lossLoc') {
		        if (LossAddr1 != '' || LossCity != '' || LossZip != '') {
		            setTimeout("Display('lossLoc')", 350);
		        }
		    } else {
		        var lsIdx_ = frGeneral.document.frmGeneral.cboLossState.selectedIndex;
		        var lcIdx_ = frGeneral.document.frmGeneral.cboLossCountry.selectedIndex;

		        frInfo.document.frmClaim.txtLossStateDesc.value = frGeneral.document.frmGeneral.cboLossState[lsIdx_].text;
		        frInfo.document.frmClaim.txtLossCountry.value = frGeneral.document.frmGeneral.cboLossCountry[lcIdx_].text;

		        frInfo.document.frmClaim.txtLossStateId.value = LossStateId;
		        frInfo.document.frmClaim.txtLossState.value = State;
		        frInfo.document.frmClaim.txtLossCountryId.value = LossCountry;
		    }

		    if (LossAddr1 != '' || LossCity != '' || LossZip != '') {
		        alert('You have changed Loss State.\nPlease re-validate loss location address');
		    }
		}
	}
	
//*******************   SCREENS  **********************
		var general="";
		var claim="";
		var workcomp="";
		var vehicle="";
		var doc="";
		var law="";
		var claimant="";
		var fin="";
		//var sir="";
		var res="";
//		var defc="";
		var iadj = "";
		var prodliab = "";
		var producer="";
		var listloc="";
		var rec="";
		var rmc = "";
		var lossLoc = "";
		//var ysi="";
		var polInfo="";
		var job='';
		var workcomp_ca='';
		var complex='';
		var assistance='';
		var request_assistance='';
		var injury ='';
		var polCCinfo='';
		var customList = '';
		var prefProvider = '';
		
function Populate_general(){
    var g = '';
	if(Polstruct!='M'){
		g="<html><head>"
				+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
				+ "</head><body>"
				+ "<form name='frmGeneral' id='frmGeneral'>"
				+ "<table border=1 frame=below rules=none cellPadding=2 cellSpacing=1 width=100% id='TableGeneral'><tr><td>"
//1:				
				+ " <table cellPadding=0 cellSpacing=1 width=100% border=0><TR>"
				+ " <TD id='Comp' style='width:70px'><B>Company:</B></TD>"
				+ " <TD id='Comp' align=left><select name='cboCompany' onChange='parent.MyOfficeName(parent.frGeneral.document.frmGeneral.cboCompany.value)'>"
				+ CompanyName
				+ "	</Select></TD>"
				+ " <TD id='Comp' align=right><B><Label id=ContBrh>Control Branch:<Label></B></TD>"
				+ " <TD id='Comp_List' align=right style='width:180px'></TD>"
				+ " <TD id='Line' align=right><B><Label>Line of Bus.:</Label></B></td>"
				+ " <TD id='Line' align=right style='width:150px'><select name='cbolLineBus' "
				if(Polstruct!='A'){ 
				g +=" onChange='parent.MyLineBus(this.value)'"
				}else{   //IF POList > 0 --> Coverage List depends on selected Policy; (TRUMBLE)
    g += " onChange='parent.Unit=this.value; parent.frGeneral.document.all[\"txtPolNo\"].options.length=0;parent.frGeneral.document.all[\"txtPolNo\"].add(new Option(\"Searching..\",0),0);setTimeout(\"parent.PolicyList_XTRCT(0, parent.ClientNumber,parent.Unit, parent.AccDate);\", 30);parent.MyLineBus(parent.Unit);'"
				}
				g += ">"
				+ LineOfBus
				+ " </Select></TD>"
				+ "</TR>"
				+ "</table>"

				+ "</td></TR><tr><td>"
//2:
				+ "<table border=0 cellPadding=0 cellSpacing=1 width=100%>"		
				+ "<TR>"
				+ "<TD id='Clnt' style='width:70px'>Client:</TD>"
				+ "<TD id='Clnt' align=left style='width:35px'><input name='txtClntNo' value='" + ClientNumber + "' style='width:35px' class=textReadOnly READONLY>&nbsp;</TD>"
				+ "<TD id='Clnt' align=left><span class=textReadOnly><B>" + trim(ClientName) + "</B></span></TD>"
				+ "<TD id='Poli' style='width:40px' align=center><B>Policy:</B></TD>"
			if (PolicyCC.hasOwnProperty('id') && SelfInsuredPrefix != 'Y') {
	            g += "<TD id='Poli' align=left  style='width:100px'><input name='txtPolNo' value='" + trim(PolicyCC.PolNo) + "' style='width:100px' class=textReadOnly READONLY></TD>"
				+ "<TD id='Poli' align=middle style='width:110px'><B>Eff:</B><input name='txtEffDT' value='" + MyDateFormat(trim(PolicyCC.Eff), 1) + "' style='width:80px' class=textReadOnly READONLY></TD>"
				+ "<TD id='Poli' align=right  style='width:110px'><B>Exp:</B><input name='txtExpDT' value='" + MyDateFormat(trim(PolicyCC.Exp), 2) + "' style='width:80px' class=textReadOnly READONLY></TD>"
	        }else if(Polstruct == 'I' && trim(EditLoc[1][9]) != '' && trim(EditLoc[1][6]) != '' && trim(EditLoc[1][7]) != '') {
			  g +="<TD id='Poli' align=left  style='width:100px'><input name='txtPolNo' value='" + trim(EditLoc[1][9]) + "' style='width:100px' class=textReadOnly READONLY></TD>"
				+ "<TD id='Poli' align=middle style='width:110px'><B>Eff:</B><input name='txtEffDT' value='" + MyDateFormat(trim(EditLoc[1][6]),1) + "' style='width:80px' class=textReadOnly READONLY></TD>"
				+ "<TD id='Poli' align=right  style='width:110px'><B>Exp:</B><input name='txtExpDT' value='" + MyDateFormat(trim(EditLoc[1][7]),2) + "' style='width:80px' class=textReadOnly READONLY></TD>"
			}else if(SCREEN=="Commerce" || SCREEN=="Commerce_2" || SCREEN=="MainWorkComp"){ //polstruct 'D' & 'F' & 'W'
			  g +="<TD id='Poli' align=left style='width:100px'><input name='txtPolNo' value='" + trim(PolNo.toUpperCase()) + "'  style='width:100px' maxlength='20'></TD>"
				+ "<TD id='Poli' align=middle style='width:110px'><B>Eff:</B><input name='txtEffDT' value='" + MyDateFormat(EffDate,1) + "' style='width:70px' maxlength='10' onBlur='{document.frmGeneral.txtEffDT.value=parent.IEffDT_Date(parent.frGeneral.document.frmGeneral.txtEffDT.value);}'></TD>"
				+ "<TD id='Poli' align=right style='width:110px'><B>Exp:</B><input name='txtExpDT' value='" + MyDateFormat(ExpDate,2) + "' style='width:70px' maxlength='10' onBlur='{document.frmGeneral.txtExpDT.value=parent.IExpDT_Date(parent.frGeneral.document.frmGeneral.txtExpDT.value);}'></TD>"
			}else if(Polstruct=='A'){ // 
			  g +="<TD id='Poli' align=left style='width:100px'>"
				+ "<select style='width:100px' name='txtPolNo' onChange='parent.POListSelect(this.options[this.selectedIndex].id,-1)'"
			  //g +=(PolicyList.indexOf("<option value=0></option>")>-1) ?  "" : " onClick='parent.PolicyList_XTRCT(\"\",parent.ClientNumber,parent.Unit, parent.AccDate, parent.PolNo, parent.VinNo, parent.LicenseNo)' "
				g +=(MainFlag=='Edit') ?  "" : " onClick='parent.PolicyList_XTRCT(\"\",parent.ClientNumber,parent.Unit, parent.AccDate, \"\", \"\", \"\")'"
				g += ">" 					
				//if(MainFlag=='Edit'){
				//g +=(PolicyList.indexOf("<option value=0></option>")>-1) ?   "" : PolicyList_XTRCT(ClaimNumber,ClientNumber,Unit,AccDate, PolNo, VinNo, LicenseNo)
				//}
			  g += "</SELECT></TD>"
						
				+ "<TD id='Poli' align=middle style='width:110px'><B>Eff:</B><input name='txtEffDT' value='' style='width:80px' class=textReadOnly READONLY></TD>"
				+ "<TD id='Poli' align=middle style='width:110px'><B>Exp:</B><input name='txtExpDT' value='' style='width:80px' class=textReadOnly READONLY></TD>"
			} else{				
				g +="<TD id='Poli' align=left style='width:100px'><input name='txtPolNo' value='" + trim(PolNo.toUpperCase()) + "'  style='width:100px' class=textReadOnly READONLY></TD>"
					+ "<TD id='Poli' align=middle style='width:110px'><B>Eff:</B><input name='txtEffDT' value='" + MyDateFormat(EffDate,1) + "' style='width:80px' class=textReadOnly READONLY></TD>"
					+ "<TD id='Poli' align=middle style='width:110px'><B>Exp:</B><input name='txtExpDT' value='" + MyDateFormat(ExpDate,2) + "' style='width:80px' class=textReadOnly READONLY></TD>"
				
			}
			if(MainFlag=='Edit'){
			   g+="<TD id='AsBranch_Label' align=right></td>"
				+ "<TD style='width:220px' id='AsBranch_List' align=right></td>"
			}
			  g +="</TR></table>"

				+ "</td></TR><tr><td>"
//3:
				+ "<table cellPadding=0 cellSpacing=1 width=100% border=0><TR>"
				+ "<TD id='Clam' style='width:70px'>Claim#:</TD>"
				+ "<TD id='Clam' style='width:100px' align=left><input name='txtClmNo' value='" + ClaimNumber + "' style='width:100px' class=textReadOnly READONLY></TD>"
				if(Symbol!=''){ //AIGRM only
					g+="<TD id='Clam' style='width:40px' align=right>Sequence#:</TD>"
					 + "<TD id='Clam' align=left style='width:40px'><input name='txtSymbol' value='" + Symbol + "' style='width:40px' class=textReadOnly READONLY></TD>"
				}
			  g +="<TD id='ClamParty' align=middle></td>"
			    + "<TD id='Clam' align=right>Status:</td>"
				+ "<TD id='Clam' align=left style='width:20px'><B><input type='text' value='" + Status + "' name='txtStatus' class=textReadOnly style='width:20px' onClick='parent.MyStatus(this.value,parent.Unit)'></B></TD>"
				+ "<TD id='Clam' align=right><B><span>Loss Date:</span></B></td>"
				+ "<TD id='Clam' align=left><input name='txtAccDT' value='" + MyDateFormat(AccDate, 1) + "' style='width:80px' maxlength='10'  onBlur='{document.frmGeneral.txtAccDT.value=parent.MyDate(parent.frGeneral.document.frmGeneral.txtAccDT.value);}'></TD>"
			  if (MainFlag == 'Edit') {
			      if (fiscYear.length > 0) {
			          g += "<td id='Clam' align=right><B><span>Fiscal year:</span></B></td>"
                     + "<td id='Clam' align=left style='width:80px'><input name='txtfiscYear' value='" + fiscYear + "' style='width:80px' class=textReadOnly READONLY></TD>"
			      }
			  }
			  g += "<td id='Clam' align=right><B>Loss Country:</B></TD>"
				+ "<td id='Clam'  style='width:70px'><select name='cboLossCountry' id='cboLossCountry'>"
				+ CountryRegions.countryList
				+ "</select></TD>"
			  g += "<td id='ClamLossStLbl' align=right><B>Loss State:</B></TD>"
			    + "<td style='width:10px'><span id='ClamLossStHlp'></span></td>"
				+ "<td id='Clam' style='width:70px'><select name='cboLossState' id='cboLossState' onChange='try{parent.LossStateChange(this.value)}catch(e){};'>"  // try{parent.AdjustersPopulate();}catch(e){};try{parent.CountyPopulate()}catch(e){};try{parent.PIPCodeList_Get(0)}catch(e){};
				+ CountryRegions.stateList
				+ "</SELECT></TD>"
			if(MainFlag!='Edit'){
			   g+="<TD id='AsBranch_Label' align=right></td>"
				+ "<TD id='AsBranch_List' align=right></td>"
			}				
				g+="</TR></table>"

				+ "</td></TR><tr><td>"
//4:
				+ " <table cellPadding=0 cellSpacing=1 width=100% border=0><TR>"
				+ "<TD id='Adju' style='width:70px'><B>Adjuster:</B></TD>"
				+ "<TD id='Adju_List' align=left style='width:120px'></TD>"
			if(UserRole.indexOf('EditAdj')!=-1 || MainFlag !='Edit'){
				g +="<TD align=left style='width:40px'>"
				g += iscboAdjuster
				  + "</TD>"
				g += "<td align=left><input type='checkbox' name='chkDefaultsGet' value='' onclick='this.checked = (parent.DefaultValues_XTRCT(this.checked, null) == true && this.checked == true) ? true : false;'>"
                    + "<span id='lblDefaultsGet' style='color:blue;cursor:hand' onclick='parent.DefaultValues_XTRCT(null, true);' ><u>Default Settings</u></span></td>"
			}
			  g +="<TD id='Adju_Super' align=right><B>Supervisor:</B></TD>"
				+ "<TD id='Adju_List_Super' aligh=right style='width:120px'>"
				+ "<select name='cboSuper' "
			  g += (SuperUser.length > 500) ? "" : " onClick='parent.SuperUser_XTRCT();' "			
			  g +=">"
				+ SuperUser
				+ "</SELECT></TD>"
			if(CanChangeSuper()){	
				g +="<TD align=left style='width:40px'>"
				g += iscboSuper
				  + "</TD>"
			}	
			  g +="<TD id='Adju_As' align=right>Assistant:</TD>"
				+ "<TD id='Adju_List_As' style='width:120px'></TD>" 
		if(UserRole.indexOf('EditAssist')!=-1 || MainFlag !='Edit'){
			  g +="<TD align=left style='width:40px'>"			  
			 g += iscboAsAdjuster			  
			  + "</TD>"
		}
			  g +="</TR></table>"  

			+ "</td></TR><tr><td>"  
//5:
			+ "<table border=0 cellPadding=0 cellSpacing=1 width=100%>"		
			+ "<TR>"  	
			+ "<TD id='ProDOpL' style='width:70px'>Opened:</td>"
			+ "<TD id='ProDOpV' style='width:70px' align=right><input name='txtOpened' value='" + MyDateFormat(CreDat) + "' style='width:70px' maxlength='10' READONLY class=textReadOnly></TD>"
			+ "<TD id='ProDRep_L' align=right><B>Reported:</B></TD>"			
			if(MainFlag!='Edit' || ReportDate=='' || ReportDateAuthority()==true){
			g+="<TD id='ProDRep' align=right style='width:70px'><input Name='txtReportDate' value='" + ReportDate + "' style='width: 70px' maxlength='10' onBlur='{parent.frGeneral.document.frmGeneral.txtReportDate.value=parent._ReportDate(this.value);parent.ReportDate=this.value;}'></TD>"
			}else{
			g+="<TD id='ProDRep' align=right style='width:60px'><span class=textReadOnly><B>" + ReportDate + "</B></span> </TD>"
			}
	//RECOVERY:
			if(MainFlag=='Edit' && Assignt=='F'){
				g+="<TD id='Recv' align=right>Recovery Poss.:" //+ " <TD id='Recv'  align=right>Does this file have recovery possibilities?</TD>"
				switch(REC_STATUS_Original){  // N <-> P(Y) -> O <-> C
					case "N":
				g +="</TD>"
				  + "<TD id='Recv' style='width:60px'><input TYPE='radio' name='recovery' value='N' onClick='parent.RadioBoxValue_Rec(\"recovery\",\"N\")' "
						if(REC_STATUS=='N'){g +=" CHECKED "}
				g +=">None</TD>"
				  + "<TD id='Recv' style='width:100px'><input TYPE='radio' name='recovery' value='Y' onClick='parent.RadioBoxValue_Rec(\"recovery\",\"Y\")'"
						if(REC_STATUS=='Y'){g +=" CHECKED "}
				g +=">Pending</TD>"
							break;														
					case "Y":
				g +="<input name='RecOpened' value='Potential' style='width:90px' READONLY class=textReadOnly></TD>"
							break;
					case "O":
				g +="<input name='RecOpened' value='Opened' style='width:70px' READONLY class=textReadOnly></TD>"
							break;
					case "C":
				g +="<input name='RecClosed' value='Closed' style='width:70px' READONLY class=textReadOnly></TD>"
							break;
				    default:
				g +="</TD>"
				  + "<TD id='Recv' align=left><input TYPE='radio' Name='recovery' value='N' onClick='parent.RadioBoxValue_Rec(\"recovery\",\"N\")'>None</TD>"
				  + "<TD id='Recv' align=left><input TYPE='radio' Name='recovery' value='Y' onClick='parent.RadioBoxValue_Rec(\"recovery\",\"Y\")'>Pending</TD>"
				}				
			}
			//-------------....-----------
			g +=" <td id='lien' align=right "
				if (Lien=='Y'){
			g +="style='COLOR: red'"
				}
			g += (Unit=='WC' || SCREEN=="MainWorkComp") ? ">Lien?" : ">Mortgage/Lien?"
			g += "</td>"
			+ "<td id='lien' style='width: 45px'><select name='lien' onChange='parent.Lien=this.value;'>" 
		  if(Lien=='N'){ 	
			g +="<option value='N' selected>No</option><option value='Y'>Yes</option>"
			  }else if(Lien=='Y'){ 
			g +="<option value='Y' selected>Yes</option><option value='N'>No</option>"
			  }else{ 
			g +="<option></option>"
				  + "<option value='Y'>Yes</option><option value='N'>No</option>"
			   } 
			g +="</select></td>"
            if(MainFlag=='Edit'){
                g += "<td align=right><span id='lblMaintanenceClaim'>Maintenance Mode?</span>"
			    + " <input type='checkbox' name='chkMaintanenceClaim' value='' onclick='parent.MaintanenceClaim_XTRCT(this.checked);'></td>"   
            }
            g += "<td id='lienAsL' align=right><B>Assignment:</B></td>"
			+ "<td id='lienAsV' style='width:70px'><select name='cboAsgmtType' onChange='parent.LocationLabel(this.value)'>" + Assign_Types + "</select></td>"
			+ "</tr></table>"	   

		+ "</td></tr></table>"
//hidden fields:
		+ "<input name=ToIntegra Type='hidden' value=''>"
		+ "<input name=iCovCod Type='hidden' value='" + CovCod + "'>"
		+ "<input name=iParty Type='hidden' value=''>"
	}else{
		 g="<html><head>"
				+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
				+ "</head><body>"
				+ "<Form name=frmGeneral>"
				+ "<table border=1 frame=below rules=none cellPadding=2 cellSpacing=1 width=100% id='TableGeneral'><tr><td>"
//1:				
//2:
				+ "<table border=0 cellPadding=0 cellSpacing=1 width=100%>"		
				+ "<TR>"
				+ "<TD id='Clnt' style='width:160px'>M/A Company-Dept Project:</TD>"
				+ "<TD id='Clnt' align=left style='width:35px'><input name='txtClntNo' value='" + ClientNumber + "' style='width:35px' class=textReadOnly READONLY></TD>"
				+ "<TD id='Clnt' align=left><span class=textReadOnly><B>" + trim(ClientName) + "</B></span></TD>"

			  g +="</TR></table>"

				+ "</td></TR><tr><td>"
//3:
				+ "<table cellPadding=0 cellSpacing=1 width=100% border=0><TR>"
				+ "<TD id='Clam' style='width:70px'>Task #:</TD>"
				+ "<TD id='Clam' style='width:100px' align=left><input name='txtClmNo' value='" + ClaimNumber + "' style='width:100px' class=textReadOnly READONLY></TD>"

			    + "<TD id='Clam' align=right>Status:</td>"
				+ "<TD id='Clam' align=left style='width:20px'><B><input type='text' value='" + Status + "' name='txtStatus' class=textReadOnly style='width:20px' onClick='parent.MyStatus(this.value,parent.Unit)'></B></TD>"
				+ "<TD id='Clam' align=right><B><span>Start Date:</span></B></td>"
				+ "<TD id='Clam' align=left><input name='txtAccDT' value='" + MyDateFormat(AccDate,1) + "' style='width:80px' maxlength='10'  onBlur='{document.frmGeneral.txtAccDT.value=parent.MyDate(parent.frGeneral.document.frmGeneral.txtAccDT.value,1);}'></TD>"
				
				g+="</TR></table>"

				+ "</td></TR><tr><td>"
//4:
				+ " <table cellPadding=0 cellSpacing=1 width=100% border=0><TR>"
			  g +="<TD id='Adju_Super' style='width:70px'><B>Sponsor:<B></TD>"
				+ "<TD id='Adju_List_Super' aligh=right style='width:120px'><select name='cboSuper'>"
				+ SuperUser
				+ "</SELECT></TD>"
				g +="<TD align=left style='width:40px'>"
				g += iscboSuper
				  + "</TD>"
				  
			  g +="<TD id='Adju' align=right><B>Member 1:</B></TD>"
				+ "<TD id='Adju_List' align=left style='width:120px'>"				
				+ "<select name='cboAdjuster' style='FONT-SIZE:8pt'>" + ADJ + "</SELECT>" //onChange='parent.Adj_UserFunc()'
				+ "</TD>"				
			if(UserRole.indexOf('EditAdj')!=-1 || MainFlag !='Edit'){
				g +="<TD align=left style='width:40px'>"
				g += iscboAdjuster
				  + "</TD>"
			}
			  g +="<TD id='Adju_As' align=right>Member 2:</TD>"
				+ "<TD id='Adju_List_As' style='width:120px'>"
				+ "<select name='cboAsAdjuster' style='FONT-SIZE:8pt'><OPTION value=0></OPTION>" + ADJ + "</SELECT>" //onChange ='parent.AsAdj_UserFunc()'
				+"</TD>" 
		if(UserRole.indexOf('EditAssist')!=-1 || MainFlag !='Edit'){
			 g +="<TD align=left style='width:40px'>"
			 g += iscboAsAdjuster			  
			  + "</TD>"
		}	
			  g +="</TR></table>"  

			+ "</td></TR><tr><td>"  
//5:
			+ "<table border=0 cellPadding=0 cellSpacing=1 width=100%>"		
			+ "<TR>"  	
			g+="</tr></table>"	   

		+ "</td></tr></table>"
//hidden fields:
		+ "<input Type='hidden' name='cboCompany' value='100'>"
		+ "<input Type='hidden' name='cboOffice' value='999'>"
		+ "<input Type='hidden' name='cbolLineBus' value='MA'>"
		//+ "<input Type='hidden' name='txtAccDT' value='" + MyDateFormat(CreDat)  + "'>"
		+ "<input Type='hidden' name='cboLossState' value='NY'>"
		+ "<input Type='hidden' name='txtOpened' value='" + MyDateFormat(CreDat)  + "'>"
		+ "<input Type='hidden' name='txtReportDate' value='07/01/2006'>"
		
		+ "<input type='hidden' name=ToIntegra  value=''>"
		+ "<input type='hidden' name=ClmParty value='1st'>"
		+ "<input name=iCovCod type='hidden' value='" + CovCod + "'>"
		+ "<input name=iParty type='hidden' value=''>"
		
		+ "<input  name='cboAsgmtType' Type='hidden' value='F'>"
		+ "<input  name='lien'  Type='hidden' value='N'>"
		
		+ "<input name='txtPolNo' value='M_A' Type='hidden'>"
		+ "<input name='txtEffDT' value='01/01/1950' Type='hidden'>"
		+ "<input name='txtExpDT' value='01/01/2050' Type='hidden'>"
	}
	return(g);
}
	
	function Variable_Populate_Minor_Screen(var_pass){
	    var c;

		switch (var_pass) {
		    case "injury":
		        //ticket 73352: R3
		        if (MainFlag != 'Edit') {
		            if (Unit != 'WC' || TYPE_Loss == 'BI') {
		                if (OccurrenceInjuryData_XTRCT()==true) {
		                    if (typeof (ClmOriginal.OccurrenceInjuryData) == "object") {
		                        if (ClmOriginal.OccurrenceInjuryData.hasOwnProperty('id')) {
		                            NCCI_SOI = ClmOriginal.OccurrenceInjuryData.soi;
		                            NCCI_NOI = ClmOriginal.OccurrenceInjuryData.noi;
		                            BodyPartCod = ClmOriginal.OccurrenceInjuryData.bodypart;
		                            JurState = ClmOriginal.OccurrenceInjuryData.juristate;
		                            Injuries = ClmOriginal.OccurrenceInjuryData.injuries;
		                            Fatal = ClmOriginal.OccurrenceInjuryData.fatal;
		                        }
		                    }
		                }
		            }
		        }
		        injury = Populate_injury(); break;
			case "workcomp" :
				workcomp=Populate_workcomp(); break;
			case "workcomp_ca" :
				workcomp_ca=Populate_workcomp_ca(); break;
			case "vehicle" :
				vehicle=Populate_vehicle(); break;
			case "doc" :
				doc=Populate_doc(); break;		
			case "law" :
				law=Populate_law(); break;
			case "claimant" :
				claimant=Populate_claimant(); break;				
			case "fin" :
				fin=Populate_fin(); break;
//			case "sir" :
//				sir=Populate_sir(); break;				
			case "res" :
				res=Populate_res(''); break;
//			case "defc" :
//				defc=Populate_defc(); break;
			case "assistance":
				assistance = Populate_assistance(); break;
			case 'request_assistance':
                request_assistance = Populate_request_assistance(frInfo.frmClaim.AssistType.value, frInfo.frmClaim.AssistType[frInfo.frmClaim.AssistType.selectedIndex].text); break;
			case "iadj" :
			    iadj = Populate_adjuster_independent(); break;
			case "prodliab":
			    prodliab = Populate_product_liability(); break;
			case "producer" :
				producer=Populate_producer(); break;
//			case "ysi" :
//				ysi=Populate_ysi(); break;
			case "listloc" :
			    listloc = Populate_listloc(); 
				break;
			case "rec" :
			    rec = Populate_rec(); break;									
			case "custom" :
				custom=Populate_custom();break;	
			case "customList":
			    customList = Populate_customList(); break;
			case "prefProvider":
			    prefProvider = Populate_prefProvider(); break;
			case "complex" :
			    complex = Populate_complex(); break;
			case "polCCinfo":
			    polCCinfo = Populate_polCCinfo(); break;
			case "rmc":
			    rmc = Populate_Reserve_MinorCodes(Unit); break;
		    case "lossLoc":
		        try {
		            lossLoc = Populate_LossLocation();
		        }catch(e){}
		        break;
			case "polInfo" :  //POLSTRUCT A - only
				try{
					if(PolInx==-1){
						alert('Please select Policy');
					}else{
						polInfo = Populate_PolVehicleList(PolInx);
					}
				}catch(e){}
		}
	}

	function Populate_claim_ClaimantRow(){
	    if (DateComp(MyDateFormat(CreDat), '02/14/2006', 1) == 1 && (TYPE_Loss == "PD")) {
			CPerson='U'
			var r=""
				+ "<table border1=1 frame=below cellPadding=1 cellSpacing=1 width=95%>"
				+ "<TR>"
				+ "<TD id='Clmt_L' style='width:75px'><B>Claimant:</B></TD>"
				+ "<TD id='ClmtNV' style='width:400px'><input name='txtClaimant' style='width:100%' maxlength='50' value='" + CName + "' onBlur='parent.CName=parent.SingleQuote(this.value);'></TD>"
                + "<input type='hidden' name='cboCPrsn' value='U'>"
				+ "</TD>"
		}else{
			if(Unit=='WC'){
				CPerson='Y'
			}		
			var r=""
				+ "<table border1=1 frame=below cellPadding=1 cellSpacing=1 width=95%>"
				+ "<TR>"
				+ "<TD id='Clmt_L' style='width:75px'><B>Claimant:</B></TD>"
				
				if(Unit=='WC'){
					r+="<input type='hidden' value='Y' name='cboCPrsn'>"
				}else{				
					r+="<TD id='ClmtPL' align=right><B>Person?</B></TD>"
					+ "<TD id='ClmtPV' style='width:40px'>"			
					+ "<select name='cboCPrsn' onChange='parent.CPerson=this.value;parent.ClmntRow(this.value);parent.frButtons.document.frmButtons.ClaimantFlag.value=1'>"
					+ "<option></option>"
					+ "<option value='Y' "
					if(CPerson=='Y'){r+=" selected "}
					r+=">YES</option>"
					+ "<option value='N' "
					if(CPerson=='N'){r+=" selected "}
					r+=">NO</option>"
					+ "</SELECT>"
					+ "</TD>"
				}				
				if(CPerson=='Y'){
					if(CName.length>0 && trim(CLName).length==0){
						if(CName.length>26){
							if(CFName=='' && CLName==''){
								var idx=Person_Name_Ref(CName)
								if(idx==-1){
									CFName=CName.substring(0,24)
									CLName=CName.substr(25)
								}else{
									CFName=CName.substring(0,idx)
									CLName=CName.substr(idx+1)
								}
							}
						}else{
							if(CName.length>0){
								CFName=CName;
							}
						}
					}
					r+="<TD id='ClmtFNL' align=right><B>First Name:</B></TD>"
					 + "<TD id='ClmtFNV' align=left style='width:180px'><input name='txtCFName' maxlength='25' style='width:100%' value='" + CFName + "' onBlur='' onChange='parent.CFName=parent.SingleQuote(this.value);parent.CName=parent.Person_Name(parent.CLName,parent.CFName);parent.frButtons.document.frmButtons.ClaimantFlag.value=1'></TD>"
					 + "<TD id='ClmtMIL' align=right><B>MI:</B></TD>"	
					 + "<TD id='ClmtMIV' align=left style='width:20px'><input name='txtCMI' maxlength='1' style='width:100%' value='" + CMI + "' onBlur='' onChange='parent.CMI=this.value;parent.frButtons.document.frmButtons.ClaimantFlag.value=1'></TD>"
					 + "<TD id='ClmtLNL' align=right><B>Last Name:</B></TD>"
					 + "<TD id='ClmtLNV' align=left style='width:180px'><input name='txtCLLast' maxlength='25' style='width:100%' value='" + CLName + "' onBlur='' onChange='parent.CLName=parent.SingleQuote(this.value);parent.CName=parent.Person_Name(parent.CLName,parent.CFName);parent.frButtons.document.frmButtons.ClaimantFlag.value=1'></TD>"
				}else{
					//to insure back ref of CName and First/Last Name - populare it - at the moment of submitting - it will be cleaned up;
					if(CName.length>26){
						if(CFName=='' && CLName==''){
							var idx=Person_Name_Ref(CName)
							if(idx==-1){
								CFName=CName.substring(0,24)
								CLName=CName.substr(25)
							}else{
								CFName=CName.substring(0,idx)
								CLName=CName.substr(idx+1)
							}
						}
					}else{
						if(CName.length>0){
							CFName=CName;
						}
					}
					r+="<TD id='ClmtNL' align=right><B>Company Name:</B></TD>"
					 + "<TD id='ClmtNV' style='width:400px'><input name='txtClaimant' style='width:100%' maxlength='50' value='" + CName + "' onBlur='parent.CName=this.value;'></TD>"
				}
		}
		
		r += "<TD id='Time' align=right>Loss Time:</TD>"
		                  + "<TD id='Time' style='width: 40px'><input name='txtTime'  style='width: 40px' value='" + AccTIME + "'  maxlength='5' onBlur='{this.value=parent.MyTime(this.value);}'></TD>"
		                  + "<TD id='Time' style='width: 45px'><select name='cboOpenAM_PM' value='" + AMPM + "' style='width: 45px'>"
		if (AMPM == 'P' || AMPM == 'PM') {
		    r += "<option value='P' selected>PM"
		                  + "<option value='A'>AM"
		} else if (AMPM == 'A' || AMPM == 'AM') {
		    r += "<option value='A' selected>AM"
		                  + "<option value='P'>PM"
		} else {
		    r += "<option></option>"
		                  + "<option value='A'>AM"
		                  + "<option value='P'>PM"
		}
		r += "</SELECT></TD>"
		r += "<TD id='ProDT_L' align=right><b>Contact Date:</b></TD>"
				 + "<TD id='ProDT' style='width:70px'><input name='txtContactDate' value='" + ContactDate + "' style='width:70px' maxlength='10' onBlur='{this.value=parent.BillingDate(this.value,\"txtContactDate\",\"regular\");parent.ContactDate=this.value;}'></TD>"
				 + "</TR>"
				 + "</table>"
				 
			return(r);
	}

	function Populate_claim_InsuredsRow() {
		if(DateComp(MyDateFormat(CreDat),'02/14/2006',1)==1){
			IPerson='U'
			var r=""
			+ "<table border1=1 frame=below cellPadding=1 cellSpacing=1  width=95%>"  
			+ "<TR id=''>"
			+ "<TD id='InsdLb1' style='width:75px'><B>Insured:</B></TD>"
			+ "<TD id='InsdNV'><input name='txtInsName' value='" + IName + "' style='width:100%' maxlength='50' onBlur='parent.IName=this.value;'>"
			+ "<input type='hidden' name='cboIPrsn' value='U'>"
			+ "</TD>"			
			+ "</TR></table>"
		}else{
			var r=""
			+ "<table border1=1 frame=below cellPadding=1 cellSpacing=1  width=95%>"  
			+ "<TR id=''>"
			+ "<TD id='InsdLb1' style='width:75px'><B>Insured:</B></TD>"
			+ "<TD id='InsdPL' style='width:55px'><B>Person?</B></TD>"
			+ "<TD id='InsdPV' style='width:40px'>"
			+ "<select name='cboIPrsn' onChange='parent.IPerson=this.value;parent.InsdRow(this.value);parent.frButtons.document.frmButtons.InsuredsFlag.value=1'>"
			+ "<option></option>"
			+ "<option value='Y' "
				if(IPerson=='Y'){r+=" selected "}
				r+=">YES</option>"
				+ "<option value='N' "
				if(IPerson=='N'){r+=" selected "}
				r+=">NO</option>"
				+ "</SELECT>"
				+ "</TD>"
				if(IPerson=='Y'){
					r+="<TD id='InsdFNL' align=right><B>First Name:</B></TD>"
					 + "<TD id='InsdFNV' align=left style='width:180px'><input name='txtIFName' maxlength='25' style='width:100%' value='" + IFName + "' onBlur='parent.IFName=parent.SingleQuote(this.value);parent.IName=parent.Person_Name(parent.ILName,parent.IFName);' onChange='parent.frButtons.document.frmButtons.InsuredsFlag.value=1'></TD>"
					 + "<TD id='InsdMIL' align=right><B>MI:</B></TD>"
					 + "<TD id='InsdMIV' align=left style='width:20px'><input name='txtIMI' maxlength='1' style='width:100%' value='" + IMI + "' onBlur='parent.IMI=this.value;' onChange='parent.frButtons.document.frmButtons.InsuredsFlag.value=1'></TD>"
					 + "<TD id='InsdLNL' align=right><B>Last Name:</B></TD>"
					 + "<TD id='InsdLNV' align=left style='width:180px'><input name='txtILLast' maxlength='25' style='width:100%' value='" + ILName + "' onBlur='parent.ILName=parent.SingleQuote(this.value);parent.IName=parent.Person_Name(parent.ILName,parent.IFName);' onChange='parent.frButtons.document.frmButtons.InsuredsFlag.value=1'></TD>"
				}else{
					//to insure back ref of CName and First/Last Name - populare it - at the moment of submitting - it will be cleaned up;
					if(IName.length>26){
						if(IFName=='' && ILName==''){
							var idx=Person_Name_Ref(IName)
							if(idx==-1){
								IFName=IName.substring(0,24)
								ILName=IName.substr(25)
							}else{
								IFName=IName.substring(0,idx)
								ILName=IName.substr(idx+1)
							}
						}
					}else{
						if(IName.length>0){
							IFName=IName;
						}
					}
					r+="<TD id='InsdNL' align=right style='width:55px'><B>Company Name:</B></TD>"
					 + "<TD id='InsdNV'><input name='txtInsName' value='" + IName + "' style='width:100%' maxlength='50' onBlur='parent.IName=this.value;'></TD>"
				}			
				r+="</TR></table>"			
		}
		return(r);
	}				

	function Populate_complex(){		
		var r=''
		+"<html>"
		+"<head>"		
		+"<title>complex</title>"
		+"<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		+"</head>"
		+"<body><form name='frmClaim'>"
		+"<table border=0 width='100%'>"
		+"<tr>"
		+"<td width='100%'>"
		+"<table border='0' width='100%'>"
		+"<tr>"
		+ "<td>Type of Issue:</td>"
		+ "<td><select name='cboTypeOfIssue'>"
		if (Complex_IssueType == '') {
		    Complex_IssueType = ComplexRefLists_XTRCT('Issue Type')
		}
		r += Complex_IssueType
		r += "</select></td>"
		
		+"<td>Referral Date:</td>"
		+"<td><input type='text' name='txtReferralDate' maxlength='10' style='width:65px' value='" + cmpxReferralDate + "'   onBlur='this.value=parent.BillingDate(this.value,\"txtReferralDate\",\"regular\")'></td>"
		+"<td>Referral Reason:</td>"
			r+= "<td><select name='cboReferralReason'>"
			if(Complex_ReferralReason==''){
				Complex_ReferralReason= ComplexRefLists_XTRCT('Referral Reason')
			}
			r+=Complex_ReferralReason
			r+="</select></td>"
		+"</tr>"
		+"<tr>"
		+"<td>Date of First Report to Carrier:</td>"
		+"<td><input type='text' name='txtDateOfFirstReport' style='width:65px' maxlength=10 value='" + cmpxFirstReport + "' onBlur='this.value=parent.BillingDate(this.value,\"txtDateOfFirstReport\",\"regular\")'></td>"
		+"<td>Carrier Number:</td>"
		+"<td><input type='text' name='txtVSONumber' size='15' value='" +  cmpxVSONumber + "' maxlength=15></td>"
		r += "<td>Carrier Analyst:</td>"  //VS Analyst
		+"<td><input type='text' name='txtVSAnalyst' size='25' value='" + cmpxVSAnal + "' maxlength=25></td>"
		+"</tr>"
		+"<tr>"

		+"<td>Carrier Response received:</td>"  //VS Response received
		+"<td>"
		+"<input TYPE='radio' NAME='radioVSResponse' VALUE='Y' onClick='parent.cmpxVSResponse=\"Y\"'>Yes"
		+"<input TYPE='radio' NAME='radioVSResponse' VALUE='N' onClick='parent.cmpxVSResponse=\"N\"'>No"
		+"</td>"
		+ "<td>Date of Carrier Response:</td>"  //Date of VS Response
		+"<td><input type='text' name='txtDateVSResponse' style='width:65px' maxlength='10' value='" + cmpxVSDate + "' onBlur='this.value=parent.BillingDate(this.value,\"txtDateVSResponse\",\"regular\")'></td>"
		+"</tr>"
		+"<tr>"
		+"<td>SLA Handling Field Assignment</td>"
		+"<td>"
		+"<input TYPE='radio' NAME='radioSLAHandling' VALUE='Y' onClick='parent.SLA =\"Y\"'>Yes"
		+"<input TYPE='radio' NAME='radioSLAHandling' VALUE='N' onClick='parent.SLA=\"N\"'>No"
		+"</td>"
		+"<td>SLA GA</td>"
		+"<td><select name='cboSLAGA'>" 
		if(SLA_GA_LIST==''){
			 SLA_GA_LIST= BranchEmployee_XTRCT(136)
		}
		r+= SLA_GA_LIST
		r+="</select></td>"
		+"<td>&nbsp;</td>"
		+"<td>&nbsp;</td>"
		+"</tr>"
		+"</table>"
		+"<table border='0' width='100%'>"
		+"<tr>"
		+"<td>Note</td>"
		+"<td><textarea rows=3 name='txtNote' cols='80%' maxlength=250>" + cmpxNote + "</textarea></td>"
		+"</tr>"
		+"</table>"
		+"<input type='hidden'  name='A' value ='" + cmpxVSAnal + "' style='width:175px'>"	
		+"</td>"
		+"</tr>"
		+"</table>"
		+"</form></body>"
		+"</html>"
		return(r)
	}
		

	function Populate_claim_HierarchyRow() {
		var c=''
			c +="<table border1=1 frame=below cellPadding=1 cellSpacing=1 width=95%>"
			  + "<tr>"
			//Level 1:
			if(LocLegend!=''){
			    c += "<td style='width:75px' id='LocLegend'><b>" + LocLegend + ":</b></td>"
				c +="<td><select name='cboLocation' onchange ='if(this.value==\"GET ANOTHER\"){parent.LookupWindow_Loc(1, parent.LocLegend, parent.DivLegend)}'>"
						 + "<option value='GET ANOTHER'>GET ANOTHER</option>"
						 try{			
						     c += "<option value='" + EditLoc[1][0] + "'  selected>" + EditLoc[1][10]  
						     c += (EditLoc[1][17].length == 0) ? "" : " [" + EditLoc[1][17] + "]"
                             c +=  '__' +EditLoc[1][1]+ ', ' +EditLoc[1][2]
						 }catch(e){
							c +="<option value='0' selected></option>"
						 }
				c +="</select>"
				  + "<span id=ClntLoc></span>"
				  + "</td>"
			}
			//Level 2:
			if(DivLegend!=''){
				try{
					c +="<td align=right>" + DivLegend + ":</td>"
					c +="<td><select name='cboDiv' onchange ='if(this.value==\"GET ANOTHER\"){parent.LookupWindow_Loc(2,parent.DivLegend, parent.Div2Legend)}'>"
					  + "<option value='GET ANOTHER'>GET ANOTHER</Option>" 	
					 try{			
					     c += "<option value='" + EditLoc[1][11] + "'  selected>" + EditLoc[1][13];
					     c += (EditLoc[1][18].length == 0) ? "" : " [" +EditLoc[1][18] + "]";
					     c += '__' + EditLoc[1][12];
					 }catch(e){
						c +="<option value='0' selected></option>"
					 }			
				}catch(e){}
				c +="</select>"
			      + "<span id=ClntDiv> " 
			     // c+=(trim(EditLoc[1][13])!='') ? " # " + trim(EditLoc[1][13]) : ''
			      + "</span>"
				  + "</td>"
			}
			//Level 3:
			if(Div2Legend!=''){
				try{
					c +="<td align=right>" + Div2Legend + ":</td>"
					c +="<td><select name='cboDiv2' onchange ='if(this.value==\"GET ANOTHER\"){parent.LookupWindow_Loc(3,parent.Div2Legend,\"\")}'>"
					  + "<option value='GET ANOTHER'>GET ANOTHER</Option>" 	
					 try{			
					     c += "<option value='" + EditLoc[1][14] + "'  selected>" + EditLoc[1][16];
					     c += (EditLoc[1][19].length == 0) ? "" : " [" + EditLoc[1][19] + "]";
                         c += '__' +EditLoc[1][15];
					 }catch(e){
						c +="<option value='0' selected></option>"
					 }			
				}catch(e){}
					c +="</select>"
					+ "<span id=ClntDiv2>"
					//c +=(EditLoc[1][16]!='') ? " # " + trim(EditLoc[1][16]) : ''
					+ "</span>"
					+ "</td>"
			}
			c +="<td>" //REGION:
			  + "<span id=ClntRegion>" 
			c += (EditLoc[1][8] != '' && EditLoc[1][8] != '0') ? " REGION: " + trim(EditLoc[1][8]) : '';
			  +"</span>"
			  + "</td>"
			  //////////
				switch(ClientNumber){	
					case '766':	
						c +="<TD id='Divi' align=right>Division:</TD>"
						      + "<TD id='Divi' align=right style='width: 90px'><select  name='txtDivision' style='width: 90px'>"
						      + "<Option></Option>"
						if(trim(DIV)=='BRONX'){
						c +="	<option value='BRONX     ' selected>BRONX</Option>"
						}else{
						c +="	<option value='BRONX     '>BRONX</Option>"
						}
						if(trim(DIV)=='BROOKLYN'){
						c +="	<option value='BROOKLYN  ' selected>BROOKLYN</Option>"
						}else{
						c +="	<option value='BROOKLYN  '>BROOKLYN</Option>"
						}
						if(trim(DIV)=='MANHATTAN'){
						c +="	<option value='MANHATTAN ' selected>MANHATTAN</Option>"
						}else{
						c +="	<option value='MANHATTAN '>MANHATTAN</Option>"
						}
						if(trim(DIV)=='STATEN ISL'){
						c +="	<option value='STATEN ISL' selected>STATEN ISLAND</Option>"
						}else{
						c +="	<option value='STATEN ISL'>STATEN ISLAND</Option>"
						}
						if(trim(DIV)=='OTHER'){
						c +="	<option value='OTHER     ' selected>OTHER</Option>"
						}else{
						c +="	<option value='OTHER     '>OTHER</Option>"
						}
						c +="</SELECT>"
						break
					case '873':
						c +="<TD id='Divi' align=right>Division:</TD>"
						c +=" <TD id='Divi' align=right style='width: 90px'><select  name='txtDivision' style='width: 90px'>"
						c +="<Option></Option>"
						if(trim(DIV)=='UNIT'){
						c +="	<option value='UNIT      ' selected>UNIT</Option>"
						}else{
						c +="	<option value='UNIT      '>UNIT</Option>"
						}
						if(trim(DIV)=='OFFICE'){
						c +="	<option value='OFFICE    ' selected>OFFICE</Option>"
						}else{
						c +="	<option value='OFFICE    '>OFFICE</Option>"
						}
						if(trim(DIV)=='MAINT AREA'){
						c +="	<option value='MAINT AREA' selected>MAINT AREA</Option>"
						}else{
						c +="	<option value='MAINT AREA'>MAINT AREA</Option>"
						}
						if(trim(DIV)=='POOL'){
						c +="	<option value='POOL      ' selected>POOL</Option>"
						}else{
						c +="	<option value='POOL      '>POOL</Option>"
						}
						if(trim(DIV)=='REC AREA'){
						c +="	<option value='REC AREA  ' selected>REC AREA</Option>"
						}else{
						c +="	<option value='REC AREA  '>REC AREA</Option>"
						}
						if(trim(DIV)=='OTHER'){
						c +="	<option value='OTHER     ' selected>OTHER</Option>"
						}else{
						c +="	<option value='OTHER     '>OTHER</Option>"
						}
						c +="</SELECT>"
						break
					case '1003':
						c +="<TD id='Divi' align=right>Division:</TD>"
						c +=" <TD id='Divi' align=right style='width: 90px'><select  name='txtDivision' style='width: 90px'>"
						c +="<Option></Option>"
						if(trim(DIV)=='INSIDE'){
						c +="	<option value='INSIDE    ' selected>INSIDE</Option>"
						}else{
						c +="	<option value='INSIDE    '>INSIDE</Option>"
						}
						if(trim(DIV)=='OUTSIDE'){
						c +="	<option value='OUTSIDE   ' selected>OUTSIDE</Option>"
						}else{
						c +="	<option value='OUTSIDE'>OUTSIDE</Option>"
						}
						c +="</SELECT>"
						break
					case '1005':
						c +="<TD id='Divi' align=right>Division:</TD>"
						c +=" <TD id='Divi' align=right style='width: 90px'><select  name='txtDivision' style='width: 90px'>"
						c +="<Option></Option>"
						if(trim(DIV)=='INSIDE'){
						c +="	<option value='INSIDE    ' selected>INSIDE</Option>"
						}else{
						c +="	<option value='INSIDE    '>INSIDE</Option>"
						}
						if(trim(DIV)=='OUTSIDE'){
						c +=" <option value='OUTSIDE   ' selected>OUTSIDE</Option>"
						}else{
						c +=" <option value='OUTSIDE'>OUTSIDE</Option>"
						}		
						c +="</SELECT>"			
						break
					default:
						c +="<input Name='txtDivision' value='" + DIV + "' type='hidden'>"
				}
			  /////////
			  c +="</tr></table>"
		return(c);
	}
		
	function Populate_claim_CoverageRow(){
		var r=""
			+ "<table border1=1 frame=below cellPadding=1 cellSpacing=1 width=95%>"  
			+ "<tr>"
			+ "<TD id='CovlL' style='width:75px'><B><span id=lblCov>Coverage:</span></B></TD>"
			+ "<TD id='CovlV' align=left><select name='cboCov' onChange='parent.Loss_Cause_List_XTRCT();parent.MyTypeOfLoss(this.name, this.value);parent.PIPCodeList_Get(0);'></SELECT></TD>"
			
		    if (Unit != 'WC') {
		        r += "<TD align=right id='LossCause'>Cause:</TD>"
                    + "<TD><select name='cboLoss_Cause'  id='cboLoss_Cause' onchange='parent.LossCause=this.value; var ct=parent.frInfo.document.frmClaim.cboCov.options[parent.frInfo.document.frmClaim.cboCov.options.selectedIndex].text;var lt=this.options[this.selectedIndex].title; parent.TYPE_Loss=parent.TYPE_Loss_Set(lt,ct);parent.CustomCause_List_XTRCT(\"\",\"\",this.value);'>" + strLoss_Cause + "</select></TD>"
		    } else {
		        r += "<TD align=right id='LDesL'>Loc. Desc.:</TD>"
	              + "<TD id='LDesV' style='width:400px'><input  name='txtLossLoc' value='" + LocDesc + "' style='width:400px' maxlength ='65'></TD>"
		    }

    	    r+="<td id='County_V' align=right>County:</td>"
    	    r += "<td id='County_List' align=left><select name='cboCounty' id='cboCounty'><OPTION value=''>-- NOT SPECIFIED --</OPTION></select></td>"  
	        
			r+="</tr>"
			+ "</table>"
		return(r);
	}

	function Populate_claim_DescrRow() {
	    var r = ""

	    r += "<table border1=0 frame=below cellPadding=0 cellSpacing=0 width=95%>"
                + " <col WIDTH=65%><col WIDTH=35%>"
                + "<tr><td>"
	                 + "<table border=0 frame=below cellPadding=1 cellSpacing=1 width=99%>"
                     + "<tr>"
                     + "<td id='Desc' style='width:75px'>Loss Description:</td>"
                     + "<td id='Desc' align=left>"
                         + "<input name='txtLossDescr' id='txtLossDescr' value=''  style='width:100%' ></br>"
                         + "<input name='txtLossDescr2' id='txtLossDescr2'  value='' style='width:100%'></br>"
                         + "<input name='txtLossDescr3' id='txtLossDescr3'  value='' style='width:100%'>"
                     + "</td></tr></table>"
                + "</td><td>"
	                 + "<table border=0 frame=below cellPadding=0 cellSpacing=0 width=99%>"
                     + "<tr>"
	                + "<td id='CarrLbl' align=right>Car.Claim #:</TD>"
                    +" <td id='Carr' style='width:180px' align=right><input id='txtCarrierClmno'  name='txtCarrierClmno' value='' style='width:100%'></td>"
	                r += "</tr><tr>"
	                  +  "<td id='ClCmLbl' align=right>Client Claim #:</TD>"
                      +  "<td id='ClCm' style='width:180px' align=right><input id='txtClientsNum'  name='txtClientsNum' value=''  style='width:100%'></td>"
	                  + "</tr><tr>"
	                  + "<td id='3-rdlbl' align=right >3-rd Party #:</TD>"
                      + "<td id='3-rd' style='width:180px' align=right><input  id='txt3PartyNo'  name='txt3PartyNo' value='' style='width:100%' maxlength=25></TD>"
                      +  "</tr></table>"
             + "</td></tr></table>"

	    return (r);
	}

/*
// 
	function Populate_claim_DescrRow(){
		var r=""

			if(Desc.length<101){
				r+="<table border1=1 frame=below cellPadding=1 cellSpacing=1 width=95%>"
				 + "<TR>"
				 + "<TD id='Desc' style='width:75px'>Description:</TD>"
				 + "<TD id='Desc' align=left><input Name='txtLossDescr' value='" + trim(Desc) + "'  style='width: 100%' maxlength='160'>"
                 + "<input name='txtLossDescr2' value='' type=hidden>"
                 + "<input name='txtLossDescr3' value='' type=hidden>"
                 + "</TD>"
				 + "</TR></table>"
			}else{ //find space in the interval 60 - 110 char; if no space - cut string				
				var subStr=Desc.substr(60,50); var ind=0;
				var re=/[ ]+/g
				var foundArr=re.exec(subStr);
				if(re.lastIndex==-1){
					ind=80 //default
				}else{
					ind=re.lastIndex
				}

				r += "<table border1=0 frame=below cellPadding=0 cellSpacing=0 width=95%>"
				 + "<tr>"
				 + "<td id='Desc' style='width:75px'>Description:</td>"
				 + "<td id='Desc' align=left>"
                 + "<input name='txtLossDescr' id='txtLossDescr' value='" + Desc.substr(0, (60 + ind)) + "' style='width:100%' maxlength='80'></br>"
                 + "<input name='txtLossDescr2' id='txtLossDescr2'  value='" + Desc.substr(60 + ind) + "' style='width:100%' maxlength='79'></br>"
				 + "<input name='txtLossDescr3' id='txtLossDescr3'  value='" + Desc.substr(60 + ind) + "' style='width:100%' maxlength='100'>"
				 + "</td></tr></table>"
			}
		return(r);
	}
*/
	function Populate_claim_0(){
		var r="" //defaults 30, 90; for reserve diary - 0 -do not enerate record in HotList
			r+="<table border=0 cellPadding=1 cellSpacing=1 width=95%><TR>"
			if(Unit=='PL'){
				r+="<TD align=right>Insured's Product:</TD>"
				 + "<TD style='width:80px'><select name='cboInsProduct'><option value=''></option>" + InsProduct + "</Select></TD>" 
			}
//MOVE DATA TO INTEGRA
			//if(Unit=='HO' || Unit=='CO' || Unit=='IM' || Unit=='OM'){
			r+="<TD align=right>Send to MSB estimation system:</TD>"
			 + "<TD style='width:10px'><input type='checkbox' name='chkToIntegra' value='' onClick='parent.TransmitToIntegra();'></TD>" 
			//}
			r+="<TD align=right>Independent Adjuster:</TD>"
			 + "<TD style='width:10px'><input type='checkbox' name='chkIAdjuster' value='" + IAdjFlag_Original + "' onClick='parent.IAdjFlag_Original=1;parent.Index(parent.Unit);parent.frIndex.document.open();parent.frIndex.document.write(parent.I);'></TD>" 
			
			 + "<TD align=right>Producer:</TD>"
			 + "<TD style='width:10px'><input type='checkbox' name='chkProducer' value='" + PrFlag_Original + "' onClick='parent.PrFlag_Original=1;parent.Index(parent.Unit);parent.frIndex.document.open();parent.frIndex.document.write(parent.I);'></TD>" 
			 + "<TD id='TimeEntr' style='width:170px' align=right>Generate Time Record [min]:</TD>"
			 + "<TD id='TimeEntr' style='width:20px'><input name='txtTimeEntr' value='" + TimeEntr + "' maxlength='2' style='width:20px' onblur='{this.value=parent.MyNumber(this.value);}'></TD>"
			 + "</TR>"
			 + "</table>"		
	  
			// + "</TD></TR><tr><td>"		
			 
			 + "<table border=0 cellPadding=1 cellSpacing=1 width=95%><TR>"
			 + "<TD align=right>Who will control the diaries?</TD>"
			 + "<TD>"
			 + "<input type=radio value='C' name=ControlledBy "
			 if(DiaryControlledBy=='C'){r+=' checked '}
			r+=" onClick='parent.DiaryControlledBy=\"C\";'>Supervisor"
			 + "<input type=radio value='O' name=ControlledBy "
			 if(DiaryControlledBy=='O'){r+=' checked '}
			r+=" onClick='parent.DiaryControlledBy=\"O\";'>Adjuster"
			 + "<input type=radio value='B' name=ControlledBy "
			 if(DiaryControlledBy=='B'){r+=' checked '}
			r+=" onClick='parent.DiaryControlledBy=\"B\";'>Both"
			 + "</TD>"
			 + "<TD id='Diar' align=right>Report Diary [days: 0-30 / 0=due today]:</TD>"
			 + "<TD id='Diar' style='width:20px' align=left><input name='txtInitDiary' id='txtInitDiary' value='" + InitDiary + "' maxlength='2' style='width:20px' onblur='{this.value=parent.DiaryRecord(this.value,30);}'></TD>" 
			 + "<TD id='Diar' align=right>Reserve Diary [days: 0-90 / 0=due today]:</td>"
			 + "<TD id='Diar' style='width:20'><input name='txtResDiary' id='txtResDiary' value='" + ResDiary + "' maxlength='2' style='width:20px' onblur='{this.value=parent.DiaryRecord(this.value,90);}'></TD>"		
			 + "</TR></table>"
			return(r);
	}

	function Populate_claim_1(){
		var r=""
			r+="<table border=0 cellPadding=1 cellSpacing=1 width=95%>"
			+ "<tr>"
			//if (MainFlag == 'Edit') {  // Acquired
			 r += (Acquired != '') ? "<td align=right>Acquired on:&nbsp;<span style='color:blue'>" + Acquired + "</span></td>" : "<td></td>"
			//}    
		if(Locked.indexOf('Edit')>-1 && ClmUserRole!='CLT' && ClmUserRole!='MSI' && ClmUserRole!='RAJ'){
			r+="<TD align='right' title='By locking this claim only staff directly related to the claim will be able to acceess it'>Locked Claim:</TD>"
			 + "<TD><input type='checkbox' title='By locking this claim only staff directly related to the claim will be able to acceess it' name='chkLocked' value='" + Locked + "'"
			r+=(Locked.indexOf('Lock')>-1) ? " checked " : ""
			r+="onClick='parent.Lock_Claim(this.checked);'></TD>" 
		}	
		//ASSIGN ASSIST FILE:
		var c=0;
		if (Claim_Assigns.length == 0 && Assignt == 'F') {
		    if (typeof AssistTypeList[0] != 'undefined') {
		        r += "<td id=ReqstAssistance align=right>"
			    + "<A href='javascript:if(parent.frInfo.frmClaim.AssistType.value==\"\"){alert(\"Please specify type of the required assistance\")}else{parent.LookupWindow_RecoveryReferredTo(parent.frInfo.frmClaim.AssistType.value)};' title='Please specify type of the required assistance'>"
			    + "ASSIGNMENT:&nbsp;"
			    + "</A></td>"
			    + "<td><select name='AssistType'><option value=\"\">NOT SPECIFIED</option>"
		        //var u = '';
		        while (c < AssistTypeList[0].length) {
		            if (typeof (AssistTypeList[0][c].descr) != "undefined") {
		                if (Status == 'C' && AssistTypeList[0][c].closed != 'Y') {
		                } else {
		                  //  if (u.indexOf(AssistTypeList[0][c].descr) == -1) {
		                  //     u += AssistTypeList[0][c].descr
		                        r += "<option value=" + AssistTypeList[0][c].assign_type + ">" + AssistTypeList[0][c].descr + "</option>"
		                   // }
		                }
		            }
		            c++
		        }
		        r += "</select></td>"
		    }
		}
		//LIST Admin FILES
		try{
		 if(Claim_Assigns[0].clmno_assist==ClaimNumber){
			r+="<td><span>ADMIN CLAIM(S):</span>"
			if(Claim_Assigns.length==2){
				r+="<input name='AdminClmNo' value='" + Claim_Assigns[1].clmno_assist + "' style='width:90px' class='textReadOnly'>"
			}else{
				r+="<select name='AdminClmNo'>"
						c=1;
						while(c<Claim_Assigns.length){
							r+="<option value='" + Claim_Assigns[c].clmno_assist + "'>" + Claim_Assigns[c].clmno_assist + " since " + Claim_Assigns[c].credat + "</option>"
							c++
						}
					r+="</select>"									
			}
			r+= "&nbsp;<input type='button' title='add admin claim to portal queue' style='width:25px' name='cmdAdQueue' value='Q' onClick='parent.mmAddToClaimList(parent.frInfo.document.frmClaim.AdminClmNo.value,\"Admin file of " + ClaimNumber + "\");'>"
			r+= "&nbsp;<input type='button' title='view admin claim summary' style='width:25px' name='cmdAdDet' value='S' onclick=\"var wnd=window.open('/portal/startup/search/searchClaim/ClaimInfo.asp?claim_no='+parent.frInfo.document.frmClaim.AdminClmNo.value+'','ClaimSummary','status=0,resizable=1;top=9,left=50,width=680,height=900'); wnd.focus()\">"
			+ "</td>"
		 }
		}catch(e){}

//MOVE DATA TO INTEGRA
		//if(Unit=='HO' || Unit=='CO' || Unit=='IM' || Unit=='OM'){
			if(IntegraDate==''){
				if(Status=='O'){
					r+="<TD align=right>Send to MSB estimation system:</TD>"
					+ "<TD style='width:10px'><input type='checkbox' name='chkToIntegra' value='' onClick='parent.TransmitToIntegra();'></TD>" 
				}
			}else{
				r+="<TD align=right>Sent to MSB:</TD>"
				 + "<TD><input type='text' name='chkToIntegra' value='" + IntegraDate + "' style='width:60px'></TD>" 
			}
		//}
//Initial Report to ISO:
		if(ISOInitialDate!=''){
			r+="<TD align=right>Reported to ISO:</TD>"
			// + "<TD><input type='text' name='RepToISO' value='" + ISOInitialDate + "' style='width:60px'></TD>"
			+ "<TD style='width:60px'><span class=textReadOnly onclick='if(this.title!=\"\"){alert(\"ISO REPORTING COMMENT: \" + this.title);};' title='" + ISOCompl + "'><u>" + ISOInitialDate + "</u></span></TD>"
		}
		if(Unit=='PL'){
			r+="<TD align=right>Insured's Product:</TD>"
			 + "<TD style='width:80px'><select name='cboInsProduct'><option value=''></option>" + InsProduct + "</Select></TD>" 
		}
		if(PrFlag_Original==0 && Unit!='WC'){	
			r+="<TD align=right>Producer:</TD>"
			 + "<TD style='width:10px'><input type='checkbox' name='chkProducer' value='" + PrFlag_Original + "' onClick='parent.PrFlag_Original=1;parent.Index(parent.Unit);parent.frIndex.document.open();parent.frIndex.document.write(parent.I);'></TD>" 
		}
		if (prodliabFlag==0 && TYPE_Loss == 'BI') {
			r+="<TD align=right>Product Liability:</TD>"
			 + "<TD style='width:10px'><input type='checkbox' name='chkProductLiability' value='' onClick='parent.prodliabFlag_Original=1;parent.Index(parent.Unit);parent.frIndex.document.open();parent.frIndex.document.write(parent.I);'></TD>" 
		}	
		if(IAdjFlag_Original==0 && Unit!='WC'){	
			r+="<TD align=right>Independent Adjuster:</TD>"
			 + "<TD style='width:10px'><input type='checkbox' name='chkIAdjuster' value='" + IAdjFlag_Original + "' onClick='parent.IAdjFlag_Original=1;parent.Index(parent.Unit);parent.frIndex.document.open();parent.frIndex.document.write(parent.I);'></TD>" 
		}		

			r+="<td id='lblTranL' align=right "
				if(Transfer=='T'){
			r+=" style='COLOR:red'"
				}
			r+=">Transferred :</td>"
				+ " <td id='lblTranV' style='width:5px'><input TYPE='checkbox' Name='chkTransfer' value='" +  Transfer + "'"
				if(Transfer=='T'){
			r+=" CHECKED "		
				}
			r+=" onClick='parent.CheckBoxTransfer()'"
			if(Locked.indexOf('Edit')==-1 || ClmUserRole=='CLT'){
			r+= " disabled "
			}
			 r+="></td>" 			 
			 + "</tr>"
			 + "</table>"
			 r+="<table border=0 cellPadding=1 cellSpacing=1 width=95%>"
			 + "<tr><td align=right><span id='TPD'></span></td></tr>"
			 + "</table>"
			 + "<table border=0 cellPadding=1 cellSpacing=1 width=95%>"			 
			 + "<tr><td align=right>"			 
			 + "<div id='MRC_tobe_appr'>" + Display_RMC_tobe_approved('') + "<div>"
			 + "</td></tr>"
			 + "</table>"
			return(r);
	}

	function Populate_claim_BI(CovText){
		var cl=''
			+ "<table border1=1 frame=below cellPadding=1 cellSpacing=1 width=95%>"  
			+ "<TR id='Row_BI_Info'>"
		if (TYPE_Loss == 'BI') {
		    if (Unit == 'WC') {
		        cl += "<TD id='Inju' style='width: 75px'>Injuries:</TD>"
				+ "<TD id='Inju'  colspan=2><input Name='txtInjuries' value='" + Injuries + "' maxlength='50' style='width:340px'></TD>"
				+ "<TD id='InjuBodyl' align=right><b>Anatomy:</b></TD>"
				+ "<TD id='InjuBodyv' align=left>"				
				+ "<select name='cboAnatomy'"
		        //+ BodyPart
		        cl += (BodyPart.indexOf("<option value=0></option>") > -1) ? "" : " onClick='parent.BodyPart_XTRCT(\"\",0,parent.JurState,parent.Unit)' "
		        cl += ">"
		        cl += (BodyPart == '') ? BodyPart_XTRCT(ClaimNumber, 0, JurState, Unit) : BodyPart
		        cl += "</SELECT></TD>"

				+ "<td id='Minr'><input type='hidden' value='' name='cboMinor'></td>"   //hidden field left for backward compatib.
				
				+ "<td id='MinrFl' align=right>Fatal?</td>"
				+ "<td id='MinrFv' style='width:45px'><select name='cboFatal'>"
		        if (Fatal == 'N' || Fatal == 'No') {
		            cl += " <option value='N' selected>No</option><option value='Y'>Yes</option>"
		        } else if (Fatal == 'Y' || Fatal == 'Yes') {
		            cl += " <option value='Y' selected>Yes</option><option value='N'>No</option>"
		        } else {
		            cl += " <option></option><option value='Y'>Yes</option><option value='N'>No</option>"
		        }
		        cl += "</SELECT>"
		        cl += "<input type='hidden' value='' name='cboCatCode'>"
			    + "</td></tr>"
		    } else {
		        cl += '<td>'
		            + "<input name='txtInjuries' type='hidden' value='" + Injuries + "'>"
		            + "<input name='cboCatCode' type='hidden' value='" + Corp + "'>"
		            + '</td></tr>';
		    }
		}else{
			cl +="<TD id='Inju' style='width: 75px'>Damage:</TD>"
				+ "<TD id='Inju' style='width: 75px'><input Name='txtInjuries' value='" + Injuries + "'  maxlength='50' style='width:340px'></TD>" 
				+ "<TD align=left>"
			if(CovText.indexOf('THEFT')!=-1){
			cl +="<A href='javascript:parent.Var_Save_CLM();parent.DisplaySubjectInsurance();'>LIST OF SUBJECTS OF INSURANCE</A>"
			}
			cl +="<input type='hidden' value='' name='cboAnatomy'>"
				+ "<input type='hidden' value='' name='cboMinor'>"
				+ "<input type='hidden' value='' name='cboFatal'>"
				+ "</TD>"

			if (Unit != 'WC') {
			    cl += "<TD id='CounL_CatL' align=right>CAT:</TD>"
			     + "<TD id='CounL_CatV' style='width:40px'><input name='cboCatCode'  value='" + Corp + "' style='width:40px' maxlength='4' onBlur='{this.value=parent.MyNumber(this.value);}'></TD>"
			    cl += "</tr>"

			} else {
			    cl += "<input type='hidden' value='' name='cboCatCode'>"
			    cl += "</tr>"
			}			
		}

		cl += "</table>"
		
		return(cl);
	}

	function Populate_ThirdPartyDeductible_Span() {
	    var r =""
	    +"<table><tr>"
	    	 + "<td align=right id='tpdL'>Is there a third party deductible:</td>"
	 		 + "<td style='width:20px'><select name=ThirdDeductible onChange='parent.TP_Deductible=this.value;'><option value=''>"
	 		 + "<option value='Y'"
	    r += (TP_Deductible == 'Y') ? " selected" : ""
	    r += ">YES"
	 		 + "<option value='N'"
	    r += (TP_Deductible == 'N') ? " selected" : ""
	    r += ">NO"
	 		 + "</select>"
	 		 + "</td>"
		     + "<td align=right style='width:160px'>Third party deductible amount:</td>"
		     + "<td align=right style='width:50px'><input Name='TP_Deductible_Amount' value='" + commafy(TP_Deductible_Amount) + "' style='width:50px' maxlength='16' style='width: 100px' onBlur='if(parent.MyNumber(parent.decommafy(this.value,1))==\"\"){this.value=parent.commafy(parent.TP_Deductible_Amount)}else{parent.TP_Deductible_Amount=parent.decommafy(this.value); this.value=parent.commafy(parent.TP_Deductible_Amount);}'></td>"
	    + "</tr></table>"

	    return (r); 
	}

	function Populate_claim_RecoveryLine(){
		var r=""
		return(r);		
	}

	function Populate_res(){	
		var cg=''; var _CovCod;
		
		if(Unit=='WC'){
			cg='LOSS TYPE'
		}else{
		    if (Unit == 'HO' || Unit == 'CO' || Unit == 'OM' || Unit == 'IM' || Unit == 'CR') {
				cg='PERIL'
			}else{
				cg='COVERAGE'
			}
		}

		try {
		    var re = /^(\d*)_/gi
		    re.exec(CovCod);
		    _CovCod = RegExp.$1;
		} catch (e) { };
		
		var r="<html><head>"
			+"<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
			+ "<Title>res</Title></head>"
			+ "<body><form name='frmClaim'>"
			+ "<table id='res' style='width:90%' cellPadding=0 cellSpacing=5>"
			+ "<caption>Claim specific transactional information:</caption>"
			+ "<tr>"
			+ "<td>"	
			+ "<span id='STATViewSwitch' style='color:blue;cursor:hand' onClick='parent.Populate_Claim_Transactions(\"STAT\")'>+ DISPLAY STATUS</span>"
			+ "</td>"
			+ "<td>"	
			+ "<span id='BRCHViewSwitch' style='color:blue;cursor:hand' onClick='parent.Populate_Claim_Transactions(\"BRCH\")'>+ DISPLAY BRANCH</span>"
			+ "</td>"
			+ "<td>"	
			+ "<span id='ADJViewSwitch' style='color:blue;cursor:hand' onClick='parent.Populate_Claim_Transactions(\"ADJ\")'>+ DISPLAY HANDLERS</span>"
			+ "</td>"
			+ "<td>"	
			+ "<span id='COVViewSwitch' style='color:blue;cursor:hand' onClick='parent.Populate_Claim_Transactions(\"COV\")'>+ DISPLAY " + cg + "</span>"
			+ "</td>"
			if(Unit=='WC'){
				r+="<td>"	
				+ "<span id='MLTViewSwitch' style='color:blue;cursor:hand' onClick='parent.Populate_Claim_Transactions(\"MLT\")'>+ DISPLAY MINOR LOSS TYPE</span>"
				+ "</td>"
				+ "<td>"	
				+ "<span id='RATEViewSwitch' style='color:blue;cursor:hand' onClick='parent.Populate_Claim_Transactions(\"RATE\")'>+ DISPLAY RATES</span>"
				+ "</td>"
				+ "<td>"
				if (_CovCod == 24) {
				    r += "<span id='OPENViewSwitch' style='color:blue;cursor:hand' onClick='parent.Populate_Claim_Transactions(\"OPEN\")'>+ DISPLAY REASON OPEN</span>"
                }
				r+="</td>"
			}
			r+="</tr>"
			+ "</table>"
			+ "<div id='ClmTrns'></div>"
			+"</form></body></html>"
		return(r)
	}
	
	function Populate_Claim_Transactions(pass){
		var cg=''; var table; var OrderBy=''; var EmpNum = "" 
		if(Unit=='WC'){
			cg='LOSS TYPE'
		}else{
		    if (Unit == 'HO' || Unit == 'CO' || Unit == 'OM' || Unit == 'IM' || Unit == 'CR') {
				cg='PERIL'
			}else{
				cg='COVERAGE'
			}
		}
		frInfo.document.getElementById("ClmTrns").innerHTML="";
		SwitchLabel = "";
		switch(pass){
			case "STAT":
				if(frInfo.document.getElementById("STATViewSwitch").innerText.indexOf('DISPLAY')>-1){
					table = 'CLAIMSTATUS'; OrderBy='ModDat'
					STAT_Hist = (STAT_Hist=='') ?  XCall_CLM__Transact(table, OrderBy,"LastUser") : STAT_Hist	
					frInfo.document.getElementById("ClmTrns").innerHTML=STAT_Hist;
					SwitchLabel  = '- HIDE'
				}else{
					SwitchLabel = '+ DISPLAY'		
				}
				break;
			case "ADJ":
				if(frInfo.document.getElementById("ADJViewSwitch").innerText.indexOf('DISPLAY')>-1){
					table = 'AdjusterAssignments';	OrderBy='AssignDate'
					ADJ_Hist = (ADJ_Hist=='') ? XCall_CLM__Transact(table, OrderBy, "EmpNum")  : ADJ_Hist	
					frInfo.document.getElementById("ClmTrns").innerHTML=ADJ_Hist;
					SwitchLabel  = '- HIDE'
				}else{
					SwitchLabel = '+ DISPLAY'					
				}
				break;
			case "COV":
				if(frInfo.document.getElementById("COVViewSwitch").innerText.indexOf('DISPLAY')>-1){
					table = 'Claims_Coverage_Hist'; 				
					COV_Hist = (COV_Hist=='') ? XCall_CLM__Transact(table, OrderBy, "CreBy")  : COV_Hist	
					frInfo.document.getElementById("ClmTrns").innerHTML=COV_Hist;	
					SwitchLabel  = '- HIDE'
				}else{
					SwitchLabel = '+ DISPLAY'		
				}
				break;
			case "MLT":
				if(frInfo.document.getElementById("MLTViewSwitch").innerText.indexOf('DISPLAY')>-1){
					table = 'Claims_MinorLossType_Hist';
					MLT_Hist = (MLT_Hist=='') ? XCall_CLM__Transact(table, OrderBy, "CreBy") : MLT_Hist
					frInfo.document.getElementById("ClmTrns").innerHTML=MLT_Hist;
					SwitchLabel  = '- HIDE'
				}else{
					SwitchLabel = '+ DISPLAY'		
				}
				break;				
			case "RATE":
				if(frInfo.document.getElementById("RATEViewSwitch").innerText.indexOf('DISPLAY')>-1){
					table = 'Claims_Rates_Hist';
					RATE_Hist = (RATE_Hist=='') ?  XCall_CLM__Transact(table, OrderBy, "CreBy") : RATE_Hist
					frInfo.document.getElementById("ClmTrns").innerHTML=RATE_Hist;	
					SwitchLabel  = '- HIDE'
				}else{
					SwitchLabel = '+ DISPLAY'		
				}
				break;
            case "OPEN":
                if (frInfo.document.getElementById("OPENViewSwitch").innerText.indexOf('DISPLAY') > -1) {
                    table = 'dbo.vClaims_OpenReason_Hist';
                    OPEN_Hist = (OPEN_Hist == '') ? XCall_CLM__Transact(table, OrderBy, "CreBy") : OPEN_Hist
                    frInfo.document.getElementById("ClmTrns").innerHTML = OPEN_Hist;
                    SwitchLabel = '- HIDE'
                } else {
                    SwitchLabel = '+ DISPLAY'
                }
                break;
			case "BRCH":
				if(frInfo.document.getElementById("BRCHViewSwitch").innerText.indexOf('DISPLAY')>-1){
					table = 'Claims_Branch_Hist';
					BRCH_Hist = (BRCH_Hist=='') ? XCall_CLM__Transact(table, OrderBy, "CreBy") : BRCH_Hist
					frInfo.document.getElementById("ClmTrns").innerHTML=BRCH_Hist;	
					SwitchLabel  = '- HIDE'
				}else{
					SwitchLabel = '+ DISPLAY'		
				}
				break;
		}
		
		frInfo.document.getElementById("STATViewSwitch").innerText= (pass=="STAT") ? SwitchLabel+ " STATUS" : "- DISPLAY STATUS"
		frInfo.document.getElementById("ADJViewSwitch").innerText=(pass=="ADJ") ? SwitchLabel   + " HANDLERS" : "- DISPLAY HANDLERS"
		frInfo.document.getElementById("BRCHViewSwitch").innerText=(pass=="BRCH") ? SwitchLabel + " BRANCH" : "- DISPLAY BRANCH"
		frInfo.document.getElementById("COVViewSwitch").innerText=(pass=="COV") ? SwitchLabel   + " " + cg : "- DISPLAY " + cg 
		if(Unit=='WC'){
		frInfo.document.getElementById("MLTViewSwitch").innerText=(pass=="MLT") ? SwitchLabel   + " MINOR LOSS TYPE" : "- DISPLAY MINOR LOSS TYPE"
		frInfo.document.getElementById("RATEViewSwitch").innerText=(pass=="RATE") ? SwitchLabel + " RATES" : "- DISPLAY RATES"
		}
	}

	function Populate_listloc(){
		MyTitle="listloc"	
		var l= "<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		+ "<Title>listloc</Title></head>"
		+ "<body><Form name='frmClaim' onKeyDown='{if(event.keyCode==13){parent.DisplayEditLoc(3)}}'>"
		+ "<table  cellPadding=1 cellSpacing=1 border=0 width=100%>"
		+ "<TR><TD>Please choose the options:</TD></TR>" 
		+ "</table>"
		+ "<BR>"
		+ "<table  cellPadding=1 cellSpacing=1 border=0 width=300px>"
		+ "<TR><TD align=right>Number:</TD><TD><input type='text' name='txtL_Number' value='' onKeyDown='{if(event.keyCode < 46 || event.keyCode > 57){if(event.keyCode !=8 && event.keyCode !=46){return(false);}}}'></TD></TR>"
		+ "<TR><TD align=right>Name:</TD><TD><input type='text' name='txtL_Name' value=''></TD></TR>"
		+ "</table><BR>"
		+ "<table  cellPadding=1 cellSpacing=1 border=0 width=300px>"
		+ "<TR><TD align=center><input type='button' name='cmdGET' value='GET' onClick='parent.DisplayEditLoc(3)'></td></tr>"
		+ "</table></form></body></html>"
		return(l);
	}

//	function Populate_defc(){
//		var d=''
//		if(DC_Flag !=0){
//			d= "<html><head>"
//			+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
//			+ "<Title>defc</Title></head><body><Form name='frmClaim'>"
//			+ "<table id='defc' cellPadding=1 cellSpacing=1 border=0 style='width:602px'>"
//		//	+ "<Caption vAlign=bottom><A name='Print' href='javascript:parent.PrintScreen(\"defc\")'>PRINT SCREEN</A></Caption>"
//			+ "<TR><TD>"
//			+ "<table cellPadding=1 cellSpacing=1 border=0 style='width:600px'>"			
//			+ "<tr><td></B><U>" + DC_FirmName + "</U></B></td></tr>"			
//			+ "<tr><td>" + DC_Street1 + "</td></tr></table>"
//			+ "<table  cellPadding=1 cellSpacing=1 border=0 style='width:300px'>"		
//			+ "<tr><td>" + DC_City + "</td><td>" + DC_State + "</td><td>" + DC_Zip + "</td></tr></table>"
//			+ "<br><table  cellPadding=1 cellSpacing=1 border=0 style='width:600px'>"
//			+ "<tr><td>Contact:</td><td>" + DC_NewContact + "</td></tr></table>"
//			+ "<table  cellPadding=1 cellSpacing=1 border=0 style='width:500px'>"
//			+ "<tr><td  style='width:60px'>Phone:</td><td>" + DC_Phone + "</td><td  style='width:60px'>Fax:</td><td>" + DC_Fax + "</td></tr></table>"
//			+ "<table  cellPadding=1 cellSpacing=1 border=0 style='width:260px'>"
//			+ "<tr><td>Suit Received:</td><td  style='width:80px'>" + DC_RSVDate + "</td></tr>"
//			+ "<tr><td>Assigned:</td><td  style='width:80px'>" + DC_AssignDate + "</td></tr>"
//			+ "<tr><td>Apparent Service:</td><td  style='width:80px'>" + DC_ServiceDat + "</td></tr>"
//			+ "<tr><td>Stip Date:</td><td  style='width:80px'>" + DC_StipDate + "</td></tr>"			
//			+ "</table>"
//			+ "</TD></TR>"
//			+ "<TR><TD>"
//			+ "<textarea cols=92 rows=3 readonly  name=Notes>" + DC_Notes + "</textarea>"
//			+ "</TD></TR>"
//			+ "<TR height=30px><TD></TD></TR>"
//			+ "</table>"	
//			+"</form></body></html>"		
//		}
//		return(d);
//	}
	
	
function Populate_AssistBranch(flag){
	//changed - 07/14/2005: all types of claims will allow Assist Branch Option. 
	// States that do not require licensing will dilsplay all Branches for the particular company
	//list of adjusters will have all adjusters
	//var AsBranch - list of Branches, that have licenced adjusters required by the current State & LOB;
	//format of the AsBranch: "~[number of adjusters]_[branch code]"
	var OfficeName='';var c=100;var RetVal='';
	var re='';var re2=''; 
	var OfficeNumber=''; var AdjNumber=0; var txtAdjNumber=''; var re_txtAdjNumber='';

	OfficeName = FullListOfBranches();

	if(flag==0){
		var AsBrh='NOT SPECIFIED'
		if(MyAssistBranch>0){
			re=eval("/value=" + MyAssistBranch + " title='([-*`,:.#)( \\w]*)'>" + MyAssistBranch + "__([-`,.#)( \\D]*)<[/]{1}optio/gi")	//$1-title; $2-number & name of the branch
			var foundArr=re.exec(OfficeName)
			if(foundArr!=null){				
				AsBrh= MyAssistBranch + ' ' + RegExp.$2				
			}
		}
		
		frGeneral.document.getElementById("AsBranch_List").innerHTML="<span  class=textReadOnly>" + AsBrh + "</span>"
		frGeneral.document.getElementById("AsBranch_Label").innerHTML="<Label id=AssBrh_ID>Adjuster Branch:</Label>"
		frGeneral.document.getElementById("AssBrh_ID").title=RegExp.$1
	}else{
		re=/~(\d*)_(\d*)/gi	//$1-number of adjusters; $2-number of the branch
		var foundArr=re.exec(AsBranch)

		while(foundArr !=null){
			AdjNumber=RegExp.$1;
			OfficeNumber=RegExp.$2;
			re2=eval('/\\B<optio{1}[- \\w=*.,)(:`#\']*>' + OfficeNumber + '__[-.)(< \\w\"`/]*option>{1}\\B/gi')

			var foundArr2=re2.exec(OfficeName);
			try{
				RetVal=RetVal + foundArr2[0] //collect office(branches) names
				if(AdjNumber>0){
					txtAdjNumber=AdjNumber + ' - ' + OfficeNumber + '__'
					re_txtAdjNumber=eval('/' + OfficeNumber + '__' + '/gi')
					RetVal=RetVal.replace(re_txtAdjNumber, txtAdjNumber) //
				}
			}catch(e){				
			}	
			foundArr=re.exec(AsBranch)
		}

		if(RetVal==''){
			frGeneral.document.getElementById("AsBranch_List").innerHTML=''
			frGeneral.document.getElementById("AsBranch_Label").innerHTML=''
		}else{
			frGeneral.document.getElementById("AsBranch_List").innerHTML="<select name='cboAssistBranch' style='FONT-SIZE:8pt; width:200px;' onChange='parent.AssistBranch_Selected()'><option value='0'></option>" + RetVal + "</SELECT>"
			frGeneral.document.getElementById("AsBranch_Label").innerHTML="<Label id=AssBrh_ID>Assist Branch:</Label>"
			
			try{
				frGeneral.document.frmGeneral.cboAssistBranch.value=MyAssistBranch 
			}catch(e){}			
		}
	}
}
	function AssistBranch_Selected(){
		if(MainFlag !='Edit'){
			if(frGeneral.document.frmGeneral.cboAssistBranch.value!='0'){
				frGeneral.document.frmGeneral.cboAdjuster.length=0;				

				var i=frGeneral.document.frmGeneral.cboAssistBranch.selectedIndex;
				frGeneral.document.frmGeneral.cboAssistBranch.title=frGeneral.document.frmGeneral.cboAssistBranch.options[i].title

				var oOption=frGeneral.document.createElement("OPTION");
				oOption.text='-- PENDING --'
				oOption.value='PENDING';
				oOption.selected=true;
				frGeneral.document.frmGeneral.cboAdjuster.add(oOption);

				try { frGeneral.document.frmGeneral.cboAdjuster.disabled = true; } catch (e) { };
				try {frGeneral.document.frmGeneral.cboAdjuster_incr.disabled=true;} catch (e) { };
				try { frGeneral.document.frmGeneral.cboAdjuster_incr_btn.disabled = true; } catch (e) { };
				
				oOption=frGeneral.document.createElement("OPTION");
				oOption.text='-- PENDING --'
				oOption.value='PENDING'
				oOption.id='0'; //
				oOption.selected=true;
				frGeneral.document.frmGeneral.cboSuper.add(oOption);	
				
				try { frGeneral.document.frmGeneral.cboSuper.disabled = true; } catch (e) { };
				try { frGeneral.document.frmGeneral.cboSuper_incr.disabled = true; } catch (e) { };
				try { frGeneral.document.frmGeneral.cboSuper_incr_btn.disabled = true; } catch (e) { };
			}else{				
				frGeneral.document.frmGeneral.cboAssistBranch.title='';

				try { frGeneral.document.frmGeneral.cboAdjuster.disabled = false; } catch (e) { };
				try { frGeneral.document.frmGeneral.cboAdjuster_incr.disabled = false; } catch (e) { };
				try { frGeneral.document.frmGeneral.cboAdjuster_incr_btn.disabled = false; } catch (e) { };

				try { frGeneral.document.frmGeneral.cboSuper.disabled = false; } catch (e) { };
				try { frGeneral.document.frmGeneral.cboSuper_incr.disabled = false; } catch (e) { };
				try { frGeneral.document.frmGeneral.cboSuper_incr_btn.disabled = false; } catch (e) { };

				var SuperLen=frGeneral.document.frmGeneral.cboSuper.options.length
				if(frGeneral.document.frmGeneral.cboSuper.options[SuperLen-1].text=='-- PENDING --'){
					frGeneral.document.frmGeneral.cboSuper.options[SuperLen-1]=null;
				}
				
				frGeneral.document.frmGeneral.cboSuper.value=0
				AdjusterListOfCompanyPopulate(true);
			}			
		}
	}

	function Populate_adjuster_independent(){
		var c="<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		+ "<Title>iadj</Title></head><body><Form name='frmClaim'>"		
		+ "&nbsp;&nbsp;&nbsp;"
		+ "<button class=BottonScreen id=btnSearch style='width:50px'  name='btnSearch' title='Search for independent adjuster' onclick='parent.LookupWindow()'>Search</button>"
		+ "&nbsp;&nbsp;&nbsp;"
		+ "<button class=BottonScreen id=btnSearch style='width:50px'  name='btnClear' title='' onclick='parent.IAdjName=\"\";parent.IAdjTaxID=\"\";parent.ClearScreen();parent.AdjustersPopulate();'>Clear</button>"

        + "<input name='cboIAdjState' type='hidden' id='cboIAdjState' value='" + IAdjState + "' />"
		+ "<input name='IAdjStateId'  type='hidden' id='IAdjStateId'  value=" + IAdjstateId + " />"   
		+ "<input name='IAdjCountryId' type='hidden' id='IAdjCountryId' value='" + IAdjCountryId + "' />"  //
		
		+ "<table border=0 cellPadding=1 cellSpacing=1 id='iadj'>"

		+ "<TR>"
		+ "<TD>Name:</TD>"
		+ "<TD colspan=3><input name='txtIAdjName' readOnly=true value='" + IAdjName + "' style='width:100%' maxlength=30></TD>"
		+ "</TR>"
		+ "<TR>"
		+ "<TD>"
		+ "<span id='lkInsAddr' style='color:blue;cursor:hand;' onClick='parent.AddressUpdate_adjuster_independent(\"Independent adjuster\");' ><u>Address:</u></span>"
		+ "</TD>"
		+ "<TD colspan=3>"
		+ "<input name='AddrViewLine' value='" + trim(IAdjAddr1) + ' ' + trim(IAdjAddr2) + "' style='width:440px'  class=textReadOnly READONLY>" // style='width:240px'
		
		+ "<input name='txtIAdjAddr1'  value='" + IAdjAddr1 + "'   type=hidden maxlength='50' >"
		+ "<input name='txtIAdjAddr2'  value='" + IAdjAddr2 + "'   type=hidden maxlength='50' ></TD>"
        + "</TR>"
		+ "<TR>"
		+ "<TD>City/Town:</TD>"
		+ "<TD><input name='txtIAdjCity'   value='" + IAdjCity + "' tyle=style='width:200px' class=textReadOnly READONLY maxlength='50' ></TD>"
		+ "<TD id='IAdjState'>State:</TD>"
		+ "<td><input name='txtIAdjState' value='" + IAdjstateDesc + "' tyle=style='width:200px' class=textReadOnly READONLY></td>"
		+ "</TR>"
		+ "<TR>"
//		+ "<TD>State:<select name='cboIAdjState'  onChange='if(parent.ValZip(parent.frInfo.document.frmClaim.txtIAdjZip.value,this.value)==false){alert(\"US Zip code should be entered in the format ##### or #####-####\");parent.frInfo.document.frmClaim.txtIAdjZip.value=\"\";parent.frInfo.document.frmClaim.txtIAdjZip.focus();}'>"
//		+ StatesCodes 
//		+ " </SELECT></TD>"
		+ "<TD>Zip/ Postal Code:</TD>"
		+ "<TD><input name='txtIAdjZip'   value='" + IAdjZip + "' style=style='width:140px'  class=textReadOnly READONLY  maxlength='20' ></TD>"
		+ "<td id='IAdjCountryL'>Country:</td>"
		+ "<td><input name='txtCountry' value='" + IAdjCountry + "' style=style='width:200px' class=textReadOnly READONLY></td>"    
		+ "</TR>"
		+ "<tr><TD>Tax ID:</TD>"
		+ "<TD colspan=3 aligh=left><input name='txtIAdjTaxID' readOnly=true value='" + IAdjTaxID + "' maxlength='9' style='width:50%' onBlur='{document.frmClaim.txtIAdjTaxID.value=parent.MyTaxID_Dash(parent.frInfo.document.frmClaim.txtIAdjTaxID.value,\"txtIAdjTaxID\");}'></TD>"
		+ "</tr>"
		+ "<tr>"
		+ "<td>Phone:</td>"
		+ "<td><input name='txtIAdjPhone'   value='" + IAdjPhone + "' maxlength=25 style='width:155px'></TD>"  //onBlur='if(parent.frInfo.document.frmClaim.txtCountry.value.toUpperCase()==\"UNITED STATES\"){this.value=parent.MyPhone_Dash(this.value,\"txtIAdjPhone\");}'
		+ "<TD>Fax:<input name='txtIAdjFax' value='" + IAdjFax + "' maxlength=25 style='width:155px'></TD></TR>"  //onBlur='if(parent.frInfo.document.frmClaim.txtCountry.value.toUpperCase()==\"UNITED STATES\"){this.value=parent.MyPhone_Dash(this.value,\"txtIAdjFax\");}'
		+ "<TR><TD>EMail:</TD>"
		+ "<TD colspan=3 aligh=left><input name='txtIAdjEMail'  value='" + IAdjEMail + "' style='width:100%' maxlength='50'></TD></TR>"
		+ "</table>"
		+ "<BR>"
		+ "<span style='color:blue'>If independent adjuster can't be located in the York's Vendor List - please contact Accounting Dept</span>"
		+"</form></body></html>"
		return(c);
	}

	function Populate_product_liability() {
	    var c = "<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		+ "<title>prodliab</title></head>"
		+ "<body><form name='frmClaim'>"
        + "<table border=0 cellPadding=1 cellSpacing=1 id='prodliab'>"
	    + "<tr>"
	    + "<TD>Product Liability:</TD>"
		+ "<TD><select name='plStatus'><option value='N'>Not Confirmed</option><option value='Y'>Confirmed</option></select></TD>"
	    + "<TD align=right>Mass Tort situation:</TD>"
		+ "<TD><select name='plMassTort'><option value='N'>No</option><option value='Y'>Yes</option></select></TD>"		
		+ "</tr>"
		+ "<tr><TD>Manufacturer:</TD>"
		+ "<td colspan=3><input name='plManufacturer'  value='" + plManufacturer + "' style='width:100%' maxlength='40' onblur='var l=this.value.length;if(l>40){alert(\"The length of the value exceeds allowed 40 characters. Please adjust it\")}';></td>"
		+ "</tr>"
		+ "<tr><TD>Pruduct Generic Name:</TD>"
		+ "<td colspan=3><input name='plGenericName'  value='" + plGenericName + "' style='width:100%' maxlength='40' onblur='var l=this.value.length;if(l>40){alert(\"The length of the value exceeds allowed 40 characters. Please adjust it\")}';></td>"
		+ "</tr>"
		+ "<tr><TD>Pruduct Brand Name:</TD>"
		+ "<td colspan=3><input name='plBrandName'  value='" + plBrandName + "' style='width:100%' maxlength='40' onblur='var l=this.value.length;if(l>40){alert(\"The length of the value exceeds allowed 40 characters. Please adjust it\")}';></td>"
		+ "</tr>"
	    + "<tr><TD>Alleged Harm:</TD>"
		//+ "<TD colspan=3><input name='plAllegedHarm'  value='" + plAllegedHarm + "' style='width:100%' maxlength='200'></TD>"
		+ "<td colspan=3><textarea cols=70 rows=3 name=plAllegedHarm>" + plAllegedHarm + "</textarea></td>"
		+ "</tr>"
		+ "</table>"
		+ "</form></body></html>"
		
	    return (c);
	}

	function Populate_request_assistance(pass, desc) {
		var AssistOfficeList='<select name="AssistOffice"><option value=""></option>'
		var i=1;
		while(i<AssistTypeList.length){
		     if (AssistTypeList[i].assign_type == pass && AssistTypeList[i].descr == desc) {			
				AssistOfficeList+='<option value='+ i +'>'+ AssistTypeList[i].branch[0] +' - '+ AssistTypeList[i].branch[1] + '</option>'			 			 
			 }
			 i++			
		}
		AssistOfficeList +='</select>'
		
		var I="<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		+ "<scr"
		+ "ipt src='/portal/openers/objClaimXml.js'></scr"
		+ "ipt>"		
		+ "<title>AssistanceRequest</title></head><body><form name='frmRequestAssist'>"
		I +="<form name=frmIndex><table width=70% border=0><tr>"
		+ "<td align=right style='color:blue'>Please "
		if(AssistOfficeList!=''){
		I+= " select office that will handle asssit claim and "
		}
		I+= " enter your instructions." 
		if(AssistOfficeList!=''){	
		I+= "</td><td>&nbsp; &nbsp;&nbsp;" + AssistOfficeList  	
		}
		I += "</td><td style='width:100px'>"
		if(UserRole.indexOf('EditFile')!=-1){
		     I +="<input type='button'  style='width:67px;HEIGHT:30px;' value='Request\nAssistance' name='cmdAss' class=BottonScreen "
		     +  " onclick='parent.Var_Save(parent.frInfo.document.title);"
		     +  "if(parent.frIndex.document.all[\"AssistOffice\"].value==\"\"){alert(\"Please specify branch\")"
		     + "}else{"
		     + " if(\"" + pass + "\"==\"R\"){if(parent.RecoveryRequiredFields_Check() == false){parent.RecoveryRequiredFields_Mark(); return;}};"
		     +  "Populate_claim_xml(\"" + pass + "\",parent.AssistTypeList[parent.frIndex.document.all[\"AssistOffice\"].value].id,"
										    + "parent.AssistTypeList[parent.frIndex.document.all[\"AssistOffice\"].value].clntno,"
										    + "parent.AssistTypeList[parent.frIndex.document.all[\"AssistOffice\"].value].pref,"
										    + "parent.AssistTypeList[parent.frIndex.document.all[\"AssistOffice\"].value].suffix,"
										    + "parent.frIndex.document.all[\"AssistOffice\"].options[parent.frIndex.document.all[\"AssistOffice\"].options.selectedIndex].text.substr(0,3),"
										    + "parent.AssistTypeList[parent.frIndex.document.all[\"AssistOffice\"].value].prop)"
		     + "};'"		 
		     + "</td><td style='width:100px'>"
		 }
		    I+="<input type='button'  style='width:67px;HEIGHT:30px;' value='Back to\nMain Menu' name='cmdAss' class = BottonScreen  onclick='if(parent.Claim_Assigns.length>0){parent.Display(\"assistance\")}else{parent.Display(\"claim\")};'"
		     + "</td></tr></table>"
		     + "</form></body></html>"
		 
		frIndex.document.open();
		frIndex.document.write(I);
		frIndex.document.close();
	
		var p=''
		switch(pass){
			case "R" :
			    p += Populate_rec();	    
				break;
			case "I" : 
				p+=Populate_ysi();
				break;
            default:  // case "S"  , "E"             
                p += Populate_field_assignment();
                break;
		}
		return(p);
	}
	
	function Populate_assistance(){
		var c=0;
		var p="<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		+ "<title>assistance</title></head><body>"
		+  "<form name='frmClaim'>"
		+ "<table border=0 cellPadding=1 cellSpacing=1>"	
		+ "<tr>"   		
		+ "<td id=ReqstAssistance><A href='javascript:if(parent.frInfo.frmClaim.AssistType.value==\"\"){alert(\"Please specify type of the required assistance\")}else{parent.LookupWindow_RecoveryReferredTo(parent.frInfo.frmClaim.AssistType.value)};' title='Please specify type of the required assistance'>"
		+ "NEW ASSIGNMENT:&nbsp;"
		+ "</A>"
		+ "<select name='AssistType'><option value=\"\">NOT SPECIFIED</option>"		
		if (typeof AssistTypeList[0] != 'undefined') {
		    while (c < AssistTypeList[0].length) {
		        if (typeof (AssistTypeList[0][c].descr) != "undefined") {
		            if (Status == 'C' && AssistTypeList[0][c].closed != 'Y') {
		            } else {
		                p += "<option value=" + AssistTypeList[0][c].assign_type + ">" + AssistTypeList[0][c].descr + "</option>"
		            }
		        }
		        c++
		    }
		}
		p+="</select></td>"
		+ "</tr>"
		+ "</table>"
		+ "<br>"
		+ "<table cellPadding=0 cellSpacing=0 border=1 style='width:100%;table-layout:fixed;'>"		
		+ "<col WIDTH=150px>"
		+ "<col WIDTH=100px><col WIDTH=130px><col WIDTH=90px><col WIDTH=40px><col WIDTH=70px>"
		+ "<col WIDTH=120px><col WIDTH=120px><col WIDTH='*'>"
		+ "<tr>"
		+ "<td align=center><b>Action</b></td>"
		//+ "<td align=center><b>Add to Portal Queue</b></td>"
		+ "<td align=center><b>Assist. Claim</b></td>"
		+ "<td align=center><b>Assignment Type</b></td>"		
		+ "<td align=center><b>Request Date</b></td>"					
		+ "<td align=center><b>Status</b></td>"		
		+ "<td align=center><b>Status Date</b></td>"		
		+ "<td align=center><b>Adjuster</b></td>"
		+ "<td align=center><b>Supervisor</b></td>"
		+ "<td align=center><b>Assist Branch</b></td>"		
		+ "</tr>"	
		c=0

		while (c < Claim_Assigns.length) {
		    if (ClmUserRole == 'CLT' && Claim_Assigns[c].descr == 'FIELD ASSIGNMENT') {
		    } else {
		        p += "<tr>"
		        p += "<td align=center>"
		        if (UserRole.indexOf('EditFile') != -1) {
		            p += "<input type='button' title='un-link (void) assist claim' style='width:45px' name='chkAssRemove' value='Remove' onClick='parent.Assignment_Delete_XTRCT(\"" + Claim_Assigns[c].clmno_assist + "\")'"
		            p += (Claim_Assigns[c].status == 'I') ? "" : " disabled "
		            p += " onClick='alert(\"cancel\")'>"
		            + "<input type='button' title='add assist claim to portal`s queue' style='width:45px' name='chkAssAddQueue' value='Queue' onClick='parent.mmAddToClaimList(\"" + Claim_Assigns[c].clmno_assist + "\",\"Assist file of " + ClaimNumber + "\");'>"
		            + "<input type='button' title='link assist claim with another feature of this occurrence' "
		            p += (Claim_Assigns[c].prop == 'Y') ? "" : " disabled "
		            p += (Claim_Assigns[c].prop == 'Y' && Claim_Assigns[c].status == 'C') ? " disabled " : ""
		            p += " style='width:45px' name='chkAssOccur' value='Occur' onClick='parent.LookupWindow_AssistOccur(\"" + ClaimNumber + "\",\"" + ClaimNumber.substring(0, (ClaimNumber.length - Suffix.length)) + "\",\"" + Claim_Assigns[c].clmno_assist + "\")'>"
		        } else { 
		            p += ' - '
		        }
		        p += "</td>"
		        + "<td align=center><u><span title='View claim summary' id='ClmAssist' style='color:blue;cursor:hand' onclick=\"var wnd=window.open('/portal/startup/search/searchClaim/ClaimInfo.asp?claim_no=" + Claim_Assigns[c].clmno_assist + "','ClaimSummary','status=0,resizable=1;top=9,left=50,width=680,height=900'); wnd.focus()\">" + Claim_Assigns[c].clmno_assist.toUpperCase() + "</span></u></td>"
		        + "<td align=center>" + Claim_Assigns[c].descr + "</td>"
		        + "<td align=center>" + Claim_Assigns[c].credat + "</td>"
		        + "<td align=center>" + Claim_Assigns[c].status + "</td>"
		        + "<td align=center>" + Claim_Assigns[c].status_date + "</td>"
		        + "<td align=center>" + Claim_Assigns[c].adjuster + "<br>" + Claim_Assigns[c].adj_phone + "</td>"
		        + "<td align=center>" + Claim_Assigns[c].supervisor + "<br>" + Claim_Assigns[c].super_phone + "</td>"
		        + "<td align=center>" + Claim_Assigns[c].branch + "</td>"
		        + "</tr>"
		    }
		c++
		}
		p+="</table>"
		+"</form></body></html>"
		return(p);
	}
	
	function Populate_producer(){
		var p="<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		+ "<Title>producer</Title></head><body><form name='frmClaim'>"

	    + "<input name = 'cboPrState' type='hidden' id= 'cboPrState' value='" + PrState + "' />"
		+ "<input name = 'PrstateId'  type='hidden' id= 'PrstateId'  value='" + PrstateId + "' />"
		+ "<input name = 'PrCountryId' type='hidden' id='PrCountryId' value='" + PrCountryId + "' />"  
		
		+ "<table border=0 cellPadding=1 cellSpacing=1 id='iadj'>"		
		+ "<TR><TD>Name:</TD>"
		+ "<TD colspan=3><input name='txtPrName' value='" + PrName + "' style='width:99%' maxlength=50></TD></TR>"
		+ "<TR><TD rowspan=1>"
		+ "<span id='lkInsAddr' style='color:blue;cursor:hand;' onClick='parent.AddressUpdate_producer(\"Producer\");' ><u>Address:</u></span>"
		+ "</TD>"
		+ "<TD colspan=3>"
		+ "<input name='AddrViewLine' value='" + trim(PrAddr1) + ' ' + trim(PrAddr2) + "' style='width:99%'   class=textReadOnly READONLY>" // style='width:240px'
		+ "<input name='txtPrAddr1' value='" + PrAddr1 + "' type = hidden maxlength='50'>"
		+ "<input name='txtPrAddr2' value='" + PrAddr2 + "' type = hidden maxlength='50'>"
		+ "</TD>"
		+ "</TR>"
		+ "<TR><TD>City:</TD>"
		+ "<TD><input name='txtPrCity' value='" + PrCity + "' class=textReadOnly READONLY maxlength='50'></TD>"
		+ "<TD>State:"
		+ "<td ><input name='PrstateDesc' value='" + PrstateDesc + "' style='width:99%' class=textReadOnly READONLY></td>"
		+ "</TD>"
		+ " </TR>"
		+ "<TR>"
		+ "<td>Zip/PostalCode:</td>"
	    + "<td><input name='txtPrZip' value='" + PrZip + "' maxlength='20' class=textReadOnly READONLY></td>"
		+ "<td id='PrCountryL' align=right>Country:</td>"
		+ "<TD><input name='txtCountry' value='" + PrCountry + "'style='width:99%' class=textReadOnly READONLY></td>"
		+ "</TR>"
		
		+ "<TR><TD>Phone:</TD>"
		+ "<TD><input name='txtPrPhone' value='" + PrPhone + "' style='width:300px' maxlength=25></TD>" // onBlur='{document.frmClaim.txtPrPhone.value=parent.MyPhone_Dash(parent.frInfo.document.frmClaim.txtPrPhone.value,\"txtPrPhone\");}'
		+ "<TD align=right>Fax:</TD>"
		+ "<TD><input name='txtPrFax' value='" + PrFax + "' style='width:300px' maxlength=25></TD>" // onBlur='{document.frmClaim.txtPrFax.value=parent.MyPhone_Dash(parent.frInfo.document.frmClaim.txtPrFax.value,\"txtPrFax\");}'
		+ "</TR>"
		+ "<TR><TD>EMail:</TD>"
		+ "<TD colspan=3><input name='txtPrEmail' value='" + PrEmail + "' style='width:99%' maxlength='50'></TD></TR>"
		+ "</table>"
		+"</form></body></html>"
		return(p);
	}

	function Populate_HistLinks(scope, obj) {
        var _o
        if (typeof obj != 'undefined' && obj != null) {
            var _c = obj.length;
            for (index = 0; index < _c; ++index) {
                if (typeof obj[index] == 'object') {
                    _o = obj[index];
                    if (_o.hasOwnProperty('uiId')) {
                        try {
                            if (scope.getElementById(_o.uiId) != null) {
                                scope.getElementById(_o.uiId).innerHTML = "<a title='History of Changes' href='#' onclick='parent.Lookup_ShowHistory_aField(\"" + _o.tbl + "\", \"" + _o.fld + "\", \"" + _o.rId + "\", \"" + _o.lbl + "\")'>H</a>"
                            }
                        } catch (e) {
                            alert('Unable to set History link [' + _o.uiId + ']:' + e.message);
                        }                        
                    }
                }
            }
        }
    }

	function Populate_fin(passFinDet) {
	    if (typeof passFinDet == 'undefined') {passFinDet = ''};

		f="<html><head>"
		 + "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 + "<Title>fin</Title></head><body><form name='frmClaim'>"			 
			f+="<table>"
			+ "<tr>"
			+ "<td>"	
			+ "<span id='FinSummaryViewSwitch' style='color:blue;cursor:hand' onClick='parent.ViewHideFinSummary()'>- HIDE FINANCIAL SUMMARY</span>"
			+ "</td>"
			+ "</tr>"
			+ "</table>"
		 
		 + "<table border=0 cellPadding=1 cellSpacing=1 border=0 style='width:95%'>"
		 + "<tr><td>"
			+ "<div id='FinSum'>"
			f+=FinInfo_Summary;
			f+="</div>"
			f+="</td></tr><tr><td>"

			f += "<table cellSpacing=15>"
			+ "<tr>"
			+ "<td align=left>"
			+ "<span id='PmntDetViewSwitch' style='color:blue;cursor:hand' onClick='parent.ViewHideFinDetails(\"P\")'>+ DISPLAY PAYMENT DETAILS</span>"
			+ "</td>"
			//+ "</tr><tr>"
			+ "<td align=left>"
			+ "<span id='ResvAllViewSwitch' style='color:blue;cursor:hand' onClick='parent.ViewHideFinDetails(\"RA\")'>+ DISPLAY RESERVE CHANGES - ALL</span>"
			+ "</td>"  //"</tr>"
			if (Reserve_MinorCodes_Flag == 1) {
			    f += "<td align=left>"
			     + "<span id='ResvTrnsViewSwitch' style='color:blue;cursor:hand' onClick='parent.ViewHideFinDetails(\"RD\")'>+ DISPLAY RESERVE CHANGES - DETAIL</span>"
			     + "</td>"
			}
			f += "<td align=left>"
			 + "<span id='ResvDetViewSwitch' style='color:blue;cursor:hand' onClick='parent.ViewHideFinDetails(\"R\")'>+ DISPLAY RESERVE HISTORY</span>"
			 + "</td></tr>"
			 + "</table>"
			
		 +"</td></tr>"
		 +"<tr><td>"
		 + "<div id='FinDet'>"
			        + passFinDet			       
			+ "</div>"
			+ "</td></tr></table>"
			+ "</form></body></html>"
		return(f)
	}
	
	function ViewHideFinSummary(){
		if(frInfo.document.getElementById("FinSum").innerHTML==""){								
			frInfo.document.getElementById("FinSum").innerHTML=FinInfo_Summary;
			frInfo.document.getElementById("FinSummaryViewSwitch").innerText="- HIDE FINANCIAL SUMMARY";
		}else{			
			frInfo.document.getElementById("FinSum").innerHTML=""
			frInfo.document.getElementById("FinSummaryViewSwitch").innerText="+ DISPLAY FINANCIAL SUMMARY";
		}
		
	}
	
	function ViewHideFinDetails(pass, fltr){
	    var f = '';
	    var reloadPage = false;

		switch(pass){
		    case 'P':
		        if (FinList == '') { ViewPayments_XTRCT('', '', '', '', '') }
		        if (FinList.length == 0) {
		            f = '<span style="color:red">No payments history is available</span>'
		        } else {
		            f = ''
					+ "<input type='button' value='Export to Excel' onClick='parent.ExportToExcel(parent.frInfo.document.getElementById(\"finlist\"), parent.frInfo.document.getElementById(\"finlisthead\"), \"Payment History\", \"Claim " + ClaimNumber + "; Claimant:" + SingleQuote(CName) + "; Loss Date:" + frGeneral.document.frmGeneral.txtAccDT.value
		            f += (typeof (fltr) == "undefined") ? '' : fltr;
		            f += "\")' id='button'1 name='button'1>"
					+ "&nbsp;&nbsp;"
					+ "<input type='button' value='Filter Payment Details' onClick='parent.FilterWindow()' id='FltrPyHist' name='FltrPyHist'>"
		            f += "&nbsp;&nbsp; <span style='color:blue'>"
		            f += (typeof (fltr) == "undefined" || fltr == "undefined") ? '' : fltr;
		            f += "</span>"
		            + "&nbsp;"
		            + "<span>Classes:<b>CHK</b>-check;<b>AGG</b>-aggregated;<b>RO</b>-record only;<b>REF</b>-refund;<b>REC</b>-recovery;<b>PPR</b>-pending processing;<b>PAP</b>-pending approval;<br>ACH</b>-ACH;<span>"
			+ "<div  style='width:915px'>"
			+ "<table id='finlisthead' cellPadding=0 cellSpacing=0 border=1 style='width:100%;table-layout:fixed;'>"
			+ "<col WIDTH=25px>"
			+ "<col WIDTH=45px'>"  // -35
		+ "<col WIDTH=35px'>"  // +35
			+ "<col WIDTH=30px>"
			+ "<col WIDTH=70px>" // -10
			+ "<col WIDTH=70px>" // -10
			+ "<col WIDTH=40px>"
			+ "<col WIDTH=60px>"    //-20
			+ "<col WIDTH='220px'>"  //-30
			+ "<col WIDTH='120px'>"  //+120
		    + "<col WIDTH=45px'>" // -30
		    + "<col WIDTH=45px'>"// -30
			+ "<col WIDTH='110px'>"  //+10
					+ "<tr><td align=center><B> # </B></td>"
					+ "<td align=center><B>Date</B></td> "
			   + "<td align=center><B>Class</B></td>"
					+ "<td align=center><B>Type</B></td>"
					+ "<td align=center><B>Bill, $</B></td>"
					+ "<td align=center><B>Paid, $</B></td>"
					+ "<td align=center><B>BillTo</B></td>"
					+ "<td align=center><B>Trans#</B></td>"
					+ "<td align=center><B>Payee</B></td>"
				+ "<td align=center><B>Invoice</B></td>"
					+ "<td align=center><B>From</B></td>"
					+ "<td align=center><B>To</B></td>"
					+ "<td align=center><B>Details</B></td></tr>"
				+ "</table>"
			+ "</div>"
			+ "<div style='width:930px; height:275px; overflow:auto;'>"
			+ "<table id='finlist' cellPadding=0 cellSpacing=0 border=1 style='border-collapse:collapse; width:910px; table-layout:fixed;'>"
				+ "<col WIDTH=25px align=center>"
				+ "<col WIDTH=45px' align=center>" // -35
            + "<col WIDTH=35px' align=center>"  // +35
				+ "<col WIDTH=30px align=center>"
				+ "<col WIDTH=70px align=right>" // -10
				+ "<col WIDTH=70px align=right>" // -10
				+ "<col WIDTH=40px align=center>"
				+ "<col WIDTH=60px align=center>"//-20
				+ "<col WIDTH='220px' align=center>"//-30
            + "<col WIDTH='120px' align=center>"//+120
				+ "<col WIDTH=45px' align=center>"// -30
				+ "<col WIDTH=45px' align=center>"// -30
				+ "<col WIDTH='110px' align=center>"// +10
					+ FinList
			+ "</table>"
			+ "</div>"
					+ "<input type='hidden' value='" + fltr + "' name='fltr'>"
		        }
		        reloadPage = ( frInfo.document.getElementById("PmntDetViewSwitch") == null) ? true : false;
                if(!reloadPage){
                    reloadPage = (frInfo.document.getElementById("PmntDetViewSwitch").innerText.indexOf("DISPLAY") > -1) ? true : false
                }
                if (reloadPage){   // || frInfo.document.getElementById("PmntDetViewSwitch").innerText.indexOf("DISPLAY") > -1) {
		            //		            frInfo.document.getElementById("FinDet").innerHTML = f;
		            fin = Populate_fin(f);
		            Display("fin");
		            setTimeout("DisplayFinDetailsMenu('D', 'D', 'H', 'D')", 100);
		        } else {
		            frInfo.document.getElementById("FinDet").innerHTML = "";
		            DisplayFinDetailsMenu('D', 'D', 'D', 'D');
		        }

		        break;
		    case 'R':
		        HisRes = (HisRes == '') ? ViewReserves_XTRCT() : HisRes
		        f = ''
				+ "<table id='res' cellPadding=1 cellSpacing=1 frame='box' rules='groups' style='width:90%'>"
				+ "<colgroup id='dat' span=1><colgroup id='ind' span=2><colgroup id='med' span=2><colgroup id='exp' span=2>"
				+ "<tr>"
				+ "<td align=center><U><B>Transaction Date</B></U></td>"
				+ "<td align=center><U><B>Indemnity Net Reserves</B></U></td>"
				+ "<td align=center><U><B>Indemnity Payments</B></U></td>"
				+ "<td align=center><U><B>Medical Net Reserves</B></U></td>"
				+ "<td align=center><U><B>Medical Payments</B></U></td>"
				+ "<td align=center><U><B>Expense Net Reserves</B></U></td>"
				+ "<td align=center><U><B>Expense Payments</B></U></td>"
				+ "</tr>"
				+ HisRes
				+ "</table>"

		        //if (frInfo.document.getElementById("ResvDetViewSwitch").innerText.indexOf("DISPLAY") > -1) {
		        //frInfo.document.getElementById("FinDet").innerHTML = f;

		        reloadPage = (frInfo.document.getElementById("ResvDetViewSwitch") == null) ? true : false;
		        if (!reloadPage) {
		            reloadPage = (frInfo.document.getElementById("ResvDetViewSwitch").innerText.indexOf("DISPLAY") > -1) ? true : false
		        }
		        if (reloadPage) { 
		            fin = Populate_fin(f);
		            Display("fin");
		            setTimeout("DisplayFinDetailsMenu('D', 'H', 'D', 'D')", 100);
		        } else {
		            frInfo.document.getElementById("FinDet").innerHTML = "";
		            DisplayFinDetailsMenu('D', 'D', 'D', 'D');
		        }
		        break;
		    case 'RD':
		        HisResTrnsact = (HisResTrnsact == '') ? ViewReservesTrnsactions_XTRCT('RD') : HisResTrnsact
		        f = HisResTrnsact

		        reloadPage = (frInfo.document.getElementById("ResvTrnsViewSwitch") == null) ? true : false;
		        if (!reloadPage) {
		            reloadPage = (frInfo.document.getElementById("ResvTrnsViewSwitch").innerText.indexOf("DISPLAY") > -1) ? true : false
		        }
		        if (reloadPage) { 
		            fin = Populate_fin(f);
		            Display("fin");
		            setTimeout("DisplayFinDetailsMenu('H', 'D', 'D', 'D')", 100);
		        } else {
		            frInfo.document.getElementById("FinDet").innerHTML = "";
		            DisplayFinDetailsMenu('D', 'D', 'D', 'D');
		        }
		        break;
		    case 'RA':
		        AllResTrnsact = (AllResTrnsact == '') ? ViewReservesTrnsactions_XTRCT('RA') : AllResTrnsact
		        f = AllResTrnsact

		        reloadPage = (frInfo.document.getElementById("ResvAllViewSwitch") == null) ? true : false;
		        if (!reloadPage) {
		            reloadPage = (frInfo.document.getElementById("ResvAllViewSwitch").innerText.indexOf("DISPLAY") > -1) ? true : false
		        }
		        if (reloadPage) { 
		            fin = Populate_fin(f);
		            Display("fin");
		            setTimeout("DisplayFinDetailsMenu('D', 'D', 'D', 'H')", 100);
		        } else {
		            frInfo.document.getElementById("FinDet").innerHTML = "";
		            DisplayFinDetailsMenu('D', 'D', 'D', 'D');
		        }
		        break;				
		}
	}

	function DisplayFinDetailsMenu(ResvTrnsViewSwitch, ResvDetViewSwitch, PmntDetViewSwitch, ResvAllViewSwitch) {
        if (Reserve_MinorCodes_Flag == 1) {
            if(ResvTrnsViewSwitch=='D'){
	            frInfo.document.getElementById("ResvTrnsViewSwitch").innerText = "+ DISPLAY RESERVE CHANGES - DETAIL";
	            frInfo.document.getElementById("ResvTrnsViewSwitch").style.fontWeight = 'normal'
	        }else{
	            frInfo.document.getElementById("ResvTrnsViewSwitch").innerText = "- HIDE RESERVE CHANGES - DETAIL";
	            frInfo.document.getElementById("ResvTrnsViewSwitch").style.fontWeight = 'bold'
	        }
	    }
	    
	    if (ResvDetViewSwitch == 'D') {
	        frInfo.document.getElementById("ResvDetViewSwitch").innerText = "+ DISPLAY RESERVE HISTORY";
	        frInfo.document.getElementById("ResvDetViewSwitch").style.fontWeight = 'normal'
	    }else{
	        frInfo.document.getElementById("ResvDetViewSwitch").innerText = "- HIDE RESERVE HISTORY";
	        frInfo.document.getElementById("ResvDetViewSwitch").style.fontWeight = 'bold'
        }
        
        if(PmntDetViewSwitch=='D'){
            frInfo.document.getElementById("PmntDetViewSwitch").innerText = "+ DISPLAY PAYMENT DETAILS";
	        frInfo.document.getElementById("PmntDetViewSwitch").style.fontWeight = 'normal'
	    }else{
            frInfo.document.getElementById("PmntDetViewSwitch").innerText = "- HIDE PAYMENT DETAILS";
	        frInfo.document.getElementById("PmntDetViewSwitch").style.fontWeight = 'bold'
        }
        
        if(ResvAllViewSwitch=='D'){
	        frInfo.document.getElementById("ResvAllViewSwitch").innerText = "+ DISPLAY RESERVE CHANGES - ALL";
	        frInfo.document.getElementById("ResvAllViewSwitch").style.fontWeight = 'normal'
	    }else{
	        frInfo.document.getElementById("ResvAllViewSwitch").innerText = "- HIDE RESERVE CHANGES - ALL";
	        frInfo.document.getElementById("ResvAllViewSwitch").style.fontWeight = 'bold'
        }	
	}
	
	
	function FilterWindow(){	
		var d = new Date;
		var url = 'XCall_ViewPaymentsFilter.asp?t=' + d.getTime();
		window.open(url,"Filter","status,height=270,width=400,top=9,left=9")
	}
//////////////////
//LOOKUP WINDOWS:
	function LookupWindow(){	
		var d = new Date;
		var url = 'Screen_Lookup.asp?t=' + d.getTime();
		window.open(url,"Filter","status=no,height=1,width=1,top=1000,left=5000")
	}

	function LookUp_ReturnValues(no, nm, taxID, addr1, addr2, city, state, zip, phone, fax, email){
		frInfo.document.frmClaim.txtIAdjName.value=nm; 
		frInfo.document.frmClaim.txtIAdjTaxID.value=taxID; 
		frInfo.document.frmClaim.txtIAdjAddr1.value=addr1;
		frInfo.document.frmClaim.txtIAdjAddr2.value=addr2; 
		frInfo.document.frmClaim.txtIAdjCity.value=city; 
		frInfo.document.frmClaim.cboIAdjState.value=state; 
		frInfo.document.frmClaim.txtIAdjZip.value=zip;	
		frInfo.document.frmClaim.txtIAdjPhone.value=phone;
		frInfo.document.frmClaim.txtIAdjFax.value=fax;
		frInfo.document.frmClaim.txtIAdjEMail.value=email;
		
		IAdjName=nm; 
		IAdjTaxID=taxID;	
		//refresh adjusters
		setTimeout("AdjustersPopulate()", 200);
	}

	function Lookup_ShowHistory_custom(recId) {
	    Lookup_ShowHistory(recId, 'claims_custom_fields')
	}

	function Lookup_ShowHistory(recId, table) {
	    var d = new Date;
	    var url = 'Screen_Lookup_ShowHistory_custom.asp?t=' + d.getTime() + "&recId=" + recId + "&table=" + table;
	    frPrint.document.location = url;
	    //window.open(url, "Filter", "status=no,height=5,width=5,top=5000,left=3000")
	}

	function Lookup_ShowHistory_aField(table_, field_, recId_, fieldLabel_) {
	    var d = new Date;
	    if (typeof fieldLabel_ == 'undefined') {
	        fieldLabel_ = field_;
        }
	    var url = 'Screen_Lookup_ShowHistory_field.asp?t=' + d.getTime() + "&recId=" + recId_ + "&field=" + field_ + "&table=" + table_ + "&label=" + fieldLabel_;
	    frPrint.document.location = url;
	   // window.open(url, "Filter", "status=no,height=5,width=5,top=5000,left=3000")    
    }
	function LookupWindow_ExternalTransaction_History(trnsaction, payType, chkno) {
	    var d = new Date;
	    var payTypeDesc = payType;
	    //if (payType == '1b') {
	    //    payTypeDesc = 'Imprest virtual card';
	    //}
	    //if (payType == '1c') {
	    //    payTypeDesc = 'Imprest debit card';
	    //}
	    
	    var url = 'Screen_Lookup_ExternalTransaction_History.asp?t=' + d.getTime() + "&ClmNo=" + ClaimNumber + "&Trnsaction=" + trnsaction + "&ChkNo=" + chkno + "&PayType=" + escape(payTypeDesc);
	    frPrint.document.location = url;
	    // window.open(url, "Filter", "status=no,height=5,width=5,top=5000,left=3000")
	}

	function LookupWindow_HRdata() {
	    var d = new Date;  //
	    if (frGeneral.document.frmGeneral.txtAccDT.value == '') {
	        alert('please define the date of accident');
	        return;
	    }
	    var url = 'Screen_Lookup_HRdata.asp?t=' + d.getTime() + "&AsOfDate=" + frGeneral.document.frmGeneral.txtAccDT.value + "&ClientNumber=" + ClientNumber + "&EmplId=" + employee_id + "&SSN=" + SSNum + '&Loc=' + EditLoc[1][0];

	    frPrint.document.location = url;
        //window.open(url, "Filter", "status=no,height=5,width=5,top=5000,left=3000")
	}


	function LookUp_ReturnValues_HRdata(arrReturned) {
	    if (typeof (arrReturned) == 'object') {
	        if (arrReturned.length > 0) {
	            try {
	                ClmOriginal.HR.id = arrReturned[0];
	                ClmOriginal.HR.Employee.ssNum =  trim(arrReturned[8]); // EE_SSN;
	                ClmOriginal.HR.Employee.lName =  trim(arrReturned[5]); //EE_LName  
	                ClmOriginal.HR.Employee.fFName =  trim(arrReturned[4]); // EE_FName ;
	              // ClmOriginal.HR.Employee.mi =  arrReturned[0]; //EE_MI_HIDDEN ;
	                ClmOriginal.HR.Employee.cAddr1 =  trim(arrReturned[10]); // ;
	                ClmOriginal.HR.Employee.cAddr2 =  trim(arrReturned[11]); // ;
	                ClmOriginal.HR.Employee.cCity =  trim(arrReturned[12]); // ;
	                ClmOriginal.HR.Employee.cState =  trim(arrReturned[13]); // ;
	                ClmOriginal.HR.Employee.cZip =  trim(arrReturned[14]); // ;
	                ClmOriginal.HR.Employee.cPhone =  trim(arrReturned[15]); // ;
	                ClmOriginal.HR.Employee.workPhone =  trim(arrReturned[16]); // ;
	                ClmOriginal.HR.Employee.eMail = trim(arrReturned[17]); // ;

	                ClmOriginal.HR.Employee.dob = MyDateFormat(trim(arrReturned[9]), 1); // EE_DOB  ;
	                ClmOriginal.HR.Employee.sex = trim(arrReturned[18]); //EE_Gender ; [19]  - placeholder 

                    ClmOriginal.HR.Employment.employeeId =  arrReturned[3]; // EE_EmpID;
	                ClmOriginal.HR.Employment.dept =  trim(arrReturned[23]);
	                ClmOriginal.HR.Employment.jobTitle =  trim(arrReturned[2]);
	                ClmOriginal.HR.Employment.emplStatusCode =  trim(arrReturned[22]);
	                ClmOriginal.HR.Employment.weekWage = trim(arrReturned[29]);
	                ClmOriginal.HR.Employment.weeklyHours = arrReturned[28];
	                ClmOriginal.HR.Employment.hireDate =  trim(arrReturned[20]);
	                ClmOriginal.HR.Employment.terminateJobDate =  trim(arrReturned[21]);
	                ClmOriginal.HR.Employment.employerLocId =  trim(arrReturned[24]);
	                ClmOriginal.HR.Employment.employerNo =  trim(arrReturned[25]);
	                ClmOriginal.HR.Employment.employerTaxId = trim(arrReturned[26]);
	                ClmOriginal.HR.Employment.HRSuperRecId = trim(arrReturned[30]);
	                //ClmOriginal.HR.Employment.HRSuperRecId = arrReturned[30];
	                ClmOriginal.HR.Employment.Loc = trim(EditLoc[1][0]);
	                ClmOriginal.HR.Employment.LocTxtCod = trim(EditLoc[1][10]);
	                ClmOriginal.HR.Employment.customJS = JSON.parse(trim(arrReturned[33]));

                    var l_ = '';
	                if (ClmOriginal.HR.Employment.employerLocId != '' && EditLoc[1][10] != '') {
	                    l_ = '\nExpected value:\nClient`s ' + LocLegend + ' Id : ' + ClmOriginal.HR.Employment.employerLocId;
                    }
	                var customJsMessage = ''; var customJsExec = '';
	                if (typeof ClmOriginal.HR.Employment.customJS == 'object'  && ClmOriginal.HR.Employment.customJS != null) {
	                    if ($.isArray(ClmOriginal.HR.Employment.customJS)) {
	                        var c = 0; var cM =  ClmOriginal.HR.Employment.customJS.length;
	                        while (c < cM) {
	                            customJsMessage += '\n' + ClmOriginal.HR.Employment.customJS[c].label + ' : ' + ClmOriginal.HR.Employment.customJS[c].value;
	                            customJsExec +=  ClmOriginal.HR.Employment.customJS[c].v + '="' + ClmOriginal.HR.Employment.customJS[c].value + '"' + ';';
                                c++
	                        }                            
	                    }
	                }


                   if (!confirm('You are about to set the following values on the claim:\n'
                                + '\nClaimant: ' + ClmOriginal.HR.Employee.fFName + ' ' + ClmOriginal.HR.Employee.lName
                                + '\nAddress: ' + ClmOriginal.HR.Employee.cAddr1 + ' ' + ClmOriginal.HR.Employee.cAddr2 + ' ' + ClmOriginal.HR.Employee.cCity + ', ' + ClmOriginal.HR.Employee.cState + '  ' + ClmOriginal.HR.Employee.cZip
                                + '\nPhone & EMail: ' + ClmOriginal.HR.Employee.workPhone + ' (work), ' + ClmOriginal.HR.Employee.cPhone + ' (cell) , ' + ClmOriginal.HR.Employee.eMail
                                + '\nDate of birth:' + ClmOriginal.HR.Employee.dob 
                                + '\n'
                                + '\nDept: ' + ClmOriginal.HR.Employment.dept + ', Employee Id: ' + ClmOriginal.HR.Employment.employeeId + ', Title: ' + ClmOriginal.HR.Employment.jobTitle
                                + '\nHire Date: ' + ClmOriginal.HR.Employment.hireDate
                                + '\nWeekly wage: ' + ClmOriginal.HR.Employment.weekWage + ', hours: ' + ClmOriginal.HR.Employment.weeklyHours
                                + '\n' + l_
                                + '\n' + customJsMessage)
                                ) {
                            ClmOriginal.HR.id = -1;
                            ClmOriginal.HR.Employee.employeeId = ''; // EE_EmpID;
                            ClmOriginal.HR.Employee.ssNum = ''; // EE_SSN;
                            ClmOriginal.HR.Employee.lName = ''; //EE_LName  
                            ClmOriginal.HR.Employee.fFName = ''; // EE_FName ;
                           // ClmOriginal.HR.Employee.mi = ''; //EE_MI_HIDDEN ;
                            ClmOriginal.HR.Employee.cAddr1 = ''; // ;
                            ClmOriginal.HR.Employee.cAddr2 = ''; // ;
                            ClmOriginal.HR.Employee.cCity = ''; // ;
                            ClmOriginal.HR.Employee.cState = ''; // ;
                            ClmOriginal.HR.Employee.cZip = ''; // ;
                            ClmOriginal.HR.Employee.cPhone = ''; // ;
                            ClmOriginal.HR.Employee.workPhone = ''; // ;
                            ClmOriginal.HR.Employee.eMail = ''; // ;
                            ClmOriginal.HR.Employee.dob = ''; // EE_DOB  ;
                            ClmOriginal.HR.Employee.sex = ''; //EE_Gender ;

                            ClmOriginal.HR.Employment.dept = '';
                            ClmOriginal.HR.Employment.jobTitle = '';
                            ClmOriginal.HR.Employment.emplStatusCode = 0;
                            ClmOriginal.HR.Employment.weekWage = 0;
                            ClmOriginal.HR.Employment.weeklyHours = 0;
                            ClmOriginal.HR.Employment.hireDate = '';
                            ClmOriginal.HR.Employment.terminateJobDate = '';
                            ClmOriginal.HR.Employment.employerLocId = '';
                            ClmOriginal.HR.Employment.employerNo = '';
                            ClmOriginal.HR.Employment.employerTaxId = '';
                            ClmOriginal.HR.Employment.HRSuperRecId = 0;
                            ClmOriginal.HR.Employment.Loc = -1;
                            ClmOriginal.HR.Employment.LocTxtCod = '';
                            ClmOriginal.HR.Employment.customJS = null;
                        return;
                    } else {
                        Var_Save_Claimant();

                        CLName = ClmOriginal.HR.Employee.lName;
                        CFName = ClmOriginal.HR.Employee.fFName;
                        CMI = '';
                        SSNum = (ClmOriginal.HR.Employee.ssNum.length> 0) ? ClmtSSN_Dash(ClmOriginal.HR.Employee.ssNum)  : '';
	                    Caddr1 =  ClmOriginal.HR.Employee.cAddr1;
	                    Caddr2 = ClmOriginal.HR.Employee.cAddr2;
	                    Ccity = ClmOriginal.HR.Employee.cCity;
	                    Cstate = ClmOriginal.HR.Employee.cState;
	                    Czip = ClmOriginal.HR.Employee.cZip;

                        Cphone = (ClmOriginal.HR.Employee.cPhone.length > 0) ? ClmtPhone_Dash(ClmOriginal.HR.Employee.cPhone) : '' ;
                        Cworkphone = (ClmOriginal.HR.Employee.workPhone.length > 0) ? ClmtPhone_Dash(ClmOriginal.HR.Employee.workPhone) : '';  
	                    CEMail = ClmOriginal.HR.Employee.eMail;
	                    DOB = ClmOriginal.HR.Employee.dob;
	                    Sex = ClmOriginal.HR.Employee.sex;

	                    employee_id = ClmOriginal.HR.Employment.employeeId;
	                    injury_dept = ClmOriginal.HR.Employment.dept;
	                    JobTitle = ClmOriginal.HR.Employment.jobTitle;
	                    EmplStatusCod = ClmOriginal.HR.Employment.emplStatusCode;
	                    WeekWage = decommafy(ClmOriginal.HR.Employment.weekWage);
	                    WeeklyHours = ClmOriginal.HR.Employment.weeklyHours;
	                    DateOfHire = ClmOriginal.HR.Employment.hireDate;
	                    TerminateJobDate = ClmOriginal.HR.Employment.terminateJobDate;
	                    HRSuperRecId = ClmOriginal.HR.Employment.HRSuperRecId;

	                    CCountryId = arrReturned[31];
	                    CstateDesc = arrReturned[32];

	                    claimant = Populate_claimant();
                        EmplStatus = (EmplStatus == '' || EmplStatus.indexOf("<option value=''></option>") > -1 || EmplStatus.indexOf("NOT APPLICABLE") > -1) ?  WC_EmplStatus_XTRCT("") : EmplStatus;

                        frButtons.document.frmButtons.WorkcompFlag.value = 1;
                        if (customJsExec.length > 0) { JSON.parse(customJsExec);};

	                    setTimeout("Display('claimant')", 200);
                    }
	            } catch (e) { }
	        }
	    }
	    return;
    }

	function LookupWindow_StateOccupCodes() {
	    var d = new Date;
	    var JuriState_ = JurState;
	    var PolicyID_ = 0;
	    var loc_= ($.isNumeric(EditLoc[1][0])) ? EditLoc[1][0]  : 0;
        var div_= ($.isNumeric(EditLoc[1][11])) ? EditLoc[1][11] : 0;
        var div2_ = ($.isNumeric(EditLoc[1][14])) ? EditLoc[1][14] : 0;

		if (PolicyCC.hasOwnProperty('id')) {
		    PolicyID_ = PolicyCC.id
		} else {
		    alert('Claim is not associated with a policy defined in the Policy system\nList of Policy-specific State Occupation codes is not available');
		    frInfo.document.frmClaim.cboWC_Occup.value = OccupID;
		    return;
		}

		var url = 'Screen_Lookup_StateOccupCodes.asp?t=' + d.getTime() + "&PolicyID=" + PolicyID_ + "&ClmNo=" + ClaimNumber + "&State=" + JuriState_ + "&LossDate=" + frGeneral.document.frmGeneral.txtAccDT.value + "&Loc=" + loc_ + "&Div=" + div_ + "&Div2=" + div2_
	    // window.open(url, "Filter", "status=no,height=5,width=5,top=5000,left=3000")
		frPrint.document.location = url;
	}


	function LookUp_ReturnValues_StateOccupCodes(arrReturned) {
	    if (typeof (arrReturned) == 'object') {
	        if (arrReturned.length > 0) {
	            OccupID = arrReturned[0];
	            OccupDescr = trim(arrReturned[1]) + ' - ' + trim(arrReturned[2]);
	            try {
	                frInfo.document.frmClaim.cboWC_Occup.options[1].value = OccupID;
	                frInfo.document.frmClaim.cboWC_Occup.options[1].text = OccupDescr;
	                try {
	                    frInfo.document.frmClaim.cboWC_Occup.title = SingleQuote(OccupDescr);
	                } catch (e) { };

	                frInfo.document.frmClaim.cboWC_Occup.options[1].selected = true;
	            } catch (e) { }
	        }
	    }
        return;
    }

	function LookupWindow_FinCorrections(Trnsaction, CorrectionID) {
	    var d = new Date;
	    var url = 'Screen_Lookup_FinCorrectionDetails.asp?t=' + d.getTime() + "&ClmNo=" + ClaimNumber + "&Trnsaction=" + Trnsaction + "&CorrectionID=" + CorrectionID
	    frPrint.document.location = url;
        //window.open(url, "Filter", "status=no,height=5,width=5,top=5000,left=3000")
	}
	
	
	function LookupWindow_Loc(Level, Legend, parentLegend){	
		var d = new Date;
		var ParentID = 0;

		if(parentLegend!='') {
			switch(Level){
				case 1:
					ParentID  = (frInfo.document.getElementById("cboDiv").value=='') ? 0 : frInfo.document.getElementById("cboDiv").value
					break;
				case 2:
					ParentID  = (frInfo.document.getElementById("cboDiv2").value=='') ? 0 : frInfo.document.getElementById("cboDiv2").value
					break;
			}
		}
		parentLegend = (parentLegend=='') ? 'NONE' : parentLegend;
		ParentID = (isNaN(ParentID)) ? '0' : ParentID
		
		var url = 'Screen_Lookup_Loc.asp?t=' + d.getTime()+ "&ClmNo=" + ClaimNumber + "&ClientNumber=" + ClientNumber + "&Level=" + Level + "&Legend=" + escape(Legend) + "&parentLegend="  + escape(parentLegend) + "&ParentID=" + ParentID + "&LossDate=" + frGeneral.document.frmGeneral.txtAccDT.value
		frPrint.document.location = url;
	    //window.open(url,"Filter","status=no,height=1,width=1,top=1000,left=5000")
	}
	
	function LookUp_Loc_ReturnValues(RetVal) {
		var arrLastIndex = RetVal.length-1;
		var level=RetVal[arrLastIndex];  
		var oOption=frInfo.document.createElement("OPTION");	
	    //level 1
		if(level==1){ //LOCATION
			EditLoc[1][0]= trim(RetVal[0]);  //loc;
			EditLoc[1][1]= trim(RetVal[1]);  //locnm;
			EditLoc[1][2]= trim(RetVal[2]);  //locnm2;
			EditLoc[1][3]= trim(RetVal[3]);  //loccity;
			EditLoc[1][4]= trim(RetVal[4]); //locstate;
			EditLoc[1][5]= trim(RetVal[5]);  //loczip;
			EditLoc[1][6]= trim(RetVal[6]);  //inception;
			EditLoc[1][7]= trim(RetVal[7]); //expiration;
			EditLoc[1][8]= trim(RetVal[8]);  //region;
			EditLoc[1][9]= trim(RetVal[9]);  //div; 
			EditLoc[1][10]=trim(RetVal[10]);  //loctxtcod;  
			EditLoc[1][11]=trim(RetVal[11]);  //div_div;
			EditLoc[1][12]=trim(RetVal[12]);  //div_divnm;
			EditLoc[1][13]=trim(RetVal[13]);  //div_divabbr;
			EditLoc[1][14]=trim(RetVal[14]);  //div2_div;
			EditLoc[1][15]=trim(RetVal[15]);  //div2_divnm;
			EditLoc[1][16] =trim(RetVal[16]);  //div2_divabbr;
			EditLoc[1][17] = trim(RetVal[17]);  //LocExtRecordCode;
			EditLoc[1][18] = trim(RetVal[18]);  //DIV_ExtRecordCode;
			EditLoc[1][19] = trim(RetVal[19]);  //DIV2_ExtRecordCode;
					
			try{
				frInfo.document.frmClaim.cboLocation.options.length=0;  
				
				oOption=frInfo.document.createElement("OPTION"); oOption.text="GET ANOTHER";  oOption.value="GET ANOTHER";				 
				frInfo.document.frmClaim.cboLocation.add(oOption);
				
				if(DivLegend!='' ){			
					frInfo.document.frmClaim.cboDiv.options.length=0;
					oOption=frInfo.document.createElement("OPTION"); oOption.text="GET ANOTHER";  oOption.value="GET ANOTHER";	 
					frInfo.document.frmClaim.cboDiv.add(oOption);
				}
				if(Div2Legend!=''){
					frInfo.document.frmClaim.cboDiv2.options.length=0;
					oOption=frInfo.document.createElement("OPTION"); oOption.text="GET ANOTHER";  oOption.value="GET ANOTHER";	
					frInfo.document.frmClaim.cboDiv2.add(oOption);
				}	
			}catch(e){}
			
			try{
				oOption=frInfo.document.createElement("OPTION"); 
				var o1 = EditLoc[1][10];
				o1 += (EditLoc[1][17].length == 0) ? "" : " [" + EditLoc[1][17] + "]";
                o1 += '__' +EditLoc[1][1]+ ' ' +EditLoc[1][2];
                oOption.text = o1;
				oOption.value=EditLoc[1][0];
				oOption.selected=true;	
				frInfo.document.frmClaim.cboLocation.add(oOption);				
			}catch(e){					
			}
			
			try{
			    if (DivLegend != '') {
					oOption =frInfo.document.createElement("OPTION");
                    var o2 = EditLoc[1][13];
				    o2 += (EditLoc[1][18].length == 0) ? "": " [" + EditLoc[1][18]+ "]";
				    o2 += '__' + EditLoc[1][12];
                    oOption.text = o2;
					oOption.value=EditLoc[1][11];
					oOption.selected=true;	
					frInfo.document.frmClaim.cboDiv.add(oOption);				
				}
				if(Div2Legend!=''){
				    oOption = frInfo.document.createElement("OPTION");
                    var o3 = EditLoc[1][16];
				    o3 += (EditLoc[1][19].length == 0) ? "": " [" +EditLoc[1][19]+ "]";
				    o3 += '__' + EditLoc[1][15];
                    oOption.text = o3;
					oOption.value=EditLoc[1][14];
					oOption.selected=true;						
					frInfo.document.frmClaim.cboDiv2.add(oOption);
				}	
			}catch(e){}
						
			frInfo.document.getElementById("ClntRegion").innerText= (EditLoc[1][8]!='' && EditLoc[1][8]!='0') ? " REGION: " + trim(EditLoc[1][8]) : '';



			if (SCREEN == "Commerce" || SCREEN == "Commerce_2" || SCREEN == "CommerceWorkComp" || SCREEN == "Rental" || SCREEN == "Main" || SCREEN == "MainWorkComp") {
				if((Polstruct=='I' || Polstruct=='V') && trim(EditLoc[1][9])!='' && trim(EditLoc[1][6])!='' && trim(EditLoc[1][7])!=''){
			        frGeneral.document.frmGeneral.txtPolNo.value = trim(EditLoc[1][9]);
			        frGeneral.document.frmGeneral.txtEffDT.value = MyDateFormat(trim(EditLoc[1][6]));
			        frGeneral.document.frmGeneral.txtExpDT.value = MyDateFormat(trim(EditLoc[1][7]));
			    }
                
                //pull new policy selection screen:
			    if (PolicyCC.hasOwnProperty('id')) {
			        setTimeout("LookUp_PolicyCC();", 200);
			    }
			}		
		}
		///level 2:
		if(level==2){ //LOCATION
			EditLoc[1][0]= 0;   //loc;
			EditLoc[1][1]= '';  //locnm;
			EditLoc[1][2]= '';  //locnm2;
			EditLoc[1][3]= '';  //loccity;
			EditLoc[1][4]= '';  //locstate;
			EditLoc[1][5]= '';  //loczip;
			EditLoc[1][6]= '';  //inception;
			EditLoc[1][7]= '';	//expiration;
			EditLoc[1][8]= trim(RetVal[7]);  //region;
			EditLoc[1][9]= '';  //div; 
			EditLoc[1][10]='';  //loctxtcod;  
			EditLoc[1][11]=trim(RetVal[0]);  //div_div;
			EditLoc[1][12]=trim(RetVal[1]);  //div_divnm;
			EditLoc[1][13]=trim(RetVal[6]);  //div_divabbr;
			EditLoc[1][14]=trim(RetVal[8]);  //div2_div;
			EditLoc[1][15]=trim(RetVal[9]);  //div2_divnm;
			EditLoc[1][16] = trim(RetVal[10]); //div2_divabbr;
			EditLoc[1][17] = '';  //LocExtRecordCode;
			EditLoc[1][18] = trim(RetVal[12]); //Div_ExtRecordCode;
			EditLoc[1][19] = trim(RetVal[13]); //Div2_ExtRecordCode;

			try{
				frInfo.document.frmClaim.cboLocation.options.length=0;  
				
				oOption=frInfo.document.createElement("OPTION"); oOption.text="GET ANOTHER";  oOption.value="GET ANOTHER";				 
				frInfo.document.frmClaim.cboLocation.add(oOption);
				
				if(DivLegend!='' ){			
					frInfo.document.frmClaim.cboDiv.options.length=0;
					oOption=frInfo.document.createElement("OPTION"); oOption.text="GET ANOTHER";  oOption.value="GET ANOTHER";	 
					frInfo.document.frmClaim.cboDiv.add(oOption);
				}
				if(Div2Legend!=''){
					frInfo.document.frmClaim.cboDiv2.options.length=0;
					oOption=frInfo.document.createElement("OPTION"); oOption.text="GET ANOTHER";  oOption.value="GET ANOTHER";	
					frInfo.document.frmClaim.cboDiv2.add(oOption);
				}	
			}catch(e){}
			
			try{
				oOption=frInfo.document.createElement("OPTION"); 
				oOption.text='';
				oOption.value=EditLoc[1][1];
				oOption.selected=true;	
				frInfo.document.frmClaim.cboLocation.add(oOption);
				frInfo.document.getElementById("ClntLoc").innerText=EditLoc[1][9];
			}catch(e){}
			
			try{
				if(DivLegend!='' ){
				    oOption = frInfo.document.createElement("OPTION");
                    var o22 = (EditLoc[1][18].length == 0) ? "": " [" +EditLoc[1][18]+ "]";				    
                    oOption.text = EditLoc[1][13] + o22  + '__'+EditLoc[1][12];
					oOption.value=EditLoc[1][11];
					oOption.selected=true;	
					frInfo.document.frmClaim.cboDiv.add(oOption);
				}
				if(Div2Legend!=''){
				    oOption = frInfo.document.createElement("OPTION");
                    var o23 = (EditLoc[1][19].length == 0) ? "" : " [" +EditLoc[1][19]+ "]";
                    oOption.text = EditLoc[1][16] + o23 + '__'+EditLoc[1][15];
					oOption.value=EditLoc[1][14];
					oOption.selected=true;						
					frInfo.document.frmClaim.cboDiv2.add(oOption);
				}	
			}catch(e){}
						
			frInfo.document.getElementById("ClntRegion").innerText= (EditLoc[1][8]!='' && EditLoc[1][8]!='0') ? " REGION: " + trim(EditLoc[1][8]) : '';
		}
		///level 3:
		if(level==3){ 
			EditLoc[1][0]= 0;  //loc;
			EditLoc[1][1]= '';  //locnm;
			EditLoc[1][2]= '';  //locnm2;
			EditLoc[1][3]= '';  //loccity;
			EditLoc[1][4]= ''; //locstate;
			EditLoc[1][5]= '';  //loczip;
			EditLoc[1][6]= '';  //inception;
			EditLoc[1][7]= ''; //expiration;
			EditLoc[1][8]= trim(RetVal[7]);  //region;
			EditLoc[1][9]= '';  //div; 
			EditLoc[1][10]='';  //loctxtcod;  
			EditLoc[1][11]=0;  //div_div;
			EditLoc[1][12]='';  //div_divnm;
			EditLoc[1][13]='';  //div_divabbr;
			EditLoc[1][14]=trim(RetVal[0]);  //div2_div;
			EditLoc[1][15]=trim(RetVal[1]);  //div2_divnm;
			EditLoc[1][16]=trim(RetVal[6]);  //div2_divabbr;
			EditLoc[1][17]= '';  //LocExtRecordCode;
			EditLoc[1][18]= ''; //Div_ExtRecordCode;
			EditLoc[1][19]= trim(RetVal[7]); //Div2_ExtRecordCode;

			try{
				frInfo.document.frmClaim.cboLocation.options.length=0;  
				
				oOption=frInfo.document.createElement("OPTION"); oOption.text="GET ANOTHER";  oOption.value="GET ANOTHER";				 
				frInfo.document.frmClaim.cboLocation.add(oOption);
				
				if(DivLegend!='' ){			
					frInfo.document.frmClaim.cboDiv.options.length=0;
					oOption=frInfo.document.createElement("OPTION"); oOption.text="GET ANOTHER";  oOption.value="GET ANOTHER";	 
					frInfo.document.frmClaim.cboDiv.add(oOption);
				}
				if(Div2Legend!=''){
					frInfo.document.frmClaim.cboDiv2.options.length=0;
					oOption=frInfo.document.createElement("OPTION"); oOption.text="GET ANOTHER";  oOption.value="GET ANOTHER";	
					frInfo.document.frmClaim.cboDiv2.add(oOption);
				}	
			}catch(e){}
			
			try{
				oOption=frInfo.document.createElement("OPTION"); 
				oOption.text='';
				oOption.value=EditLoc[1][1];
				oOption.selected=true;	
				frInfo.document.frmClaim.cboLocation.add(oOption);
				frInfo.document.getElementById("ClntLoc").innerText=EditLoc[1][9];
			}catch(e){
				alert(e.message)		
			}
			
			try{
				if(DivLegend!='' ){
					oOption=frInfo.document.createElement("OPTION"); 
					oOption.text='';
					oOption.value=EditLoc[1][11];
					oOption.selected=true;	
					frInfo.document.frmClaim.cboDiv.add(oOption);
					//frInfo.document.getElementById("ClntDiv").innerText= EditLoc[1][13];
				}
				if(Div2Legend!=''){
				    oOption = frInfo.document.createElement("OPTION");
                    var o33 = (EditLoc[1][19].length == 0) ? "": " [" +EditLoc[1][19]+ "]";
					oOption.text=EditLoc[1][16] + o33 +'__'+EditLoc[1][15];
					oOption.value=EditLoc[1][14];
					oOption.selected=true;						
					frInfo.document.frmClaim.cboDiv2.add(oOption);
					//frInfo.document.getElementById("ClntDiv2").innerText= (EditLoc[1][16]!='') ? " # " + trim(EditLoc[1][16]) : '';
				}	
			}catch(e){}
						
			frInfo.document.getElementById("ClntRegion").innerText= (EditLoc[1][8]!='' && EditLoc[1][8]!='0') ? " REGION: " + trim(EditLoc[1][8]) : '';
		}
	}
	
	
	function LookupWindow_AssistOccur(AdminClmNo,AdminOccurr,AssistClmNo){	
		var d = new Date;
		var url = 'Screen_Lookup_AssistOccur.asp?t=' + d.getTime() + "&AdminClmNo=" + AdminClmNo +  "&AdminOccurr=" + AdminOccurr +
												 "&AssistClmNo=" + AssistClmNo
								
	    //window.open(url,"Filter","status=no,height=5,width=5,top=5000,left=3000")
		frPrint.document.location = url;
	}
	
	function LookUp_ReturnValues_AssistOccur(arrClmNo){ 
		AssistOccur_XTRCT(arrClmNo[0], arrClmNo[1])
	}
	
	function LookupWindow_RecoveryReferredTo(AssistType){
	    var d = new Date;
		if(AssistType=='R'){
			var modalURL= 'Screen_Lookup_RecoveryReferredTo.asp?t=' + d.getTime() 
			var addRetVal=window.showModalDialog(modalURL,"","dialogHeight:1px;dialogwidth:1px;dialogTop:4000;dialogLeft:3000;resizable:yes;help:no;status:no");

			if(typeof(addRetVal)=="undefined"){
			}else{
				if(trim(addRetVal[2])=='N'){   // non-internal recoveryu requests are being processed on the Main File;
					REC_STATUS='Y';				//??
					ReferredTo = trim(addRetVal[0]);
					Display("rec");					
				}else{
					ReferredTo = trim(addRetVal[0]);
					Display("request_assistance");
				}
			}
		}else{
			Display("request_assistance");
		}
	}
	

	function FinDetails_FilteredSearch(Payee, BillTo, PayDate_from, PayDate_to, PayType){
		var Fltr="Data was filtered by"
		if(Payee.length!=0){Fltr += " Payee:" + Payee + "; "}
		if(BillTo.length!=0){Fltr += " Bill To #:" + BillTo + "; "}
		if(PayDate_from.length!=0){Fltr += " Pay Date:" + PayDate_from + " - " + PayDate_to + "; "}
		if(PayType.length!=0){Fltr  += " Pay Type:" + PayType + "; "}
		
		frInfo.document.getElementById("PmntDetViewSwitch").innerText="+ DISPLAY PAYMENT DETAILS"

		if (Fltr != "Data was filtered by") {
		    //setTimeout('frInfo.document.getElementById("FinDet").innerHTML = "<span>procesing ...</span>"', 10);
			ViewPayments_XTRCT(BillTo, Payee, PayDate_from, PayDate_to, PayType)
			if(FinList.length==0){
				alert(Fltr + '\nNo records has been found');				
				ViewHideFinDetails('P', '');	 
			}else{			
				ViewHideFinDetails('P', Fltr); 
			}
		}else{
			FinList='';  //no filters -  refresh full list
			ViewHideFinDetails('P', '');
		}
	}


	function Populate_prefProvider() {
	    var f = "<html><head>"
	    + "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 + "<style>HTML,BODY {height:99%; width:100%; margin:0 0 0 0;padding:0 0 0 0; }</style>"
		 + "<title>prefProvider</title></head><body>"
         + "<iframe frameborder=0 style='overflow:auto;margin:0 0 0 0;padding:0 0 0 0;' scrolling=auto style='width:100%;height:99%' src='/portaL/Net/Referrals/ClaimReferrals.aspx?claim_no=" + ClaimNumber + "'></iframe>"
	     + "</body></html>"
	    return (f);      //BlankPage.htm
	}
	
	
	function Populate_customList() {
	    var l = 0;
        customList_FilterMatch()

	    l = 0;
	    
	    var f = "<html><head>"
		 + "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 + "<title>customList</title></head><body><form name='frmClaim'>"
		 + "<table id='custom' cellPadding=1 cellSpacing=1 width=95% border=0 >"
	    while(ClmOriginal.CustomLists.hasOwnProperty(l)){  //(l < l_lists) {
	        f += "<tr><td>"
	        f += (typeof (CustomLists[l]) == "object" && CustomLists[l] != null) ? Populate_customList_Details(l) : Populate_customList_Removed(l)
	        f += "</td></tr>"

	        l++
	    }
	    f += "</table></form></body></html>"
	    return (f);
	}
	
	function Populate_customList_Details(l){
	    var f= ""
             + "<table id='custom' cellPadding=1 cellSpacing=1 width=90% border=0 style='width:100%'>"
             + "<tr>"
             + "<td style='width:120px'>"//list definition:
                + "<span><b>" + CustomLists[l].name + "</b></span>"
                 + "<br>Refers to: <span>" + CustomLists[l].refer + "</span>"
                 + "<br><br>&nbsp;&nbsp;&nbsp;"
	            f += (UserRole.indexOf('EditFile') != -1) ? "<input type='button' name='cmdEdit' value='Edit' style='width:60px' class=BottonScreen onclick='parent.CustomList_XTRCT(" + CustomLists[l].id + ")'>" : ""

            f += "</td>"
             + "<td  align=left>" // levels settings:
             + "<table border=0 style='width:100%'>"
            var i = 0;
            while (i < CustomLists[l].levels.length) {
                f += "<tr>"
                            + "<td style='width:120px'>" + CustomLists[l].levels[i].no + ' - ' + CustomLists[l].levels[i].name + ":</td>"
                            + "<td>" +  CustomLists[l].levels[i].val   +  "</td>"
                          + "</tr>"
                i++
            }
            f += "</table>"
             + "</td>"
             + "</tr>"
             + "</table>"
        
        return(f);
	}

	function Populate_customList_Removed(l) {
	    var f = ""
             + "<table id='custom' cellPadding=1 cellSpacing=1 width=90% border=0 style='width:100%'>"
             + "<tr>"
             + "<td style='width:120px'>"//list definition:
                + "<span><b>" + ClmOriginal.CustomLists[l].name + "</b></span>"
                 + "<br>Refers to: <span>" + ClmOriginal.CustomLists[l].refer + "</span>"
                 + "<br><br>&nbsp;&nbsp;&nbsp;"
	    f += "</td>"
         + "<td  align=left><span>Your changes to the claim affected filters defining applicability of the Custom List:"
	    j = ClmOriginal.CustomLists[l].filters.length - 1;
	    while (j > -1) {
	        f +='<br> - '
	            for (filter in ClmOriginal.CustomLists[l].filters[j]) {
	                f += "<b>" 
				        switch(filter){
					        case 'lob':
					            f += "Line of business"; break;
				            case 'cov':
				                f += "Coverage (loss type)"; break;
				            case 'veh':
				                f += "Insured Vehicle VIN"; break;
				            case 'polid':
				                f += "Policy id"; break;
				            case 'state':
				                f += "Jurisdiction state"; break;
				            case 'd':
				                f += DivLegend; break;
				            case 'l':
				                f += LocLegend; break;
				            case 'propid':
				                f += "Insured Property id"; break;
					        default:
					            f += "Unknown";
				        }
				    f += " : "
				    f += ClmOriginal.CustomLists[l].filters[j][filter].replace(/\|/gi, '');
				    f += '</b>' + '; '
	            }
	        j--  // cycle of list filters
	    }
	    if (ClmOriginal.CustomLists[l].subsetMatchRqrd == 'Y') {
	        j = ClmOriginal.CustomLists[l].subsets.length - 1;
	        f += '<br> Required data subset filters:'
	        while (j > -1) {
	            f += '<br> - '
	            for (filter in ClmOriginal.CustomLists[l].subsets[j]) {
	                f += "<b>"
	                switch (filter) {
	                    case 'lob':
	                        f += "Line of business"; break;
	                    case 'cov':
	                        f += "Coverage (loss type)"; break;
	                    case 'veh':
	                        f += "Insured Vehicle VIN"; break;
	                    case 'polid':
	                        f += "Policy id"; break;
	                    case 'state':
	                        f += "Jurisdiction state"; break;
	                    case 'd':
	                        f += DivLegend; break;
	                    case 'l':
	                        f += LocLegend; break;
	                    case 'propid':
	                        f += "Insured Property id"; break;
	                    default:
	                        f += "Unknown";
	                }
	                f += " : "
	                f += ClmOriginal.CustomLists[l].subsets[j][filter].replace(/\|/gi, '');
	                f += '</b>' + '; '
	            }
	            j--  // cycle of list filters
	        }
	    }
	    f += "<br>Please update the claim to see if the Custom List applies.</span>"
         + "</td>"
         + "</tr>"
         + "</table>"

	    return (f);
	}

	function customList_FilterMatch() {
	    // go over the ClmOriginal.CustomLists elements
	    //    compare filters of each element with the current IE filter values.
	    //     if they match:  verify object CustomLists has the given element --> (if Not -- add it)     --- > this will allow to show it
	    //     if they dont match:  delete the fivent element from the  object  CustomLists   
	    var i = 0; var j = 0; var filterElementsMatch = true;

	    if (!ClmOriginal.CustomLists.hasOwnProperty(0)) { return };
	    
	    var cov_ = 0; var vin_ = ''; var policyId_ = 0; var div_ = 0; var loc_ = 0;

	    while (ClmOriginal.CustomLists.hasOwnProperty(i)) {
	        filterElementsMatch = true;
	        if (ClmOriginal.CustomLists[i].hasOwnProperty('filters')) {
	            j = ClmOriginal.CustomLists[i].filters.length-1;
	            while (j > -1) {
	                filterElementsMatch = customList_FilterElementsMatch(ClmOriginal.CustomLists[i].filters[j]);
                    // match found
	                if (filterElementsMatch == true) {
	                    if (!CustomLists.hasOwnProperty(i)) {
	                        CustomLists[i] = {};
	                        objectExtend(ClmOriginal.CustomLists[i], CustomLists[i]);
	                        break;
	                    }
	                }
	                j--  // cycle of list filters
	            }
	        }
            // filter's match not found:
	        if (filterElementsMatch == false) {
	            if (CustomLists.hasOwnProperty(i)) {
	                delete CustomLists[i];
	            }
	        } else {  // verify subset if external filter matches 
	            if (ClmOriginal.CustomLists[i].hasOwnProperty('subsets')) {
	                j = ClmOriginal.CustomLists[i].subsets.length - 1;
	                while (j > -1) {
	                    filterElementsMatch = customList_FilterElementsMatch(ClmOriginal.CustomLists[i].subsets[j]);
	                    // match found
	                    if (filterElementsMatch == true) {
	                        if (!CustomLists.hasOwnProperty(i)) {
	                            CustomLists[i] = {};
	                            objectExtend(ClmOriginal.CustomLists[i], CustomLists[i]);
	                            break;
	                        }
	                    }
	                    j--  // cycle of list subsets
	                }
	                // match not found:
	                if (filterElementsMatch == false && ClmOriginal.CustomLists[i].subsetMatchRqrd =='Y') {
	                    if (CustomLists.hasOwnProperty(i)) {
	                        delete CustomLists[i];
	                    }
	                } 
	            }
	        }
            i++ // cycle of lists
        }

	    if (!isItArray(CustomLists)) {
            CustomLists.length=0;
        }
	    return;
	}
	

	function customList_FilterElementsMatch(filter) {
	    var retval = true;

	    if(filter.hasOwnProperty('lob')) {
	        if (filter.lob.indexOf('|' + Unit + '|') < 0) {
	            retval = false;	            
	            return (retval);
	        }
	    }
	    if (filter.hasOwnProperty('cov')) {
	        var cov_ = -1;
	        var re = /^(\d*)_/gi
	        re.exec(CovCod);
	        cov_ = RegExp.$1;
	        if (filter.cov.indexOf('|' + cov_ + '|') < 0) {
	            retval = false;
	            return(retval);
	        }
	    }
	    if (filter.hasOwnProperty('veh')) {
	        var vin_ = '...';
	        if (PolicyCC.hasOwnProperty('VEH')) {
	            if (typeof (PolicyCC.VEH) != "undefined") {
	                if (PolicyCC.VEH[0] != null) {
	                    vin_ = PolicyCC.VEH[0].VIN;
	                }
	            }
	        }
	        if (filter.veh.indexOf('|' + vin_ + '|') < 0) {
	            retval = false;
	            return(retval);
	        }
	    }
	    if (filter.hasOwnProperty('propid')) {
	        var propid_ = 0;
	        if (PolicyCC.hasOwnProperty('PROP')) {
	            if (typeof (PolicyCC.PROP) != "undefined") {
	                if (PolicyCC.PROP[0] != null) {
	                    propid_ = PolicyCC.VEH[0].ID;
	                }
	            }
	        }
	        if (filter.propid.indexOf('|' + propid_ + '|') < 0) {
	            retval = false;
	            return (retval);
	        }
	    }
	    if (filter.hasOwnProperty('polid')) {
	        var policyId_ = 0;
	        if (PolicyCC.hasOwnProperty('id')) {
	            policyId_ = PolicyCC.id;
	        }
	        if (filter.polid.indexOf('|' + policyId_ + '|') < 0) {
	            retval = false;
	            return(retval);
	        }
	    }
	    if (filter.hasOwnProperty('state')) {
	        if (filter.state.indexOf('|' + JurState + '|') < 0) {
	            retval = false;
	            return(retval);
	        }
	    }
	    if (filter.hasOwnProperty('d')) {
	        if (DivLegend != '') {
	            div_ = EditLoc[1][11];
	        }
	        if (filter.d.indexOf('|' + div_ + '|') < 0) {
	            retval = false;
	            return(retval);
	        }
	    }
	    if (filter.hasOwnProperty('l')) {
	        if (LocLegend != '') {
	            loc_ = EditLoc[1][0];
	        }
	        if (filter.l.indexOf('|' + loc_ + '|') < 0) {
	            retval = false;
	            return(retval);
	        }
	    }
	    return(retval);
	}

	
	function Populate_custom(){
	    var f

		f="<html><head>"
		 + "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 + "<title>custom</title></head><body><form name='frmClaim'>"
		if (typeof T1_HTML != 'undefined' && typeof T2_HTML != 'undefined' && typeof T3_HTML != 'undefined') {
		    f += "<table id='custom' cellPadding=1 cellSpacing=1 border=0 width=90%>"
            + "<caption vAlign=bottom ><Label style=COLOR:blue>*** REQUIRED FIELDS COLORED BLUE ***</Label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
		    // + " <A name='Print' href='javascript:parent.PrintScreen(\"custom\")'>PRINT SCREEN</A>"
            + "</caption>"
            + "<tr>" + T1_HTML + T2_HTML + T3_HTML + "</tr>"
            + "<tr>" + T4_HTML + T5_HTML + T6_HTML + "</tr>"
            + "<tr>" + T7_HTML + T8_HTML + T9_HTML + "</tr>"
            + "<tr>" + T10_HTML + T11_HTML + T12_HTML + "</tr>"
            + "<tr>" + T13_HTML + T14_HTML + T15_HTML + "</tr>"
            + "<tr>" + T16_HTML + T17_HTML + T18_HTML + "</tr>"
            + "<tr>" + T19_HTML + T20_HTML + T21_HTML + "</tr>"
            + "<tr>" + T22_HTML + T23_HTML + T24_HTML + "</tr>"
            + "<tr>" + T25_HTML + T26_HTML + T27_HTML + "</tr>"
            + "<tr>" + T28_HTML + T29_HTML + T30_HTML + "</tr>"
            + "<tr>" + T31_HTML + T32_HTML + T33_HTML + "</tr>"
            + "<tr>" + T34_HTML + T35_HTML + T36_HTML + "</tr>"
            + "<tr>" + T37_HTML + T38_HTML + T39_HTML + "</tr>"
            + "</table>"
		}
		if (MainFlag == 'Edit') {
		    f += '<div id="CF_Container_1" class="Cf_Container" data-cf-ObjectRegistryId="6"  data-ObjectKey= "' + claim_id + '"  ></div>'
		}
		f += "</form></body></html>"		
		return(f);
	}

	function Populate_LossLocation() {
	    var n = 'LossAddress';
	    var c = "<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		+ "<title>lossLoc</title></head><body><form name='frmClaim'>"
       
		+ "<input name = 'txtLossStateId' type='hidden' id='txtLossStateId'  value='" + LossStateId + "' />"  // 31
        + "<input name = 'txtLossState'  type='hidden' id='txtLossState'  value='" + State + "' />"  // 31
		+ "<input name = 'txtLossCountryId' type='hidden' id='txtLossCountryId'  value = '" + LossCountry + "' />"  //

		+ "<table border=0 cellPadding=1 cellSpacing=1 id='lossLoc'  style='width:500px'>"
        + "<colgroup span=1 style='width:100px'>"
        + "<col style='width:60px'><col style='width:\"*\"'><col style='width:60px'><col style='width:\"*\"'>"
        + "<colgroup span=3 style='width:*'>"
		+ "<tr>"
		+ "<td id='LossAddr1' rowspan=1>"
		+ "<span id='lkInsAddr' style='color:blue;cursor:hand;' onclick='parent.AddressUpdate_loss(\"" + n + "\");' ><u>Address:</u></span>"
        + "</td>"

		+ "<td colspan=3>"
		+ "<input name='AddrViewLine' value='" + trim(LossAddr1) + ' ' + trim(LossAddr2) + "'  class=textReadOnly READONLY>" // style='width:240px'
		+ "<input name='txtLossAddr1' value='" + trim(LossAddr1) + "'  type=hidden   maxlength='50'>"
		+ "<input name='txtLossAddr2' value='" + trim(LossAddr2) + "'  type=hidden  maxlength='50'>"
		+ "</td>" 
        + "</tr>"

		+ "<tr>"
		+ "<td id='LossCity'>City/Town:</TD>"
		+ "<td><input name='txtLossCity' value='" + trim(LossCity) + "' style='width:100%'  maxlength='18' class=textReadOnly READONLY></TD>"  // style='width:200px'
		+ "<td id='LossState'>State:</TD>"
		+ "<td><input name='txtLossStateDesc' value='' style='width:100%' class=textReadOnly READONLY></td>"
		+ "</tr>"

		+ "<tr>"
		+ "<td id='LossZip'>Zip/Postal Code:</TD>"
		+ "<td><input name='txtLossZip' value='" + LossZip + "' style='width:100%' class=textReadOnly READONLY></td>"
		+ "<td id='LossCountry'>Country:</td>"
		+ "<td><input name='txtLossCountry' value='' style='width:100%' class=textReadOnly READONLY></td>"
        + "</tr>"
        
        + "<tr>"
        + "<td id='LossLocDesc'>Loc Desc:</TD>"
        + "<td colspan=3><input  name='txtLossLoc' value='" + LocDesc + "' style='width:100%' maxlength ='65'></TD>"
        + "</tr>"

        + "</table>"
	    + "</form></body></html>"

        return (c);
	}


	function Populate_claimant() {
	    var re = eval('/~' + Unit + '/gi');
	    var n = ''

		if(re.test(Reserve_LOB_MCodes)==false || Unit=='WC'){
			n='Claimant'
		}else{
			n='LossAddress'
		}	
		var c= "<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		+ "<Title>claimant</Title></head><body><Form name='frmClaim'>"

        + "<input name = 'cboClmtState' type='hidden' id = 'cboClmtState' value = '" + Cstate + "' />"  // NY
		+ "<input name = 'ClmtStateId'  type='hidden' id = 'ClmtStateId'  value = '" + CstateId + "' />"  // 31
		+ "<input name = 'ClmtCountryId'  type='hidden' id = 'ClmtCountryId'  value = '" + CCountryId + "' />"  //
		
		+ "<table border=0 cellPadding=1 cellSpacing=1 id='claimant'>"
		+ "<tr>"
		+ "<td id='ClmtAddr1' rowspan=1>"
		+ "<span id='lkInsAddr' style='color:blue;cursor:hand;' onClick='parent.AddressUpdate_claimant(\"" + n + "\");' ><u>Address:</u></span>"

		+ "</TD>"
		+ "<TD colspan=3>"
		+ "<input name='AddrViewLine' value='" + trim(Caddr1) + ' ' + trim(Caddr2) + "' style='width:440px'  class=textReadOnly READONLY>" // style='width:240px'
		+ "<input name='txtClmtAddress' value='" + Caddr1 + "' type=hidden maxlength='30'>"
		+ "<input name='txtClmtAddress2' value='" + Caddr2 + "'  type=hidden  maxlength='30'>"
		+ "</TD></tr>"

		+ "<TR>"
		+ "<TD id='ClmtCity'>City/Town:</TD>"
		+ "<TD><input name='txtClmtCity' value='" + Ccity + "' style='width:100%'  maxlength='18' class=textReadOnly READONLY></TD>"  // style='width:200px'
		+ "<TD id='ClmtState'>State:</TD>"
		+ "<td><input name='txtClmtState' value='" + CstateDesc + "' style='width:100%' class=textReadOnly READONLY></td>"

		+ "</tr>"
		+ "<tr>"
		+ "<TD id='ClmtZip'>Zip/Postal Code:</TD>"
		+ "<TD><input name='txtClmtZip' value='" + Czip + "' style='width:100%' class=textReadOnly READONLY></td>"
		+ "<td id='ClmtCountry'>Country:</td>"
		+ "<TD><input name='txtCountry' value='" + CCountry + "' style='width:100%' class=textReadOnly READONLY></td>"         
        
		+ "</tr>"
		if (n == 'Claimant') {
		    c += "<TR>"
		    + "<TD id='ClmntSSN'>"
		    c += (CPerson == 'Y') ? "SSN#" : "TIN"
		    c += ":</TD>"
		    + "<TD><input name='txtClmtSSN' value='" + SSNum + "' maxlength='9' onBlur='{document.frmClaim.txtClmtSSN.value=parent.ClmtSSN_Dash(parent.frInfo.document.frmClaim.txtClmtSSN.value);}'></TD>"
		    + "<TD id='MaidenName'>Other Name"
		    c += (CPerson == 'Y') ? "(Maiden/AKA)" : ""
		    c += ":</TD>"
		    + "<TD><input name='txtMaidenName' value='" + MaidenName + "' style='width:200px' maxlength='25'></TD>"
            if(CPerson =='Y'){
		     c += "<TD id='ClmntGender'>Gender:</TD>"
		    + "<TD><select name=cboClmtSex><option value=''></option> <option value='F'>Female</option> <option value='M'>Male</option><option value='U'>Unknown</option></select>"
		    }else{
		     c += "<TD id='ClmntGender'></TD><td><input type='hidden' name='cboClmtSex' value=''></td>"
		    }
		     c += "</TR>"
		} else {
		   // c += "</tr>"
		    c += "<tr>"

		    c += "<td id='ClmntSSN'>TaxID:</td>"
		        + "<td><input name='txtClmtSSN' value='" + SSNum + "' maxlength='9' onBlur='{document.frmClaim.txtClmtSSN.value=parent.ClmtSSN_Dash(parent.frInfo.document.frmClaim.txtClmtSSN.value);}'></td>"
		        + "</tr>"
		}
		if (n == 'Claimant') {
		    if (CPerson == 'Y') {
		        c += "<TR>"
		            + "<TD id='ClmntDOB'>Birth Date:</TD>"
		            + "<TD><input name='txtClmtDOB' value='" + MyDateFormat(DOB) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this);}'></TD>"
		            + "<TD id='ClmntDeath'>Death Date:</TD>"
		            + "<TD><input name='txtDODEATH' value='" + MyDateFormat(DODEATH) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this);}'></TD>"
		            + "<TD>Age:</TD>"
		            + "<TD>"
		        if (MainFlag == 'Edit') {
		            c += "<span>AT LOSS DATE:&nbsp;" + age_loss + "</span><BR><span>CURRENT AGE:&nbsp;" + age_current + "</span><BR><span>Life Expectancy:&nbsp;" + LifeExpect + "</span>"
		        }
		        c += "</TD>"
		      + "</TR>"
		    } else {
		         c += " <input type='hidden' name='txtClmtDOB' value=''> <input type='hidden' name='txtDODEATH' value=''>"
		    }
        }
		c += "<TR><TD>Phone:</TD>"
		+ "<TD><input name='txtClmtPhone' value='" + Cphone + "' maxlength='25'  style='width:155px' ></TD>"   
		+ "<TD>Bus. Phone:</TD>"
		+ "<TD><input name='txtClmtBusPhone' value='" + Cworkphone + "' maxlength='25'  style='width:155px' ></TD>" 
		if (n == 'Claimant') {
		    c += "</TR>"
		    c += "<tr>"
		    c += "<TD>Language:</TD><TD colspan=3><input type='text' name='CLanguage' value='" + CLanguage + "' style='width:100%' maxlength='30'></TD>"
//		    if (EnablePlasticCard == 'Y' || EnablePlasticCard == 'A') {
		    c += "<TD><input type='checkbox' id='chkDebitCardAuth'><span id='spnDebitCardAuth'>Debit card authorized</TD>"
//	        }
	        if (ACH.Enabled == 'Y') {
	            c += "<td> <span id='scnACH' style='color:blue;cursor:hand;' onClick='parent.AchRegistration_Display();'><u>ACH Payments</u></span></td>"
	        }
		    c += "</TR>"
		} else {
		    c += "</TR>"
		}
		if (n == 'Claimant') {
		    if (CPerson == 'Y') {
		        c += "<TR>"
		        + "<TD>Driver License:</TD>"
		        + "<TD><input name='CLICENSENO' value='" + CLICENSENO + "' maxlength='15'></TD>"
		        + "<TD>License State:</TD>"
		        + "<TD><select name='CLICSTATE'>" + CountryRegions.stateLegacyList + "</select></TD>"  //stateListHtmlGet('USA', 0, '')

		        if (MainFlag == 'Edit' && TYPE_Loss == 'BI') {
		            c += "<td id='ClmntARS'>Age Status:</td>"
                    + "<td align='right'>"
                    + "<select name='Auth_Age_Release'>" + Auth_Age_Release_List
		            + "</select></td>"
		        } else {
		            c += "<td><input type='hidden' name='Auth_Age_Release' value=0></td>"
                      + "<td></td>"
		        }
		        if (MainFlag == 'Edit') {
		            c += "<tr>"
		        + "<td> Medicare<br>Beneficiary:</td>"
		        + "<td>"
		        + "<span style='color:blue'>"
		            switch (MEDICARE_BENEFICIARY) {
		                case "N":
		                    c += "No"; break;
		                case "Y":
		                    c += "Yes"; break;
		                default:
		                    c += "Not Queried"
		            }
		            c += "</span>" //+ "<input TYPE='checkbox' name='MEDICARE_BENEFICIARY' Value='' onClick='if(this.checked!=false){parent.MEDICARE_BENEFICIARY=\"Y\"}else{parent.MEDICARE_BENEFICIARY=\"N\"};'>"
		        + "&nbsp;&nbsp; <span id='HICN_L'> HICN:</span><input name='HICN' value='" + HICN + "' maxlength='12' style='width:100px'>"
		        if (LitLog.hasOwnProperty('MedicareId')){
		            c += "<td><span id='MMDetails' style='color:blue;cursor:hand;' onClick='parent. MedicareMedicaidDetails_XTRCT(" + LitLog.MedicareId + "," + LitLog.MedicareReasonId + ");' ><u>Medicare Medicaid Details</u></span></td>"
		           + "<td>RRE:" + RRE + "</td>"
		         }else{
		            c += "<td>RRE:</td>"
		              +  "<td>" + RRE + "</td>"		         
		         } 
		        c+="<td id='ClmntSSRS'>SS# Release<br>Status:</td>"
                + "<td align='right'>"
                + "<select name='Auth_SSN_Release'>" + Auth_SSN_Release_List
		        + "</select></td>"
                + "</tr>"
                }
            } else {
                c += " <input type='hidden' name='CLICENSENO' value=''> <input type='hidden' name='CLICSTATE' value=''>"
                + "<input type='hidden' name='Auth_Age_Release' value=0><input type='hidden' name='HICN' value=''>"
                + "<input type='hidden' name='Auth_SSN_Release' value=''>"  //"C"	Claimant not to be contacted
            }
		}
		c+="<TR><TD>Contact:</TD><TD colspan=3><input type='text' name='CCONTACT' value='" + CContact + "' style='width:99%' maxlength='50'></TD></TR>"
		 + "<TR><TD>EMail:</TD><TD colspan=3><input type='text' name='CEMAIL' value='" + CEMail + "' style='width:99%' maxlength='50'></TD>"
		if (MainFlag == 'Edit' && Unit == 'WC') {  //  && UserRole.indexOf('EditFile') != -1 : INC0114661
		    c += "<td>"
                  + "<img src='Image/mobile.png' alt='Mobile Authorization' height='20px' width='20px' onclick='parent.MobileAuthorization_XTRCT(0)' />&nbsp;&nbsp;"
                  + "<span style='color:blue;cursor:hand;text-decoration:underline;'  onclick='parent.MobileAuthorization_XTRCT(0)'>Mobile Authorization </span>"
                  + "</td>"
		}		
		c += "</TR>"	
		if (n != 'Claimant') {
		    c += ""  
		     + "<input type='hidden' name='txtClmtDOB' value=''>"
		     + "<input type='hidden' name='txtDODEATH' value=''>"
		     + "<input type='hidden' name='cboClmtSex' value=''>"
		     + "<input type='hidden' name='CLanguage' value=''>"
		     + "<input type='hidden' name='CLICENSENO' value=''>"
		     + "<input type='hidden' name='CLICSTATE' value=''>"
		     + "<input type='hidden' name='txtMaidenName' value=''>"
		}
		c+="</TR>"
		+ "</table>"
		if(n!='Claimant'){
		c+="<BR><input TYPE='checkbox' name='chkLossAddress' Value='' onClick='if(this.checked!=false && confirm(\"This will insert Insureds Address\")){parent.LossAddressInsert();}'>Insert Insureds Address</TD>"
		}
        c += "<table width=70% >"
		+ "<tr><td>* Fields marked as <B>Bold</B> are required</td>"
        if(HRdata=='Y'){
            c += "<td aligh=right> <span id='lkHRdata' style='color:blue;cursor:hand;' onClick='parent.LookupWindow_HRdata();' ><u>Demographics Info</u></span></td>"
        }
        c+="</tr>"
		+ "</table>"
		c+="</form></body></html>"
		return(c);
	}



	function Populate_law() {
	    var re = eval('/~' + Unit + '/gi')
	    var lCaption = '';

	    if (Unit == 'WC') {
	        lCaption = 'Claimant`s Rep'   // 'Claimant`s\n\rCounsel'
	    } else {
	        if (re.test(Reserve_LOB_MCodes) == false) {
	            lCaption = 'Plaintiff Rep'  // 'Plaintiff\n\rCounsel'
	        } else {
	            lCaption = 'Insured`s Rep' //for property claims
	        }
	    }


		var l= "<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		+ "<title>law</title></head><body><form name='frmClaim'>"

		+ "<input name = 'cboLawState' type='hidden' id= 'cboLawState' value='" + Lstate + "' />"
		+ "<input name = 'LawStateId'  type='hidden' id= 'LawStateId'  value='" + LstateId + "' />"
		+ "<input name = 'LawCountryId' type='hidden' id= 'LawCountryId' value='" + LCountryId + "' />"

        + "<table style='width:100%' border=0 frame='vsides'>"
        + "<tr><td style='width:65%'>"

		+ "<table id='law' border=0 cellPadding=1 cellSpacing=1  style='width:99%' >"
		+ "<caption><u><b>" + lCaption + "</b></u></caption>"
		//+ "<colgroup  span=1  style='width:60px'><colgroup span=3 style='width:\"*\"'>"
		+ "<colgroup>"
		+ "<col style='width:60px'><col style='width:\"*\"'><col style='width:60px'><col style='width:\"*\"'>"
        + "</colgroup>"
		+ "<tr>"
		+ "<td id='lawType'>Type:</td>"
		+ "<td><select name='cboLawTyp'>"
		+ "<option></option>"
		if(Ltype=='L'){
		l  +="<OPTION value='L' selected>Lawyer"
		}else{
		l  +="	<OPTION value='L'>Lawyer"
		}
		if(Ltype=='P'){
		l  +="<OPTION value='P' selected>Public Adjuster"
		}else{
		l  +="<OPTION value='P'>Public Adjuster"
		}
		if (Ltype == 'G') {
		    l += "<OPTION value='G' selected>Guardian/Conservator"
		} else {
		    l += "<OPTION value='G'>Guardian/Conservator"
		}
		if (Ltype == 'A') {
		    l += "<OPTION value='A' selected>Power of Attorney"
		} else {
		    l += "<OPTION value='A'>Power of Attorney"
		}
		if (Ltype == 'O') {
		    l += "<OPTION value='O' selected>Other"
		} else {
		    l += "<OPTION value='O'>Other"
		}
		l += "</select></td>"
		+ "<td>York<br>notice date:</td>"
		+ "<td><input type='text' name='represent_notice_date'  value='" + MyDateFormat(represent_notice_date, 1) + "' maxlength='10' style='width:60px' onBlur='this.value=parent.MyDate(this.value);if(parent.DateComp(parent.frGeneral.document.frmGeneral.txtAccDT.value,this.value)<0){alert(\"Representation notice date cannot be less then Loss Date\");this.value=\"\";}' </td>"
		+ "</tr>"
		+ "<tr><td id='lawName'>Corporate<br>name:</td>"
		+ "<td colspan=3><input name='txtLawName' value='" + LName + "'  style='width:99%' maxlength='50'></td>"  //style='width:700px' 
		+ "</tr>"

		+ "<tr><td id='lawFLName'>Personal<br>First name:</td>"
		+ "<td><input name='txtLawFName' value='" + LFName + "'  maxlength='25' style='width:99%'></td>"
		+ "<td>Last:</td><td><input name='txtLawLName' value='" + LLName + "'  maxlength='25' style='width:99%'></td></tr>"

		+ "<tr><td rowspan=1 id='lawAddr' rowspan=2>"
		+ "<span id='lkInsAddr' style='color:blue;cursor:hand;' onClick='parent.AddressUpdate_law(\"Legal Rep\");' ><u>Address:</u></span>"
		+ "</TD>"
		+ "<TD colspan=3>"	     
		 + "<div name='AddrViewLine' id='AddrViewLine'>" + trim(Laddr1) + ' ' + trim(Laddr2) + "</div>" 
		
		+ "<input name='txtLawAddress' value='" + Laddr1 + "'  type = hidden maxlength='50'>"
		+ "<input name='txtLawAddress2' value='" + Laddr2 + "'  type = hidden maxlength='50'>"
		+ "</td>"  // style='width:450px'  ></TD>"
		+ "</tr>"

		+ "<tr><td id='lawCity'>City/Town:</td>"
		+ "<td><input name='txtLawCity'  style='width:99%' class=textReadOnly READONLY maxlength='50'   value='" + Lcity + "'></td>"  // style='width:210px'
		+ "<td id='lawState'>State:</td>"
		+ "<td  colspan=1><input name='txtLawState' value='" + LstateDesc + "' style='width:99%' class=textReadOnly READONLY></td>"
		+ "<TR>"
		+ "<td>Zip/PostalCode:</td>"
        + "<td><input name='txtLawZip' value='" + Lzip + "' maxlength='20' class=textReadOnly READONLY></td>"
		+ "<td id='LawCountryL'>Country:</td>"
		+ "<TD colspan=1><input name='txtCountry' value='" + LCountry + "'style='width:99%' class=textReadOnly READONLY></td>"      		
		+ "</tr>"
		+ "<tr>"
		+ "<TD id='lawPhone'>Phone:</TD>"
		+ "<TD><input name='txtLawPhone' value='" + Lphone + "' maxlength='25'  style='width:155px' ></TD>"  //onBlur='if(parent.frInfo.document.frmClaim.txtCountry.value.toUpperCase()==\"UNITED STATES\"){this.value=parent.MyPhone_Dash(this.value,\"txtLawPhone\");}'
		+ "<TD>Fax:</TD>"
		+ "<TD><input name='txtLawFax' value='" + Lfax + "' maxlength='25'  style='width:155px' ></TD>"  //onBlur='if(parent.frInfo.document.frmClaim.txtCountry.value.toUpperCase()==\"UNITED STATES\"){this.value=parent.MyPhone_Dash(this.value,\"txtLawFax\");}'
		+ "</tr>"				
		+ "<tr><td id='lawTAXID'>TaxID:</td>"		
		+ "<td><input name='txtLawTaxID' value='" + LtaxId + "' maxlength='9' onBlur='{document.frmClaim.txtLawTaxID.value=parent.MyTaxID_Dash(parent.frInfo.document.frmClaim.txtLawTaxID.value,\"txtLawTaxID\");}'></td>"
        + "<td>EMail:</td>"
        + "<td colspan=1><input type='text' name='LEMAIL' value='" + LEMail + "' style='width:100%' maxlength='50'></td>"
        + "</tr>"
		+ "<tr><td>Contact:</td><td colspan=3><input type='text' name='LCONTACT' value='" + LContact + "' style='width:100%' maxlength='50'></td>"
		+ "</tr>"			
		+" <tr height=30px><td></td></tr>"
		+ "</table>"

        + "</td><td style='width:35%'>"
		if (DC_Flag != 0) {
		    l += "<table id='defc' cellPadding=1 cellSpacing=1 border=0 style='width:99%' >"  // style='width:602px'
		    l += "<caption><u><b>Defense Counsel</b></u>"
		    if (DC_Id > 0){
		    	l += "&nbsp;&nbsp;&nbsp;<span id='LitLogDetails' style='color:blue;cursor:hand;' onClick='parent.LitLogType5_XTRCT(" + DC_Id + ");' >(<u>View Details</u>)</span>" 
		    }
		    l += "</caption>"
		    + "<TR><TD>"
			+ "<table cellPadding=1 cellSpacing=1 border=0 >"  //style='width:600px'
			+ "<tr><td></B><U>" + DC_FirmName + "</U></B></td></tr>"
			+ "<tr><td>" + DC_Street1 + "</td></tr></table>"
			+ "<table  cellPadding=1 cellSpacing=1 border=0 >"  // style='width:300px'
			+ "<tr><td>" + DC_City + "</td><td>" + DC_State + "</td><td>" + DC_Zip + "</td></tr>"
            + "</table>"
			+ "<br><table  cellPadding=1 cellSpacing=1 border=0 >"  //style='width:600px'
			+ "<tr><td>Contact:</td><td>" + DC_NewContact + "</td></tr></table>"
			+ "<table  cellPadding=1 cellSpacing=1 border=0 >"  // style='width:500px'
			+ "<tr><td  style='width:60px'>Phone:</td><td>" + DC_Phone + "</td><td  style='width:60px'>Fax:</td><td>" + DC_Fax + "</td></tr>"
            + "</table>"
			    + "<table  cellPadding=1 cellSpacing=1 border=0 style='width:260px'>"
			    + "<tr><td>Suit Received:</td><td  style='width:80px'>" + DC_RSVDate + "</td></tr>"
			    + "<tr><td>Assigned:</td><td  style='width:80px'>" + DC_AssignDate + "</td></tr>"
			    + "<tr><td>Apparent Service:</td><td  style='width:80px'>" + DC_ServiceDat + "</td></tr>"
			    + "<tr><td>Stip Date:</td><td  style='width:80px'>" + DC_StipDate + "</td></tr>"
			    + "</table>"
			+ "</TD></TR>"
			+ "<tr><td>"
			+ "<textarea cols=70 rows=4 readonly  name=Notes>" + DC_Notes + "</textarea>"
			+ "</td></tr>"
			+ "<tr height=30px><td align=right>"
			+ "</td></tr></table>"
		}
		l +="</td></tr>"
          + "</table>"

		l  += "</form></body></html>"

		return(l);
	}
	
	function Populate_doc(){
	    var f = "<html><head>"
	    +"<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 + "<style>HTML,BODY {height:99%; width:100%; margin:0 0 0 0;padding:0 0 0 0; }</style>"
		 + "<title>doc</title></head><body>"
         + "<iframe id='ifrDoctors' frameborder=0 style='overflow:auto;margin:0 0 0 0;padding:0 0 0 0;' scrolling=auto style='width:100%;height:99%' src='/portaL/Net/Doctors/ClaimDoctors.aspx?EmployeeNumber=" + EmpNum + "&clntno=" + ClientNumber + "&doi=" + MyDateFormat(AccDate, 1) + "&LOB=" + Unit + "&claimMpnId=" + ClmOriginal.Doctors.claimMpnId
	    f += "&Claim_ID="
	    f += (claim_id == 0) ? ClmOriginal.Doctors.doctorId : claim_id
	    f += "'></iframe>"
          + "</body></html>"
	    return (f);      //BlankPage.htm
	}

	function Populate_vehicle() {
	    var f = "<html><head>"
	    + "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 + "<style>HTML,BODY {height:99%; width:100%; margin:0 0 0 0;padding:0 0 0 0; }</style>"
		 + "<title>vehicle</title></head><body>"
         + "<iframe id='ifrVehicles' frameborder=0 style='overflow:auto;margin:0 0 0 0;padding:0 0 0 0;' scrolling=auto style='width:100%;height:99%'></iframe>"
         + "</body></html>"
	    return (f);      //BlankPage.htm
	}	


//Injury-NON_WORKCOMP
	function Populate_injury() {
	    var jurStatesCodes = Populate_polCCstates();
	    jurStatesCodes = (jurStatesCodes == '') ? CountryRegions.stateLegacyList : jurStatesCodes; // stateListHtmlGet('USA', 0, '')
	    
	    var cl = "<html><head>"
		 + "<link  rel='stylesheet' href='projectSS.css' type='text/css'>"
		 + "<title>injury</title></head><body><form name='frmClaim'>"
	     + "<table border=0 cellPadding=1 cellSpacing=1 width=80%>"
	     + "<colgroup  span=1  style='width:100px'><colgroup span=5 style='width:\"*\"'>"
	    //0
		    + "<tr>"
		        + "<td id='JurSt'> Jurisdiction State:</td>"
			    + "<td colspan=2><select name='cboWC_JuriState' onChange='parent.JurState=this.value;parent.InjuryFlag=1;'>" + jurStatesCodes + "</select></td>"

			+ "<td align=right>"
	    if (MainFlag == 'Edit') {
	        cl += "<A href='javascript:parent.Var_Save_WorkComp();parent.Variable_Populate(\"workcomp\");parent.Settlement_Get.count=0;parent.Settlement_Get(\"injury\")' title='Review Settlement-related information'>Settlement:</A>"
	    } else { 
	        cl += "Settlement:"
	    }
	        cl += "</td>"
			    + "<td>"
			    + "<input TYPE='radio' Name='Settlement' value='Y' onClick='parent.Settlement=\"Y\"' "
	        cl += (SettlList.length > 0) ? " checked " : ""
	        cl += ">YES"
	        cl += "<input TYPE='radio' Name='Settlement' value='N' onClick='parent.Settlement=\"N\"'"
	        cl += (SettlList.length == 0) ? " checked" : ""
	        cl += ">NO"
	        cl += "</td>"	
	        		    
	           + "<td id='CounL_CatL' align=right>CAT:<input name='cboCatCode'  value='" + Corp + "' style='width:45px' maxlength='4' onBlur='{this.value=parent.MyNumber(this.value);}'></td>"
	        + "</tr>"
	    //1
			+ "<tr>"
			    + "<td align=right   align=right>Injury Desc:</td>"
				+ "<td id='Inju' colspan=2><input Name='txtInjuries' value='" + Injuries + "' maxlength='50' style='width:340px'></td>"
				// removed as per Jim R: field Minor  
				// hidden field left for backward compatib.
				+ "<td id='Minr' align = right>Fatal?</td>  <input type='hidden' value='' name='cboMinor'>" 
			    + "<td id='MinrFl'>"
				        + "<select name='cboFatal'>"
			          if(Fatal=='N' || Fatal=='No'){ 	
			        cl +=" <option value='N' selected>No</option><option value='Y'>Yes</option>"
			          } else if( Fatal=='Y' || Fatal=='Yes'){ 
			        cl +=" <option value='Y' selected>Yes</option><option value='N'>No</option>"
			          }else{ 
			        cl +=" <option></option><option value='Y'>Yes</option><option value='N'>No</option>"
			           }
			           cl += "</select></td>"
			   + "<td colspan=2 id='PDRate' align=right>PD Rating:<input type='text' value='" + PDRating + "' name='PDRating' style='width:35px' maxlength='5' onBlur='if(this.value!=\"\" && isNaN(this.value)){alert(\"PD Rating must be numeric value\");this.value=\"\";}; if(this.value!=\"\" && parseFloat(this.value)>100){alert(\"PD Rating must be in the range 0-100\");this.value=\"\";};if(this.value!=\"\" && parseFloat(this.value)<0){alert(\"PD Rating must be in the range 0-100\");this.value=\"\";};parent.Populate_Severity(this.value);'></td>"
			 + "</tr>"
			 
			 + "<tr>"
				+ "<td id='InjuBodyl'  align=right>Anatomy:</td>"
				+ "<td id='InjuBodyv' align=left colspan=2>"
			        cl += "<select name='cboAnatomy'"
			        cl += (BodyPart.indexOf("<option value=0></option>") > -1) ? "" : " onClick='parent.BodyPart_XTRCT(\"\",0,parent.JurState,parent.Unit)' "
			        cl += ">"
			        if (BodyPartCod == '') {
			            cl += (BodyPart == '') ? BodyPart_XTRCT(ClaimNumber, 0, JurState, Unit) : BodyPart
			        } else {  // if body part is known  - pull list without a claim #
			            cl += (BodyPart == '' || BodyPart == "<option value=0>NOT SPECIFIED</option>") ? BodyPart_XTRCT('', 0, JurState, Unit) : BodyPart
			        }
			        cl += "</SELECT>"
			+ "</td>"
			        if (MainFlag == 'Edit') {
			            cl += "<td>"
			                    + "<span style='color:blue;cursor:hand' onClick='parent.WC_Diagnoses_XTRCT()'>Diagnoses:</span>"
			                    + "<span id=lblNoDiagnosis>" + DiagNo + "</span>"
			                + "</td>"			            
			                + "<td colspan=2 align=right>"
			                    + "<span style='color:blue;cursor:hand' onClick='parent.WC_MultipleBodyParts_XTRCT()'>List of Injured Body Parts:</span>"
			                    + "<span id=MultBodyParts>" + MultipleBodyParts + "</span>"
			                + "</td>"
			        } else {
			            cl += "<td colspan=3></td>"
			        }
			cl += "</tr>"


			cl += "<tr>"

			cl += "<td align=right>Reported to Employer:"
			cl += "</td>"
			    + "<td colspan=2><input style='width:60px' name='txtWC_D_Report' value='" + MyDateFormat(DateReport) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_ReportedToEmployer_date(this.value);}'>"
			    + "</td>"

	           + "<td  align=right>Pre-Existing Disability:"
	           + "</td>"
	           + "<td  colspan=2><select size='1' name='cboPreExistDisability'>"
	            + "<option value='U'>Unknown"
			    + "<option value='Y'>Yes"
			    + "<option value='N'>No"
			    + "</select></td>"
	        + "</tr>"

    //2
			+ "<tr>"
	        + "<td   colspan=6>NCCI codes:</TD>"
	        + "</tr>"
	        
			 + "<tr>"
			    + "<td  align=right id='InjSc'  style='width: 85px'>Cause of Injury:</td>"
			    + "<td colspan=2><select name='cboNCCI_SOI'"
			         cl += (NCCI_Source_Of_Injury_List.indexOf("<option value=0></option>") > -1) ? "" : " onClick='parent.WC_Source_Of_Injury_XTRCT(\"\", parent.JurState, parent.NCCI_SOI)' "
			         cl += ">"
			 	            if (NCCI_SOI == '') {
			 	                cl += (NCCI_Source_Of_Injury_List == '') ? "<option value=0>NOT SPECIFIED</option>" : NCCI_Source_Of_Injury_List
			 	            } else {
			         cl += (NCCI_Source_Of_Injury_List == '') ? WC_Source_Of_Injury_XTRCT("", "", NCCI_SOI) : NCCI_Source_Of_Injury_List
			 	            }
			         cl += "</select></td>"
			    + "<td align=right id='InjNa'>Nature Of Injury:</TD>"
			    + "<td colspan=2><select name='cboNCCI_NOI' "
			         cl += (NCCI_Nature_Of_Injury_List.indexOf("<option value=0></option>") > -1) ? "" : " onClick='parent.WC_Nature_Of_Injury_XTRCT(\"\", parent.JurState, parent.NCCI_NOI)' "
			         cl += ">"
			 	            if (NCCI_NOI == '') {
			 	                cl += (NCCI_Nature_Of_Injury_List == '') ? "<option value=0>NOT SPECIFIED</option>" : NCCI_Nature_Of_Injury_List
			 	            } else {
			         cl += (NCCI_Nature_Of_Injury_List == '') ? WC_Nature_Of_Injury_XTRCT("", "", NCCI_NOI) : NCCI_Nature_Of_Injury_List
			 	            }
			         cl += "</select></td>"
			 + "</tr>"
			         //3

		 + "</table>"
     	 + "</form></body></html>"
	    return (cl);
	}
	
//Workcomp:
	function Populate_workcomp() {
	    var jurStatesCodes = Populate_polCCstates();
	    jurStatesCodes = (jurStatesCodes == '') ? CountryRegions.stateLegacyList : jurStatesCodes;  //stateListHtmlGet('USA', 0, '')
	    
		var w="<html><head>"
		 + "<link  rel='stylesheet' href='projectSS.css' type='text/css'>"
		 + "<Title>workcomp</Title></head><body><Form name='frmClaim'>"
 //0
		w+="<table id='workcomp' border=0 cellPadding=0 cellSpacing=0 width=100%>"
		    + "<TR>"
		    + "<TD id='JurSt'><B>Jurisdiction State:</B></TD>"
		    + "<TD><select name='cboWC_JuriState' onChange='parent.JurState=this.value;'>" + jurStatesCodes + "</SELECT></TD>"	
			+ "<TD id='MarSt'><B>Marital Status:</B></TD>"
			+ "<TD><select  name='cboWC_MStatus'>"  
			+ " <option value=''></option>"
			w += (Marital=='1') ? "<option value='1' selected>Single" : "<option value='1'>Single, Divorced, Widowed"
			w += (Marital=='2') ? "<option value='2' selected>Married" : "<option value='2'>Married"
			w += (Marital=='3') ? "<option value='3' selected>Separated" : "<option value='3'>Separated"
			w += (Marital=='4' || Marital=='') ? "<OPTION value='4' selected>Unknown" : "<OPTION value='4'>Unknown"
			w +="</SELECT></TD>"
		   + "<TD id='DepNo'><B>Dependents #:</B></TD>"
		   + "<TD><input style='width:40px' name='txtWC_DependNo' value='" + Dependents + "' maxlength='2' onBlur='{document.frmClaim.txtWC_DependNo.value=parent.WC_DependNo_Number(parent.frInfo.document.frmClaim.txtWC_DependNo.value);}'></TD>"
		   	+ "<TD>Employee ID:</TD>"
			+ "<TD><input type='text' name='employee_id' value='" + employee_id + "' maxlength='20'  style='width:100px'></TD>"
    	   + "</TR>"
		   + "<TR>"
//1
		    + "<TD id='EmpSt'><B>Employment State:</B></TD>"
		    + "<TD><select name='cboWC_EmplState'>" + CountryRegions.stateLegacyList + "</SELECT></TD>"  // stateListHtmlGet('USA', 0, '')	
		   	+ "<TD>Employee Status:</TD>"
			+ "<TD><select name='cboEmplStatus'"
			w += (EmplStatus.indexOf("<option value=''></option>")>-1) ?  "" : " onClick='parent.WC_EmplStatus_XTRCT(\"\")' "
			w +=">"
			w += (EmplStatus=='') ? WC_EmplStatus_XTRCT(ClaimNumber) : EmplStatus
			w +="</SELECT></TD>"
			+ "<TD>Job Title:</TD>" 
			+ "<TD><input type='text' name='txtJobTitle' value='" +  JobTitle + "' maxlength='50'  style='width:170px'></TD>"
		   	+ "<TD>Dept where injury occured:</TD>"
		   	+ "<TD>"
		    + "<input name='injury_dept' value='" + injury_dept + "' maxlength='50' style='width:200px'>"
			+ "</TD>"
		    + "</TR>"
//2
		    + "<TR>"
		    + "<TD id='JobCd'><B>Client Job Code:</B></TD>"	//Job 	 		 
			+ "<TD><select name='cboWC_JobCode'"			
			w+= (JobCode.indexOf("<option value=''></option>")>-1) ? "" : " onClick='parent.WC_JobCode_XTRCT(\"\",parent.ClientNumber, parent.JurState)' "
			w+=">"
			w+= (JobCode=='') ? WC_JobCode_XTRCT(ClaimNumber,ClientNumber,JurState) : JobCode
			w += "</select></TD>"
			 + "<TD id='lblOccup'>State/NCCI Occup Code:</TD>"
			 + "<TD><select name='cboWC_Occup' style='width:150px' title='" + SingleQuote(OccupDescr) + "' "
			 + " onchange ='if(this.value==\"GET ANOTHER\"){parent.LookupWindow_StateOccupCodes()}'>"
						 + "<option value='GET ANOTHER'>GET ANOTHER</option>"
			w += "<option   value='" + OccupID + "'  selected>" + OccupDescr + "</option>"
			w += "</select></TD>"
		     + "<TD id='lblAWW'>Av. weekly wage:"  
             + "&nbsp;<span style='color:blue;cursor:hand' id='spnWageCalculate'></span>"
             + "</TD>"
		     + "<TD><input style='width:70px' name='txtWC_WeeklyWage' value='" + WeekWage + "' maxlength='9' onBlur='{this.value=parent.format(parent.MyAmount(parent.frInfo.document.frmClaim.txtWC_WeeklyWage.value),2);}';>"
             + "&nbsp; <span id=AWWhist> </span>"
             + "</TD>"
		     + "<TD id='WageM'>Wage Method:</TD>"
			 +"<TD><select  name='cboWage_Method' value=''>"  
			 +" <option value=0></option>"
			w += (wage_method=='1') ? "<option value='1' selected>Actual" : "<option value='1'>Actual"
			w += (wage_method=='2') ? "<option value='2' selected>Estimated" : "<option value='2'>Estimated"
			w += (wage_method=='3') ? "<option value='3' selected>Min weekly" : "<option value='3'>Min weekly"
			w += (wage_method=='4') ? "<OPTION value='4' selected>Max weekly" : "<OPTION value='4'>Max weekly"
			w +="</SELECT></TD>"
		      +"</TR>"
		+ "</table>"
		
		+ "<br>"
		
		 + "<table id='workcomp' border=0 cellPadding=0 cellSpacing=0 width=90%>"
//1
		  + "<TR>"
		  + "<TD align=right>RATES:</TD>"
			+ "<TD align=right>Temp. Total:</TD>"
			+ "<TD><input style='width:60px' name='txtWC_TTRate' value='" + TempTotalRATE + "' maxlength='7' onBlur='{this.value=parent.format(parent.MyAmount(this.value),2);}'></TD>"
		    + "<TD align=right>Temp. Partial:</TD>"
		    + "<TD><input style='width:60px' name='txtWC_TPRate' value='" + TempPartialRATE + "' maxlength='7'  onBlur='{this.value=parent.format(parent.MyAmount(this.value),2);}'></TD>"
		    + "<TD align=right>Perm. Dis:</TD>"
		    + "<TD><input style='width:60px' name='txtWC_PTRate' value='" + PermTotalRATE + "' maxlength='7' onBlur='{this.value=parent.format(parent.MyAmount(this.value),2);}'></TD>"
		    + "<TD align=right>Voc Rehab:</TD>"
		    + "<TD><input style='width:60px' name='txtWC_VRRate' value='" + VocRehabRATE + "' maxlength='7' onBlur='{this.value=parent.format(parent.MyAmount(this.value),2);}'></TD>"
		    + "<TD></TD>"
		  + "</TD><TR>"		
		  + "<TR>"
		  + "<TD rowspan=2 align=right>DATES:</TD>"
			+ "<TD id='DHire' align=right><B>Hire:</B></TD>"	
			+ "<TD><input style='width:60px' name='txtWC_Hire' value='" + MyDateFormat(DateOfHire) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_hire_date(this.value);}'></TD>"
			+ "<TD align=right>Start Job:</TD>"	
			+ "<TD><input style='width:60px' name='txtStartJobDate' value='" + MyDateFormat(StartJobDate) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this);  this.value=parent.val_StartJob_date(this.value);}'></TD>"
			+ "<TD align=right id='lblLstWrkd'>Initial Last Worked:</TD>"
			+ "<TD><input style='width:60px' name='txtWC_LastWorkDay' value='" + MyDateFormat(LastWorkDay) + "' maxlength='10' title='The last date worked prior to initial disability entitlement' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_LastWorkDay_date(this.value);}'></TD>"
		    + "<TD align=right id='lblCrntLstWrkd'>Current Last Worked:</TD>"
		    + "<TD><input style='width:60px' name='txtWC_CurrentLastWorkDay' value='" + MyDateFormat(CurrentLastWorkDay) + "' maxlength='10' title='The last date worked prior to the first day of disability for a period subsequent to the first period of disability' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_CurrentLastWorkDay_date(this.value);}'></TD>"
		    + "<TR>"
//3
		  + "<TR>"
		    + "<TD> </TD>"
			+ "<TD align=right>First Lost Time:</TD>"		 
			+ "<TD><input  style='width:60px' name='txtWC_FirstLostTimeDay' value='" + MyDateFormat(FirstLostTimeDay) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_FirstLostTime_date(this.value);}'></TD>"
			+ "<TD align=right id='lblInitDiab'>Initial Disability:</TD>"		 		 
			+ "<TD><input  style='width:60px' name='txtWC_D_InitDisab' value='" + MyDateFormat(DateInitia) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_InitialDisability_date(this.value);}'></TD>"
			+ "<TD align=right id='EmlrD'><B>Reported to Employer:</B></TD>"		 
			+ "<TD><input style='width:60px' name='txtWC_D_Report' value='" + MyDateFormat(DateReport) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_ReportedToEmployer_date(this.value);}'></TD>"
		    + "<TD align=right id='lblTermDt'>Job Termination:</TD>"
		    + "<TD><input style='width:60px' name='txtTerminateJobDate' value='" + MyDateFormat(TerminateJobDate) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_JobTermination_date(this.value);}'></TD>"
		    + "<TR>"
	+ "</table>"
//4
	if(MainFlag=='Edit'){
	w+="<br>"
	+ "<table><tr>"
		 + "<td colspan=2 align=right>"		 
		 + "<A href='javascript:parent.Var_Save_WorkComp();parent.Variable_Populate(\"workcomp\");parent.WC_Employer_Get(\"VIEW\",parent.Employer,parent.EmployerNo,\"\",\"\");' title='Review Employer`s Info'>EMPLOYER IS THE CLIENT:</A>"
		 + "</td>"
		 + "<td id='lblWC_Employer'>" 		 
		 + "<input TYPE='radio' Name='WC_Employer' value='C' onClick='parent.Employer=\"C\";parent.EmployerNo=parent.ClientNumber;parent.WC_Employer_Get(\"SAVE\",parent.Employer,parent.EmployerNo,\"\",\"\")'>YES"		 
	     + "<input TYPE='radio' Name='WC_Employer' value='' onClick='parent.Var_Save_WorkComp();parent.Variable_Populate(\"workcomp\");parent.WC_Employer_Get(\"VIEW\",parent.Employer,parent.EmployerNo,\"\",\"\");'>NO"
		 + "</td>"
		 +"</tr>"	
	+ "</table>"
	}
	w += "<table>"
		    + "<tr>"
		    + "<td>Business Owner:</td>"
		    + "<td align=right>"
		    + "<input type='checkbox' name='business_owner' onClick='parent.business_owner=(this.checked) ? \"Y\" : \"N\";'"
	    w += (business_owner == 'Y') ? " checked " : ""
	    w += ">"
		    + "</td>"
		    + "<td align=right>Safety Provided:"
		    + "<input type='checkbox' name='safety_provided' onClick='parent.safety_provided=(this.checked) ? \"Y\" : \"N\";'"
	    w += (safety_provided == 'Y') ? " checked " : ""
	    w += "></td>"
		    + "<td align=right>Safety Used:"
		    + "<input type='checkbox' name='safety_used' onClick='parent.safety_used=(this.checked) ? \"Y\" : \"N\";'"
	    w += (safety_used == 'Y') ? " checked " : ""
	    w += "></td>"
		    + "<td align=right>Drug Test:"
		    + "<input type='checkbox' name='drug_test' onClick='parent.drug_test=(this.checked) ? \"Y\" : \"N\";'"
	    w += (drug_test == 'Y') ? " checked " : ""
	    w += "></td>"

		+ "<td align='right' style='width:190px'>"
	    w += "Concurrent Employment:<input type='checkbox' name='WC_ConcurEmpl'"
	    w += (ConcurEMPLMNT == 'Y') ? " checked " : ""
	    w += " onClick='parent.ConcurEMPLMNT=(this.checked)?\"Y\":\"N\";if(this.checked){parent.frInfo.document.getElementById(\"CCE\").innerHTML=parent.Populate__workcomp_CCE()}else{parent.frInfo.document.getElementById(\"CCE\").innerHTML=\"\"};'>"
		    + "</td>"
	    w += "<td id='CCE'>"
	    if (ConcurEMPLMNT == 'Y') {
	        w += Populate__workcomp_CCE();
	    }
	    w += "</td>"
	    
    + "</tr></table>"

	+ "<br>"
	
	+ "<table width=100%>"
		+ "<tr>"
		+ "<td>EMPLOYEE SCHEDULE:</td>"
		
		+"<td align=center>Irregular schedule:"
		+"<input type='checkbox' name='chkIrrSchedule' value='' onClick='parent.irregular=(this.checked) ? \"Y\" : \"N\";' "
	   w +=  (irregular=='Y') ? " checked "  : ""
	   w += "></td>"
		
		+ "<td align=right>Began work</td>"
		+ "<td><input type='text' name='time_began_work' value='" + time_began_work + "' style='width:32px' maxlength='5' onBlur='{this.value=parent.MyTime(this.value);}'>"
		+ "<select size='1' name='began_am_pm'>"
		+ "<option value=''>" 
		+ "<option value='A'>AM"
		+ "<option value='P'>PM"
		+ "</select></td>"

		+ "<TD align=right>Weekly Hours:</TD>"
		+ "<TD><input style='width:40px' name='txtWC_Hours' value='" + WeeklyHours + "' maxlength='4' onBlur='{this.value=parent.val_weekly_hours(this.value);}'>"
        + "<span id='WeekHours_hist'></span>"
        + "</TD>"
		+ "<td>Hours:</td>"
		+ "<td colspan=3>Sun:<input type='text' value='" + hours_sun + "' name='hours_sun' style='width:29px' maxlength='4' onBlur='{this.value=parent.val_daily_hours(this.value);parent.hours_sun= this.value;parent.frInfo.document.frmClaim.txtWC_Hours.value=parent.format(parent.weekly_hours_calculate(),2);}'>"
		+ "&nbsp;Mon:<input type='text' value='" + hours_mon + "' name='hours_mon' style='width:29px' maxlength='4' onBlur='{this.value=parent.val_daily_hours(this.value);parent.hours_mon=this.value;parent.frInfo.document.frmClaim.txtWC_Hours.value=parent.format(parent.weekly_hours_calculate(),2);}'>"
		+ "&nbsp;Tue:<input type='text' value='" + hours_tue + "' name='hours_tue' style='width:29px' maxlength='4' onBlur='{this.value=parent.val_daily_hours(this.value);parent.hours_tue=this.value;parent.frInfo.document.frmClaim.txtWC_Hours.value=parent.format(parent.weekly_hours_calculate(),2);}'>"
		+ "&nbsp;Wed:<input type='text' value='" + hours_wed + "' name='hours_wed' style='width:29px' maxlength='4' onBlur='{this.value=parent.val_daily_hours(this.value);parent.hours_wed=this.value;parent.frInfo.document.frmClaim.txtWC_Hours.value=parent.format(parent.weekly_hours_calculate(),2);}'>"
		+ "&nbsp;Thu:<input type='text' value='" + hours_thu + "' name='hours_thu' style='width:29px' maxlength='4' onBlur='{this.value=parent.val_daily_hours(this.value);parent.hours_thu=this.value;parent.frInfo.document.frmClaim.txtWC_Hours.value=parent.format(parent.weekly_hours_calculate(),2);}'>"
		+ "&nbsp;Fri:<input type='text' value='" + hours_fri + "' name='hours_fri' style='width:29px' maxlength='4' onBlur='{this.value=parent.val_daily_hours(this.value);parent.hours_fri=this.value;parent.frInfo.document.frmClaim.txtWC_Hours.value=parent.format(parent.weekly_hours_calculate(),2);}'>"
		+ "&nbsp;Sat:<input type='text' value='" + hours_sat + "' name='hours_sat' style='width:29px' maxlength='4' onBlur='{this.value=parent.val_daily_hours(this.value);parent.hours_sat=this.value;parent.frInfo.document.frmClaim.txtWC_Hours.value=parent.format(parent.weekly_hours_calculate(),2);}'></td>"
		+ "</tr>"
		+ "<tr>"
		+ "<td colspan=10>Employee Duty Description:<input name='EmplDutyDescr' value='" + EmplDutyDescr + "' type='text' style='width:85%' value='' maxlength=100></td>"
		+ "</tr>"
		+ "</table>"
		

		+ "<table>"		
		+"<tr>"
		+ "<td>Social Security Ben. Offset:"
		+ "<input type='checkbox' name='chk_social_security_offset' onClick='parent.social_security_offset=(this.checked) ? \"Y\" : \"N\";'"
		w+= (social_security_offset=='Y') ? " checked " : ""
		w+= "></td>"
		
		+ "<td>Unemployment Ben. Offset:"
		+ "<input type='checkbox' name='chk_unemployment_offset' onClick='parent.unemployment_offset=(this.checked) ? \"Y\" : \"N\";'"
		w+= (unemployment_offset=='Y') ? " checked " : ""
		w+= "></td>"
		
		+ "<td>Pension Plan Ben. Offset:"
		+ "<input type='checkbox' name='chk_pension_plan_offset' onClick='parent.pension_plan_offset=(this.checked) ? \"Y\" : \"N\";'"
		w+= (pension_plan_offset=='Y') ? " checked " : ""
		w+= "></td>"
		
		+ "<td>Special Fund Ben. Offset:"
		+ "<input type='checkbox' name='chk_special_fund_offset' onClick='parent.special_fund_offset=(this.checked) ? \"Y\" : \"N\";'"
		w+= (special_fund_offset=='Y') ? " checked " : ""
		w+= "></td>"	
				
		+ "<td>Other Benefit Offset:"
		+ "<input type='checkbox' name='chk_other_benefit_offset' onClick='parent.other_benefit_offset=(this.checked) ? \"Y\" : \"N\";'"
		w+= (other_benefit_offset=='Y') ? " checked " : ""
		w+= "></td>"	
		+"</tr>"
		+ "</table>"

		+ "<br>"		
		+ "<table>"
		+ "<tr><td>* Fields marked as <B>Bold</B> are required</td></tr>"
		+ "</table>"
     	+"</form></body></html>"		 
		 return(w);
	}

function Populate_workcomp_ca(){   		    		    
 var c=""
	+"<html>"
	+"<head>"
	+"<title>workcomp_ca</title>"
	+ "<link   rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
	+"</head>"
	+"<body><form name='frmClaim'>"		
	+"<table border='0' width='100%'>"
	+"<tr>"	
	+"<td width='100%'>"		
		+"<table border='0' width='100%'>"
		+ "<tr>"
			+"<td align=right>CLAIM FORM DATES:</td>"
			+"<td align=right>Provided to Employee:</td>"
			+"<td><input type='text' name='FormProvideDate' value='" + MyDateFormat(FormProvideDate) + "'  style='width:60px' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_clmform_provide_date(this.value);}'></td>"
			+"<td align=right>Received by Employer:</td>"
			+"<td><input type='text' name='FormReceiveDate' value='" + MyDateFormat(FormReceiveDate) + "'  style='width:60px' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_clmform_receive_date(this.value);}'></td>"

			+ "<td align=right>Received by TPA:</td>"
			+ "<td><input type='text' name='FormReceiveDateTPA' value='" + MyDateFormat(FormReceiveDateTPA) + "'  style='width:60px' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_clmform_receive_date(this.value);}'></td>"

             c += "<td colspan=4 align=center id='RTW'>"
             if (RTWSameEmpl == 'Y') {
                 c += Populate__workcomp_RTW(true);
             } else {
                 c += Populate__workcomp_RTW(false);
             }
             c += "</td>"

		+"</tr>"		
		+"<tr>" 
			+"<td align=right>PROGRESS DATES:</td>"		
			+ "<TD align=right>First Treatment:</TD>"
			+ "<TD><input style='width:60px' name='txtWC_D_FTreat' value='" + MyDateFormat(FirstTREAT) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_FirstTreatment_date(this.value);};parent.FirstTREAT=this.value;'></TD>"
			+ "<TD align=right>Max.Med. Improvement:</TD>"
			+ "<TD><input style='width:60px' name='txtWC_DateMaxMedImpr' value='" + DateMaxMedImpr + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_DateMaxMedImpr_date(this.value);parent.DateMaxMedImpr=this.value;}'></TD>"


            + "<td align=right id='lblRlsdRTW'>Released to RTW:</td>"
            + "<td><input style='width:60px' name='txtReleasedRTW' value='" + MyDateFormat(ReleasedRTW) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_ReleasedRTW_date(this.value);};parent.ReleasedRTW=this.value;'></td>"
		

			+ "<td align=right id='lblInRTW'>Initial RTW:</td>"
			+ "<td><input style='width:60px' name='txtWC_D_Returned' value='" + MyDateFormat(DateReturn) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_Returned_date(this.value);};parent.DateReturn=this.value;'></td>"
		
			+ "<TD align=right id='lblCuRTW'>Current RTW:</TD>"
			+ "<TD><input style='width:60px' name='txtWC_D_Returned_current' value='" + MyDateFormat(datereturn_current) + "' maxlength='10' onBlur='{this.value=parent._DOB_Date(this.value,this); this.value=parent.val_Returned_date_current(this.value);};parent.datereturn_current=this.value;'></TD>"
		+ "</tr>" 	
		+ "<tr>"
             c += "<td align=right>"
             c += "</td>"
			+ "<td colspan=2 align=right>"
			if(MainFlag=='Edit'){
			c += "<A href='javascript:parent.ModifiedDuty_Get(1,\"\",\"\",\"\",\"\",1)' title='Review history of modified duties'>Modified Duty:</A>"
			}else{
			c +="Modified Duty:"
			}
			c += "</TD>"
			+ "<td>"
			+ "<input TYPE='radio' Name='WC_MoDuty' value='Y' onClick='parent.MODIFIEDUTY=\"Y\"'>YES"
			+ "<input TYPE='radio' Name='WC_MoDuty' value='N' onClick='parent.MODIFIEDUTY=\"N\"'>NO"
			+ "</td>"
			+ "<td colspan=2 align=right>"
			if(MainFlag=='Edit'){
			c += "<A href='javascript:parent.ModifiedDuty_Get(0,\"\",\"\",\"\",\"\",0)' title='Review lost time adjustments'>Lost Time Adjustments:</A>"
			}else{
			c +="Lost Time Adjustments:"
			}
			c += "</td>"
			+ "<td>"
			+ "<input TYPE='radio' Name='WC_LostTime' value='Y' onClick='parent.LOSTTIME=\"Y\"'>YES"
			+ "<input TYPE='radio' Name='WC_LostTime' value='N' onClick='parent.LOSTTIME=\"N\"'>NO"
			+ "</td>"			
			+ "<td align=right>"
			if(MainFlag=='Edit'){
			c += "<a href='javascript:parent.Var_Save_WorkComp();parent.Variable_Populate(\"workcomp\");parent.OSHA_Get()' title='Review OSHA-related information'>OSHA Info:</a>"
			}else{
			c +="OSHA Info:"
			}
			c +="</td>"		
			+ "<td>"
			+ "<input TYPE='radio' Name='WC_OSHA' value='Y' onClick='parent.OSHA=\"Y\"'>YES"
			+ "<input TYPE='radio' Name='WC_OSHA' value='N' onClick='parent.OSHA=\"N\"'>NO"
			+ "</td>"
			+ "<td align=right>"
			if (MainFlag == 'Edit') {
			    c += "<A href='javascript:parent.Var_Save_WorkComp();parent.Variable_Populate(\"workcomp\");parent.Settlement_Get.count=0;parent.Settlement_Get(\"workcomp_ca\")' title='Review Settlement-related information'>Settlement:</A>"
			} else { 
			 c +="Settlement:"
			}
			c += "</td>"
			+ "<td>"
			+ "<input TYPE='radio' Name='Settlement' value='Y' onClick='parent.Settlement=\"Y\"' "
			c += (SettlList.length > 0) ? " checked " : "" 
			c+= ">YES"
			c += "<input TYPE='radio' Name='Settlement' value='N' onClick='parent.Settlement=\"N\"'"
			c+= (SettlList.length == 0) ? " checked" : "" 
			c+= ">NO"
			c+= "</td>"
		+ "</tr>"		 
		+ "<tr>"
		    +"<td></td>"
			+"<td align=right>Pre-Existing Disability:</td>"
			+"<td><select size='1' name='cboPreExistDisability'>"
			    +"<option value='Y'>Yes"
			    +"<option value='N'>No"
			    +"<option value='U'>Unknown"
			    +"</select></td>"		
			+"<td align=right>AOE / COE:</td>"
			+"<td><select name='cboADE_CDE'>"
			    +"<option value='Y'>Yes"
			    +"<option value='N'>No"
			    +"<option value='U'>Unknown"
			    +"</select></td>"
			+"<td colspan=2 align=right>Injury on Empl. Premises:</td>"
			+"<td><select name='cboWC_InjOnEmplP' onchange='parent.InjOnEmplP=this.value; if(this.value==\"Y\" && parent.frInfo.document.frmClaim.txtEmplZip.value==\"\"){parent.frInfo.document.frmClaim.txtEmplZip.value=parent.set_loss_zip();}'>"
				+"<option value='Y'>Yes"
				+"<option value='N'>No"
				+"<option value='U'>Unknown"
				+"</select>"
			+ "</td>"
			+ "<td colspan=2 align=right id='LossSiteZip'><B>Loss Site Zip:</B></td>"
			+ "<td><input  style='width:70px' name='txtEmplZip' value='" + EmplZip + "' maxlength='10' onBlur='if(parent.ValZip(this.value,\"CA\")==false){alert(\"US Zip code should be entered in the format ##### or #####-####\");this.value=\"\";this.focus();}'></TD>"
		+"</tr>"
		+ "<tr>"
			+ "<td></td>"
			 + "<td align=right id='MrLTy'>Minor Loss Type:</td>"
			 + "<td colspan=2><select name='cboMinor_LossType'>"
			c += WC_Minor_LossType_XTRCT(ClaimNumber, CovCod, Minor_LossType);
			 c += "</select></td>"
             + "<td colspan=2 align=right id='MultBodyParts_label'>"
			 if (MainFlag == 'Edit') {
			     c += "<span style='color:blue;cursor:hand' onClick='parent.WC_MultipleBodyParts_XTRCT()'><u>Injured Body Parts:</u></span>"
			       + "<span id=MultBodyParts>" + MultipleBodyParts + "</span>"
             }
			 c += "</td>"
			 + "<td align=right>"
			 if (MainFlag == 'Edit') {
			     c += "<span style='color:blue;cursor:hand' onClick='parent.WC_Diagnoses_XTRCT()'><u>Diagnoses:</u></span>"
			        + "<span id=lblNoDiagnosis>" + DiagNo + "</span>"
			 }
			 c += "</td>"
			+ "<td colspan=2 align=right>"
			 if (MainFlag == 'Edit') {
			     c += "<span style='color:blue;cursor:hand' onClick='parent.OverpmntsCredits_Get()'><u>OvrPmnt/Cred Adj:</u></span>"
			        + "<span id='OverpmntsCredits'><b>" + CreditOvrPmnts + "</b></span>"
			 }
			 c += "</td>"
			 + "<td colspan=2 align=right>"
			 c+="</td>"	
			+ "</td>"
		+ "</tr>"
		+"</table>"

		+ "<br>"
		
		+"<table border=0 width='100%'>"
		+"<tr>"
		+"<td rowspan=3>LOSS COVERAGE:</td>"
		+"<td id='MainLCov'><B>Main:</B></td>"
		+ "<td colspan=2><select name='cboWC_LossCov'"		      
			c+= (WC_LossCov.indexOf("<option value=''></option>")>-1) ?  "" : " onClick='parent.WC_LossCov_XTRCT(\"\")' "
			c+=">"
			c+= (WC_LossCov=='') ? WC_LossCov_XTRCT(ClaimNumber) : WC_LossCov
		 	c+="</SELECT></td>"
		+ "<td id='PDRate'>PD Rating:<input type='text' value='" + PDRating + "' name='PDRating' style='width:35px' maxlength='5' onBlur='if(this.value!=\"\" && isNaN(this.value)){alert(\"PD Rating must be numeric value\");this.value=\"\";}; if(this.value!=\"\" && parseFloat(this.value)>100){alert(\"PD Rating must be in the range 0-100\");this.value=\"\";};if(this.value!=\"\" && parseFloat(this.value)<0){alert(\"PD Rating must be in the range 0-100\");this.value=\"\";};parent.Populate_Severity(this.value);'></td>"

         + "<td rowspan=2 colspan=2></td>"
         + "<td rowspan=2 align=right>Initial Treatment type:</td>"
         + "<td rowspan=2>"
			+ "<select name='cboInitialTreatment'>"
		 	+ WC_InitialTreatment_List  // located in jsOccupation
		 	+ "</select>"
         +"</td>"

        c += "</tr>" 	
					
		+"<tr>"
		+"<td rowspan=2>Other:</td>"
		+"<td><input type='checkbox' name='chkLifePension' onClick='parent.life_pension=(this.checked) ? \"Y\" : \"N\";' "
        c += (life_pension == 'Y') ? " checked " : ""
		c += ">Life Pension</td>"
		+ "<td><input type='checkbox' name='chkJointCoverage' onClick='parent.joint_coverage=(this.checked) ? \"Y\" : \"N\";'"
		c += (joint_coverage == 'Y') ? " checked " : ""
		c += ">Joint Coverage</td>"		
    	+"<td><input type='checkbox' name='chkAgreement' onClick='parent.comp_with_liability=(this.checked) ? \"Y\" : \"N\";'"
		c+= (comp_with_liability=='Y') ? " checked " : ""
		c+= ">Agreement to Compensate with Liability</td>"
		+"</tr>"

		+ "<tr>"
        + "<td><input type='checkbox' name='chkCumulative' onClick='parent.cumulative_trauma=(this.checked) ? \"Y\" : \"N\";parent.frInfo.document.getElementById(\"CumTrauma\").innerHTML=parent.Populate_workcomp_CumTrauma(this.checked);'"
		c+= (cumulative_trauma=='Y') ? " checked " : ""
		c+= ">Cumulative Trauma </td>"
		+ "<td><input type='checkbox' name='chkLifeMedical' onClick='parent.life_medical=(this.checked) ? \"Y\" : \"N\";parent.frInfo.document.getElementById(\"FMType\").innerHTML=parent.Populate_workcomp_FMType(this.checked);'"
		+ ">Life Medical  &nbsp; <span id='LifeMedical_hist'> </span>  </td>"        
		+ "<td><input type='checkbox' name='chkSafety' onClick='parent.safety_officer=(this.checked) ? \"Y\" : \"N\";'"
		c+= (safety_officer=='Y') ? " checked " : ""
		c += ">Safety Officer (4850, etc)</td>"
    	+ "<td colspan=3 align=right>WCAB Fraud Indicator:</td>"
		+ "<td>"
		+ "<select name='cboBoardFraud'><option value=0>Not Fraudulent</option><option value=1>Partially Fraudulent</option><option value=2>Fully Fraudulent</option></select>"
		+ "</td>"
		+ "</tr>"
		+ "<tr>"
		+ "<td></td>"
		//+ "<td></td>"
		+ "<td colspan=3 id='CumTrauma'>"
		if(cumulative_trauma=='Y'){
		    c += Populate_workcomp_CumTrauma(true);		    
		}else{
		    c += Populate_workcomp_CumTrauma(false);
		}
		c+="</td>"
		+ "<td colspan=2 id='FMType'>"
		if (life_medical=='Y') {
		    c += Populate_workcomp_FMType(true);
		} else {
		    c += Populate_workcomp_FMType(false);
		}
		c += "</td>"
		+ "<td></td>"
		+ "<td></td>"
		+ "<td></td>"
		+"</tr>"
		 + "</table>"
	 
	 + "<BR>"	    
	 c+="<table id='workcomp' border=0 cellPadding=0 cellSpacing=0 width=100%>"
	  + "<TR>"
		  + "<TD rowspan=2 style='width:40px'>NCCI<BR> CODES:</TD>"
			+ "<TD style='width:80px' align=right id='InjSc'><B>Cause of Injury:</B></TD>"		
			+ "<TD><select name='cboNCCI_SOI'"
	 c += (NCCI_Source_Of_Injury_List.indexOf("<option value=0></option>") > -1) ? "" : " onClick='parent.WC_Source_Of_Injury_XTRCT(\"\", parent.JurState, parent.NCCI_SOI)' "
			c +=">"
			c += (NCCI_Source_Of_Injury_List == '') ? WC_Source_Of_Injury_XTRCT(ClaimNumber, JurState, NCCI_SOI) : NCCI_Source_Of_Injury_List	 
			c += "</SELECT></TD>"
			+ "<TD style='width:80px' align=right id='InjNa'><B>Nature Of Injury:</B></TD>"	
			+ "<TD><select name='cboNCCI_NOI' "
			c += (NCCI_Nature_Of_Injury_List.indexOf("<option value=0></option>") > -1) ? "" : " onClick='parent.WC_Nature_Of_Injury_XTRCT(\"\", parent.JurState, parent.NCCI_NOI)' "
			c +=">"
			c += (NCCI_Nature_Of_Injury_List == '') ? WC_Nature_Of_Injury_XTRCT(ClaimNumber, JurState, NCCI_NOI) : NCCI_Nature_Of_Injury_List
			c += "</SELECT></TD>"
			+ "</TR>" 
//6
		  + "<TR>"
			+ "<TD align=right id='SevCd'><b>Severity:</b></TD>"
			+ "<TD><select name='cboNCCI_SEV'>" + NCCI_Severity_List + "</SELECT></TD>"
			+ "<TD align=right id='ClsCd'><B>Class Code:</B></TD>"
			+ "<TD><select name='cboWC_ClassCode' "  
			c += (NCCI_ClassCode.indexOf("<option value=''></option>")>-1) ?  "" : " onClick='parent.NCCI_ClassCode_XTRCT(\"\", parent.ClientNumber, parent.JurState)' "
			c +=">"
			c += (NCCI_ClassCode=='') ? NCCI_ClassCode_XTRCT(ClaimNumber,ClientNumber,JurState) : NCCI_ClassCode
			c +="</SELECT></TD>"
		  + "</TD><TR>"
	 + "</table>"
//7
		+ "<BR>"
		c += ""
         + "&nbsp;&nbsp;Salary Continued:"		 
		 + "<input type='checkbox' name='chkSalaryContinued' value='' onClick='parent.SalaryCont=(this.checked) ? \"Y\" : \"N\";'"
		 c+= (SalaryCont=='Y') ? " checked" : ""
		 c+=">" 

		 + "&nbsp;&nbsp;Injury day paid in full:"		 
		 + "<input type='checkbox' name='chkInjDayPaid' value='' onClick='parent.InjDayPaid=(this.checked) ? \"Y\" : \"N\";'"
		 c+= (InjDayPaid=='Y') ? " checked" : ""
		 c+=">" 
		
		+"&nbsp;&nbsp;Other worker injured:<input type='checkbox' name='chkOtherEmplInjured' onClick='parent.other_worker_injured=(this.checked) ? \"Y\" : \"N\";'"
		c+= (other_worker_injured=='Y') ? " checked " : ""
		c+=">"
		+"&nbsp;&nbsp;Unable to work one day:<input type='checkbox' name='chkUnableWorkOneDay' onClick='parent.unable_work_oneday=(this.checked) ? \"Y\" : \"N\";'"
		c+= (unable_work_oneday=='Y') ? " checked " : ""
		c+=">"
		+"&nbsp;&nbsp; Surgery:<input type='checkbox' name='chkSurgery' onClick='parent.surgery=(this.checked) ? \"Y\" : \"N\";'"
		c+= (surgery=='Y') ? " checked " : ""
		c+=">"
		if (JurState == 'CA') {
		    c += "&nbsp;&nbsp;132A claim:<input type='checkbox' name='CA_132A' onClick='parent.CA_132A=(this.checked) ? \"Y\" : \"N\";'"
		    c += (CA_132A == 'Y') ? " checked " : ""
		    c += ">"
		      + "&nbsp;&nbsp; Serious & Willful:<input type='checkbox' name='Serious_Willful' onClick='parent.Serious_Willful=(this.checked) ? \"Y\" : \"N\";'"
		    c += (Serious_Willful == 'Y') ? " checked " : ""
		    c += ">"
		}
	 c+= "<br>"
	  + "<table>"
	  + "<tr><td>* Fields marked as <B>Bold</B> are required</td></tr>"
	  + "</table>"
	c+="</form></body>" 
	 + "</html>"
	return(c)
}	
	
function Populate__workcomp_CCE(){
	var CCE= "<table id='workcomp_CCE' border=0 cellPadding=1 cellSpacing=1 >"		  
			+ "<TR>"
			+ "<TD>Concurrent empl. wages:</TD>"
			+ "<TD><input name='txtWC_ConcurWAGE' value='" + ConcurWAGE + "' maxlength='9' style='width:60px' onBlur='{this.value=parent.format(parent.MyAmount(this.value),2);}'></TD>"
			+ "<TD>Insured's wages:</TD>"
			+ "<TD><input name='txtWC_InsuredWAGE' value='" + InsuredWAGE + "' maxlength='9' style='width:60px' onBlur='{this.value=parent.format(parent.MyAmount(this.value),2);}'></TD>"
			+ "</TR>"	
			+ "</table>"
	return(CCE)
}

function Populate__workcomp_RTW(pass) {
    var RTW = "<table id='workcomp_RTW' border=0 cellPadding=1 cellSpacing=1>"
			+ "<tr>"
			+ "<td>RTW Same Employer / Position:</td>"
			+ "<td>"			
			+ "<input type='checkbox' name='chkRTWSameEmpl' "
	            RTW += (RTWSameEmpl == 'Y') ? " checked " : ""
	            RTW += " onclick='parent.RTWSameEmpl=(this.checked) ? \"Y\": \"N\";"
	            RTW += " parent.frInfo.document.getElementById(\"RTW\").innerHTML=parent.Populate__workcomp_RTW(this.checked); "
                RTW += "if(this.checked){parent.frInfo.document.getElementById(\"RTWPosition\").style.display=\"inline\";}else{parent.frInfo.document.getElementById(\"RTWPosition\").style.display=\"none\";}"
                RTW += "'>"

	        RTW += "&nbsp;<span id='RTWPosition'><input name='return_position' value='" + return_position + "' maxlength='50'></span>"
			RTW +="</td>"
			    + "</tr>"
			    + "</table>"
		
    return (RTW)
}

function Populate_workcomp_CumTrauma(pass){
    var r = (pass == false) ? '' : "Trauma Exposure from:<input type='text' name='cumulative_trauma_date'  value='" + MyDateFormat(cumulative_trauma_date, 1) + "' maxlength='10' style='width:60px' onBlur='this.value=parent.MyDate(this.value,1);if(parent.DateComp(this.value,parent.frGeneral.document.frmGeneral.txtAccDT.value)<0){alert(\"The date can`t be greater then Loss Date\");this.value=\"\";}'> - to:<input type='text' name='cumulative_trauma_last_date'  value='" + MyDateFormat(cumulative_trauma_last_date, 1) + "' maxlength='10' style='width:60px' onBlur='this.value=parent.MyDate(this.value,1);if(parent.DateComp(parent.frInfo.document.frmClaim.cumulative_trauma_date.value,this.value)<0){alert(\"The [to] date can`t be greater then Trauma start date\");this.value=\"\";}'>"
    return(r);
}

function Populate_workcomp_FMType(pass) {
    var r = (pass == false) ? '' : "<span id=\"FM_type\">FutureMed Type:</span> &nbsp;<span id='FMtype_hist'></span> <select name='cboFM_type' id='cboFM_type' onChange='parent.future_medical_type=this.value;'><option value='M'>Maintenance</option><option value='A'>Active</option></select>" ;
    return (r);
}

function Populate_field_assignment(){
  var r="<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css' />"
	    + "<title>fieldassign</title></head>"
	    + "<form name='frmClaim'>"
	    + "<table cellPadding=1 cellSpacing=1 width=95% border=0><tr>"
		+ "<td align=right>Assignment Comment:</td><td><textarea cols=100 rows=20 name=txtAssigNote></textarea></td>"
		+ "</tr></table>"
		+ "</form></body></html>"	
	return(r);
}

function Populate_rec(){
  var r="<html><head>"
		+ "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
	    + "<Title>rec</Title></head><body><Form name='frmClaim'>"
  //var SStateID = 0; var SCountryId = ''; var SCountry = ''; var SStateDesc = ''; var ACStateID = 0; var ACCountryId = ''; var ACCountry = ''; var ACStateDesc = '';
	    + "<input name = 'cboSState' type='hidden' id = 'cboClmtState' value = '" + SSTATE + "' />"  // NY
		+ "<input name = 'SStateId'  type='hidden' id = 'SStateID'  value = '" + SStateID + "' />"  // 31
		+ "<input name = 'SCountryId'  type='hidden' id = 'SCountryId'  value = '" + SCountryId + "' />"  //

	    + "<input name = 'cboAState' type='hidden' id = 'cboAState' value = '" + ACSTATE + "' />"  // NY
		+ "<input name = 'ACStateID'  type='hidden' id = 'ACStateID'  value = '" + ACStateID + "' />"  // 31
		+ "<input name = 'ACCountryId'  type='hidden' id = 'ACCountryId'  value = '" + ACCountryId + "' />"  //
    
	    
		+ "<table id='rec' cellPadding=1 cellSpacing=1 width=99% BORDER=1 frame=below>"		
		+ "<Col width=50% ><Col=50%>"
		+"<TR>"
		+"<TD>"
			+ "<table  cellPadding=1 cellSpacing=1  width=100% frame=void rules=none  >"  //
			    +"<Caption><B>Responsible Party</B></Caption>"
			    +"<Col width='30'><Col='*'>"
			    +"<TR>"				
				     +"<TD id=rp_name>Name:</TD>"				 
				    + "<TD><input type='text' name='txtSName' value='" + SNAME + "' style='width:99%'  maxlength='50'></TD>"
			    +"</TR>"
			    +"<TR>"
				    + "<TD rowspan=1>" //Address:"
				        + "<span id='SAddr' style='color:blue;cursor:hand;' onClick='parent.AddressUpdate_Rec_ResponsibleParty(\"Responsible Party address\");' ><u>Address:</u></span>"
		                + "<input name='txtSAddress1' value='" + SADDR1 + "' type=hidden maxlength='50'>"
		                + "<input name='txtSAddress2' value='" + SADDR2 + "' type=hidden  maxlength='50'>"
				    + "</TD>"
                    + "<TD rowspan=1>"
                        + "<div name='SAddrViewLine' id='SAddrViewLine'>" + trim(SADDR1) + ' ' + trim(SADDR2) + "</div>" // style='width:240px'
		                //+ "<input name='SAddrViewLine' value='" + trim(SADDR1) + ' ' + trim(SADDR2) + "' style='width:100%' class=textReadOnly READONLY>" // style='width:240px'
        	        + "</TD>"
                   +"</TR>"
       		       +"<TR>"
			    	    + "<TD>City/Town:</TD>"
				        +"<TD><input type='text' name='txtSCity' value='" + SCITY + "'  maxlength=50 style='width:99%' class=textReadOnly READONLY ></td>"  //
    		        +"</TR>	"	
				+"</TD> "
			+ "</TR>"

       		   + "<TR>"
			    	+ "<TD>State:</TD>"
				    + "<TD><table  cellPadding=0 cellSpacing=1  frame=void rules=none width=100% >"
					        + "<TR>"
						        + "<td align=left style='width:50%'><input name='SStateDesc' value='" + SStateDesc + "' style='width:99%' class=textReadOnly READONLY></td>" //
        						+ "<TD align=right>Zip/Postal Code: </td>"
                                + "<td style='width:90px'><input type='text' name='txtSZip' value='" + SZIP + "' maxlength='20'  class=textReadOnly READONLY >" // onBlur='if(parent.ValZip(this.value,parent.frInfo.document.frmClaim.cboSState.value)==false){alert(\"US Zip code should be entered in the format ##### or #####-####\");this.value=\"\";this.focus();}'>"
						        + "</td>"
					    + "</TR>	"
					    + "</table>"
				+ "</TD> "
			+ "</TR>"
			
			
       		+ "<TR>"
			    	+ "<td id='SCountryL'>Country:</td>"
				    + "<TD><table  cellPadding=0 cellSpacing=1  frame=void rules=none width=100% >"
					         + "<TR>"
				                + "<TD><input name='SCountry' value='" + SCountry + "' class=textReadOnly READONLY></TD> "
						        + "<TD align=right>Email:</td>"
						        + "<TD  style='width:195px'><input type='text' name='txtSEmail' value='" + SEMAIL + "'  style='width:99%' ></td>"
					           + "</TR>"
					+ "</table>"
				+ "</TD>"
		+ "</TR>"
		+ "<TR>"
				+"<TD>Phone:</TD>"
				+"<TD><table  cellPadding=0 cellSpacing=1  frame=void rules=none  width=99% >"
					+"<TR>"
 						+"<TD style='width:175px'><input type='text' name='txtSPhone' value='" + SPHONE + "' style='width:99%' maxlength=25></td>"
						+ "<TD align=right>Fax:</TD>"
						+"<TD style='width:175px'><input type='text' name='txtSFax' value='" + SFAX + "' style='width:99%' maxlength=25></td>"
					+"</TR>"
					+"</table>"
				+"</TD> "
			+"</TR>"
			+"</table>"
		+"</TD>"
		+"<TD>"
			+"<table  cellPadding=1 cellSpacing=1 frame=void rules=none width=100% >"
			+"<Caption><B>Adverse Responsible Carrier</B></Caption>"
			+"<Col width='30'><Col='*'>"
			+"<TR>"
				+"<TD>Contact:</TD>"
				+"<TD><input type='text' name='txtAContact' value='" + ACCONTACT + "' style='width:98%'  maxlength='50'></TD>"
			+ "<TR>"

		    + "<TR>"
			    + "<TD rowspan=1>" //Address:"
			        + "<span id='ACAddr' style='color:blue;cursor:hand;' onClick='parent.AddressUpdate_Rec_AdverseCarrier(\"Adverse Carrier address\");' ><u>Address:</u></span>"
	                + "<input  name='txtAAddress1' value='" + ACADDR1 + "' type=hidden maxlength='50'>"
	                + "<input  name='txtAAddress2' value='" + ACADDR2 + "' type=hidden  maxlength='50'>"
			    + "</TD>"
                + "<TD rowspan=1>"
                    + "<div name='AddrViewLine' id='AddrViewLine'>" + trim(ACADDR1) + ' ' + trim(ACADDR2) + "</div>" 
	                //+ "<input name='AddrViewLine' value='" + trim(ACADDR1) + ' ' + trim(ACADDR2) + "' style='width:99%' class=textReadOnly READONLY>" // style='width:240px'
		        + "</TD>"
           + "</TR>"
   		   + "<TR>"
		    	+ "<TD>City/Town:</TD>"
		    	+ "<TD><input type='text' name='txtACity' value='" + ACCITY + "' style='width:99%' maxlength=50  class=textReadOnly READONLY ></td>"
           + "</TR>"
   		   + "<TR>"
   		         + "<TD>State:</td>"
			     + "<TD><table  cellPadding=0 cellSpacing=1  frame=void rules=none width=100% >"
				        + "<TR>"
					        + "<td align=left style='width:50%'><input name='ACStateDesc' value='" + ACStateDesc + "' style='width:99%' class=textReadOnly READONLY></td>"
    						+ "<TD align=right>Zip/Postal Code: </td>"
                            + "<td style='width:90px'><input type='text' name='txtAZip' value='" + ACZIP + "' maxlength='20'  class=textReadOnly READONLY >" // onBlur='if(parent.ValZip(this.value,parent.frInfo.document.frmClaim.cboSState.value)==false){alert(\"US Zip code should be entered in the format ##### or #####-####\");this.value=\"\";this.focus();}'>"
					        + "</td>"
				        + "</TR>"
				    +  "</table>"
			   + "</TD> "
		    + "</TR>"
   		    + "<TR>"
		    	    + "<td id='ACCountryL'>Country:</td>"
			        + "<TD><table  cellPadding=0 cellSpacing=1  frame=void rules=none width=98% >"
				             + "<TR>"
			                    + "<TD><input name='ACCountry' value='" + ACCountry + "' class=textReadOnly READONLY></TD> "
					            + "<TD align=right>Email:</td>"
					            + "<TD  style='width:195px'><input type='text' name='txtAEmail' value='" + ACEMAIL + "'  style='width:99%' ></td>"
				               + "</TR>"
				    + "</table>"
			    + "</TD>"
	    + "</TR>"
			
			+"<TR>"
				+"<TD>Phone:</TD>"
				+"<TD><table  celladding=0 cellSpacing=1 frame=void rules=none  width=100% >"
					+"<TR>"
 						+"<TD style='width:175px'><input type='text' name='txtAPhone' value='" + ACPHONE + "' style='width:99%' maxlength=25></td>"
						+"<TD align=right>Fax:</TD>"
						+"<TD style='width:175px'><input type='text' name='txtAFax' value='" + ACFAX + "' style='width:99%' maxlength=25></TD>"
//						+"<TD style='width:20px'>Email:</TD>"
//						+"<TD><input type='text' name='txtAEmail' value='" + ACEMAIL + "'  style='width:980%' ></TD>"
					+"</TR>"
					+"</table>"
				+"</TD> "
			+"</TR>"
			+"</table>"
		+"</TD>"
		+"</TR>"
		+"</table>"		
		+"<table cellPadding=1 cellSpacing=1 width=95% BORDER=0>"		
		+"<TR>"
		+"<TD>"
			+"<table cellPadding=1 cellSpacing=1 width=100% BORDER=0>"
			+"<Col=50%><Col=50%>"	
			+"<TR>"
				+"<TD>Adjuster:</TD>"
				+"<TD align=left><select name='cboRAdjNo' onClick='{if(parent.RecovUser==0){document.frmClaim.txtRecReserve.focus();}}' onDblClick='{if(parent.RecovUser==0){document.frmClaim.txtRecReserve.focus();}}' onFocus='{if(parent.RecovUser==0){document.frmClaim.txtRecReserve.focus();}}'>" + RADJ + "</SELECT></TD>"
			+"</TR><TR>"
				+"<TD>Reserve:</TD>"
				+"<TD align=left><input type='text' name='txtRecReserve' value='" + RECRESERVE + "' onblur='{if(isNaN(document.frmClaim.txtRecReserve.value)){alert(\"Incorrect Symbol\");document.frmClaim.txtRecReserve.value=0;document.frmClaim.txtRecReserve.focus()}}' style='width:100%' maxlength=9></TD>"
			+"</TR><TR>"
				+"<TD>York Fee:</TD>"
				+"<TD align=left><input type='text' name='txtSYorkAmount' value='" + SYORKAMOUNT + "' onblur='{if(isNaN(document.frmClaim.txtSYorkAmount.value)){alert(\"Incorrect Symbol\");document.frmClaim.txtSYorkAmount.value=0;document.frmClaim.txtSYorkAmount.focus()}}' style='width:100%' maxlength=9></TD>"
			+"</TR>"
			+"</table>"
		+"</TD>"

		+"<TD>"
			+"<table cellPadding=1 cellSpacing=1 width=100% BORDER=0>"
			+"<Col=50%><Col=50%>"	
			+"<TR>"			
				+"<TD align=right id=PerAg>Pursuit Against:</TD>"
				if(REC_STATUS=='Y' && REC_STATUS_Original!='Y'){
					 r+="<TD><select Name='cboPursuitAgainst' style='width:99%'><option value=0></option>" + PursuitAgainstCodes + "</Select></TD>"
				}else{
					 r+="<TD><select Name='cboPursuitAgainst' style='width:99%' onClick='{if(parent.RecovUser==0){document.frmClaim.txtACClmNo.focus();}}' onDblClick='{if(parent.RecovUser==0){document.frmClaim.txtACClmNo.focus();}}' onFocus='{if(parent.RecovUser==0){document.frmClaim.txtACClmNo.focus();}}'><option value=0></option>" + PursuitAgainstCodes + "</Select></TD>"
				}
			r+="</TR><TR>"				
				+"<TD align=right id='RecType'>Recovery Type:</TD>" //Subrogation
				if(REC_STATUS=='Y' && REC_STATUS_Original!='Y'){
					r+="<TD><select Name='cboSubrogationType' style='width:99%' onChange='parent.RecFields_Mark_BLUE(this.value)'><option value=0></option>" + SubrogationTypeCodes + "</Select></TD>"
				}else{
					r+="<TD><select Name='cboSubrogationType' style='width:99%' onClick='{if(parent.RecovUser==0){document.frmClaim.txtACClmNo.focus();}}' onDblClick='{if(parent.RecovUser==0){document.frmClaim.txtACClmNo.focus();}}' onFocus='{if(parent.RecovUser==0){document.frmClaim.txtACClmNo.focus();}}'><option value=0></option>" + SubrogationTypeCodes + "</Select></TD>"
				}
			r+="</TR><TR>"			
				+"<TD align=right>Disposition:</TD>"
				+"<TD><select Name='cboDisposition' onClick='{if(parent.RecovUser==0){document.frmClaim.txtACClmNo.focus();}}' onDblClick='{if(parent.RecovUser==0){document.frmClaim.txtACClmNo.focus();}}' onFocus='{if(parent.RecovUser==0){document.frmClaim.txtACClmNo.focus();}}'><option value=0></option>" + DispositionCodes + "</Select></TD>"
			+"</TR>"
			+"</table>"
		+"</TD>"

		+"<TD>"
			+"<table cellPadding=1 cellSpacing=1 width=100% BORDER=0>"
			+"<Col=50%><Col=50%>"
			+"<TR>"	
				+"<TD align=right>Adverse Carrier's Claim#:</TD>"
				+"<TD style='width:150px'><input type='text' name='txtACClmNo' value='" + ACCLMNO + "' style='width:100%' maxlength=20></TD>"
		r+="</TR><TR>"
				+"<TD align=right id=ReferTo>Subrogation Referred To:</TD>"
				if(REC_STATUS=='Y' && REC_STATUS_Original!='Y'){
					r+="<TD><select Name='cboReferredTo' style='width:99%'><option value=0></option>" + ReferredToCodes + "</Select></TD>" //this value should be selected by adjuster when Recovery is set up as PENDING
				}else{
					r+="<TD><select Name='cboReferredTo' style='width:99%' onClick='{if(parent.RecovUser==0){document.frmClaim.txtRecovPercent.focus();}}' onDblClick='{if(parent.RecovUser==0){document.frmClaim.txtRecovPercent.focus();}}' onFocus='{if(parent.RecovUser==0){document.frmClaim.txtRecovPercent.focus();}}'><option value=0></option>" + ReferredToCodes + "</Select></TD>"
				}
			   r+="</TR><TR>"
				+"<TD align=right>Resolving By:</TD>"
				+"<TD><select Name='cboResolvedBy' style='width:99%' onClick='{if(parent.RecovUser==0){document.frmClaim.txtRecovPercent.focus();}}' onDblClick='{if(parent.RecovUser==0){document.frmClaim.txtRecovPercent.focus();}}' onFocus='{if(parent.RecovUser==0){document.frmClaim.txtRecovPercent.focus();}}'><option value=0></option>" + ResolvedByCodes + "</Select></TD>"
			+"</TR>"
			+"</table>"
		+"</TD>"
				
		+"<TD>"
			+"<table cellPadding=1 cellSpacing=1 width=100% BORDER=0>"
			+"<Col=50%><Col=50%>"	
			+"<TR>"			
				+ "<TD align=right id=RecPerc>Likely Percentage of Recovery(%):</TD>"
				+ "<TD style='width:40px'><input type='text' Name='txtRecovPercent' value='" + RECOVPERCENTAGE + "' onblur='{if(isNaN(document.frmClaim.txtRecovPercent.value)){alert(\"Incorrect Symbol\");document.frmClaim.txtRecovPercent.value=0;document.frmClaim.txtRecovPercent.focus()}}' style='width:40px' maxlength=3></TD>"
			r+="</TR><TR>"
				+ "<TD align=right>Recovery Settlement:</TD>"
				+ "<TD style='width:40px'><input type='text' Name='txtRSettlement' value='" + SETTLEMENT + "' onblur='{if(isNaN(document.frmClaim.txtRSettlement.value)){alert(\"Incorrect Symbol\");document.frmClaim.txtRSettlement.value=0;document.frmClaim.txtRSettlement.focus()}}' style='width:40px' maxlength=9></TD>"
			+"</TR><TR>"			
				+"<TD align=right>Amount Received:</TD>"
				+"<TD style='width:40px'><input type='text' Name='txtRAmount' value='" + RAMOUNT + "' onblur='{if(isNaN(document.frmClaim.txtRAmount.value)){alert(\"Incorrect Symbol\");document.frmClaim.txtRAmount.value=0;document.frmClaim.txtRAmount.focus()}}' style='width:40px' maxlength=9></TD>"
			+"</TR>"
			+"</table>"
		+"</TD>"
		+"</TR>"		
		+"</table>"
		
		+"<table cellPadding=0 cellSpacing=0 width=95% BORDER=0>"
		+"<Col=60%><Col=40%>"	
		+"<TR>"
		+"<TD>"			
			+"<table cellPadding=1 cellSpacing=1 width=100% BORDER=0>"
			+"<Col=25%><Col=25%><Col=25%><Col=25%>"	
			+"<TR>"
			+"<TD id=RecStatus align=left><B>STATUS:</B>"
				switch(REC_STATUS_Original){
					case "N":
						 r+="NONE</TD><TD></TD>" // No status						  
						break;
					case "Y":
						r+="PENDING</TD>" // pending file can be opened
						if(REC_STATUS=='O'){
						r+="<TD align=middle id=RecStatus><input TYPE='checkbox' Name='chkRecovery' Value='O' CHECKED onClick='{if(parent.RecovUser==0){alert(\"You may not change the Recovery Status.\");document.frmClaim.chkRecovery.checked=true;}else{parent.CheckBox_RecClosed(\"O\")}}'>change to OPENED</TD>"
						}else{
						r += "<TD align=middle id=RecStatus><input TYPE='checkbox' Name='chkRecovery' Value='' onClick='{if(parent.RecovUser==0 && parent.frInfo.document.frmClaim.cboReferredTo.options[parent.frInfo.document.frmClaim.cboReferredTo.options.selectedIndex].internal==\"Y\"){alert(\"Only Recovery Adjusters may change Recovery Status.\");document.frmClaim.chkRecovery.checked=false;}else{parent.CheckBox_RecClosed(\"O\")}}'>change to OPENED</TD>"
						}
						break;
					case "O":
						r+="OPENED</TD>" // opened file can be closed
						if(REC_STATUS=='C'){
						r+="<TD align=middle id=RecStatus><input TYPE='checkbox' Name='chkRecovery' Value='C' CHECKED onClick='{if(parent.RecovUser==0){alert(\"You may not change the Recovery Status.\");document.frmClaim.chkRecovery.checked=true;}else{parent.CheckBox_RecClosed(\"C\")}}'>change to CLOSED</TD>"
						}else{
						r += "<TD align=middle id=RecStatus><input TYPE='checkbox' Name='chkRecovery' Value='' onClick='{if(parent.RecovUser==0 && parent.frInfo.document.frmClaim.cboReferredTo.options[parent.frInfo.document.frmClaim.cboReferredTo.options.selectedIndex].internal==\"Y\"){alert(\"Only Recovery Adjusters may change Recovery Status.\");document.frmClaim.chkRecovery.checked=false;}else{parent.CheckBox_RecClosed(\"C\")}}'>change to CLOSED</TD>"
						}
						break;
					case "C":
						r+="CLOSED</TD>" // closed file can be opened
						if(REC_STATUS=='O'){
						r+="<TD align=middle id=RecStatus><input TYPE='checkbox' Name='chkRecovery' Value='O' CHECKED onClick='{if(parent.RecovUser==0){alert(\"You may not change the Recovery Status.\");document.frmClaim.chkRecovery.checked=true;}else{parent.CheckBox_RecClosed(\"O\")}}'>change to OPENED</TD>"
						}else{
						r += "<TD align=middle id=RecStatus><input TYPE='checkbox' Name='chkRecovery' Value='' onClick='{if(parent.RecovUser==0 && parent.frInfo.document.frmClaim.cboReferredTo.options[parent.frInfo.document.frmClaim.cboReferredTo.options.selectedIndex].internal==\"Y\"){alert(\"Only Recovery Adjusters may change Recovery Status.\");document.frmClaim.chkRecovery.checked=false;}else{parent.CheckBox_RecClosed(\"O\")}}'>change to OPENED</TD>"
						}
						break;
					default:
						r+="NOT SPECIFIED</TD><TD></TD>" //new recovery potential
				}

			r+="<TD align=right id=RecStatus colspan=2><A href='javascript:parent.Populate_ExtraRecoveryTypes(" + RECOVERY_ExtraTypes.length + ")' title='Enter the number of recovery possibilities on the file'>RECOVERY POTENTIALS #:</A><select name='RecTypesNum' onChange='parent.Populate_ExtraRecoveryTypes(this.value)'><option value=1>1</option><option value=2>2</option><option value=3>3</option><option value=4>4</option></SELECT></TD>"
			 + "</TR><TR>"
				+"<TD>Rental Vehicle Involved:</TD>"
				+"<TD align=left><input type='radio' name='RentalVeh' value='SLI'  onClick='parent.RadioBoxValue_Rec(\"RentalVeh\",\"S\")' onMouseDown='{if(parent.RecovUser==0 && parent.REC_STATUS_Original==\"C\"){document.frmClaim.txtRecReserve.focus();}}'" 
				if(RVEHICLE=='S'){ 
					r +=" CHECKED "
				}
				if(REC_STATUS_Original=='C' && RecovUser==0){r+=" DISABLED=true "}
				r +=">SLI</TD>"
				+"<TD align=left><input type='radio' name='RentalVeh' value='TARL' onClick='parent.RadioBoxValue_Rec(\"RentalVeh\",\"T\")'  "
				if(RVEHICLE=='T'){
					r +=" CHECKED "
				}
				if(REC_STATUS_Original=='C' && RecovUser==0){r+=" DISABLED=true "}
				r +=">TARL</TD>"			
				+"<TD align=left><input type='radio' name='RentalVeh' value='None' onClick='parent.RadioBoxValue_Rec(\"RentalVeh\",\"N\")' "
				if(RVEHICLE=='N'){
					r +=" CHECKED "
				}
				if(REC_STATUS_Original=='C' && RecovUser==0){r+=" DISABLED=true "}
				r +=">None</TD>"	
			+"</TR>"
			+"<TR>"
				+"<TD>Settlement Payable to:</TD>"
				+"<TD align=left><input type='radio' name='RPayableTo' value='York'  onClick='parent.RadioBoxValue_Rec(\"RPayableTo\",\"Y\")' "
				if(RPAYABLETO=='Y'){ //RPAYABLETO 
					r +=" CHECKED "
				}
				if(REC_STATUS_Original=='C' && RecovUser==0){r +=" DISABLED=true "}
				r +=">York</TD>"
				+"<TD align=left><input type='radio' name='RPayableTo' value='Client' onClick='parent.RadioBoxValue_Rec(\"RPayableTo\",\"C\")' "
				if(RPAYABLETO=='C'){ //RPAYABLETO 
					r +=" CHECKED "
				}
				if(REC_STATUS_Original=='C' && RecovUser==0){r +=" DISABLED=true "}
				r +=">Client</TD>"
				+"<TD align=left><input type='radio' name='RPayableTo' value='Carrier' onClick='parent.RadioBoxValue_Rec(\"RPayableTo\",\"R\")' "
				if(RPAYABLETO=='R'){ //RPAYABLETO 
					r +=" CHECKED "
				}
				if(REC_STATUS_Original=='C' && RecovUser==0){r +=" DISABLED=true "}
				r +=">Carrier</TD>"
			+"</TR>"
			+"<TR>"
				+"<TD>Billing Arrangement:</TD>" 
				+"<TD align=left><input type='radio' name='SYorkFee' value='% of Recovery'  onClick='parent.RadioBoxValue_Rec(\"SYorkFee\",\"R\")' "
				if(SYORKFEE=='R'){ //SYORKFEE
					r +=" CHECKED "
				}
				if(REC_STATUS_Original=='C' && RecovUser==0){r +=" DISABLED=true "}
				r +=">% of Recovery</TD>"
				+"<TD align=left><input type='radio' name='SYorkFee' value='Flat Fee' onClick='parent.RadioBoxValue_Rec(\"SYorkFee\",\"F\")' "
				if(SYORKFEE=='F'){ //SYORKFEE
					r +=" CHECKED "
				}
				if(REC_STATUS_Original=='C' && RecovUser==0){r +=" DISABLED=true "}
				r +=">Flat Fee</TD>"
				+"<TD align=left><input type='radio' name='SYorkFee' value='Time and Expense' onClick='parent.RadioBoxValue_Rec(\"SYorkFee\",\"T\")' "
				if(SYORKFEE=='T'){ //SYORKFEE
					r +=" CHECKED "
				}
				if(REC_STATUS_Original=='C' && RecovUser==0){r +=" DISABLED=true "}
				r +=">T&E</TD>"
			+"</TR>"			
			+"</table>"			
		+"</TD>"
		+"<TD>"
			+"<table cellPadding=0 cellSpacing=0 width=100% BORDER=0>"			
			+"<TR>"
				+ "<TD align=right valign=top style='width:60px'>Notes:</TD>"
				+"<TD align=right rowspan=2><Textarea cols=50 rows=6 name=txtSNotes>" + SNOTES + "</Textarea></TD>"
			+ "</TR>"
			+ "<TR>"
				+ "<TD style='width:60px'>Statute Date:<input style='width:60px' type=text name='StatuteDate' value='" + MyDateFormat(StatuteDate) + "' maxlength='10' onBlur='{this.value=parent.MyDate(this.value, 1);}'></TD>"				
			+ "</TR>"
			+"</table>"				
		+"</TD>"
		+"</TR>"
		+"</table>"
		+"</form></body></html>"	
	return(r);
}


function ModifiedDuty_Save(DutyType,flag, viewMode){
	var N='';var D='';var M='';
    var c = 1;
    var from; var to;  

    while (WC_MDuty.length > c) {
        //Delete marked deleted;
        if (WC_MDuty[c].DEL == 1) {
            D = D + WC_MDuty[c].ID + ',';
            // adjust  deleted records in cache:
            PeriodList("", c, "", "");   
        }
        ////////////////
        //Save Modified;
        if (WC_MDuty[c].MOD == 1) {
            eval('from  = frInfo.document.frmClaim.MD_From' + c + '.value');
            eval('to  = frInfo.document.frmClaim.MD_To' + c + '.value');

            if (from.length < 10 || to.length < 10) {
                if (confirm("Changes on record " + c + " won't be saved. Both 'From' and 'To' Dates must be entered.\nWould you like to cancel saving?")) {
                    return;
                } else {
                    continue;
                }
            } else {
                if (DateComp(frGeneral.document.frmGeneral.txtAccDT.value, from) < 0) {
                    if (confirm("Changes on record " + c + " won't be saved: 'From' date can't be less than Loss Date.\nWould you like to cancel saving?")) {
                        return;
                    } else {
                        continue;
                    }
                }
                if (DateComp(from, to, 0) < 0) {
                    if (confirm("Changes on record " + c + " won't be saved: The `FROM-TO` interval (" + from + " - " + to + ") is incorrect.\nWould you like to cancel saving?")) {
                        return
                    } else {
                        continue;
                    }
                }
            }            
            M = M += '~'
				+ 'DUTYFROM="' + from + '",'
				+ 'DUTYTO="' + to + '",'
				+ 'NOTE="' + Quote(eval('frInfo.document.frmClaim.MD_Note' + c + '.value')) + '" '
				+ 'where DUTYID=' + WC_MDuty[c].ID + '|';
            //ticket 100707  - alerts the person entering the data if dates are overlapping
            PeriodList("", c, from, to);  
        }
        c++
    }
    D = D.slice(0, -1);
    ///////////
    //Save NEW:	
    var DutyType;
    try {
        DutyType = (typeof (frInfo.document.frmClaim.MD_Type0.value) == "undefined") ? 0 : frInfo.document.frmClaim.MD_Type0.value;
    } catch (e) {
        DutyType = 0;
    }
    // as per Paul Tellez: ticket 75211  period From and To  dates are required
    if (frInfo.document.frmClaim.MD_From0.value.length == 10 && frInfo.document.frmClaim.MD_To0.value.length == 10) {
        if (DateComp(frGeneral.document.frmGeneral.txtAccDT.value, frInfo.document.frmClaim.MD_From0.value) < 0) {
            alert("New record cannot be saved.\nFrom date can't be less than Loss Date");
            return;
        }
        if (DateComp(frInfo.document.frmClaim.MD_From0.value, frInfo.document.frmClaim.MD_To0.value, 0) < 0) {
            alert("The FROM-TO interval (" + frInfo.document.frmClaim.MD_From0.value + " - " + frInfo.document.frmClaim.MD_To0.value + ") is incorrect.\nNew record won`t be saved.");
            return;
        } else {
            N = '"' + frInfo.document.frmClaim.MD_From0.value + '","' + frInfo.document.frmClaim.MD_To0.value + '","' + Quote(trim(frInfo.document.frmClaim.MD_Note0.value)) + '",' + DutyType + ''
        }

        PeriodList("", 0, frInfo.document.frmClaim.MD_From0.value, frInfo.document.frmClaim.MD_To0.value)  // 0 - new record
    } else {
        if (frInfo.document.frmClaim.MD_From0.value.length == 0 && frInfo.document.frmClaim.MD_To0.value.length == 0) {
            N = '';  //no new record;
        } else {
            alert('New record cannot be saved.\nBoth From and To dates have to be defined');
            return;
        }
    }
    //verify  overlapping of the time periods:
    PeriodList("overlap", null, null, null);
    
    if (typeof (PeriodList.overlap) != "undefined") {
        var overlapNote = 'List of periods contains following overlapping periods\n';

        if (PeriodList.overlap.length > 0) {
            for (var c = 0; c < PeriodList.overlap.length; c++) {
                overlapNote += '\n[' + PeriodList.overlap[c][0] + "]: " + eval('frInfo.document.frmClaim.MD_From' + PeriodList.overlap[c][0] + '.value') + " - " + eval('frInfo.document.frmClaim.MD_To' + PeriodList.overlap[c][0] + '.value')
                                + " and "
                                + "[" + PeriodList.overlap[c][1] + "]: " + eval('frInfo.document.frmClaim.MD_From' + PeriodList.overlap[c][1] + '.value') + " - " + eval('frInfo.document.frmClaim.MD_To' + PeriodList.overlap[c][1] + '.value')
            }

            overlapNote += '\n\nWould you like to save information as it is?'

            if (!confirm(overlapNote)) { return };
        }
    }
    ModifiedDuty_Get(DutyType, escape(N), escape(D), escape(M), flag, viewMode);
}

function ModifiedDuty_Excel(dataTable, dataTableID, headerTable, headerTableID, Title) {
    var dTable = (dataTable == null) ? '' : dataTable.outerHTML
    var hTable = (headerTable == null) ? '' : hTable.outerHTML

    if(dTable == '' && hTable == '') {
        return;
    }

    var oneRow;
    var CellValue = ''

    frPrint.document.open();
    frPrint.document.write("<div id='myDiv'>" + dTable + hTable + "</div>");
    frPrint.document.close();

    var theTable = frPrint.document.getElementById(dataTableID)  //"ModDutyTable"
    
    theTable.deleteRow(1);   // new entry row
    theTable.deleteCaption();

     for (var i = 0; i < theTable.rows.length; i++) {
        oneRow = theTable.rows[i];
        CellValue =''

        //replace HTML Element with text:
        for (var j = 1; j < oneRow.cells.length; j++) {
            switch (oneRow.cells[j].firstChild.type) {
                case "select-one":
                    CellValue = oneRow.cells[j].firstChild.options[oneRow.cells[j].firstChild.options.selectedIndex].text;
                    break;
                case "text":
                    CellValue = oneRow.cells[j].firstChild.value;
                    break;
                case "":
                    CellValue = (oneRow.cells[j].firstChild.tagName == 'A') ? oneRow.cells[j].firstChild.firstChild.data : 'undefined';
                    break;
                case "checkbox":
                    CellValue = (oneRow.cells[j].firstChild.checked) ? 'Yes' : 'No';
                    break;
                default:
                    CellValue = (oneRow.cells[j].innerHTML == '=X=') ? '-' : oneRow.cells[j].innerHTML;                                    
            }

            if (oneRow.cells[j].innerHTML != CellValue) {

                oneRow.cells[j].removeChild(theTable.rows[i].cells[j].firstChild);
                oneRow.cells[j].innerHTML = "<span>" + CellValue + "</span>";
            }
        }
     }

     if (hTable == '') {
        ExportToExcel(frPrint.document.getElementById(dataTableID), null, Title, "Claim " + ClaimNumber + "; Claimant:" + SingleQuote(CName) + "; Loss Date:" + frGeneral.document.frmGeneral.txtAccDT.value)
     } else {
        ExportToExcel(frPrint.document.getElementById(dataTableID), frPrint.document.getElementById(headerTableID), Title, "Claim " + ClaimNumber + "; Claimant:" + SingleQuote(CName) + "; Loss Date:" + frGeneral.document.frmGeneral.txtAccDT.value)
     }
}

function ModifiedDuty_Get(DutyType, N, D, M, flag, viewMode){ //Type: LostTime-0|Modified Duties-1;  N-new; D-deleted; M-modified
	if(flag!='SAVE'){
		switch(DutyType){
			case 0:
				if(LOSTTIME!='Y'){
					alert('No indication that lost time history is available');
					return;
				}
				break;
			default:
				if(MODIFIEDUTY!='Y'){
					alert('No indication that modified duties were performed');
					return;
				}
				break;
		}
	}
    if (N == '' && D == '' && M == '' && viewMode==0) { Var_Save_WorkComp(); Variable_Populate("workcomp"); } //load new screen;

        var RetVal = ModDuty_XTRCT(DutyType, N, D, M, viewMode)
		WC_MDuty.length=0;
 
		var DutyTitle = (DutyType==0) ? "History of the Lost Time" : "History of Modified Duties";
		var t = Populate_ModifiedDuty(DutyType, RetVal, DutyTitle, viewMode);

		frInfo.document.open();
		frInfo.document.write(t);
		frInfo.document.close();

		try {
		    frButtons.document.frmButtons.cmdEdit.disabled = true;   // block high level action button
		} catch (e) { };

		if (typeof frInfo.document.frmClaim.viewMode != 'undefined') {
		    frInfo.document.frmClaim.viewMode.value = viewMode;
        } 
}

var WC_MDuty=new Array();	
var WC_MDuty_SubDetails=new Array();

function WC_MDuty_SubData(BNFT_TYPE,BNFT_AMNT,BNFT_NOTE, PRMNT_OFFER, PRMNT_ACCEPT, PRMNT_START, PRMNT_AMOUNT, PRMNT_TYPE, PRMNT_NOTE){
	this.BNFT_TYPE=BNFT_TYPE;
	this.BNFT_AMNT=BNFT_AMNT;
	this.BNFT_NOTE=BNFT_NOTE;
	this.PRMNT_OFFER=PRMNT_OFFER;
	this.PRMNT_ACCEPT=PRMNT_ACCEPT;
	this.PRMNT_START=PRMNT_START;
	this.PRMNT_AMOUNT=PRMNT_AMOUNT;
	this.PRMNT_TYPE=PRMNT_TYPE;
	this.PRMNT_NOTE=PRMNT_NOTE;
}

function Populate_ModifiedDuty(DutyType, arrElmnts, DutyTitle, viewMode){	
	//var DutyTitle = (DutyType==1) ? "History of Modified Duties" : "History of the Lost Time"
	var n=-1; //new records counter
	var c = 1;
	PeriodList("cache_remove",null,null,null);
	
	try{
		eval(arrElmnts)
	}catch(e){
		arrElmnts=''
	}
	var f="";
		f="<html><head>"
		 +"<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 +"<title>duty</Title></head><body><Form name='frmClaim'>"
		 +"<table  cellPadding=1 cellSpacing=10 border=0 width=400px>"
		 +"<tr>"
		 +"<td align=middle>"
		 +"<input type='button' name='mdSave' value='Save' style='width:60px' class=BottonScreen onclick='parent.ModifiedDuty_Save(" + DutyType + ",\"SAVE\", " + viewMode + ")'>"  
		 +"</td>"
		 +"<td align=middle>"
		 + "<input type='button' name='mdExcel' value='Export to Excel' style='width:120px' class=BottonScreen onclick='parent.ModifiedDuty_Excel(this.form.all[\"ModDutyTable\"],\"ModDutyTable\",null, \"\", \"" + DutyTitle + "\")'>" 
		 +"</td>"
		 +"<td align=middle>"
		 + "<input type='button' name='mdBack' value='Back' style='width:60px' class=BottonScreen onclick='parent.Display(\"workcomp_ca\");'>"
		 + "</td>"
		    if (DutyType!=0){
             f+="<td align=right>&nbsp;&nbsp;&nbsp; View:"
		      + "<select name='viewMode' id='viewMode' onchange='parent.ModifiedDuty_Get(parent.DutyType, \"\", \"\", \"\", \"\", this.value)'>"
              + "<option value=1>All</option><option value=2>Not linked to payments</option><option value=3>Linked to payments</option>"
              + "</select>"
		      + "</td>"
		  }
       f += "</tr>"
		 +"</table>"
		 
		 +"<table id='ModDutyTable' border=0  cellPadding=1  width=100%>"
		 +"<caption>"
		 f+= (arrElmnts=='') ? "No history were recorded yet" : DutyTitle //"List of Modified Duties"
		 f+="</caption>"
		 f+= "<tr id='ModDutyHead'>"
				+"<td>#</td>"
				+"<td>DELETE</td>"
				+"<td>FROM</td>"
				+"<td>TO</td>"	
				+"<td>DAYS<BR>calendar</td>"
				+"<td>DAYS<BR>(OSHA)</td>"
			if(DutyType!=0){  //0 - Lost Time; 
			    f+="<td align=center>TYPE</td>"
				f+="<td>OFFER<BR>permanent</td>"
				 + "<td>BENEFIT<BR>estimated</td>"				
			}
				f+="<td>NOTE</td>"
				f+="<td>CREATED</td>"
				f+="<td>MODIFIED</td>"
			+"</tr>"
			+"<tr>"
				+"<td>new</td>"
				+"<td>=X=</td>"
				+"<td>"
				+ "<input name='MD_From0' value='' maxlength='10' style='width:65px' onBlur='this.value=parent.MyDate(this.value,1);'>"   
				+"</td>"		
				+"<td>"
				+ "<input name='MD_To0' value='' maxlength='10' style='width:65px' onBlur='this.value=parent.MyDate(this.value,1);'>"
				+"</td>"
				+"<td>=X=</td>"
				+"<td>=X=</td>"
				if(DutyType!=0){  //0 - Lost Time;
				    f+= "<td><select name='MD_Type0' style='width:140px'>" + ModDutyType_List + "</select></td>"
					f+="<td>=X=</td>"
					 + "<td>=X=</td>"	
				}
				f+="<td>"
				+ "<input name='MD_Note0' value='' maxlength='80' style='width:520px'>"	 
				+ "</td>"		
				f+="<td>=X=</td>"
				f+="<td>=X=</td>"
			+"</tr>"
			
		 if(arrElmnts!=''){
		     while (WC_MDuty.length > c) {
		         PeriodList("" ,c, WC_MDuty[c].FROM, WC_MDuty[c].TO)   // collect data in cache 

				if(WC_MDuty[c].TO==''){
					ModDuty_To_++
			 	 }
				f+="<tr>"
				+ "<td>" + c + "</td>"
				+ "<td>"
				if (WC_MDuty[c].RqstId == 0) {
				f +=  "<input type='checkbox' name='MD_Del" + c + "' value='' onClick='parent.WC_MDuty[" + c + "].DEL=(this.checked)?1:0;'>"
				} else {
                 f += "<span id='MD_Del" + c + "'>=X=<span>"
                }
                f += "</td>"
				+"<td>"
				+ "<input name='MD_From" + c + "' value='" + MyDateFormat(WC_MDuty[c].FROM) + "' style='width:65px' maxlength='10' onChange='parent.WC_MDuty[" + c + "].MOD=1;' onBlur='this.value=parent.MyDate(this.value,1);' "
                f += (WC_MDuty[c].RqstId == 0) ? '' : " class=textReadOnly "
                f += ">"	
				+"</td>"		
				+"<td>"
				+ "<input name='MD_To" + c + "' value='" + MyDateFormat(WC_MDuty[c].TO) + "' style='width:65px' maxlength='10' onChange='parent.WC_MDuty[" + c + "].MOD=1;' onBlur='this.value=parent.MyDate(this.value,1);'"
                f +=  (WC_MDuty[c].RqstId == 0) ? '' : " class=textReadOnly "
                f += ">"	
				+"</td>"				
				+"<td>" + WC_MDuty[c].CalDAYS + "</td>"		
				+"<td>" + WC_MDuty[c].BusDAYS + "</td>"	
				if(DutyType!=0){  //0 - Lost Time; 
					f+="<td align=left>" + WC_MDuty[c].TYPE + "</td>"
					f+="<td>"
						+ "<A href='javascript:parent.ModifiedDuty_SubDetails_Get(\"PRMNT\"," + WC_MDuty[c].PERM_ID + "," + WC_MDuty[c].ID + "," + c + "," + viewMode + ")' title='Review permanent work offer'>" 
						f+= (WC_MDuty[c].PERM_ID==0) ? "NO" : "YES" ;
						f+="</A>"
					+"</td>"
					+"<td>"
						+ "<A href='javascript:parent.ModifiedDuty_SubDetails_Get(\"BNFT\"," + WC_MDuty[c].BEN_ID + "," + WC_MDuty[c].ID + "," + c + "," + viewMode + ")' title='Review Employee Benefits of the bringing worker back for modified duties'>"
						f += (WC_MDuty[c].BEN_ID == 0) ? "NO" : WC_MDuty[c].BEN_AMOUNT; 
						f+="</A>"
					"</td>"	
				}
				f+="<td>"
				+"<input name='MD_Note" + c + "' value='" + WC_MDuty[c].NOTE + "' maxlength='80' style='width:520px' onChange='parent.WC_MDuty[" + c + "].MOD=1;'>"	 
				+"</td>"
				f+="<td><span>" + WC_MDuty[c].CreBy + "</span></td>"
				f+="<td><span>" + WC_MDuty[c].ModBy + "</span></td>"		
				+"</tr>"
				c++
			}
		 }
		f+="</table></form></body></html>"
	return(f);
}

function ModifiedDuty_SubDetails_Get(Flag, FlagID, DutyID, c, viewMode) {
	WC_MDuty_SubDetails[0]=new WC_MDuty_SubData("",0,"","","","",0,"","")
	if(FlagID!=0){
		var s = WC_ModDuty_SubDetails_XTRCT(DutyID).split("^");
		WC_MDuty_SubDetails[0] = new WC_MDuty_SubData(s[0],s[1],s[2],s[3],s[4],s[5],s[6],s[7],s[8]);
	}
	switch(Flag){	
		case "BNFT":
		    ModifiedDuty_Benefit_Get(FlagID, DutyID, c, viewMode);
			break;			
		case "PRMNT":
		    ModifiedDuty_Permanent_Get(FlagID, DutyID, c, viewMode)
			break;
	}
}

function ModifiedDuty_Permanent_Get(PermID, DutyID, c, viewMode) {
	var f="";
		f="<html><head>"
		 +"<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 +"<title>workcomp_prmnt</title></head><body><form name='frmClaim'>"
		 +"<table  cellPadding=1 cellSpacing=10 border=0 width=300>"
		 +"<tr>"
		 +"<td align=middle>"
		 +"<input type='button' name='Save' value='Save' style='width:60px' class=BottonScreen onclick='parent.WC_ModifiedDuty_PrmntOffer(" + DutyID + "," + PermID + ","
												+"parent.frInfo.document.all[\"PrmntOffer\"].value,"
												+"parent.frInfo.document.all[\"PrmntAccept\"].value,"
												+"parent.frInfo.document.all[\"PrmntStart\"].value,"
												+"escape(parent.SingleQuote(parent.frInfo.document.all[\"PrmntType\"].value)),"
												+"escape(parent.frInfo.document.all[\"PrmntAmnt\"].value),"
												+"escape(parent.SingleQuote(parent.frInfo.document.all[\"PrmntNote\"].value))," + c + ")'>"
		 +"</td>"
		 +"<td align=middle>"
		 +"<input type='button' name='Back' value='Back' style='width:60px' class=BottonScreen onclick='parent.ScrollToTheLine(\"WC_ModDuty\",\"MD_Note"+c+"\"," + viewMode + ");'>"
		 +"</td>"		 
		 +"</tr>"
		 +"</table>"
		
		 +"<table border=0  cellPadding=1  width=95%>"
		 +"<caption><b>Permanent Work Offer</b></caption>"
		 + "<tr>"
				+"<td style='width:75px'>Offer Date:</td>"
				+"<td><input name='PrmntOffer' value='" + WC_MDuty_SubDetails[0].PRMNT_OFFER + "' maxlength='10' style='width:60px' onBlur='{this.value=parent.BillingDate(this.value,\"PrmntOffer\",\"regular\");}'></td>"
		 + "</tr><tr>"
				+"<td style='width:75px'>Accept Date:</td>"
				+"<td><input name='PrmntAccept' value='" + WC_MDuty_SubDetails[0].PRMNT_ACCEPT + "' maxlength='10' style='width:60px' onBlur='{this.value=parent.BillingDate(this.value,\"PrmntAccept\",\"regular\");}'></td>"	
		 + "</tr><tr>"
				+"<td style='width:75px'>Start Date:</td>"
				+"<td><input name='PrmntStart' value='" + WC_MDuty_SubDetails[0].PRMNT_START + "' maxlength='10' style='width:60px' onBlur='{this.value=parent.BillingDate(this.value,\"PrmntStart\",\"regular\");}'></td>"	
		 + "</tr><tr>"
				+"<td style='width:75px'>Pay Amount:</td>"
				+ "<td><input name='PrmntAmnt' value='" + WC_MDuty_SubDetails[0].PRMNT_AMOUNT + "' maxlength='9' style='width:90px' onBlur='{this.value=parent.format(parent.MyAmount(this.value),2);}'>"
				+ " - permanent Post Injury AWW (when reduced due to Modified Duty)</td>"	
		 + "</tr><tr>"
				+"<td style='width:75px'>Duty Type:</td>"
				+"<td><input name='PrmntType' value='" + WC_MDuty_SubDetails[0].PRMNT_TYPE + "' maxlength='50' style='width:50%' ></td>"	
		 + "</tr><tr>"
				+"<td style='width:75px'>Note:</td>"
				+"<td>"				
				+ "<input name='PrmntNote' value='" + WC_MDuty_SubDetails[0].PRMNT_NOTE + "' maxlength='180' style='width:100%' >"
				+ "</td>"
		 + "</tr>"
		 + "<table></form></body></html>"

		frInfo.document.open();
		frInfo.document.write(f);
		frInfo.document.close();
		frInfo.document.all["PrmntOffer"].focus();
}

function ScrollToTheLine(scrn,pos, viewMode){
	switch(scrn){		
		case "WC_ModDuty":
		    ModifiedDuty_Get(1, '', '', '', '', viewMode);	
			frInfo.document.all[pos].focus();break;
		case "FinDetails":			
			frInfo.document.all[pos].focus();break;
		case "workcomp":
			Display('workcomp');break;
		case "workcomp_ca":
			Display('workcomp_ca');break;
	}
}

function ModifiedDuty_Benefit_Get(BenID, DutyID, c,  viewMode){
	var f="";
		f="<html><head>"
		 +"<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 +"<title>workcomp_benft</Title></head><body><Form name='frmClaim'>"
		 +"<table  cellPadding=1 cellSpacing=10 border=0 width=300>"
		 +"<tr>"
		 +"<td align=middle>"
		 +"<input type='button' name='mdSave' value='Save' style='width:60px' class=BottonScreen onclick='parent.WC_ModifiedDuty_Benefits(" + DutyID + "," + BenID + ","
															+"escape(parent.SingleQuote(parent.frInfo.document.all[\"BType\"].value)),"
															+"parent.frInfo.document.all[\"BAmnt\"].value,"
															+"escape(parent.SingleQuote(parent.frInfo.document.all[\"BNote\"].value))," + c + ")'>"
		 +"</td>"
		 +"<td align=middle>"
		 + "<input type='button' name='mdBack' value='Back' style='width:60px' class=BottonScreen onclick='parent.ScrollToTheLine(\"WC_ModDuty\",\"MD_Note" + c + "\"," + viewMode + ");'>"
		  
		 +"</td>"		 
		 +"</tr>"
		 +"</table>"
		 
		 +"<table border=0  cellPadding=1  width=95%>"
		 +"<caption>Benefits of assigning worker to a modified duty</caption>"
		 + "<tr>"
				+"<td style='width:75px'>Amount:</td>"
				+"<td><input name='BAmnt' value='" + WC_MDuty_SubDetails[0].BNFT_AMNT + "' maxlength='12' style='width:90px' onBlur='{this.value=parent.format(parent.MyAmount(this.value),2);}'></td>"	
		 + "</tr><tr>"
				+"<td style='width:75px'>Type:</td>"
				+"<td><input name='BType' value='" + WC_MDuty_SubDetails[0].BNFT_TYPE + "' maxlength='50' style='width:50%'></td>"
		 + "</tr><tr>"
				+"<td style='width:75px'>Note</td>"
				+"<td><input name='BNote' value='" + WC_MDuty_SubDetails[0].BNFT_NOTE + "' maxlength='180' style='width:100%'></td>"
		 + "</tr>"
		 + "<table></form></body></html>"
		 
		frInfo.document.open();
		frInfo.document.write(f);
		frInfo.document.close();
		frInfo.document.all["BAmnt"].focus();
}

function OSHA_Get(){
	var s=WC_OSHA_XTRCT("","","","","","","","","","").split("^");
	OSHA_Populate(s[0],s[1],s[2],s[3],s[4],s[5],s[6],s[7],s[8],s[9]);
}

function OSHA_Populate(OSHA_Rec,OSHA_Sharps,OSHA_Privacy,OSHA_Mask,OSHA_Desc,OSHA_Time,OSHA_AMPM,OSHA_No,OSHA_HOSP,OSHA_EMER){
	var f="";
		f="<html><head>"
		 +"<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 +"<title>workcomp_osha</Title></head><body><Form name='frmClaim'>"
		 +"<table cellPadding=1 cellSpacing=10 border=0 width=300>"
		 +"<tr>"
		 +"<td align=middle>"		 
		 +"<input type='button' name='mdSave' value='Save' style='width:60px' class=BottonScreen onclick='parent.WC_OSHA_XTRCT(\"SAVE\","
															+"parent.OSHA_Rec,"
															+"parent.OSHA_Sharps,"
															+"parent.OSHA_Privacy,"
															+"parent.OSHA_Mask,"
															+"parent.OSHA_HOSP,"
															+"parent.OSHA_EMER,"
															+"escape(parent.SingleQuote(parent.frInfo.document.all[\"oshaNo\"].value)),"
															+"parent.frInfo.document.all[\"oshaTIME\"].value,"
															+"parent.OSHA_AMPM,"																												
															+"escape(parent.SingleQuote(parent.frInfo.document.all[\"oshaDESC\"].value))"																
															+")'>"
		 +"</td>"
		 +"<td align=middle>"		 
		 +"<input type='button' name='mdDelete' value='Delete' style='width:60px' class=BottonScreen onclick='parent.WC_OSHA_XTRCT(\"DEL\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\");parent.OSHA_Populate(\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\");'"
		 +"</td>"
		 +"<td align=middle>"
		 +"<input type='button' name='mdBack' value='Back' style='width:60px' class=BottonScreen onclick='parent.ScrollToTheLine(\"workcomp_ca\",\"\");'>"
		 +"</td>"		 
		 +"</tr>"
		 +"</table>"
		 
		 +"<table border=0  cellPadding=1  width=95%>"
		 +"<caption>OSHA report settings</caption>"
		 + "<tr>"
				+"<td style='width:75px'>OSHA Record:</td>"
				+"<td>"
				+"<input type='checkbox' name='oshaRec' value='' "
				f += (OSHA_Rec=="Y") ? " checked " : ""
				f+=" onclick='if(this.checked){parent.OSHA_Rec=\"Y\"}else{parent.OSHA_Rec=\"N\"}'>"
				+"</td>"	
		 + "</tr><tr>"
				+"<td style='width:75px'>Reportable:</td>"
				+"<td>"
				+"<input type='checkbox' name='oshaSHARPS' value='' "
				f += (OSHA_Sharps=="Y") ? " checked " : ""
				f+=" onClick='if(this.checked){parent.OSHA_Sharps=\"Y\"}else{parent.OSHA_Sharps=\"N\"}'>"
				+"</td>"
		 + "</tr><tr>"
				+"<td style='width:75px'>Privacy Flag:</td>"
				+"<td>"
				+"<input type='checkbox' name='oshaPRIVACY' value='' "
				f+= (OSHA_Privacy=="Y") ? " checked " : ""
				f+=" onClick='if(this.checked){parent.OSHA_Privacy=\"Y\"}else{parent.OSHA_Privacy=\"N\"}'>"
				+"</td>"
		+ "</tr><tr>"
				+"<td style='width:75px'>Masked Flag:</td>"
				+"<td>"
				+"<input type='checkbox' name='oshaMASK' value='' "
				f+= (OSHA_Mask=="Y") ? " checked " : ""
				f+=" onClick='if(this.checked){parent.OSHA_Mask=\"Y\"}else{parent.OSHA_Mask=\"N\"}'>"
				+"</td>"
		+ "</tr><tr>"
				+"<td style='width:75px'>OSHA #:</td>"
				+"<td><input name='oshaNo' value='" + OSHA_No + "' maxlength='10' style='width:100px'></td>"
		+ "</tr><tr>"
				+"<td style='width:75px'>Time Employee began work:</td>"
				+"<td>"
				+"<input name='oshaTIME' value='" + OSHA_Time + "' style='width: 40px'  maxlength='5' onBlur='{this.value=parent.MyTime(this.value);}'>"
				+ "<input TYPE='radio' Name='oshaAMPM' value='A' onClick='parent.OSHA_AMPM=\"A\"'"
				f += (OSHA_AMPM=='A') ? " checked " : ""
				f += ">AM"
				+ "<input TYPE='radio' Name='oshaAMPM' value='P' onClick='parent.OSHA_AMPM=\"P\"'" 
				f += (OSHA_AMPM=='P') ? " checked " : ""
				f += ">PM"
		 + "</TD>"	
				+"</td>"
		+ "</tr><tr>"
				+"<td style='width:75px'>Description:</td>"
				+"<td><input name='oshaDESC' value='" + OSHA_Desc + "' maxlength='80' style='width:80%'></td>"				
		+ "</tr><tr>"
				+"<td style='width:75px'>Hospitalized:</td>"
				+"<td>"
				+"<input type='checkbox' name='oshaHOSP' value='' "
				f+= (OSHA_HOSP=="Y") ? " checked " : ""
				f+=" onClick='if(this.checked){parent.OSHA_HOSP=\"Y\"}else{parent.OSHA_HOSP=\"N\"}'>"
				+"</td>"
		+ "</tr><tr>"
				+"<td style='width:75px'>Treated in emergency room:</td>"
				+"<td>"
				+"<input type='checkbox' name='oshaEMER' value='' "
				f+= (OSHA_EMER=="Y") ? " checked " : ""
				f+=" onClick='if(this.checked){parent.OSHA_EMER=\"Y\"}else{parent.OSHA_EMER=\"N\"}'>"
				+"</td></tr>"
		 + "<table></form></body></html>"
		 
		frInfo.document.open();
		frInfo.document.write(f);
		frInfo.document.close();
		frInfo.document.all["oshaNo"].focus();

		frButtons.document.frmButtons.cmdEdit.disabled = true;
}

function Settlement_Get(parentScreen) {
    var c = 1;
    var f = "";
    
    if (Settlement_Get.count == 0) {//records have to be retrieved;
        Settlement_Get.count++;
        
        try { eval('Var_Save_' + parentScreen + '();') } catch (e) { };
        try { Variable_Populate(parentScreen); } catch (e) { };

        Settlement_XTRCT("GET", parentScreen, -1, 0, "", "", 0.00, false, "");
    } else {
        f = "<html><head>"
		     + "<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		     + "<title>settlement</Title></head><body><Form name='frmClaim'>"
		     + "<table  cellPadding=1 cellSpacing=10 border=0 width=500>"
		     + "<tr>"
		     + "<td align=middle>"
		     + "<input type='button' name='mdSave' value='Save' style='width:60px' class=BottonScreen onclick='parent.Settlement_XTRCT(\"SAVE\",\"" + parentScreen + "\",parent.frInfo.document.frmClaim.STMT_Type0.value, parent.frInfo.document.frmClaim.STMT_Amount0.value, parent.frInfo.document.frmClaim.STMT_Date0.value, parent.frInfo.document.frmClaim.STMT_FundDate0.value, parent.frInfo.document.frmClaim.STMT_WeekNum0.value, parent.frInfo.document.frmClaim.STMT_TPOC0.checked,parent.frInfo.document.frmClaim.STMT_CmsNoticeDate0.value)'>"
		     + "</td>"
		     + "<td align=middle>"
		     + "<input type='button' name='mdExcel' value='Export to Excel' style='width:120px' class=BottonScreen onclick='parent.ModifiedDuty_Excel(this.form.all[\"SettlHist\"], \"SettlHist\", null, \"\", \"SETTLEMENT HISTORY\")'>"
		     + "</td>"
		     + "<td align=middle>"
		     + "<input type='button' name='mdBack' value='Back' style='width:60px' class=BottonScreen onclick='parent.Display(\"" + parentScreen + "\");'>"
		     + "</td>"
		     + "<td align=right>"
		        + "<span style='color:blue'><b>PD Rating: " + format(PDRating,2) + "</b></span>"
		     + "</td>"
		     
		     + "</tr>"
		     + "</table>"

		     + "<table id='SettlHist' border=0  cellPadding=1  width=100%>"
		     + "<caption>"
        f += "</caption>"
        f += "<tr>"
				    + "<td>#</td>"
				    + "<td>DELETE</td>"
				    + "<td>SETTLEMENT TYPE</td>"
				    + "<td>AMOUNT</td>"
				    + "<td>DATE</td>"
				    + "<td title='Funding Delayed Beyond TPOC Start Date.  If funding for the TPOC amount is delayed, provide actual or estimated date of funding.  This is not the same as the date of payment (check date).'>"
				        + "FUNDING DATE</td>"
				    + "<td>WEEKS #</td>"
				    + "<td title='TPOC generally reflects a [one-time] or [lump sum] payment of a settlement, judgment, award, or other payment intended to resolve/partially resolve a claim.  If ongoing responsibility for medical (ORM) is indicated, the TPOC settlement should have the effect of releasing the insurer from ORM'>"
				        + "TPOC"
                        + "<img id='imgTROC' src='/portal/startup/Images/nav/help.gif' style='cursor:hand' onclick='parent.helpShowHelp(\"Portal/NET/LitigationLog/LitLogMM/txtTROCDate\")' title='Help'>"
                        + "</td>"	 
				    + "<td>CMS NOTICE DATE</td>"
				    +"<td>CREATED</td>"
			    + "</tr>"
			    + "<tr>"
				    + "<td><span id=STMT_ID>new</td>"
				    + "<td>=X=</td>"
				    + "<td>"
				    + "<select name='STMT_Type0' "
        f += (SettlType.indexOf("<option value=''></option>") > -1) ? "" : " onClick='parent.SettlType_XTRCT(\"WC\")' onChange='if(this.value==\"\" || this.value==\"0\"){/*parent.frInfo.document.frmClaim.settlement_date.value=\"\";parent.frInfo.document.frmClaim.settlement_funding_date.value=\"\";*/}';"
        f += ">"
        f += (SettlType == '') ? SettlType_XTRCT('WC') : SettlType
        f += "</SELECT>"
				    + "</td>"
                    + "<td >"
				    + "<input name='STMT_Amount0' value='' maxlength='12' style='width:80px' onBlur='if(parent.MyNumber(parent.decommafy(this.value,1))==\"\"){this.value=0}else{this.value=parent.commafy(this.value);}'>"
				    + "</td>"
                    + "<td>"  
                    + "<input name='STMT_Date0' value='' maxlength='10' style='width:60px' onBlur='{this.value=parent._DOB_Date(this.value,this);this.value=parent.val_settlement_date(this.value);}'>"
				    + "</td>"
                    + "<td>"
				    + "<input  name='STMT_FundDate0' value='' maxlength='10' style='width:60px' onBlur='{this.value=parent.val_settlement_funding_date(this.value, \"funding\");}'>"
				    + "</td>"
                    + "<td >"
				    + "<input  name='STMT_WeekNum0' value='' maxlength='7' style='width:48px' onBlur='{this.value=parent.format(parent.MyAmount(this.value),2); if(!isNaN(this.value)){if(this.value>=5000){alert(\"Value of this field should be less than 5000\");this.value=\"\";this.focus();}};}'>"
				    + "</td>"
                    + "<td >"
				    + "<input  name='STMT_TPOC0'  type='checkbox' value=''>"
				    + "</td>"
                   + "<td>"
				    + "<input  name='STMT_CmsNoticeDate0'  value='' maxlength='10' style='width:60px' onBlur='{this.value=parent.val_settlement_funding_date(this.value, \"CMS notice\");}'>"
				    + "</td>"
				    + "<td>=X=</td>"
			    + "</tr>"
        while (SettlList.length > c) {
            f += "<tr>"
				    + "<td>" + c + "</td>"
                    + "<td>"
				    + "<input type='checkbox' name='STMT_Del" + c + "' value='' onClick='parent.SettlList[" + c + "].DEL=(this.checked)?1:0;'>"
				    + "</td>"
				    + "<td>"
				    + "<span id='STMT_Type" + c + "'>" + SettlList[c].TYPE + "</span>"
                    + "</td>"
				    + "<td>"
				    + "<span id='STMT_Amount" + c + "'>" + SettlList[c].AMOUNT + "</span>"
                    + "</td>"
				    + "<td>"
				    + "<span id='STMT_Date" + c + "'>" + SettlList[c].DATE + "</span>"
                    + "</td>"
				    + "<td>"
				    + "<span id='STMT_FundDate" + c + "'>" + SettlList[c].FUNDDATE + "</span>"
                    + "</td>"
				    + "<td >"
				    + "<span id='STMT_WeekNum" + c + "'>" + format(SettlList[c].WEEKNUM, 2) + "</span>"
                    + "</td>"
				    + "<td >"
				    + "<span id='STMT_TPOC" + c + "'>"
                f += (SettlList[c].TPOC == 'Y') ? "Yes" : "No"
                f += "</span>"
                    + "</td>"
				    + "<td >"
				    + "<span id='STMT_CmsNoticeDate" + c + "'>" + SettlList[c].CmsNoticeDate + "</span>"
                    + "</td>"                    
				    + "<td>"
				    + "<span id='STMT_CreBy" + c + "'>" + SettlList[c].CREBY + "</span>"
                    + "</td>"
			    + "</tr>"

            c++
        }
        f += "</table></form></body></html>"

        frInfo.document.open();
        frInfo.document.write(f);
        frInfo.document.close();

        frButtons.document.frmButtons.cmdEdit.disabled = true;
    }
}

function WC_Employer_Get(Action, _EmployerType, _EmployerNo, _Key, _Sic_Code) {
	if(typeof(_EmployerNo)=="undefined"){
		if(_EmployerType=='C'){_EmployerNo=ClientNumber;}
	}
	var s=WC_Employer_XTRCT(Action, _EmployerType, _EmployerNo, _Key, _Sic_Code).split("^");
	if(typeof(s)=="undefined"){
	}else{
		if(Action=='SAVE'){
			alert('Change has been saved')
			Employer= _EmployerType;
			EmployerNo= _EmployerNo;
		}

		WC_Employer_Populate(s[0],s[1],s[2],s[3],s[4],s[5],s[6],s[7],s[8],s[9]);
		frInfo.document.frmClaim.SIC_CODE.value = s[7];
	}

	//only Supervisor can change Employer: noboby will be able to modify employee
    //frInfo.document.getElementById("keyTable").style.display = (CanChangeSuper() == false) ? "none" : "inline";
}

function WC_Employer_Populate(Name,Addr1,Addr2,City,State,Zip,TaxID,SIC_CODE,EmployerType,EmployerNo){
	var f="";
		f="<html><head>"
		 +"<link  rel='stylesheet' href='ProjectSS.CSS' type='text/css'>"
		 +"<title>workcomp_employer</Title></head><body><Form name='frmClaim'>"
		 +"<table cellPadding=1 cellSpacing=10 border=0 width=300>"
		 +"<tr>"
		 +"<td align=middle>"		 
		 +"<input type='button' name='mdSave' value='Save' style='width:60px' class=BottonScreen onclick='parent.WC_Employer_Get(\"SAVE\","
															+"parent.frInfo.document.all[\"EmployerType\"].value,"
															+"parent.frInfo.document.all[\"EmployerNo\"].value,"															
															+"\"\",parent.frInfo.document.all[\"SIC_CODE\"].value)'>"
		 +"</td>"
		 +"<td align=middle>"
		 +"<input type='button' name='mdBack' value='Back' style='width:60px' class=BottonScreen onclick='parent.ScrollToTheLine(\"workcomp\",\"\");'>"
		 +"</td>"		 
		 +"</tr>"
		 +"</table>"
		 +"<BR>"
		 +"<table border=0  cellPadding=1  width=95%>"
		 +"<caption align=left>EMPLOYER:</caption>"
		 + "<tr>"
				+"<td style='width:75px'>Name:</td>"
				+"<td>"
				+"<span id=Name>" + Name + "</span>"
				+"</td>"	
		 + "</tr><tr>"
				+"<td style='width:75px' rowspan=2>Address:</td>"
				+"<td>"
				+ "<span id=Addr1>" + Addr1 + "</span>"
				+"</td>"
		 + "</tr><tr>"
				+"<td>"
				+ "<span id=Addr2>" + Addr2 + "</span>"
				+"</td>"
		+ "</tr><tr>"
				+"<td style='width:75px'>City:</td>"
				+"<td>"
				+ "<span id=City>" + City + "</span>"
				+ "&nbsp;&nbsp; State:<span id=State>" + State + "</span>"
				+ "&nbsp;&nbsp; Zip:<span id=Zip>" + Zip + "</span>"
				+"</td>"
		+ "</tr><tr>"
				+"<td style='width:75px'>TaxID:</td>"
				+"<td>"
				+ "<span id=TaxID>" + TaxID + "</span>"
				+"</td>"
		+ "</tr><tr>"
				+"<td style='width:75px'>SIC code:</td>"					
				+ "<td><select name=SIC_CODE>" + WC_SIC_CODE_LIST + 
				+ "</select></td>"				
		+ "</tr>"
		 + "</table>"
		 + "<input type=hidden name='EmployerType' value='" + EmployerType + "'>"
		 + "<input type=hidden name='EmployerNo' value='" + EmployerNo + "'>"

		 + "</form></body></html>"
		 
		frInfo.document.open();
		frInfo.document.write(f);
		frInfo.document.close();	
}

function isItArray(o) {
    return Object.prototype.toString.call(o) === "[object Array]"
}

////////////  send email  /////////////

function portalSendEMail(sTo, sSubject, sHTMLToSend, sBodyText, sFilePath, sFileTitle, additionToBody, relatedTo, relationType) {
            mmSendEMail(sTo, sSubject, sHTMLToSend, sBodyText) //, sFilePath, sFileTitle, additionToBody, relatedTo, relationType) 
}

////////// END POLICY CC SECTION  ///////

    function MyInsName(i_pass) {
        frInfo.document.frmClaim.txtInsName.value = i_pass
    }

	//////////////////// Claim MODE  ////////////////////////////////
	
	function ClaimMode_Populate() {
	    var c = "Mode:"
	    //CLAIM MODE: L - delayed; N - denied; A - accepted (default)   //
	    c += "<u><span style='color:blue;cursor:hand' onClick='parent.WC_Mode_XTRCT()'>" + ClaimModeDescr(ClmMode) + "</span></u>"
       	    c += (ModeAppr != '') ? "&nbsp;<span id='ModeDate'>since " + ModeAppr + "</span>" : "<span id='ModeDate'></span>"
	        c += (ModePending != '') ? "&nbsp;<span id='ModePending'> Pending Approval: " + ClaimModeDescr(ModePending) + "</span>" : "<span id='ModePending'></span>"

	    return (c)
	}

function ClaimModeDescr(mode){
	var ModeDescr='';
	switch(mode){
			case "A": ModeDescr="ACCEPTED"; break;
			case "L": ModeDescr="DELAYED"; break;
			case "N": ModeDescr = "DENIED"; break;
			case "R": ModeDescr = "DENIED-ACCEPTED"; break;
	}
	return(ModeDescr);
}

	function ClearScreen(){
		var c =frInfo.document.body.getElementsByTagName("INPUT");
		var i=0
		while(i<c.length){		
			c[i].value='';
			i++
		}
		i=0
		var s =frInfo.document.body.getElementsByTagName("SELECT");
		while(i<s.length){		
			s[i].value='';
			i++
		}
	}

	function PrintScreen(pass){
		frPrint.document.open();
		
		var f=''
		f+="<html><head><title>Claim " + ClaimNumber + "</title> "
		+ "<link  rel='stylesheet' href='ProjectSS_print.CSS' type='text/css'>"
		+ "</head><body><FORM name='fPrint'>"
		f+=frGeneral.document.frmGeneral.innerHTML;			
		f+=frInfo.document.frmClaim.innerHTML;				
		f+="</form></body></html>"
		
		frPrint.document.write(f);		
		frPrint.document.close();
		
		var i=0; var c=0; var nm = ''; var Opt;
		var Sel=new Array(); 
		Sel = frPrint.document.body.getElementsByTagName("SELECT");

		while(i<Sel.length){		
			c=Sel[i].selectedIndex			
				if(c==-1){		
					nm = ''					
				}else{		
					nm = Sel[i].options[Sel[i].selectedIndex].text;
				}	
				Sel[i].outerHTML = "<span id='input_select'>" + nm  + "</span>"
	
			//i++  don't un-comment 
		}
		
		frPrint.focus();
		setTimeout("RunPrint()", 200)		
	}
	
	function RunPrint(){		
		frPrint.focus();		
		frPrint.print()
		
		frPrint.document.open();
		frPrint.document.close();
	}

	function GenericCustomFieldsSave() {
	    var retVal = true;

	    if (CustomFields_wasInitialized()) {
	        var isEditableFlag = (UserRole.indexOf('EditFile') != -1) ? true : false;
	        if (isEditableFlag) {
	            var wasDataSaved = CustomFields_onDataSave(isEditableFlag);
	        }
	        if (!wasDataSaved) { retVal = false; };
	    }
	    return (retVal);
	}

	function objectExtend(parent, child, property) {
	    var p;
	    var i,
            toStr = Object.prototype.toString,
            astr = "[object Array]";

	    child = child || {};

	    for (i in parent) {
	        if (parent.hasOwnProperty(i)) {
	            if (property == i || typeof property == 'undefined') {
	                if (typeof parent[i] === 'object') {
	                    child[i] = (toStr.call(parent[i]) === astr) ? [] : {};
	                    objectExtend(parent[i], child[i]);
	                } else {
	                    p = parent[i];
	                    child[i] = p;
	                }
	            }
	        }
	    }
	}

	function isItArray(o) {
	    return Object.prototype.toString.call(o) === "[object Array]"
	}

