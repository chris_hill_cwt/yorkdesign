

 function Address() {
     //basic fileds

     this.AddressValidationLevels = {
            None:0,
            CountryIsOptional: 1,
            Zip : 2,
            Google: 3
     }

	this.Title="Address and Contact Info";
	this.Addr1=null;
	this.Addr2=null;
	this.City=null;
	this.State=null;               // two char for capability
	this.PostalCode=null;
	this.County=null;
	this.Country=null;
	this.CountryName=null;

	this.Phone1=null;
	this.Phone1Type=null;
	this.Phone1Ext=null;
	
	this.Phone2=null;
	this.Phone2Type=null;
	this.Phone2Ext=null;
		
	this.Fax=null;
	this.Email=null;
	this.Contact=null;
   
	this.Edit = _addrEdit;
	// basic fileds with  'Len' suffix
	// basic fileds with 'Required'
    
    
    // Release11
	this.AddrValidationLevel = this.AddressValidationLevels.Google;       // google

	this.StateID = null;
	this.StateName = null;


	this.GetFullAddress = function (linesSeparator) {
	    var html = this.Addr1
                + (this.Addr2.length == 0 ? this.Addr2 : linesSeparator + this.Addr2)
                + linesSeparator + this.City + ", " + this.StateName + "  " + this.PostalCode
                + linesSeparator + this.CountryName;
	    
        return html;
	}



}


function  _addrEdit () {
	var params="Title=" + escape(this.Title);
    params +=  _addrAddPar("Addr1", this.Addr1);
    params +=  _addrAddPar("Addr1Len", this.Addr1Len);
    params +=  _addrAddPar("Addr1Req", this.Addr1Required);

    params +=  _addrAddPar("Addr2", this.Addr2);
    params +=  _addrAddPar("Addr2Len", this.Addr2Len);
    params +=  _addrAddPar("Addr2Req", this.Addr2Required);

    params +=  _addrAddPar("City", this.City);
    params +=  _addrAddPar("CityLen", this.CityLen);
    params +=  _addrAddPar("CityReq", this.CityRequired);

    params +=  _addrAddPar("State", this.State);
    params +=  _addrAddPar("StateLen", this.StateLen);
    params +=  _addrAddPar("StateReq", this.StateRequired);

    params +=  _addrAddPar("PostalCode", this.PostalCode);
    params +=  _addrAddPar("PostalCodeLen", this.PostalCodeLen);
    params += _addrAddPar("PostalCodeReq", this.PostalCodeRequired);

    params +=  _addrAddPar("County", this.County);
    params +=  _addrAddPar("CountyLen", this.CountyLen);
    params +=  _addrAddPar("CountyReq", this.CountyRequired);

    params +=  _addrAddPar("Country", this.Country);
    params +=  _addrAddPar("CountryName", this.CountryName); //not used on.aspx
    params +=  _addrAddPar("CountryReq", this.CountryRequired);

    params +=  _addrAddPar("Phone1", this.Phone1);
    params +=  _addrAddPar("Phone1Len", this.Phone1Len);
    params +=  _addrAddPar("Phone1Req", this.Phone1Required);

    params +=  _addrAddPar("Phone1Type", this.Phone1Type);
    params +=  _addrAddPar("Phone1TypeReq", this.Phone1TypeRequired);

    params +=  _addrAddPar("Phone1Ext", this.Phone1Ext);
    params +=  _addrAddPar("Phone1ExtLen", this.Phone1ExtLen);

    params +=  _addrAddPar("Phone2", this.Phone2);
    params +=  _addrAddPar("Phone2Len", this.Phone2Len);
    params +=  _addrAddPar("Phone2Req", this.Phone2Required);

    params +=  _addrAddPar("Phone2Type", this.Phone2Type);
    params +=  _addrAddPar("Phone2TypeReq", this.Phone2TypeRequired);

    params +=  _addrAddPar("Phone2Ext", this.Phone2Ext);
    params +=  _addrAddPar("Phone2ExtLen", this.Phone2ExtLen);

    params += _addrAddPar("Fax", this.Fax);
    params += _addrAddPar("FaxLen", this.FaxLen);

    params += _addrAddPar("Email", this.Email);
    params += _addrAddPar("EmailLen", this.EmailLen);
    params +=  _addrAddPar("EmailReq", this.EmailRequired);

    params += _addrAddPar("Contact", this.Contact);
    params += _addrAddPar("ContactLen", this.ContactLen);
    params += _addrAddPar("ContactReq", this.ContactRequired);

    params += _addrAddPar("AddrValidationLevel", this.AddrValidationLevel);
    params += _addrAddPar("StateID", this.StateID);

    var result = window.showModalDialog("/portal/net/shared/GeneralPages/AddressControl/AddressControl.aspx?" + params, this,
                         "DialogWidth:550px;DialogHeight:450px;status:no;toolbar:0;scrollbars:0;resizable:1") 
   
   if (result==null  || result == false)    return false;
   return true;
   
}

function _addrAddPar(name,value) {
   return 	 (value !=null) ?  "&" + name + "=" + escape(value) :	 "";
}


