//////////////////// policyCC  ////////////////////////////////
   
    function polCCinfo_Driver(){
            var p="<table>"
               + "<tr>"
            if (PolicyCC.DRVR.length == 0) {
                 p +="<td style='COLOR: blue'>Not Selected</td>"
            } else {
                p +=""
                +"<td>Eff Date:</td>"
		        + "<td style='COLOR: blue'>" + PolicyCC.DRVR[0].EffDT + "</td>"
 		        + "<td>Exp Date:</td>"
		        + "<td style='COLOR: blue'>" + PolicyCC.DRVR[0].ExpDT + "</td>"

		        + "<td>Name:</td>"
		        + "<td style='COLOR: blue'>" + PolicyCC.DRVR[0].FName + " " + PolicyCC.DRVR[0].LName + "</td>"
		        + "<td>License#:</td>"
		        + "<td style='COLOR: blue' >" + PolicyCC.DRVR[0].LicNO + "</td>"
		        + "<td>Lic State:</td>"
		        + "<td style='COLOR: blue'>" + PolicyCC.DRVR[0].LicState + "</td>"
            }
		        p+= "</tr>"
		        + "</table>"
		        
		        return (p);
    }

    function polCCinfo_Vehicle(){
        var p="<table>"
		        + "<tr>"
            if (PolicyCC.VEH.length == 0) {
                 p +="<td style='COLOR: blue'>Not Selected</td>"
            } else {
                p +=""
 		        + "<td>Eff Date:</td>"
		        + "<td style='COLOR: blue'>" + PolicyCC.VEH[0].EffDT + "</td>"
 		        + "<td>Exp Date:</td>"
		        + "<td style='COLOR: blue'>" + PolicyCC.VEH[0].ExpDT + "</td>"
		        
		        + "<td>VIN#:</td>"
		        + "<td style='COLOR: blue'>" + PolicyCC.VEH[0].VIN + "</td>"
		        + "<td>Make of:</td>"
		        + "<td style='COLOR: blue' >" + PolicyCC.VEH[0].Make + "</td>"
		        + "<td>Model:</td>"
		        + "<td style='COLOR: blue' >" + PolicyCC.VEH[0].Model + "</td>"
		        + "<td>Year:</td>"
		        + "<td style='COLOR: blue'>" + PolicyCC.VEH[0].Year + "</td>"
		        + "<td>Plate:</td>"
		        + "<td style='COLOR: blue'>" + PolicyCC.VEH[0].Plate + "</td>"
            }
            p += "</tr>"
		        + "</table>"

            return (p);
    }

    function polCCinfo_Coverage(CovList) {
//debugger
        var c = 0
        var p="<table>"
        if (CovList.length == 0) {
            p += "<tr><td style='COLOR: blue'>Not Selected</td>"
        } else {
            while (c < CovList.length) {
                p += "<tr>"
                    + "<td style='COLOR: blue'>" + CovList[c].Cd + " " + CovList[c].Nm + ":</td>"
 		            + "<td>Eff Date:</td>"
		            + "<td style='COLOR: blue'>" + CovList[c].EffDT + "</td>"
 		            + "<td>Exp Date:</td>"
		            + "<td style='COLOR: blue'>" + CovList[c].ExpDT + "</td>"

		            + "<td>Occur Limit:</td>"
		            + "<td style='COLOR: blue'>" + CovList[c].OcLimit + "</td>"
		            + "<td>Feature Limit:</td>"
		            + "<td style='COLOR: blue' >" + CovList[c].FeLimit + "</td>"
		            + "</tr>"
                c++
            }
        }
        p += "</table>"

        return (p);
    }

    function polCCinfo_Property(){
        var p="<table>"
		        + "<tr>"
            if (PolicyCC.PROP.length == 0) {
                 p +="<td style='COLOR: blue'>Not Selected</td>"
            } else {
                p +="<td style='COLOR: blue'>" + PolicyCC.PROP[0].Desc + "</td>"
            }
            p += "</tr>"
		        + "</table>"

            return (p);
    }


    function Populate_polCCinfo() {
        var cov = new Array();

        var p = ""
        + "<html><head>"
        + "<link rel='STYLESHEET' href='ProjectSS.CSS' type='TEXT/CSS' />"
        + "<title>polCCinfo</title></head><body>"
        + "<form name='frmClaim'>"
        if (PolicyCC.hasOwnProperty('id')) {
            p += "<table border=0 width=800px>"
             + "<tr>"
            if (SelfInsuredPrefix == 'Y') {
                p += "<td><b>Prefix:</b>" + Prefix + " - Self-Insured</td>"
                 + "<td><b>Excess Policy id:</b>" + PolicyCC.id + "</td>"
            } else {
            p += "<td colspan=2><b>Policy id:</b>" + PolicyCC.id + "" + "</td>"
            }
            p += "<td><b>Type:</b>"
           switch(PolicyCC.Type){
                    case "WC":
                        p += 'Workcomp'
                            break;
                    case "PR":
                        p += 'Property'
                            break;
                    case "CA":
                        p += 'Casualty'
                            break;
                    case "AL":
                        p += 'Auto'
                            break;
                    }
            p += "</td>"
            + "<td><b>Active:</b>" + PolicyCC.Active + ""
            + "</td>"            
//             + "<td><b>Scheduled:</b>"
//            p += (PolicyCC.Shdl == 1) ? " Yes " : " No "
//            p += "</td>"
             + "<td><b>Name:</b>" + PolicyCC.PolNo + "</td>"
             + "<td><b>Period:</b>" + PolicyCC.Eff + ' - ' + PolicyCC.Exp + "</td>"
             + "<td><u><span id='PolicyCCView' style='color:blue;cursor:hand' onclick='parent.PolicyCC_View(" + PolicyCC.id + ")'>Review</span></u>"
             + "&nbsp; &nbsp; &nbsp;"
            if (PolicyCC.Docs == 'Y') {
                p += "<u><span id='PolicyCCDocs' style='color:blue;cursor:hand' onclick='parent.ShowPolicyDocuments(" + PolicyCC.id + ",false)'>Documents</span></u>"
            }
            p += "</td>"
             + "</tr>"
             + "</table>"
            if (PolicyCC.Carrier.Name == null) {
                 p+="<table border=0 width=800px>"
                 + "<tr>"
                 + "<td><b> Carrier:</b> Undefined </td>"
                 + "</tr><table>"
            } else {
                 p+="<table border=0 width=800px>"
                 + "<tr>"
                 + "<td><b> Carrier:</b></td>"
                 + "<td>" + PolicyCC.Carrier.Name + ", taxID:" + PolicyCC.Carrier.TaxID + "." + "</td>"
                 + "<td align=right><b>Address:</b></td>"
                 + "<td colspan=3 align=left>" + PolicyCC.Carrier.Addr1 + ' ' + PolicyCC.Carrier.Addr2 + ' ' + PolicyCC.Carrier.City + ', ' + PolicyCC.Carrier.State + ' ' + PolicyCC.Carrier.Zip + "</td>"
                 + "</tr>"
                 + "</table>"
            }
        }   
        //scheduled auto                                 
        if (PolicyCC.Type == 'AL' && PolicyCC.Shdl == 1) {
            // List of Coverage codes is associated with  Vehicle
            if(typeof(PolicyCC.VEH[0])!="undefined"){
                cov  = PolicyCC.VEH[0].COV;
            }
            p+= "<table border=0>"
             + "<tr><td> <input type='button'  id='btnDriverGet' name='btnDriverGet' value='Excluded Driver:' style='WIDTH:90px;HEIGHT:20px;' class=BottonScreen onClick='parent.LookupWindow_PolicyCC(\"Drvr\", 1)' />"
             + "</td><td>"
            p += polCCinfo_Driver()
            p += "</td></tr>"
             + "<tr><td> <input type='button' id='btnVehicleGet' name='btnVehicleGet' value='Vehicle:' style='WIDTH:90px;HEIGHT:20px;' class=BottonScreen onClick='parent.LookupWindow_PolicyCC(\"Veh\", 1)' />"
             + "</td><td>"
            p += polCCinfo_Vehicle()
            p += "</td></tr>"
             + "<tr><td> <input type='button' id='btnCovGet' name='btnCovGet' value='Coverage:' style='WIDTH:90px;HEIGHT:20px;' class=BottonScreen onClick='parent.LookupWindow_PolicyCC(\"Cov\", 1)' />"
             + "</td><td>"
            p += polCCinfo_Coverage(cov);
            p += "</td></tr>"             
             + "</table>" 
        }
        //auto
        if (PolicyCC.Type == 'AL' && PolicyCC.Shdl == 0) {
            // List of Coverage codes is associated with Policy 
            if(typeof(PolicyCC.COV[0])!="undefined"){
                cov  =  PolicyCC.COV;
            }
            p += "<table border=0>"
             + "<tr><td> <input type='button' id='btnDriverGet' name='btnDriverGet' value='Excluded Driver:' style='WIDTH:90px;HEIGHT:20px;' class=BottonScreen onClick='parent.LookupWindow_PolicyCC(\"Drvr\", 0)' />"
             + "</td><td>"
            p += polCCinfo_Driver()
            p += "</td></tr>"
             + "<tr><td> <input type='button' id='btnVehicleGet' name='btnVehicleGet' value='Vehicle:' style='WIDTH:90px;HEIGHT:20px;' class=BottonScreen onClick='parent.LookupWindow_PolicyCC(\"Veh\", 0)' />"
             + "</td><td>"
            p += polCCinfo_Vehicle()
            p += "</td></tr>"
             + "<tr><td> <input type='button' id='btnCovGet' name='btnCovGet' value='Coverage:' style='WIDTH:90px;HEIGHT:20px;' class=BottonScreen onClick='parent.LookupWindow_PolicyCC(\"Cov\", 0)' />"
             + "</td><td>"
            p += polCCinfo_Coverage(cov)
             + "</td></tr>"
             + "</table>" 
        }

        //property scheduled
        if (PolicyCC.Type == 'PR' && PolicyCC.Shdl == 1) {
            // List of Coverage codes is associated with a property
            if(typeof(PolicyCC.PROP[0])!="undefined"){
                cov  = PolicyCC.PROP[0].COV;
            }
            p += "<table border=0>"
             + "<tr><td> <input type='button' id='btnPropGet' name='btnPropGet' value='Property:' style='WIDTH:60px;HEIGHT:20px;' class=BottonScreen onClick='parent.LookupWindow_PolicyCC(\"Prop\", 1)' />"
             + "</td><td>"
            p += polCCinfo_Property()
            p += "</td></tr>"
             + "<tr><td> <input type='button' id='btnCovGet' name='btnCovGet' value='Coverage:' style='WIDTH:60px;HEIGHT:20px;' class=BottonScreen onClick='alert(\"List of Codes is Property-specific\")' />"
             + "</td><td>"
            p += polCCinfo_Coverage(cov)
            p += "</td></tr>"
             + "</table>" 
        }

        //property
        if (PolicyCC.Type == 'PR' && PolicyCC.Shdl == 0) {
            // List of Coverage codes is associated with Policy 
            if(typeof(PolicyCC.COV[0])!="undefined"){
                cov  = PolicyCC.COV;
            }
            p += "<table border=0>"
              + "<tr><td> <input type='button' id='btnPropGet' name='btnPropGet' value='Property:' style='WIDTH:60px;HEIGHT:20px;' class=BottonScreen onClick='parent.LookupWindow_PolicyCC(\"Prop\", 0)' />"
             + "</td><td>"
            p += polCCinfo_Property()
            p += "</td></tr>"
             + "<tr><td> <input type='button'  id='btnCovGet'  name='btnCovGet' value='Coverage:' style='WIDTH:60px;HEIGHT:20px;' class=BottonScreen onClick='alert(\"List of Codes is Policy-specific\")' />"
             + "</td><td>"
            p += polCCinfo_Coverage(cov)
            p += "</td></tr>"
             + "</table>"
        }

        // casualty  
        if (PolicyCC.Type == 'CA') {
            // List of Coverage codes is associated with Policy 
            if(typeof(PolicyCC.COV[0])!="undefined"){
                cov  = PolicyCC.COV;
            }
            p += "<table border=0>"
             + "<tr><td> <input type='button' id='btnCovGet'  name='btnCovGet' value='Coverage:' style='WIDTH:60px;HEIGHT:20px;' class=BottonScreen onClick='parent.LookupWindow_PolicyCC(\"Cov\", 0)' />"
             + "</td><td>"
             + polCCinfo_Coverage(cov)
             + "</td></tr>"
             + "</table>" 
        }

        // workcomp
        if (PolicyCC.Type == 'WC') {
            p += "<table border=0>"
             + "<tr><td></td></tr>"
             + "</table>"
        }
        p += "</br> <div id='PolicyFinMessage' style='color:red' overflow:auto>"
        p += (typeof(PolicyCC.FIN.Mes) != "undefined") ? PolicyCC.FIN.Mes : ''
        p += "</div>"

        p += "</br> <div id='PolicyFinLimits' overflow:auto>"  
        p  += (PolicyCC.Type == 'PR') ? PolicyCC_Property_FinLimits() : PolicyCC_NonProperty_FinLimits()   
        p  += "</div>"

        p +="</FORM></BODY></HTML>"
        
          return (p);
    }


    function ShowPolicyDocuments(policyID, canEdit) {
        var wndParams = "alwaysRaised=1,dependent=yes,help=no,status=no,resizable=yes,menubar=no";
        var keyType = 2;
        var keyValue = policyID;
        var readonlyFlag = canEdit ? "" : "&ReadOnly=true";
        var configOptions = "&EnableLinkDocuments=true";
        window.open('/portal/NET/Documents/documentsbrowser.aspx?keyType=' + keyType + '&keyValue=' + keyValue + readonlyFlag + configOptions, 'wndDocuments', 'status,height=600,width=900,top=100,left=100,scrollbars=1,resizable=yes');
    }
    
    function PolicyCC_View(PolicyCCid_) {
        var d = new Date;   //ReadOnly=true&
        var url = "/portal/net/PolicyManagement/PolicyUI/PolicyDetails.aspx?PolicyID=" + PolicyCCid_ + "&t=" + d.getTime();
        var newWnd = window.open(url, 'Policy', 'status,height=600,width=900,top=100,left=100,scrollbars=1,resizable=yes');
        if (newWnd != null) {
            newWnd.focus();
            newWnd.attachEvent("onunload", PolicyWndUnloadEvent);
        }
    }

    function PolicyWndUnloadEvent() {
        //reload Policy Infromation:
        var PolicyId_ = 0;
        var VehicleId_ = 0;
        var CovCode_ = 0;
        var PropertyId_ = 0;        
        var DriverId_ = 0;

        PolicyId_ = PolicyCC.id;
        ///////////
        // Vehicle:
        if (PolicyCC.Type == 'AL') {
            if (typeof (PolicyCC.VEH) != "undefined") {
                if (PolicyCC.VEH[0] != null) {
                    VehicleId_ = PolicyCC.VEH[0].ID;
                }
            }
            
            if (typeof (PolicyCC.DRVR) != "undefined") {
                if (PolicyCC.DRVR[0] != null){
                    DriverId_ = PolicyCC.DRVR[0].ID;
                }
            }
        }
        
        ////////////
        //Coverage:               
        // scheduled auto:
        if (PolicyCC.Type == 'AL' && PolicyCC.Shdl == 1) {
            if (PolicyCC.VEH[0].hasOwnProperty('COV')) {
                if (PolicyCC.VEH[0].COV[0] != null) {
                    CovCode_ = PolicyCC.VEH[0].COV[0].Cd;
                }
            }
        }
        //Coverage:
        // non-scheduled auto:  //  casualty
        if ((PolicyCC.Type == 'AL' && PolicyCC.Shdl == 0) || PolicyCC.Type == 'CA') {
            if (typeof (PolicyCC.COV) != "undefined") {
                if (PolicyCC.COV[0] != null) {
                    CovCode_ = PolicyCC.COV[0].Cd;   //PolicyCC.VEH[0].COV[0].Cd;
                }
            }
        }
        ////////////
        //Property
        if(PolicyCC.Type == 'PR') {
            if (typeof (PolicyCC.PROP) != "undefined") {
                if (PolicyCC.PROP[0] != null) {
                    PropertyId_ = PolicyCC.PROP[0].ID;
                }
            }        
        }
        
        PolicyCC_XTRCT(PolicyId_, CovCode_, PropertyId_, VehicleId_, DriverId_);  

        if (PolicyCC.hasOwnProperty('id')) {
            Variable_Populate_Minor_Screen('polCCinfo');
            setTimeout("Display('polCCinfo')", 50);
        } else {
            Variable_Populate('claim');
            setTimeout("Display('claim')", 50);
        }
    }

    function LookupWindow_PolicyCC(Item, Scheduled) {
        var d = new Date;
        var PolicyID_ = PolicyCC.id;
        var VehID_ =0;
        var PropID_ = 0;

        if (Item == 'Veh') {
            if (!confirm('Selection of a vehicle will automatically update Insured Vehicle on the claim.\nWould you like to proceed?')) {
                return;
            }
        }

        if (typeof (PolicyCC.VEH) != "undefined") {
            VehID_ =(PolicyCC.VEH[0]!= null) ? PolicyCC.VEH[0].ID : 0;
        }
        if(typeof(PolicyCC.PROP) != "undefined"){
            PropID_ = (PolicyCC.PROP[0]!= null) ? PolicyCC.PROP[0].ID : 0;
        }
        var ScheduleType = '-';

        if (Scheduled == 1) { 
            if(Item=='Cov'){
                if (VehID_ == 0) {
                    alert('Please select Vehicle. List of Coverage codes is Vehicle-specific');
                    return;
                } else {
                    ScheduleType = 'Vehicle';
                }
            }
        }


        var url = 'Screen_Lookup_PolicyCC.asp?t=' + d.getTime() + "&Item=" + Item
                                                                + "&PolicyId=" + PolicyID_
                                                                + "&VehID=" + VehID_
                                                                + "&PropID=" + PropID_
                                                                + "&LossDate=" + frGeneral.document.frmGeneral.txtAccDT.value
                                                                + "&ReportDate=" + ReportDate
                                                                + "&ScheduleType=" + ScheduleType;
        //window.open(url, "Filter", "status=no,height=1,width=1,top=1000,left=5000")
        frPrint.document.location = url;
    }

    function LookUp_PolicyCC_ReturnValues(RetVal, Legend) {
        var arrLastIndex = RetVal.length - 1;
        if (Legend == 'Property') {
            if (arrLastIndex >= 0) {
                PolicyCC.PROP = eval('[' + RetVal[2] + '];'); //  jsCode_HIDDEN
            }
        }

        if (Legend == 'Vehicle') {
            if (arrLastIndex >= 0) {
                PolicyCC.VEH = eval('[' + RetVal[7] + '];');  //  jsCode_HIDDEN
            }
            // update selected vehicle info on a claim:
            PolicyInsuredVehicleExtractValues(PolicyCC.VEH[0]);
        }

        if (Legend == 'Coverage') {
            if (arrLastIndex >= 0) {
                if (PolicyCC.Shdl == 1) {
                    PolicyCC.VEH[0].COV = eval('[' + RetVal[6] + '];');  //  jsCode_HIDDEN
                } else {
                    PolicyCC.COV=eval('[' + RetVal[6] + '];');  //  jsCode_HIDDEN
                }
            }
        }

        if (Legend == 'Driver') {
            if (arrLastIndex >= 0) {
                PolicyCC.DRVR = eval('[' + RetVal[8] + '];');  //  jsCode_HIDDEN
            }
        }

        Variable_Populate_Minor_Screen('polCCinfo');
        setTimeout("Display('polCCinfo')", 50);
    }

    function Populate_polCCstates(){
        var RetVal = '';
        
        if (PolicyCC.hasOwnProperty('State')){
            if (PolicyCC.State.length > 0) {
                var reState = eval("/'(" + PolicyCC.State.join("|") + ")'>([\\w \\.]+)</gi");  

                foundArr = reState.exec(StatesCodes);

                while (foundArr != null) {
                    RetVal = RetVal + "<option value='" + RegExp.$1 + "'>" + RegExp.$2 + "</option>"
                    foundArr = reState.exec(StatesCodes);
                }
                
                if(RetVal != '') {
                    RetVal = "<option value=''></option>" + RetVal; 
                }
            }
        }
        return (RetVal);      
    }

    function Populate_polCCcov() {
        var Cov_ = Cov_Full;  // full list of coverage codes;

        if (PolicyCC.hasOwnProperty('id')) {
            if (PolicyCC.Type == 'AL' && PolicyCC.Shdl == 1) {
                if (typeof (PolicyCC.VEH) != "undefined" && PolicyCC.VEH[0] != null) {
                    if (PolicyCC.VEH[0].hasOwnProperty('COV')) {
                        if (PolicyCC.VEH[0].COV[0] != null) {
                            Cov_ = PolicyCC.VEH[0].COV[0].PLCov;
                        }
                    }
                }
            }
            // non-scheduled auto:  //  casualty
            if ((PolicyCC.Type == 'AL' && PolicyCC.Shdl == 0) || PolicyCC.Type == 'CA') {
                if (typeof (PolicyCC.COV) != "undefined") {
                    if (PolicyCC.COV[0] != null) {
                        Cov_ = PolicyCC.COV[0].PLCov;
                    }
                }

            }
            //default Coverage list for other Lines:  PolicyCC.Type == 'WC' || PolicyCC.Type == 'PR'
        }
        return (Cov_);
    }

    // GET Policy CC:
    function LookUp_PolicyCC() {
        var d = new Date;
        var Client_= ClientNumber;
        var Prefix_ = Prefix;
        var PolLoc = '';

        //DO NOT allow tio change POLICY if there are  reserves pending approval:
        var _ReserveTypesPendAppr = IsReservePendingApproval();
        if (_ReserveTypesPendAppr.IR == 1 || _ReserveTypesPendAppr.ER == 1 || _ReserveTypesPendAppr.MR == 1) {
            alert("Claim has reserves pending approval\nPolicy can't be changed");
            return;
        }

		var LOB_ = frGeneral.document.frmGeneral.cbolLineBus.value;
		    if (LOB_ == '') {
		        alert('Please specify Date of Loss');
		        return;
		    }
		    
		var ClmNo_=ClaimNumber;
		var LossDate_ = frGeneral.document.frmGeneral.txtAccDT.value;
		    if (LossDate_ == '') {
		        alert('Please set Date of Loss');
		        return;
		    }
		var State_= JurState;
		var Loc_ = 0
		if (LocFlag > 0) {
		    Loc_ = EditLoc[1][0];
		        if (Loc_ == 0) {
		            alert("Please selected " + LocLegend);
		            return;
		        }
		}

		if (Polstruct == 'I' || Polstruct == 'V') {
		    PolLoc = (LocLegend == '') ? '[Location-specific]' : '['+LocLegend + '-specific]'
		}

		var url = 'Screen_Lookup_PolicyCCGet.asp?t=' + d.getTime() + "&ClntNo=" + Client_
                                                                + "&Prefix=" + Prefix_
                                                                + "&LOB=" + LOB_
                                                                + "&ClmNo=" + ClmNo_
                                                                + "&LossDate=" + LossDate_
                                                                + "&State=" + State_
                                                                + "&Loc=" + Loc_
                                                                + "&PolLoc=" + PolLoc
        //window.open(url, "Filter", "status=no,height=1,width=1,top=1000,left=5000")
		frPrint.document.location = url;
    }

    function LookUp_PolicyCC_GET_ReturnValues(RetVal) {
        var arrLastIndex = RetVal.length - 1;

   
        var PolicyId_ = 0;
        var CovCode_ = 0;
        var PropertyId_ = 0;
        var VehicleId_ = 0;
        var DriverId_ = 0;

        var PolNo_ = '';   var EffDT_ = '';  var ExpDT_ = '';
        
        // get Policy Info:
        if (arrLastIndex >= 0) {
            if (!isNaN(RetVal[0])) {
                PolicyId_ = RetVal[0];
                PropertyId_ = 	RetVal[1];
                VehicleId_ = RetVal[2];
                DriverId_ = RetVal[3];

                if(trim(RetVal[8]) == 'Yes') {  //Assigned  7
                    var re = /^(\d*)_/gi
                    var CovText = (typeof (frInfo.document.frmClaim.cboCov) != "undefined") ? frInfo.document.frmClaim.cboCov.options[frInfo.document.frmClaim.cboCov.options.selectedIndex].text : CovCod;
                    re.exec(CovText)
                    CovCode_ = RegExp.$1;
                }
//debugger
                if(PolicyId_ > 0) {  // New PolicyCC selected 
                    PolicyCC_XTRCT(PolicyId_, CovCode_, PropertyId_, VehicleId_, DriverId_)   //
                    if (!PolicyCC.hasOwnProperty('id')) {alert('Unable to pull Policy information');};                    
                }else{                   
                    PolicyCC = {};   // remove association with a new Policy;
                    // set new Policy , Eff, Exp dates:
                    
                    PolNo_ = RetVal[5]; //4
                    EffDT_ = RetVal[6]; //5
                    ExpDT_ = RetVal[7]; //6

                    frGeneral.document.frmGeneral.txtPolNo.value = PolNo_;
                    frGeneral.document.frmGeneral.txtEffDT.value = MyDateFormat(trim(EffDT_));
                    frGeneral.document.frmGeneral.txtExpDT.value = MyDateFormat(trim(ExpDT_));

                    Cov = Cov_Full;  //restore full List of Coverage codes;
                }
                
                var _LOB = frGeneral.document.frmGeneral.cbolLineBus.value
                Index(_LOB);

                frIndex.document.open();
                setTimeout("frIndex.document.write(I)", 50);  // reload buttons
                frIndex.document.close();

                if (PolicyCC.hasOwnProperty('id')) {
                    Variable_Populate_Minor_Screen('polCCinfo');
                    setTimeout("Display('polCCinfo')", 50);
                } else {
                    Variable_Populate('claim');
                    setTimeout("Display('claim')", 50);
                }
            }
        }
    }


    function PolicyCC_NonProperty_FinLimits() {
        var t = "";
        if (PolicyCC.FIN.hasOwnProperty('OccLimit')) {
                t = "<table  cellPadding=1 cellSpacing=1 border=0 style='width:95%'><tr><td>"
                + "<table  cellPadding=1 cellSpacing=1 border=0 style='width:80%'>"
                  + "<TR><TD></TD><TD  align=right><U><B>Limit</B></U></TD>"
                 + "<TD align=right><U><B>Gross Reserves,$</B></U></TD>"
                 + "<TD align=right><U><B>Paid,$</B></U></TD>"
                 + "<TD align=right><U><B>Pending Payment,$</B></U></TD>"
                 + "<TD align=right><U><B>York Fees,$</B></U></TD>"
                 + "<TD align=right><U><B>Paid Applied to the Limit,$</B></U></TD>"
                 + "</tr>"
                 + "<TR><TD width=120px><b>Feature</b></TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.FeaLimit[0].limit) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.FeaLimit[0].grossRes) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.FeaLimit[0].paid) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.FeaLimit[0].pend) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.FeaLimit[0].billed) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.FeaLimit[0].paidToLimit) + "</TD>"
            t = t + "</tr>"

            t = t + "<TR><TD width=120px><b>Occurrence</b></TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.OccLimit[0].limit) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.OccLimit[0].grossRes) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.OccLimit[0].paid) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.OccLimit[0].pend) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.OccLimit[0].billed) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.OccLimit[0].paidToLimit) + "</TD>"
            t = t + "</tr>"

            t = t + "<TR><TD width=120px><b>Aggregate</b></TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.AggLimit[0].limit) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.AggLimit[0].grossRes) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.AggLimit[0].paid) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.AggLimit[0].pend) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.AggLimit[0].billed) + "</TD>"
            t = t + "<TD align=right>" + commafy(PolicyCC.FIN.AggLimit[0].paidToLimit) + "</TD>"
            t = t + "</tr>"

            t = t + "</table>"
            if (PolicyCC.FIN.OccSubLimit.length > 0) {
                t += "</td></tr><tr><td style='height:30px'></td></tr><tr><td>"

                        + "<table   cellPadding=1 cellSpacing=1 border=0 style='width:95%' frame='border'>"
                        + "<TR><TD width=120px><U><B> Sub Limit</B></U></TD>"
                        + "<TD align=right><U><B>Occurrence Limit, $</B></U></TD>"
                        + "<TD align=right><U><B>Occurrence Paid, $</B></U></TD>"
                        + "<TD align=right><U><B>Occurrence Pending requests, $</B></U></TD>"
                        + "<TD align=right><U><B>Aggregate Limit, $</B></U></TD>"
                        + "<TD align=right><U><B>Aggregate Paid, $</B></U></TD>"
                        + "<TD align=right><U><B>Aggregate Pending requests, $</B></U></TD>"
                        + "</tr>"

                var c = 0
                while (c < PolicyCC.FIN.OccSubLimit.length) {

                    //alert('Sublimit description:' + PolicyCC.FIN.OccSubLimit[c].desc)
                    t = t + "<TR><TD width=120px><B>" + PolicyCC.FIN.OccSubLimit[c].desc + "</B></TD>"

                    t = t + "<TD align=right>" + commafy(PolicyCC.FIN.OccSubLimit[c].limit) + "</TD>"
                    t = t + "<TD align=right>" + commafy(PolicyCC.FIN.OccSubLimit[c].paidToLimit) + "</TD>"
                    t = t + "<TD align=right>" + commafy(PolicyCC.FIN.OccSubLimit[c].pendToLimit) + "</TD>"
                    t = t + "<TD align=right>" + commafy(PolicyCC.FIN.AggSubLimit[c].limit) + "</TD>"
                    t = t + "<TD align=right>" + commafy(PolicyCC.FIN.AggSubLimit[c].paidToLimit) + "</TD>"
                    t = t + "<TD align=right>" + commafy(PolicyCC.FIN.AggSubLimit[c].pendToLimit) + "</TD></tr>"

                    c++
                }
                t = t + "</table>"
            }
            t += "</td></tr></table>"
        }
           return (t);
    }

    function  PolicyCC_Property_FinLimits(){
            var c = 0

            var t = "";
            if (PolicyCC.FIN.hasOwnProperty('OccLimit')) {
                t = "<table width = '800px' border =0><B>Feature:</B>"
		    + "<col WIDTH=100px>"
		    + "<col WIDTH=100px>"
		    + "<col WIDTH=100px>"
		    + "<col WIDTH=100px>"
		    + "<col WIDTH=100px>"
		    + "<col WIDTH=100px>"
		    + "<col WIDTH=100px>"
		    + "<col WIDTH=100px>"
                t = t + "<TR><TD><B><U>Transaction Type</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Limit</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Gross Reserves</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Payments</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Net Reserves</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Pending Payments</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Remaining Reserves</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Incurred</U></B></TD>"
                t = t + "</tr>"

                c = 0
                while (c < PolicyCC.FIN.FeaLimit.length) {
                    t = t + "<TR><TD><I>" + PolicyCC.FIN.FeaLimit[c].desc + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.FeaLimit[c].limit) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.FeaLimit[c].grossRes) + "</I></TD>"
                    
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.FeaLimit[c].paid) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.FeaLimit[c].netRes) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.FeaLimit[c].pend) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.FeaLimit[c].remainRes) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>-</I></TD>"
                    t = t + "</tr>"

                    c++
                }

                t += "</table><BR><BR>"

                t = t + "<table width = '800px'><B>Occurrence:</B>"
		            + "<col WIDTH=100px>"
		            + "<col WIDTH=100px>"
		            + "<col WIDTH=100px>"
		            + "<col WIDTH=100px>"
		            + "<col WIDTH=100px>"
		            + "<col WIDTH=100px>"
		            + "<col WIDTH=100px>"
		            + "<col WIDTH=100px>"
                /*
                t = t + "<TR><TD width= 220><B><U>Transaction Type</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Limit</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Gross Reserves</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Net Reserves</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Payments</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Incurred</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Pending Paymants</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Remaining Reserves</U></B></TD>"
                t = t + "</tr>"
                */
                c = 0
                while (c < PolicyCC.FIN.OccLimit.length) {

                    t = t + "<TR><TD><I>" + PolicyCC.FIN.OccLimit[c].desc + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.OccLimit[c].limit) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.OccLimit[c].grossRes) + "</I></TD>"
                    
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.OccLimit[c].paid) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.OccLimit[c].netRes) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.OccLimit[c].pend) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.OccLimit[c].remainRes) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>-</I></TD>"
                    t = t + "</tr>"
                    c++
                }

                t = t + "<TR><TD>Sub Limit</TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "</tr>"

                c = 0
                while (c < PolicyCC.FIN.OccSubLimit.length) {

                    t = t + "<TR><TD><I>" + PolicyCC.FIN.OccSubLimit[c].desc + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.OccSubLimit[c].limit) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>-</I></TD>"

                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.OccSubLimit[c].paidToLimit) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>-</I></TD>"

                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.OccSubLimit[c].pendToLimit) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>-</I></TD>"
                    t = t + "<TD align= 'right' ><I>-</I></TD>"
                    t = t + "</tr>"
                    c++
                }


                t = t + "</table><BR>"

                t = t + "<table width = '800px'><B>Aggregate:</B>"
		        + "<col WIDTH=100px>"
		        + "<col WIDTH=100px>"
		        + "<col WIDTH=100px>"
		        + "<col WIDTH=100px>"
		        + "<col WIDTH=100px>"
		        + "<col WIDTH=100px>"
		        + "<col WIDTH=100px>"
		        + "<col WIDTH=100px>"
                /*
                t = t + "<TR><TD width= 220><B><U>Transaction Type</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Limit</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Gross Reserves</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Net Reserves</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Payments</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Incurred</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Pending Paymants</U></B></TD>"
                t = t + "<TD align= 'right' ><B><U>Remaining Reserves</U></B></TD>"
                t = t + "</tr>"
                */
                c = 0
                while (c < PolicyCC.FIN.AggLimit.length) {

                    t = t + "<TR><TD><I>" + PolicyCC.FIN.AggLimit[c].desc + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggLimit[c].limit) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggLimit[c].grossRes) + "</I></TD>"

                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggLimit[c].paid) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggLimit[c].netRes) + "</I></TD>"
                    
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggLimit[c].pend) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggLimit[c].remainRes) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggLimit[c].incur) + "</I></TD>"
                    t = t + "</tr>"

                    c++
                }

                t = t + "<TR><TD>Sub Limit</TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "<TD></TD>"
                t = t + "</tr>"


                c = 0
                while (c < PolicyCC.FIN.AggSubLimit.length) {

                    t = t + "<TR><TD><I>" + PolicyCC.FIN.AggSubLimit[c].desc + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggSubLimit[c].limit) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>-</I></TD>"

                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggSubLimit[c].paidToLimit) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>-</I></TD>"

                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggSubLimit[c].pendToLimit) + "</I></TD>"
                    t = t + "<TD align= 'right' ><I>-</I></TD>"
                    t = t + "<TD align= 'right' ><I>" + commafy(PolicyCC.FIN.AggSubLimit[c].incur) + "</I></TD>"
                    t = t + "</tr>"
                    c++
                }


                t = t + "</table>"

            }
            return (t);
        }

        // reserve change verification:
        function PolicyCC_AllowChangeReserve() {        
             var clm_or = ClmOriginal
            if (PolicyCC.hasOwnProperty('id')) {  //POlicyCC is selected  and then changed:
                if (trim(clm_or.PolNo.toUpperCase()) == trim(frGeneral.document.frmGeneral.txtPolNo.value.toUpperCase()) && MyDateFormat(clm_or.EffDT) == MyDateFormat(frGeneral.document.frmGeneral.txtEffDT.value) && MyDateFormat(clm_or.ExpDT) == MyDateFormat(frGeneral.document.frmGeneral.txtExpDT.value)) {
                    return (true);
                } else {
                    return (false);
                }
            } else {
                return (true);
            }

        }


        function Reserves_Limits_Compare(IR_MC_) {
            var RetVal =  new Array();

            IR_Disp_ = DisplayReserve(IR_Disp);
            MR_Disp_ = DisplayReserve(MR_Disp);
            ER_Disp_ = DisplayReserve(ER_Disp);

            IR_ = DisplayReserve(IR);
            MR_ = DisplayReserve(MR);
            ER_ = DisplayReserve(ER);
//debugger
            var c = -1
            if( (parseFloat(IR_Disp_) != 0 && parseFloat(IR_Disp_) != parseFloat(IR_)) ){
                c++
                RetVal[c]='IR'
            }
            if(parseFloat(MR_Disp_) != 0 && parseFloat(MR_Disp_) != parseFloat(MR_)){
                c++
                RetVal[c]='MR'
            }
            if(parseFloat(ER_Disp_) != 0 && parseFloat(ER_Disp_) != parseFloat(ER_)){
                c++
                RetVal[c]='ER'
            }

            if (PolicyCC.hasOwnProperty('id')) {
                if (RetVal.length>0){                 
                    var GrossReserve = 0
                    if (PolicyCC.InclExp == 1) {
                        GrossReserve = parseFloat(IR_Disp_) + parseFloat(MR_Disp_) + parseFloat(ER_Disp_);
                    } else {
                        GrossReserve = parseFloat(IR_Disp_) + parseFloat(MR_Disp_);
                    }

                    if (GrossReserve > 0) {
                        var message = PolicyCC_LimitsVerify_XTRCT(GrossReserve, IR_MC_);
                        if (message == '') {
                            while (c >= 0) {
                                delete RetVal[c]
                                c--
                            }
                        } else {
                            var re = /;/g
                            message = 'Changed reserves exceed policy limits!\n\n' + message.replace(re, '\n')
                            alert(message);
                        }
                    }
                }

            }
            return (RetVal);
        }

