function enableInput(turnOn)
{
    var item$ = $("button, input[type=button]");
    return
    item$.each
    (
        function ()
        {
            if (this.disabled == null)
                this.disabled = !turnOn;
        }
    );
}

var ACTION_CODE_GET_ONE = "GET_ONE";
var ACTION_CODE_GET_MANY = "GET_MANY";
var ACTION_CODE_UPDATE = "UPDATE";
var ACTION_CODE_ADD = "ADD";
var ACTION_CODE_DELETE = "DELETE";
var ACTION_CODE_CONFIRMED_UPDATE = "CONFIRMED_UPDATE";

var RESULT_TYPE_TABLE = "_RESULT_TYPE_TABLE";
var RESULT_TYPE_JSON = "_RESULT_TYPE_JSON";
var RESULT_TYPE_MESSAGE = "_RESULT_TYPE_MESSAGE";
var RESULT_TYPE_SELECT = "_RESULT_TYPE_SELECT";

var __p_Page_Info_Span_Id = "_Page_Info_Span_Id";
var __p_Page_Message_Text = "";

function showMessage(messageText)
{
    __p_Page_Message_Text = messageText;

    if (messageText == null)
        return;

    if (messageText.length < 1)
        return;

    // to suppress alert add invisible span to the page with id = __p_Page_Info_Span_Id
    var pageInfoSpanElement = document.getElementById(__p_Page_Info_Span_Id);
    if (pageInfoSpanElement != null)
    {
        var spanMessageText = messageText.replace(/\n/g, "<br />");
        $(pageInfoSpanElement).text(spanMessageText);
        return;
    }

    alert(messageText);
}


function viewOuterHtml()
{
    var d = window.open();
    d.document.open('text/plain').write(document.documentElement.outerHTML);
}


var AJAX_RESULT_DATA_PREFIX = "RESULT_DATA:";
var AJAX_RESULT_ERROR_DESCR = "ERROR_DESCR:";
var AJAX_RESULT_STATUS_PREFIX_LENGHT = AJAX_RESULT_DATA_PREFIX.length;


function isServerResponseFormedProperly(serverResponseText)
{
    if (serverResponseText == null)
        return false;

    if (serverResponseText.length < AJAX_RESULT_STATUS_PREFIX_LENGHT)
        return false;

    if (serverResponseText.substr(0, AJAX_RESULT_STATUS_PREFIX_LENGHT) == AJAX_RESULT_DATA_PREFIX)
        return true;

    if (serverResponseText.substr(0, AJAX_RESULT_STATUS_PREFIX_LENGHT) == AJAX_RESULT_ERROR_DESCR)
        return true;

    return false;
}


function getServerResponseInfo(serverResponseText, InfoType)
{
    if (!isServerResponseFormedProperly(serverResponseText))
        return "";

    if (serverResponseText.substr(0, AJAX_RESULT_STATUS_PREFIX_LENGHT) != InfoType)
        return "";

    return serverResponseText.substr(AJAX_RESULT_STATUS_PREFIX_LENGHT);
}


function getServerResponseData(serverResponseText)
{
    return getServerResponseInfo(serverResponseText, AJAX_RESULT_DATA_PREFIX);
}


function getServerErrorDescription(serverResponseText)
{
    return getServerResponseInfo(serverResponseText, AJAX_RESULT_ERROR_DESCR);
}

function sendSynchronousRequest(requestUrl)
{
    enableInput(false);
    setProgressImageOn();
    var resultData = " ";
    var xmlhttp = $.ajax(
        { type: 'GET',
            async: false,
            url: requestUrl,
            data: resultData,
            success: function (data)
            {
                resultData = data;
            },
            error: function (request, status, error)
            {
                if (request.status && request.status == 400)
                {
                    alert(request.responseText);
                    enableInput(true);
                }
                else
                {
                    alert("Something went wrong, error: " + error);
                    enableInput(true);
                }
            }
        });

    xmlhttp = null;
    enableInput(true);
    setProgressImageOff();
    return $.trim(resultData);
}


function requestDataSynchronously(requestUrl)
{
    var serverResponseText = sendSynchronousRequest(requestUrl);
    return processServerResponse(serverResponseText);
}


function processServerResponse(serverResponseText)
{
    if (!isServerResponseFormedProperly(serverResponseText))
    {
        alert(getServerErrorDescription(serverResponseText));
        return '';
    }

    var errorMessage = getServerErrorDescription(serverResponseText)
    if ('' != errorMessage)
    {
        alert(errorMessage);
        return '';
    }

    return getServerResponseData(serverResponseText);
}


function sendSynchronousRequestMs(requestUrl)
{
    var data = " ";
    var xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    //xmlhttp.Open("POST", requestUrl, false);
    xmlhttp.Open("GET", requestUrl, false);
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("Content-Length", requestUrl.length);

    xmlhttp.Send(data);

    var serverResponseText = "Server error.";
    if (xmlhttp.status == 200)
        serverResponseText = xmlhttp.responseText;

    xmlhttp = null;

    return $.trim(serverResponseText);
}


function checkResponseData(serverResponseText, serverResponseData, requestUrl)
{
    if (serverResponseData != "")
        return true;

    var errorDescription = getServerErrorDescription(serverResponseText);
    if (errorDescription != "")
        alert(errorDescription);
    else
        alert("The request [" + requestUrl + "] failed.");

    return false;
}


function setElementVisibility(elementId, isVisible)
{
    var aVisibility = 'hidden';
    if (isVisible)
        aVisibility = 'visible';

    var anElement = document.getElementById(elementId);
    if (null == anElement)
        return;
    anElement.style.visibility = aVisibility;
}


function setTableHtmlText(tableId, htmlText)
{
    var table = $("table#" + tableId);
    table.html(htmlText);
}


function formatJQueryRowExpresion(rowIndex)
{
    var rowId = formatRowId(rowIndex);
    if (null == rowId)
        return "";

    return " tr[id=" + rowId + "] ";
}


function formatJQueryColumnExpresion(rowIndex, columnName)
{
    var columnId = formatColumnId(rowIndex, columnName);
    if (null == columnId)
        return "";

    return " td[id='" + columnId + "'] ";
}


function formatJQueryColumnExpresionBegin(rowIndex, columnName)
{
    return " td[id*=" + columnName + "] ";
}


function formatJQueryCellExpresion(tableId, rowIndex, columnName)
{
    return "table#" + tableId + formatJQueryRowExpresion(rowIndex) + formatJQueryColumnExpresion(rowIndex, columnName);
}


function formatJQueryCellExpresionBegin(tableId, rowIndex, columnName)
{
    return "table#" + tableId + formatJQueryRowExpresion(rowIndex) + formatJQueryColumnExpresionBegin(rowIndex, columnName);
}


function setTableCellText(tableId, rowIndex, columnName, newValue)
{
    var cellExpresion = formatJQueryCellExpresion(tableId, rowIndex, columnName);
    $(cellExpresion).html(newValue);
}


function setTableRowText(tableId, rowIndex, tableData)
{
    var row = tableData[rowIndex];

    for (columnName in row)
    {
        setTableCellText(tableId, rowIndex, columnName, row[columnName]);
    }
}


//function showTableData(tableData)
//{
//    for (var rowIndex = 0; rowIndex < tableData.length; rowIndex++)
//    {
//        var row = tableData[rowIndex];

//        var rowText = "row[" + rowIndex + "]: ";
//        for (columnName in row)
//        {
//            rowText = rowText + " " + columnName + ":[ " + row[columnName] + "];  ";
//        }

//        alert(rowText);
//    }
//}


function assignFromData(tableId, tableData)
{
    for (var rowIndex = 0; rowIndex < tableData.length; rowIndex++)
    {
        setTableRowText(tableId, rowIndex, tableData);
    }
}


function assignDataColumnsFromTable(tableId, tableData, columnList)
{
    tableData.length = 0;
    var table$ = $("#" + tableId);
    var rows$ = $("tr", table$);
    var numberOfRows = rows$.length;

    for (var rowIndex = 1; rowIndex < numberOfRows; rowIndex++)
    {
        var rowItem = rows$[rowIndex];
        var row = new Object();
        for (columnIndex in columnList)
        {
            var columnName = columnList[columnIndex];
            var cellExpresion = columnName + (rowIndex - 1);
            var td$ = $("#" + cellExpresion, rowItem);
            var cellText = $.trim(td$.text());
            row[columnName] = cellText;
        }

        tableData.push(row);
    }
}


function assignDataFromTable(tableId, tableData)
{
    var columnList = [];
    $.each($('#h_row_mainTableHeadRow th'), function (index, value)
    {
        columnList.push($(this).attr('id').replace("th_", ""));
    });

    assignDataColumnsFromTable(tableId, tableData, columnList);
}


//function cloneRow(table, originalRowId, newRowId)
//{
//    var row = document.getElementById(originalRowId);

//    var clone = row.cloneNode(true); // copy children too
//    clone.id = newRowId;

//    if (null != table.tBodies && null != table.tBodies[0])
//    {
//        var tableBody = table.tBodies[0];
//        tableBody.appendChild(clone);
//    }
//    else
//    {
//        table.appendChild(clone); // add new row to end of table
//    }
//}

//function appendRow(tableId)
//{
//    var table = document.getElementById(tableId); // find table to append to
//    //    var tableBody = table.tBodies[0];
//    //    var newNode = tblBody.rows[0].cloneNode(true);

//    var numberOfDataRows = table.rows.length;

//    if (null != table.tHead)
//        numberOfDataRows--;

//    var originalRowIndex = numberOfDataRows - 1;
//    var newRowIndex = originalRowIndex + 1;
//    cloneRow(table, formatRowId(originalRowIndex), formatRowId(newRowIndex));

//    return newRowIndex;
//}


function assignTableColumnsFromData(tableId, tableData, columnList)
{
    for (var rowIndex = 0; rowIndex < tableData.length; rowIndex++)
    {
        var row = tableData[rowIndex];

        for (columnIndex in columnList)
        {
            var columnName = columnList[columnIndex];
            setTableCellText(tableId, rowIndex, columnName, row[columnName]);
        }
    }
}


function syncTableIds(tableId, row, columnList)
{
    //var rowExpresion = "table#" + tableId + " tr[id*=row_] ";
    var rowExpresion = "table#" + tableId + " tr ";

    var numberOfRows = $(rowExpresion).length;

    for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++)
    {
        syncTableRowIds(tableId, rowIndex, columnList);

    }
}


function syncTableRowIds(tableId, rowIndex, columnList)
{
    for (columnIndex in columnList)
    {
        var columnName = columnList[columnIndex];
        syncTableCellId(tableId, rowIndex, columnName);
    }
}


function syncTableCellId(tableId, rowIndex, columnName)
{
    changeTableCellId(tableId, rowIndex, columnName, rowIndex);
}


function changeTableCellId(tableId, rowIndex, columnName, newRowIndex)
{
    var cellExpresion = formatJQueryCellExpresionBegin(tableId, rowIndex, columnName);
    // alert(cellExpresion + " lenghth: " + $(cellExpresion).length);

    var row = $(cellExpresion).first();

    var columnId = formatColumnId(rowIndex, columnName);
    if (null == columnId)
        return;

    row.attr('id', columnId);
}


function formatRowId(rowIndex)
{
    return "row" + rowIndex;
}


function formatColumnId(rowIndex, columnName)
{
    return columnName + rowIndex;
}


function assignValueToElement(elementId, inputValue)
{
    formRef = $('form').get(0);
    createInputParameter(elementId, inputValue, formRef);
}

var _g_DISP_TEXT_PREFIX = "disp_txt_";

function createInputParameter(elementId, inputValue, formRef)
{
    var formElement = document.getElementById(elementId);

    // special treatment for items that are not passed as HTTP parameter
    var spanItem$ = $("#" + _g_DISP_TEXT_PREFIX + elementId)
    if (spanItem$.length > 0)
        spanItem$.text(inputValue);

    if (formElement == null)
    {
        try
        {
            formElement = document.createElement('input');
            formElement.type = 'hidden';
            formElement.id = elementId;
            formElement.name = elementId;

            if (formRef != null)
                formRef.appendChild(formElement);
        }
        catch (ex)
        {
            alert(ex.message);
            return null;
        }
    }

    var element$ = $('#' + elementId);
    if (element$.length == 0)
        return;

    //if (element$.is('[value]'))
    if (hasElementValueAttribute(element$))
    {
        formElement.value = inputValue;
        formElement.disabled = false;
    }
    else
    {
        if (element$.get(0).tagName.toUpperCase() == 'TD')
        {
            if (inputValue == '')
                inputValue = ' ';
        }
        element$.text(inputValue);
    }

    return formElement;
}


function getPageElementValue(elementId)
{
    var element = document.getElementById(elementId);

    if (element == null)
        return '';

    var element$ = $('#' + elementId);

    if (hasElementValueAttribute(element$))
    //if (element$.is('[value]'))
        return element$.val();

    return element$.text();
}


function getDateFromElement(dateElementId)
{
    var value = getPageElementValue(dateElementId);
    return new Date(value);
}


var __g_TraceArea = '__TraceArea';

function logPageChange(newText)
{
    var oldText = getPageElementValue(__g_TraceArea);
    if (!oldText)
        oldText = '';

    assignValueToElement(__g_TraceArea, oldText + '; ' + newText);
}


function setRadioCheck(radioGroupObj, elementName)
{
    if (radioGroupObj == null)
        return;

    for (i = 0; i < radioGroupObj.length; i++)
    {
        if (radioGroupObj[i].value == elementName)
        {
            radioGroupObj[i].checked = true;
            break;
        }
    }
}


function runCommand(spName)
{
    return ' EXECUTE ' + spName + ' ';
}


function runGet()
{
    return ' SELECT ';
}


function getPortalPrefix()
{
    var href = window.location.href.toUpperCase();
    if (href.indexOf("/PORTAL") > -1)
        return "/Portal/Net/ClaimMvc";

    if (href.indexOf("/PUBLICPORTAL") > -1)
        return "/PublicPortal";

    return "";
}



//function sendSqlRequest(sqlRequest, resultType, userAction)
//{
//    sqlRequest = sqlRequest.replace(/=/g, "~`~`~`~");
//    sqlRequest = sqlRequest.replace(/&/g, "~```~```");
//    sqlRequest = sqlRequest.replace(/\?/g, "`~~``~~");
//    sqlRequest = encodeURIComponent(sqlRequest);

//    var requestUrl = getPortalPrefix() + '/Cc/DataRequest?sql=' + sqlRequest + '&ResultType=' + resultType;
//    if (!userAction)
//        requestUrl += ("&_USER_ACTION=" + ACTION_CODE_GET_MANY);
//    else
//        requestUrl += ("&_USER_ACTION=" + userAction);

//    //alert(requestUrl);
//    var serverResponseText = sendSynchronousRequest(requestUrl);
//    return serverResponseText;
//}


function sendSqlRequest(sqlRequest, resultType, userAction)
{
    var requestUrl = getPortalPrefix() + '/Cc/DataRequest';
    if (!userAction)
        userAction = ACTION_CODE_GET_MANY;

    var httpParameters = { "sql": sqlRequest, "ResultType": resultType, "_USER_ACTION": userAction };

    var serverResponseText = sendSynchronousPostRequest(requestUrl, httpParameters);
    return serverResponseText;
}


function getServerMessageUa(sqlRequest, userAction)
{
    var serverResponseText = sendSqlRequest(sqlRequest, RESULT_TYPE_MESSAGE, userAction);

    if (!isServerResponseFormedProperly(serverResponseText))
    {
        alert("[" + serverResponseText + "]");
        alert(getServerErrorDescription(serverResponseText));
        return false;
    }

    var errorMessage = getServerErrorDescription(serverResponseText)
    if ('' != errorMessage)
    {
        alert(errorMessage);
        return false;
    }

    var resultText = getServerResponseData(serverResponseText);
    if ('' == resultText)
    {
        alert("No data returned.");
        return false;
    }

    alert(resultText);

    return true;
}


function getServerMessage(sqlRequest)
{
    return getServerMessageUa(sqlRequest, ACTION_CODE_GET_MANY);
}


function getTextFromServerUaWithoutMessage(sqlRequest, resultType, userAction)
{
    var serverResponseText = sendSqlRequest(sqlRequest, resultType, userAction);
    return processServerResponse(serverResponseText);
}


function getTextFromServerUa(sqlRequest, resultType, userAction)
{
    var resultText = getTextFromServerUaWithoutMessage(sqlRequest, resultType, userAction);
    if ('' == resultText)
    {
        alert("No data returned.");
        return '';
    }

    return resultText;
}


function getTableFromServerUa(sqlRequest, userAction)
{
    return getTextFromServerUa(sqlRequest, RESULT_TYPE_TABLE, userAction);
}

function getTableFromServer(sqlRequest)
{
    return getTableFromServerUa(sqlRequest, ACTION_CODE_GET_MANY);
}


function getJsonFromServerUa(sqlRequest, userAction)
{
    var serverResponseText = getTextFromServerUa(sqlRequest, RESULT_TYPE_JSON, userAction);
    return processJsonResponseText(serverResponseText);
}


function processJsonResponseText(serverResponseText)
{
    setJsonResponseText(serverResponseText);
    var xmlDoc = createXmlDocumentFromJsonResponse();
    var result_data = xmlDoc.getElementsByTagName("result_data");
    if (result_data.length > 0)
        __p_Request_Result = $.parseJSON($(result_data[0]).text());

    var table_data = xmlDoc.getElementsByTagName("table_data");
    if (table_data.length > 0)
        return $(table_data[0]).text();

    return getJsonResponseText();
}


function getJsonFromServer(sqlRequest)
{
    return getJsonFromServerUa(sqlRequest, ACTION_CODE_GET_MANY);
}


var _g_mvc_CURRENT_APPLICATION_USER = "_CURRENT_APPLICATION_USER";
var _g_mvc_CURRENT_USER_CULTURE = "_CURRENT_USER_CULTURE";


function getCurrentUserCulture()
{
    return getPageElementValue(_g_mvc_CURRENT_USER_CULTURE);
}

function initCurrentUserCulture()
{
    g_Page_Culture = getPageElementValue(_g_mvc_CURRENT_USER_CULTURE);
}


function getCurrentLogon()
{
    return getPageElementValue(_g_mvc_CURRENT_APPLICATION_USER);
}


function getCurrentLogonHttpParam()
{
    return _g_mvc_CURRENT_APPLICATION_USER + '=' + getCurrentLogon();
}

function getDateFromElement(dateElementId)
{
    var value = getPageElementValue(dateElementId);
    return new Date(value);
}


function stringToNumeric(elementValue)
{
    elementValue = $.trim(elementValue);
    //if (elementValue == '')
    //    elementValue = '0'

    var numericElementValue = Number(elementValue);
    return numericElementValue;
}


function getNumericFromElement(elementId)
{
    var elementValue = getPageElementValue(elementId);
    var numericElementValue = stringToNumeric(elementValue);
    return numericElementValue;
}


function isNumericValue(elementValue)
{
    var numericElementValue = stringToNumeric(elementValue);

    if (isNaN(numericElementValue))
        return false;

    return true;
}


function isNumericElement(elementId)
{
    var elementValue = getPageElementValue(elementId);
    return isNumericValue(elementValue);
}


function isPositiveNumericValue(elementValue)
{
    var numericElementValue = stringToNumeric(elementValue);

    if (isNaN(numericElementValue))
        return false;

    if (numericElementValue < 0.00001)
        return false;

    return true;
}


function isPositiveNumericElement(elementId)
{
    var elementValue = getPageElementValue(elementId);
    return isPositiveNumericValue(elementValue);
}


var _g_Wrong_data_entry_text = 'Wrong data entry.';

function initTotalCalculation(idPatern, totalElementId)
{
    var element$ = $(idPatern);
    var calculationProperties = { elements: element$, resultElementId: totalElementId, decimals: 3, errorResultText: '???', errorElementTooltipText: _g_Wrong_data_entry_text };
    element$.blur(calculationProperties, calculateTotalForElementSet);
}

function initTotalCalculation$(element$, totalElementId)
{
    var calculationProperties = { elements: element$, resultElementId: totalElementId, decimals: 3, errorResultText: '???', errorElementTooltipText: _g_Wrong_data_entry_text };
    element$.blur(calculationProperties, calculateTotalForElementSet);
}

var _g_ERROR_DATA_ENTRY_INPUT_CLASS = "errorDataEntryInput";
var _g_WARNING_DATA_ENTRY_INPUT_CLASS = "warningDataEntryInput";
var _g_ORIGINAL_BACKGROUND_COLOR_ATTR = "data-original-background-color";

var _g_INPUT_ERROR_COLOR = "#FF9999";
var _g_INPUT_WARNING_COLOR = "#99FFFF";

function addErrorForInput(element$)
{
    element$.attr(_g_ORIGINAL_BACKGROUND_COLOR_ATTR, element$.css("background-color"));
    element$.css("background-color", _g_INPUT_ERROR_COLOR);
}

function addErrorForInput(element$, toolTipText)
{
    if (toolTipText != null && toolTipText != '')
        element$.attr('title', toolTipText);
    element$.attr(_g_ORIGINAL_BACKGROUND_COLOR_ATTR, element$.css("background-color"));
    element$.css("background-color", _g_INPUT_ERROR_COLOR);
}


function addWarningForInput(element$)
{
    element$.attr(_g_ORIGINAL_BACKGROUND_COLOR_ATTR, element$.css("background-color"));
    element$.css("background-color", _g_INPUT_WARNING_COLOR);
}


function addWarningForInput(element$, toolTipText)
{
    if (toolTipText != null && toolTipText != '')
        element$.attr('title', toolTipText);
    element$.attr(_g_ORIGINAL_BACKGROUND_COLOR_ATTR, element$.css("background-color"));
    element$.css("background-color", _g_INPUT_WARNING_COLOR);
}


function removeProblemFromInput(element$)
{
    element$.attr('title', '');

    var original_background_color = element$.attr("data-original-background-color");
    if (original_background_color !== undefined)
        element$.css("background-color", original_background_color);
}


function calculateTotalForElementSet(calculationProperties)
{
    var total = 0;
    var noProblem = true;

    calculationProperties.data.elements.each(
            function ()
            {
                var this$ = $(this);
                var numericElementValue = new Number(this$.val());
                if (isNaN(numericElementValue))
                {
                    noProblem = false;
                    addErrorForInput(this$, calculationProperties.data.errorElementTooltipText);
                    this.focus();
                    return false;
                }
                else
                    removeProblemFromInput(this$);

                total += numericElementValue; // parseFloat($(this).val(), 10);

                return true;
            }
        );

    var resultText = calculationProperties.data.errorResultText;
    if (noProblem)
        resultText = total.toFixed(calculationProperties.data.decimals);

    assignValueToElement(calculationProperties.data.resultElementId, resultText);
}


function getRowIndexFromName(elementId, columnName)
{
    var columnNameLength = columnName.length;
    var elementIdLength = elementId.length;
    if (elementIdLength <= columnNameLength)
        return -1;

    var idName = elementId.substring(0, columnNameLength);
    if (idName != columnName)
        return -1;

    var rowIndexStr = elementId.substring(columnNameLength);
    return Number(rowIndexStr);
}


var RECORD_CHANGE_FIELD = "_chng";

function updateTableData(tableData, rowIndex, columnName, value)
{
    if (assignTableCell(tableData, rowIndex, columnName, value))
        assignValueToElement(columnName + rowIndex, value);
}


function assignTableCell(tableData, rowIndex, columnName, value)
{
    if (tableData[rowIndex][columnName] == value)
        return false;

    tableData[rowIndex][RECORD_CHANGE_FIELD] = 1;
    setPageDataWereChanged();
    tableData[rowIndex][columnName] = value;

    return true;
}


function onNumericFieldBlurFunction(onNumericFieldBlurProperties)
{
    var this$ = $(this);
    var columnName = onNumericFieldBlurProperties.data.columnName;
    var rowIndex = getRowIndexFromName(this.id, columnName)
    if (isNaN(rowIndex))
        return;

    if (rowIndex == -1)
    {
        //        alert('this.id ' + this.id + ' columnName ' + columnName);
        return;
    }

    var elementValue = this$.val();
    elementValue = $.trim(elementValue);

    var tableData = onNumericFieldBlurProperties.data.tableData;
    if (tableData != undefined)
        assignTableCell(tableData, rowIndex, columnName, elementValue);

    var numericElementValue = new Number(0);
    if (elementValue != '')
        numericElementValue = new Number(elementValue);

    if (isNaN(numericElementValue))
    {
        addErrorForInput(this$, onNumericFieldBlurProperties.data.errorElementTooltipText);
        this.focus();

        if (onNumericFieldBlurProperties.data.callback != null)
            onNumericFieldBlurProperties.data.callback(false);
        return;
    }
    else
        removeProblemFromInput(this$);

    if (onNumericFieldBlurProperties.data.callback != null)
        onNumericFieldBlurProperties.data.callback(true);
}


function gatherUploadData(tableData, columns)
{
    var resultText = '';
    var numberOfRows = tableData.length;
    var numberOfColumns = columns.length;
    for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++)
    {
        if (tableData[rowIndex][RECORD_CHANGE_FIELD] == 0)
            continue;

        for (var columnIndex = 0; columnIndex < numberOfColumns; columnIndex++)
        {
            var columnName = columns[columnIndex];
            resultText += tableData[rowIndex][columnName];
            if ((columnIndex + 1) < numberOfColumns)
                resultText += '~';
        }

        if ((rowIndex + 1) < numberOfRows)
            resultText += '^';
    }

    return resultText;
}


function convertFieldBooleanData(columnData)
{
    if (columnData === true)
        columnData = "Y";

    if (columnData === false)
        columnData = "N";

    if (typeof columnData === "string")
    {
        columnData = columnData.toUpperCase();
        if (columnData === "TRUE")
            columnData = "Y";

        if (columnData === "FALSE")
            columnData = "N";
    }

    return columnData;
}


function prepareXmlField(tableData, columns, rowIndex, columnIndex)
{
    var resultText = "";
    var columnName = columns[columnIndex];
    var columnData = tableData[rowIndex][columnName];
    columnData = convertFieldBooleanData(columnData);
    resultText += ("<f" + columnIndex + ">");
    resultText += $.trim(columnData);
    resultText += ("</f" + columnIndex + ">");

    return resultText;
}


function prepareXmlRecord(tableData, columns, rowIndex, markedOnly)
{
    if (markedOnly)
    {
        if (tableData[rowIndex][RECORD_CHANGE_FIELD] === 0)
            return "";
    }

    var numberOfColumns = columns.length;
    var resultText = "<r>";
    for (var columnIndex = 0; columnIndex < numberOfColumns; columnIndex++)
        resultText += prepareXmlField(tableData, columns, rowIndex, columnIndex);

    resultText += "</r>";

    return resultText;
}


function prepareXmlBulkData(tableData, columns, markedOnly)
{
    if (markedOnly === undefined)
        markedOnly = false;

    var resultText = "<root>";
    var numberOfRows = tableData.length;
    for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++)
        resultText += prepareXmlRecord(tableData, columns, rowIndex, markedOnly)

    resultText += "</root>";
    return resultText;
}


function gatherXmlBulkData(tableData, columns)
{
    return prepareXmlBulkData(tableData, columns, true);
}

function gatherXmlBulkDataAll(tableData, columns)
{
    return prepareXmlBulkData(tableData, columns, false);
}


function gatherXmlBulkDataChanged(tableData, columns)
{
    return prepareXmlBulkData(tableData, columns, true);
}


var __g_SELECT_COLUMN = "Select";

function prepareXmlBulkDataFiltered(tableData, columns, filterColumn)
{
    if (!filterColumn)
        filterColumn = __g_SELECT_COLUMN;

    var resultText = "<root>";
    var numberOfRows = tableData.length;
    for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++)
    {
        var itemValue = convertFieldBooleanData(tableData[rowIndex][filterColumn]);
        if (itemValue == "Y")
            resultText += prepareXmlRecord(tableData, columns, rowIndex, false);
    }

    resultText += "</root>";
    return resultText;
}


function getNumberOfSelectedItems(tableData, filterColumn)
{
    var numberOfSelectedItems = 0;

    if (!filterColumn)
        filterColumn = __g_SELECT_COLUMN;

    var numberOfRows = tableData.length;
    for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++)
    {
        var itemValue = convertFieldBooleanData(tableData[rowIndex][filterColumn]);
        if (itemValue == "Y")
            numberOfSelectedItems++;
    }

    return numberOfSelectedItems;
}


function getXmlBulkDataSpParamFiltered(filterColumn)
{
    var XmlBulkData = prepareXmlBulkDataFiltered(_mainTableData, g_Send_ToServer_Columns, filterColumn);
    return " '" + XmlBulkData + "' ";
}



function isXmlBulkDataEmpty(XmlBulkDataSpParam)
{
    if (XmlBulkDataSpParam.length < 18)
        return true;

    return false;
}


function setSendToServerTable()
{
    assignValueToElement('_sendToServerTable_UpdateServerTable_Metadata', g_sendToServerTable_Metadata_0);
    var dataForUpload = gatherUploadData(_mainTableData, g_Send_ToServer_Columns);
    assignValueToElement('_sendToServerTable_UpdateServerTable_Data', dataForUpload);
}

function getXmlBulkDataHttpParam()
{
    var XmlBulkData = gatherXmlBulkData(_mainTableData, g_Send_ToServer_Columns);
    return '&' + 'XmlBulkData' + '=' + XmlBulkData;
}


function getXmlBulkDataSpParam()
{
    var XmlBulkData = gatherXmlBulkData(_mainTableData, g_Send_ToServer_Columns);
    return " '" + XmlBulkData + "' ";
}

function getXmlBulkDataSpParamAll()
{
    var XmlBulkData = gatherXmlBulkDataAll(_mainTableData, g_Send_ToServer_Columns);
    return " '" + XmlBulkData + "' ";
}

function getXmlBulkDataSpParamChanged()
{
    var XmlBulkData = gatherXmlBulkDataChanged(_mainTableData, g_Send_ToServer_Columns);
    return " '" + XmlBulkData + "' ";
}


function getParamsForSendToServerTable()
{
    var params = '&' + '_sendToServerTable_UpdateServerTable_Metadata' + '=' + g_sendToServerTable_Metadata_0;
    var dataForUpload = gatherUploadData(_mainTableData, g_Send_ToServer_Columns);
    params += '&' + '_sendToServerTable_UpdateServerTable_Data' + '=' + dataForUpload;

    return params;
}


function getCurrentLogonSpParam()
{
    return " '" + getCurrentLogon() + "' ";
}


function validateNumericElementSet(element$)
{
    var noProblem = true;

    element$.each(
            function ()
            {
                var this$ = $(this);
                var numericElementValue = new Number(this$.val());
                if (isNaN(numericElementValue))
                    noProblem = false;

                return true;
            }
        );

    return noProblem;
}


function replaceBlankWithZeroInElementSet(element$)
{
    var noBlank = true;

    element$.each(
            function ()
            {
                var this$ = $(this);
                var elementValue = $.trim(this$.val());
                if (elementValue == '')
                {
                    this$.val('0');
                    noBlank = false;
                }

                return true;
            }
        );

    return noBlank;
}


function replaceBlankWithZeroInElementSetTableData(tableData, columnName)
{
    for (var rowIndex = 0; rowIndex < tableData.length; rowIndex++)
    {
        var row = tableData[rowIndex];

        var elementValue = $.trim(row[columnName]);
        if (elementValue == '')
            row[columnName] = '0'
    }
}

var __g_USER_ACTION = '_USER_ACTION';

function assignFormUserAction(formRef, actionName)
{
    createInputParameter(__g_USER_ACTION, actionName, formRef);
}


function getFormUserAction()
{
    return getPageElementValue(__g_USER_ACTION);
}


function setClickOnItem$(item$, itemText, clickFunction)
{
    if (itemText != null)
        item$.text(itemText);
    item$.click(clickFunction);
    item$.css({ 'color': 'blue', 'text-decoration': 'underline', 'cursor': 'pointer' });
}

function setClickOnItems(itemId, itemText, clickFunction)
{
    var item$ = $('td[id*=' + itemId + ']');
    setClickOnItem$(item$, itemText, clickFunction);
}

var __g_PROBLEM_OK = '0';
var __g_PROBLEM_WARNING = '4';
var __g_PROBLEM_ERROR = '9';

function setProblemsWithJson(jsonText)
{
    var problemData = null;

    try
    {
        problemData = $.parseJSON(jsonText);
    }
    catch (ex)
    {
        alert("Exception " + ex.message);
        return;
    }

    if (problemData == null)
        return;

    for (var rowIndex = 0; rowIndex < problemData.length; rowIndex++)
    {
        var itemRowIndex = problemData[rowIndex].RowIndex;

        if (problemData[rowIndex].ProblemLevel == __g_PROBLEM_ERROR)
            addErrorForInput($("#" + problemData[rowIndex].ItemName + itemRowIndex), problemData[rowIndex].ProblemDescription);

        if (problemData[rowIndex].ProblemLevel == __g_PROBLEM_WARNING)
            addWarningForInput($("#" + problemData[rowIndex].ItemName + itemRowIndex), problemData[rowIndex].ProblemDescription);
    }
}

function updateElementsFromJson(resultText, asTable)
{
    try
    {
        var recordData = jQuery.parseJSON(resultText);
        var numberOfRows = recordData.length;

        for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++)
        {
            var rowData = recordData[rowIndex];
            $.map(rowData, function (value, key)
            {
                var elementId = key;
                if (asTable)
                    elementId += rowIndex;
                assignValueToElement(elementId, value);
            }
            );
        }
    }
    catch (ex)
    {
        alert("Exception " + ex.message);
        return;
    }
}


function blockFormAutoSubmit() { return false; }


var _g_OPTION_TEXT_EMPTY = " - EMPTY - ";
var _g_OPTION_TEXT_SELECT = " - SELECT - ";
var _g_OPTION_TEXT_ANY = " - ANY - ";

function setContentToSelect(sourceSelectId, targetSelectId, storedProcName, prefixText)
{
    var sourceSelect = document.getElementById(sourceSelectId);
    if (sourceSelect == null)
    {
        alert("please check the SELECT item with id [" + sourceSelectId + "].");
        return;
    }

    var sqlRequest = runCommand(storedProcName) + "'" + sourceSelect.value + "' ";
    var resultText = getTextFromServerUaWithoutMessage(sqlRequest, RESULT_TYPE_SELECT, ACTION_CODE_GET_MANY);

    var selectText = '';
    if (prefixText != null && prefixText != '')
        selectText += ("<option value='' >" + prefixText + "</option>");
    selectText += resultText;

    $("#" + targetSelectId).html(selectText);
}


var _g_CHECK_BOX_ID_PREFIX = "chkbox_";

function generateThCheckboxId(originalId)
{
    return _g_CHECK_BOX_ID_PREFIX + "_th_" + originalId;
}

function generateCheckboxId(originalId)
{
    return _g_CHECK_BOX_ID_PREFIX + originalId;
}



function convertItemValueToBoolean(itemValue)
{
    if (itemValue == true || itemValue == false)
        if (typeof itemValue === "boolean")
            return itemValue;

    if (typeof itemValue !== "string")
        return false;

    itemValue = itemValue.toUpperCase();
    if (itemValue === "TRUE" || itemValue === "Y")
        itemValue = true;

    if (itemValue === "FALSE" || itemValue === "N")
        itemValue = false;

    return itemValue;
}


function setCheckBoxOnItems(columnName, tableData)
{
    setCheckBoxOnItems2(columnName, tableData, true);
}

function setCheckBoxOnItems2(columnName, tableData, initialColumnValue)
{
    var thCheckboxId = generateThCheckboxId(columnName);
    var thCheckbox$ = $("[id=" + thCheckboxId + "]");
    // create a new check box in the column header if does not exist
    if (thCheckbox$.length < 1)
    {
        thCheckbox$ = $("<input />", { type: "checkbox", id: thCheckboxId });
        thCheckbox$.attr("checked", initialColumnValue);
        var thCheckboxProperties = { columnId: columnName };
        var thItem$ = $("th[id=th_" + columnName + "]");
        thCheckbox$.appendTo(thItem$);

        thCheckbox$.click(function ()
        {
            var checkedFlag = this.checked;
            for (var rowIndex = 0; rowIndex < tableData.length; rowIndex++)
                setValueToTableCell(tableData, rowIndex, columnName, checkedFlag);

            var tdItem$ = $("td[id*=" + columnName + "]");

            tdItem$.each(
                function ()
                {
                    var aCheckbox$ = $("#" + generateCheckboxId(this.id));
                    aCheckbox$.attr("checked", checkedFlag);
                    //                    checkboxChangeFunction(this.id, columnName, tableData, aCheckbox$);
                }
                );
        });
    }


    // create one check box for each cell if does not exist
    var tdItem$ = $("td[id*=" + columnName + "]");
    tdItem$.each(
        function ()
        {
            this.innerText = "";
            var rowIndex = getRowIndexFromName(this.id, columnName);
            if (rowIndex == -1)
                return;

            var itemValue = convertItemValueToBoolean(tableData[rowIndex][columnName]);

            if (itemValue == true || itemValue == false) { }
            else
            {
                if (itemValue.toUpperCase() == "TRUE" || itemValue.toUpperCase() == "Y")
                    itemValue = true;
                else
                    itemValue = false;
            }

            var tdCheckboxId = generateCheckboxId(this.id);
            var newCheckbox$ = $("#" + tdCheckboxId + " input[type=checkbox]");
            // create a new check box in the column header if does not exist
            if (newCheckbox$.length < 1)
            {
                newCheckbox$ = $("<input />", { type: "checkbox", id: tdCheckboxId });
                newCheckbox$.attr("checked", itemValue);
                var checkboxProperties = { tdId: this.id, columnId: columnName, tableData: tableData };
                newCheckbox$.change(checkboxProperties, checkboxChangeEventHandle);
                newCheckbox$.appendTo(this);
            }
        }
     );
}


function setValueToTableCell(tableData, rowIndex, columnName, itemValue)
{
    if (tableData[rowIndex][columnName] != itemValue)
    {
        if (tableData[rowIndex][RECORD_CHANGE_FIELD] !== undefined)
        {
            tableData[rowIndex][RECORD_CHANGE_FIELD] = 1;
            setPageDataWereChanged();
        }

        tableData[rowIndex][columnName] = itemValue;
    }
}


function checkboxChangeFunction(tdItemId, columnName, tableData, this$)
{
    var rowIndex = getRowIndexFromName(tdItemId, columnName);

    if (isNaN(rowIndex))
    {
        //alert('rowIndex ' + rowIndex + ' tdItemId ' + tdItemId);
        return;
    }

    if (rowIndex == -1)
    {
        alert('id ' + elementId + ' columnName ' + columnName);
        return;
    }

    var checkedFlag = this$.prop("checked");
    setValueToTableCell(tableData, rowIndex, columnName, checkedFlag);
}


function checkboxChangeEventHandle(checkboxProperties)
{
    var tdItemId = checkboxProperties.data.tdId;
    var columnName = checkboxProperties.data.columnId;
    var tableData = checkboxProperties.data.tableData;
    checkboxChangeFunction(tdItemId, columnName, tableData, $(this));
}


function clearFormUserAction(dataFormRef) { assignFormUserAction(dataFormRef, ""); }

function createTableTextFromArray(tableData)
{
    var bodyText = "";

    for (var rowIndex = 0; rowIndex < tableData.length; rowIndex++)
    {
        var rowData = tableData[rowIndex];
        var rowText = "<tr id='row" + rowIndex + "' >";
        for (columnName in rowData)
        {
            var itemValue = $.trim(tableData[rowIndex][columnName]);
            if ("" === itemValue)
                itemValue = "&nbsp";

            rowText += " <td id='" + columnName + rowIndex + "' >";
            rowText += itemValue;
            rowText += "</td>";
        }

        rowText += " </tr>";
        bodyText += rowText;
    }

    return bodyText;
}


function createTableTextFromArrayAndColumnList(tableData, columnList)
{
    var bodyText = "";

    for (var rowIndex = 0; rowIndex < tableData.length; rowIndex++)
    {
        var rowText = "<tr id='row" + rowIndex + "' >";
        for (var columnIndex = 0; columnIndex < columnList.length; columnIndex++)
        {
            var columnName = columnList[columnIndex];
            if (!tableData[rowIndex].hasOwnProperty(columnName))
            {
                alert("There is not property [" + columnName + "] in the data object!");
                return;
            }

            var itemValue = $.trim(tableData[rowIndex][columnName]);
            if ("" === itemValue)
                itemValue = "&nbsp";

            rowText += " <td id='" + columnName + rowIndex + "' >";
            rowText += itemValue;
            rowText += "</td>";
        }

        rowText += " </tr>";
        bodyText += rowText;
    }

    return bodyText;
}


function requestCsvFile(sqlRequest, header, columnNames)
{
    sqlRequest = sqlRequest.replace(/=/g, "~`~`~`~");
    sqlRequest = sqlRequest.replace(/&/g, "~```~```");
    sqlRequest = sqlRequest.replace(/\?/g, "`~~``~~");
    sqlRequest = sqlRequest.replace(/\#/g, "```~~```~~");

    var dataFormRef = $('form').get(0);
    createInputParameter("sql", sqlRequest, dataFormRef);
    createInputParameter("Header", header, dataFormRef);
    createInputParameter("ColumnNames", columnNames, dataFormRef);
    dataFormRef.action = getPortalPrefix() + "/Cc/CsvDataRequest";
    dataFormRef.submit();
}


function checkEmptyField(fieldId, noProblem)
{
    removeProblemFromInput("#" + fieldId);
    var fieldValue = $.trim(getPageElementValue(fieldId));
    if (fieldValue != "")
        return noProblem;

    addErrorForInput($("#" + fieldId), "The field must have value.");
    return false;
}


function compareDateRange(dateFrom, dateTo, noProblem, isDateToMandatory)
{
    if (isDateToMandatory == null)
        isDateToMandatory = true;

    removeProblemFromInput($("#disp_d_" + dateFrom));
    removeProblemFromInput($("#disp_d_" + dateTo));

    var dtDateFrom = getDateFromElement(dateFrom);
    if (isNaN(dtDateFrom))
    {
        addErrorForInput($("#disp_d_" + dateFrom), "Incorrect format of the data field.");
        return false;
    }

    var fieldValue = $.trim(getPageElementValue(dateTo));
    if (fieldValue == "")
    {
        if (isDateToMandatory)
        {
            addErrorForInput($("#disp_d_" + dateTo), "The field must have value.");
            return false;
        }
        else // as the DateTo is empty and optional, we cannot compare
            return noProblem;
    }

    var dtDateTo = new Date(fieldValue);
    if (isNaN(dtDateTo))
    {
        addErrorForInput($("#disp_d_" + dateTo), "Incorrect format of the data field.");
        return false;
    }

    if (dtDateFrom > dtDateTo)
    {
        addErrorForInput($("#disp_d_" + dateFrom), "The first date in the range must be earlier than the last date.");
        addErrorForInput($("#disp_d_" + dateTo), "The first date in the range must be earlier than the last date.");
        return false;
    }

    return noProblem;
}


function setProgressImageOn() { if ($("#progress_gif_image").length > 0) $("#progress_gif_image")[0].style.display = ""; }
function setProgressImageOff() { if ($("#progress_gif_image").length > 0) $("#progress_gif_image")[0].style.display = "none"; }

function composeProcedureRequest(procedureName, itemArray)
{
    var sqlRequest = runCommand(procedureName);
    var arrayLength = itemArray.length;
    for (var index = 0; index < arrayLength; index++)
    {
        var itemName = itemArray[index];
        var itemValue = $.trim(getPageElementValue(itemName));
        itemValue = itemValue.replace("'", "`");
        sqlRequest += "'";
        sqlRequest += itemValue;
        sqlRequest += "'";
        if ((index + 1) < arrayLength)
            sqlRequest += ",";
    }

    return sqlRequest;
}



function createXmlDocumentFromText(xmlText)
{
    if (window.DOMParser)
    {
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(xmlText, "text/xml");
    }
    else // code for IE
    {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = false;
        xmlDoc.loadXML(xmlText);
    }
    return xmlDoc;
}


function setJsonResponseText(xmlText)
{
    __p_Json_Request_Result_Text = xmlText;
}

function getJsonResponseText()
{
    if (typeof __p_Json_Request_Result_Text == 'undefined')
        return "";
    return __p_Json_Request_Result_Text;
}


function createXmlDocumentFromJsonResponse()
{
    return createXmlDocumentFromText(getJsonResponseText());
}


function getSectionTextFromJsonResponse(sectionName)
{
    var sectionText = "";
    try
    {
        var xmlDoc = createXmlDocumentFromJsonResponse();
        section = xmlDoc.getElementsByTagName(sectionName);
    }
    catch (ex)
    {
        alert(ex.message);
    }

    if (section.length > 0)
        return ($(section[0]).text());

    return "";
}


function getProblemDataTextFromJsonResponse()
{
    return getSectionTextFromJsonResponse("problem_data");
}


function sendSynchronousPostRequest(requestUrl, httpParameters)
{
    enableInput(false);
    setProgressImageOn();
    var resultData = " ";
    var xmlhttp = $.ajax(
        { type: 'POST',
            async: false,
            url: requestUrl,
            data: httpParameters,
            success: function (data)
            {
                resultData = data;
            },
            error: function (request, status, error)
            {
                if (request.status && request.status == 400)
                {
                    alert(request.responseText);
                    enableInput(true);
                }
                else
                {
                    alert("Something went wrong, error: " + error);
                    enableInput(true);
                }
            }
        });

    xmlhttp = null;
    enableInput(true);
    setProgressImageOff();
    return $.trim(resultData);
}


function populateSelect(selectElementId, tableData)
{
    var selectElement$ = $("#" + selectElementId);
    populateSelectElement(selectElement$, tableData);
}


function populateSelectElement(selectElement$, tableData)
{
    selectElement$.empty();
    var columnList = getColumnList(tableData);
    var keyColumn = columnList[0];
    var valueColumn = columnList[1];

    var numberOfItems = tableData.length;
    for (var rowIndex = 0; rowIndex < numberOfItems; rowIndex++)
    {
        var rowData = tableData[rowIndex];
        var value = rowData[keyColumn];
        var text = rowData[valueColumn];
        var optionProps = { value: value, text: text };
        selectElement$.append($('<option>', optionProps));
    }
}


function getColumnList(tableData)
{
    var keyList = [];
    var numberOfRows = tableData.length;
    if (numberOfRows < 1)
        return keyList;

    var rowData = tableData[0];

    // newer browsers should use -> Object.keys(rowData);
    for (var key in rowData)
    {
        if (!rowData.hasOwnProperty(key))
            continue;

        if (key == "RecordId")
            continue;

        if (key == "_chng")
            continue;

        keyList.push(key);
    }

    return keyList;
}


function createTableHead(columnList)
{
    if (columnList.length < 1)
        return "";

    var headText = "<thead><tr id='h_row_mainTableHeadRow'  >";

    for (var columnIndex = 0; columnIndex < columnList.length; columnIndex++)
    {
        var columnId = columnList[columnIndex];
        var columnName = columnId.replace("_", " ");
        if (columnName == "Select")
            columnName = " ";
        headText += "<th  id='th_" + columnId + "' >&nbsp;&nbsp;" + columnName + "&nbsp;</th>";
    }

    headText += "</tr></thead>";

    return headText;
}


function createTableBodyFromData(tableData, columnList, evenRowClassName)
{
    var bodyText = "";
    var numberOfRows = tableData.length;
    if (numberOfRows < 1)
        return bodyText;

    bodyText = "<tbody>";
    for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++)
    {
        var rowText = "<tr id='row" + rowIndex + "' ";

        if (rowIndex % 2 == 0)
            rowText += " class='" + evenRowClassName + "' ";
        rowText += " >";
        for (var columnIndex = 0; columnIndex < columnList.length; columnIndex++)
        {
            var columnName = columnList[columnIndex];

            var rowData = tableData[rowIndex];

            if (!rowData.hasOwnProperty(columnName))
            {
                alert("There is no property [" + columnName + "] in the data object!");
                return;
            }

            var itemValue = $.trim(rowData[columnName]);
            if ("" === itemValue)
                itemValue = "&nbsp";

            rowText += " <td id='" + columnName + rowIndex + "' >";
            rowText += "&nbsp;" + itemValue + "&nbsp;";
            rowText += "</td>";
        }

        rowText += " </tr>";
        bodyText += rowText;
    }

    bodyText += "</tbody>";
    return bodyText;
}


function createTableFromData(tableData)
{
    var columnList = getColumnList(tableData);
    var tableHead = createTableHead(columnList);

    var evenRowClassName = "evenRow";
    var tableBody = createTableBodyFromData(tableData, columnList);
    var tableText = tableHead + tableBody;

    return tableText;
}


function Sec_isUserGrantedAny(securityFunction)
{
    if (typeof __g_User_Functions == 'undefined' || __g_User_Functions.length < 1)
        return true;

    securityFunction = securityFunction.toUpperCase();
    if (-1 == jQuery.inArray(securityFunction, __g_User_Functions))
        return false;
    return true;
}

var _g_CELL_SELECT_ID_PREFIX = "select_options_";

function makeIdForCellSelect(recordId, columnName)
{
    return _g_CELL_SELECT_ID_PREFIX + recordId + "_" + columnName;
}


function setSelectOnItems(columnName, tableData, selectHtml)
{
    // create one select for each cell if does not exist
    var tdItem$ = $("td[id*=" + columnName + "]");
    tdItem$.each(
        function ()
        {
            this.innerText = "";
            var rowIndex = getRowIndexFromName(this.id, columnName);
            if (rowIndex == -1)
                return;

            var itemValue = tableData[rowIndex][columnName];
            var recordId = tableData[rowIndex]["RecordId"];

            var selectId = makeIdForCellSelect(recordId, columnName);
            var select$ = $("#" + selectId + " select");
            if (select$.length < 1)
            {
                select$ = $("<select />", { id: selectId });
                select$.html(selectHtml);
                select$.val(itemValue);
                //select$.click(function () { alert($(this).attr("id")); });
                select$.appendTo(this);
            }
        }
     );
}


function hasElementAttribute(element$, attrName)
{
    var attr = element$.attr(attrName);

    if (typeof attr !== typeof undefined && attr !== false)
        return true;

    return false;
}


function hasElementValueAttribute(element$)
{
    if (element$[0].value !== undefined)
        return true;
    return false;
}


function assignVisibilityForElements(elementIdPatern, aVisibility)
{
    var item$ = $("[id*=" + elementIdPatern + "]");

    item$.each(function ()
    {
        this.style.visibility = aVisibility;
    });
}


function setVisibilityForElements(elementIdPatern, isVisible)
{
    var aVisibility = 'hidden';
    if (isVisible)
        aVisibility = 'visible';

    assignVisibilityForElements(elementIdPatern, aVisibility);
}


function checkNumericField(fieldId, noProblem)
{
    var nonEmpty = true;
    nonEmpty = checkEmptyField(fieldId, nonEmpty);
    if (!nonEmpty)
        return false;

    if (isNumericElement(elementId)(fieldId))
        return noProblem;

    addErrorForInput($("#" + fieldId), "The field must have numeric value.");
    return false;
}


function checkPositiveNumericField(fieldId, noProblem)
{
    var nonEmpty = true;
    nonEmpty = checkEmptyField(fieldId, nonEmpty);
    if (!nonEmpty)
        return false;

    if (isPositiveNumericElement(fieldId))
        return noProblem;

    addErrorForInput($("#" + fieldId), "The field must have positive numeric value.");
    return false;
}


function isIntegerValue(x)
{
    //return (typeof x === 'number') && (x % 1 === 0);
    return (x % 1 === 0);
}


function checkIntegerField(fieldId, noProblem)
{
    var nonEmpty = checkEmptyField(fieldId, noProblem);
    if (!nonEmpty)
        return false;

    var elementValue = getPageElementValue(fieldId);
    var numericElementValue = stringToNumeric(elementValue);
    if (isNaN(numericElementValue))
    {
        addErrorForInput($("#" + fieldId), "The field must have numeric value.");
        return false;
    }

    if (isIntegerValue(numericElementValue))
    {
        return noProblem;
    }

    addErrorForInput($("#" + fieldId), "The field must have integer numeric value.");
    return false;
}


function addSpanWithClickToItem$(item$, itemText, clickFunction)
{
    var span$ = $("<span>");
    span$.text(itemText);
    span$.click(clickFunction);
    span$.css({ 'color': 'blue', 'text-decoration': 'underline', 'cursor': 'pointer' });
    item$.append(" ");
    item$.append(span$);
}


function addSpanWithClickToItems(itemId, itemText, clickFunction)
{
    var item$ = $('td[id*=' + itemId + ']');
    addSpanWithClickToItem$(item$, itemText, clickFunction);
}


function getRowIndexByRecordId(tableData, currentRecordId)
{
    for (var rowIndex = 0; rowIndex < tableData.length; rowIndex++)
    {
        //var recordId = new Number(tableData[rowIndex].RecordId);
        var recordId = tableData[rowIndex].RecordId;
        if (currentRecordId == recordId)
            return rowIndex;
    }

    return -1;
}


function isNonNegativeNumericValue(elementValue)
{
    var numericElementValue = stringToNumeric(elementValue);

    if (isNaN(numericElementValue))
        return false;

    if (numericElementValue < 0.00000)
        return false;

    return true;
}


function isNonNegativeNumericElement(elementId)
{
    var elementValue = getPageElementValue(elementId);
    return isNonNegativeNumericValue(elementValue);
}


function checkNonNegativeNumericField(fieldId, noProblem)
{
    var nonEmpty = true;
    nonEmpty = checkEmptyField(fieldId, nonEmpty);
    if (!nonEmpty)
        return false;

    if (isNonNegativeNumericElement(fieldId))
        return noProblem;

    addErrorForInput($("#" + fieldId), "The field must have non negative numeric value.");
    return false;
}
