var _g_dictionaryYesNo = [{ "value": ' ', "text": '' }, { "value": 'Y', "text": 'Yes' }, { "value": 'N', "text": 'No'}];
var _g_dictionaryYesNoNa = [{ "value": ' ', "text": '' }, { "value": 'Y', "text": 'Yes' }, { "value": 'N', "text": 'No' }, { "value": 'N/A', "text": 'N/A'}];

var _p_Custom_Fields_Data_Object;
var _p_Custom_Fields_Properties;

var _CF_DATE_TYPE = "date";
var _CF_STRING_TYPE = "string";
var _CF_INITIAL_VALUE_ATTRIBUTE = "data-cf-initial-value";
var _CF_EMPTY_READ_ONLY_STRING = "- undefined -";

var _CF_MONEY_COMPARISON_DELTA = 0.001;

var _CF_NUMBER_OF_COLUMNS_ATTRIBUTE = "data-cf-number-of-columns";
var _CF_LABEL_POSITION_ATTRIBUTE = "data-cf-label-position";
var _CF_LABEL_POSITION_LEFT = "left";
var _CF_LABEL_POSITION_ABOVE = "above";

var _CF_LABEL_POSITION_DEFAULT = _CF_LABEL_POSITION_ABOVE;
var _CF_NUMBER_OF_COLUMNS_DEFAULT = "1";

var _CF_LABEL_POSITION_UNDEFINED = "";
var _CF_NUMBER_OF_COLUMNS_UNDEFINED = "-1";


function CustomFields_CreatePlaceholderTdId(tableId, rowIndex, columnIndex)
{
    return "td_pl_" + tableId + "_" + rowIndex + "_" + columnIndex;
}

function CustomFields_Commafy(pass)
{
    var re = /(-?\d+)(\d{3})/
    var num = pass + ""

    while (re.test(num))
    {
        num = num.replace(re, "$1,$2")
    }
    return (num);
}


function CustomFields_MoneyFormat(originalValue, decimalPlaces)
{
    if (originalValue == "")
        return originalValue;

    var numberValue = new Number(originalValue);
    if (isNaN(numberValue))
        return (originalValue);

    try
    {
        var str = "" + Math.round(numberValue * Math.pow(10, decimalPlaces));
        while (str.length <= decimalPlaces)
            str = "0" + str;
        var decimalPoint = str.length - decimalPlaces;
        var newValue = str.substring(0, decimalPoint) + "." + str.substring(decimalPoint, str.length);
        return CustomFields_Commafy(newValue);
    }
    catch (e)
    {
        return originalValue;
    }
}


function CustomFields_MoneyValueEqual(initialValue, currentValue)
{
    initialValue = initialValue.replace(/,/g, "");
    initialValue = new Number(initialValue);
    if (isNaN(initialValue))
        return true;

    currentValue = currentValue.replace(/,/g, "");
    currentValue = new Number(currentValue);
    if (isNaN(currentValue))
        return true;

    if (Math.abs(currentValue - initialValue) > _CF_MONEY_COMPARISON_DELTA)
        return false;

    return true;
}


//$(function ()
//{

// var customFieldsProperties = { readOnly: false, contextDocument: this.document };
// CustomFields_Initialize("div.Cf_Container", customFieldsProperties);
//});

var _CF_ATTR_DATA_MAX_LENGTH = "data-maxlength";
var _CF_ATTR_DATA_LABEL = "data-label";


function CustomFields_isMoneyType(fieldType)
{
    if (typeof (fieldType) != "string")
        return false;

    fieldType = fieldType.toLowerCase();

    if (fieldType == "money"
     || fieldType == "smallmoney"
    )
        return true;

    return false;
}


function CustomFields_isNumericType(fieldType)
{
    if (typeof (fieldType) != "string")
        return false;

    fieldType = fieldType.toLowerCase();

    if (fieldType == "int"
     || fieldType == "money"
     || fieldType == "smallmoney"
     || fieldType == "decimal"
     || fieldType == "numeric"
     || fieldType == "bigint"
     || fieldType == "tinyint"
     || fieldType == "smallint"
     || fieldType == "float"
     || fieldType == "real"
     )
        return true;

    return false;
}

function CustomFields_checkSize()
{
    var textAreaItem$ = $(this);
    removeProblemFromInput(textAreaItem$);
    var maxLength = textAreaItem$.attr(_CF_ATTR_DATA_MAX_LENGTH);

    if (typeof maxLength !== "undefined")
    {
        if (this.value.length > maxLength)
        {
            this.blur();
            var problemText = "Text cannot exceed " + maxLength + " characters in the field [" + textAreaItem$.attr(_CF_ATTR_DATA_LABEL) + "].";
            addErrorForInput(textAreaItem$, problemText);
            alert(problemText);
            this.value = this.value.substr(0, maxLength);
        }
    }
}


function CustomFields_checkForNumeric()
{
    var element$ = $(this);
    removeProblemFromInput(element$);
    var elementValue = element$.val();
    elementValue = elementValue.replace(/,/g, "");
    var itemValue = new Number(elementValue);
    if (isNaN(itemValue))
    {
        var problemText = "Integer value is expected for the field [" + element$.attr(_CF_ATTR_DATA_LABEL) + "].";
        addErrorForInput(element$, problemText);
        element$.val("");
        alert(problemText);
        element$.focus();
    }
}


function CustomFields_convertDictionatyIdToName(dictionary, elementId)
{
    for (var index = 0; index < dictionary.length; index++)
    {
        var dictionaryItem = dictionary[index];
        if (dictionaryItem.value == elementId)
            return dictionaryItem.text;
    }

    return "";
}

function CustomFields_FindDictionary(responseData, dictionaryName)
{
    if (typeof dictionaryName == "undefined" || dictionaryName == null || dictionaryName == "")
    {
        return null;
    }

    var dictionaryResult = true;
    var dictionarySection = responseData.Dictionaries;
    var dictionaryData = null;

    if (typeof dictionarySection == "undefined" || dictionarySection == null)
    {
        dictionaryResult = false;
    }
    else
    {
        dictionaryData = responseData.Dictionaries.Data;
        if (typeof dictionaryData == "undefined" || dictionaryData == null)
        {
            dictionaryResult = false;
        }
        else
        {
            if(dictionaryData.length == 0)
                dictionaryResult = false;
        }
    }

    if (dictionaryResult == false)
    {
        alert("No data found for the dictionary [" + dictionaryName + "].");
        return null;
    }

    var numberOfDictionaries = dictionaryData.length;
    for (var dictionaryIndex = 0; dictionaryIndex < numberOfDictionaries; dictionaryIndex++)
    {
        if (dictionaryData[dictionaryIndex].Name == dictionaryName)
            return dictionaryData[dictionaryIndex].Values;
    }

    return null;
}


function CustomFields_getDictionary(responseData, dictionaryName)
{
    var dictionaryValues = CustomFields_FindDictionary(responseData, dictionaryName);
    var newDictionary = CustomFields_NormalizeDictionary(dictionaryValues);
    return newDictionary;
}


function CustomFields_NormalizeDictionary(dictionaryValues)
{
    if (dictionaryValues == null)
        return null;

    var newDictionary = []; 
    var emptyItem = { "value": ' ', "text": '' };
    newDictionary.push(emptyItem);
    var numberOfItems = dictionaryValues.length;
    for (var itemIndex = 0; itemIndex < numberOfItems; itemIndex++)
    {
        var newItem = { "value": dictionaryValues[itemIndex], "text": dictionaryValues[itemIndex] };
        newDictionary.push(newItem);
    }

    return newDictionary;
}


function CustomFields_Initialize(cfContainerClass, customFieldsProperties)
{
    CustomFields_Initialize2(cfContainerClass, customFieldsProperties, _CF_LABEL_POSITION_ABOVE, 1);
}


function CustomFields_Initialize2(cfContainerClass, customFieldsProperties, labelPosition, numberOfColumns)
{
    if (typeof customFieldsProperties == "undefined")
    {
        customFieldsProperties = {};
    }

    if (!customFieldsProperties["readOnly"])
    {
        customFieldsProperties.readOnly = false;
    }

    if (!customFieldsProperties["additionalApplyTo"])
    {
        customFieldsProperties.additionalApplyTo = [];
    }

    if (!customFieldsProperties["contextDocument"])
    {
        customFieldsProperties.contextDocument = document;
    }

    _p_Custom_Fields_Properties = customFieldsProperties;
    var requestObject = [];
    var container$ = $(cfContainerClass, customFieldsProperties.contextDocument);
    for (var containerIndex = 0; containerIndex < container$.length; containerIndex++)
    {
        var container = container$[containerIndex];
        var object_registry_id = $(container).attr("data-cf-ObjectRegistryId");
        var object_key = $(container).attr("data-ObjectKey");
        var apply_to = $.parseJSON($(container).attr("data-cf-Applyto"));

        for (var applyToIndex = 0; applyToIndex < customFieldsProperties.additionalApplyTo.length; applyToIndex++)
        {
            apply_to.push(customFieldsProperties.additionalApplyTo[applyToIndex]);
        }

        requestObject[containerIndex] = { "ObjectRegistryId": object_registry_id
                                        , "ObjectKey": object_key
                                        , "Applyto": apply_to, "RequestorId": container.id
                                        , "ReadOnly": customFieldsProperties.readOnly };
    }

    var responseData = getCfDataObject(requestObject);
    if (!responseData)
        return;

    try
    {
        _p_Custom_Fields_Data_Object = responseData;

        // for debug
        $("#Custom_Fields_Storage", customFieldsProperties.contextDocument).text(JSON.stringify(responseData));

        if (!responseData.Status.Success)
        {
            alert("Error while loading data\n" + responseData.Status.Description);
            return;
        }

        for (var sectionIndex = 0; sectionIndex < responseData.Data.length; sectionIndex++)
        {
            var sectionData = responseData.Data[sectionIndex];
            if (!sectionData.Status.Success)
            {
                alert(sectionData.Status.Description);
                continue;
            }

            CustomFields_CreateContainer(sectionData, sectionData.RequestorId, labelPosition, numberOfColumns);
        }
    }
    catch (e)
    {
        alert("Error info [" + e.message + "]; [" + e.name + "].");
    }
}


function CustomFields_CreateContainer(dataObject, containerId, labelPosition, numberOfColumns)
{
    var customFieldsBuilder = new CustomFieldsBuilder();
    var container$ = $("#" + containerId, _p_Custom_Fields_Properties.contextDocument);
    if (container$.length < 1)
    {
        alert("There is no container with ID=[" + containerId + "].");
        return;
    }

    container$.empty();

    for (var index = 0; index < dataObject.Groups.length; index++)
    {
        var group = dataObject.Groups[index];

        var numberOfColumnsAttr = new Number($("#" + containerId).attr(_CF_NUMBER_OF_COLUMNS_ATTRIBUTE));
        if (typeof numberOfColumnsAttr != "undefined")
        {
            if (!isNaN(numberOfColumnsAttr))
                numberOfColumns = numberOfColumnsAttr;
        }

        var labelPositionAttr = $("#" + containerId).attr(_CF_LABEL_POSITION_ATTRIBUTE);
        if (typeof labelPositionAttr != "undefined" && labelPositionAttr != "")
            labelPosition = labelPositionAttr;

        var NumberOfColumns = assignNumberOfColumns(numberOfColumns, group.NumberOfColumns);
        var LabelPosition = assignLabelPosition(labelPosition, group.LabelPosition);

        customFieldsBuilder.createGroupDiv(container$, containerId, index, group.Name, group.Fields, group.Caption, NumberOfColumns, LabelPosition);
    }
}


function assignLabelPosition(originalLabelPosition, backEndLabelPosition)
{
    // if the original was defined use it
    if (typeof originalLabelPosition != "undefined" && originalLabelPosition !=_CF_LABEL_POSITION_UNDEFINED)
    {
        return originalLabelPosition;
    }

    // if backEnd was not defined let's use the default
    if (typeof backEndLabelPosition == "undefined" || backEndLabelPosition ==_CF_LABEL_POSITION_UNDEFINED)
    {
        return _CF_LABEL_POSITION_DEFAULT;
    }

    return backEndLabelPosition;
}


function assignNumberOfColumns(originalNumberOfColumns, backEndNumberOfColumns)
{
    // if the original was defined use it
    if (typeof originalNumberOfColumns != "undefined" && originalNumberOfColumns != _CF_NUMBER_OF_COLUMNS_UNDEFINED)
    {
        return originalNumberOfColumns;
    }

    // if backEnd was not defined let's use the default
    if (typeof backEndNumberOfColumns == "undefined" || backEndNumberOfColumns == _CF_NUMBER_OF_COLUMNS_UNDEFINED)
    {
        return _CF_NUMBER_OF_COLUMNS_DEFAULT;
    }

    return backEndNumberOfColumns;
}


function assignLabelPosition(originalLabelPosition, backEndLabelPosition)
{
    // if the original defined use it
    if (typeof originalLabelPosition != "undefined" && originalLabelPosition != _CF_LABEL_POSITION_UNDEFINED)
    {
        return originalLabelPosition;
    }

    // if backEnd was not defined let's use the default
    if (typeof backEndLabelPosition == "undefined" || backEndLabelPosition == _CF_LABEL_POSITION_UNDEFINED)
    {
        return _CF_LABEL_POSITION_DEFAULT;
    }


    // only if the HTML page author did not define anything and backEnd defined, let's use backEnd
    return backEndLabelPosition;


    //var _CF_NUMBER_OF_COLUMNS_DEFAULT = "1";
}


function createFieldList(dataObject)
{
    var requestObject = {};
    requestObject.ObjectRegistryId = dataObject.ObjectRegistryId;
    requestObject.ObjectKey = dataObject.ObjectKey;
    requestObject.Fields = [];

    for (var index = 0; index < dataObject.Groups.length; index++)
    {
        var group = dataObject.Groups[index];
        var groupTitle = group.Name;
        var fieldData = group.Fields;
        for (var fieldIndex = 0; fieldIndex < fieldData.length; fieldIndex++)
        {
            var elementId = fieldData[fieldIndex].Id;
            var elementValue = CustomFields_getElementValueInContext(elementId, _p_Custom_Fields_Properties.contextDocument);
            var field = { Id: elementId, Value: elementValue };
            requestObject.Fields.push(field);
        }
    }
}


function CustomFields_formRequestObject(customFieldsDataObject)
{
    var requestObject = [];
    for (var sectionIndex = 0; sectionIndex < customFieldsDataObject.Data.length; sectionIndex++)
    {
        var sectionData = customFieldsDataObject.Data[sectionIndex];

        requestObject[sectionIndex] = { "SessionKey": sectionData.SessionKey, "ObjectRegistryId": sectionData.ObjectRegistryId, "ObjectKey": sectionData.ObjectKey, "RequestorId": sectionData.RequestorId };
        var fieldCounter = 0;
        var fields = [];
        for (var groupIndex = 0; groupIndex < sectionData.Groups.length; groupIndex++)
        {
            var group = sectionData.Groups[groupIndex];
            for (var fieldIndex = 0; fieldIndex < group.Fields.length; fieldIndex++)
            {
                var field = group.Fields[fieldIndex];
                fields[fieldCounter] = { Name: field.Id, Value: field.Value };
                fieldCounter++;
            }
        } // end group

        requestObject[sectionIndex].Fields = fields;
    }

    // debug
    $("#Custom_Fields_Request").text(JSON.stringify(requestObject));

    return requestObject;
}

function CustomFields_wasInitialized()
{
    if (typeof _p_Custom_Fields_Data_Object == "undefined")
    {
        return false;
    }

    return true;
}

function CustomFields_onDataSave(isEditableFlag)
{
    var isEditable = true;

    if (typeof isEditableFlag != "undefined")
    {
        isEditable = isEditableFlag;
    }

    if (!isEditable)
        return true;

    if (typeof _p_Custom_Fields_Data_Object == "undefined")
    {
        return false;
    }

    if (typeof _p_Custom_Fields_Data_Object.Data == "undefined")
    {
        return false;
    }

    var dataWereSaved = false;
    try
    {
        if (!CustomFields_ValidateDataObject(_p_Custom_Fields_Data_Object))
        {
            //alert("Field validation failed! Please correct.");
            return false;
        }

        var wasDataObjectChanged = CustomFields_WasDataObjectChanged(_p_Custom_Fields_Data_Object);
        if (!wasDataObjectChanged)
            return true;

        if (isEditable)
            CustomFields_UpdateDataObjectFromUserInterfaceItems(_p_Custom_Fields_Data_Object);

        var requestObject = CustomFields_formRequestObject(_p_Custom_Fields_Data_Object);

        var responseDataObject = saveCfDataObject(requestObject);
        if (!responseDataObject)
        {
            alert("No response from server.");
            return false;
        }

        // for debug
        $("#Custom_Fields_Save_Response").text(JSON.stringify(responseDataObject));

        CustomFields_removeProblemIndicatorsFromUserInterfaceItems(_p_Custom_Fields_Data_Object);
        dataWereSaved = CustomFields_setProblemIndicatorsToUserInterfaceItems(responseDataObject, isEditable);
    }
    catch (e)
    {
        alert("Error info [" + e.message + "]; [" + e.name + "].");
        dataWereSaved = false;
    }

    return dataWereSaved;
}



var _CUSTOM_FIELDS_LABEL_PREFIX = "label_";

function CustomFields_setProblemIndicatorsToUserInterfaceItems(responseDataObject, isEditable)
{
    var errorFound = false;
    // set new problem problem tooltips if any
    for (var sectionIndex = 0; sectionIndex < responseDataObject.Result.length; sectionIndex++)
    {
        var fieldsWithErrors = responseDataObject.Result[sectionIndex].FieldsWithErrors;

        for (var fieldIndex = 0; fieldIndex < fieldsWithErrors.length; fieldIndex++)
        {
            var field = fieldsWithErrors[fieldIndex];
            var elementId = field.Name;
            if (!isEditable)
                elementId = _CUSTOM_FIELDS_LABEL_PREFIX + elementId;
            var problemText = field.Value;
            var element$ = $("#" + elementId, _p_Custom_Fields_Properties.contextDocument);
            if (field.ErrorLevel == __g_PROBLEM_ERROR)
            {
                addErrorForInput(element$, problemText);
                errorFound = true;
            }
            else
                if (field.ErrorLevel == __g_PROBLEM_WARNING)
                {
                    addWarningForInput(element$, problemText);
                }

            if (isEditable)
                element$.focus();

        }
    } // end section

    return !errorFound;
}


function CustomFields_UpdateDataObjectFromUserInterfaceItems(customFieldsDataObject)
{
    for (var sectionIndex = 0; sectionIndex < customFieldsDataObject.Data.length; sectionIndex++)
    {
        var sectionData = customFieldsDataObject.Data[sectionIndex];

        for (var groupIndex = 0; groupIndex < sectionData.Groups.length; groupIndex++)
        {
            var group = sectionData.Groups[groupIndex];
            for (var fieldIndex = 0; fieldIndex < group.Fields.length; fieldIndex++)
            {
                var field = group.Fields[fieldIndex];
                var elementId = field.Id;
                var fieldType = field.Type.toLowerCase();
                if (fieldType == _CF_DATE_TYPE)
                    elementId = PREFIX_DATE_TYPE_ELEMENT + elementId;

                var elementValue = CustomFields_getElementValueInContext(elementId, _p_Custom_Fields_Properties.contextDocument);

                elementValue = elementValue.replace(/"/g, '`');
                elementValue = elementValue.replace(/\"/g, '`');
                if (CustomFields_isNumericType(fieldType))
                    elementValue = elementValue.replace(/,/g, "");

                field.Value = elementValue;
            }

        } // end group
    } // end section
}


function CustomFields_removeProblemIndicatorsFromUserInterfaceItems(customFieldsDataObject)
{
    // remove old problem tooltips
    for (var sectionIndex = 0; sectionIndex < _p_Custom_Fields_Data_Object.Data.length; sectionIndex++)
    {
        var sectionData = _p_Custom_Fields_Data_Object.Data[sectionIndex];
        for (var groupIndex = 0; groupIndex < sectionData.Groups.length; groupIndex++)
        {
            var group = sectionData.Groups[groupIndex];
            for (var fieldIndex = 0; fieldIndex < group.Fields.length; fieldIndex++)
            {
                var field = group.Fields[fieldIndex];
                var element$ = $("#" + field.Id, _p_Custom_Fields_Properties.contextDocument);
                removeProblemFromInput(element$);
            }
        } // end group
    } // end section
}


function CustomFields_getElementInitialValueInContext(elementId, contextObject)
{
    var element$ = $("#" + elementId, contextObject);
    if (element$.length < 1)
        return "";

    var elementValue = "";
    var attr = element$.attr(_CF_INITIAL_VALUE_ATTRIBUTE);
    if (typeof attr !== typeof undefined && attr !== false)
        elementValue = element$.attr(_CF_INITIAL_VALUE_ATTRIBUTE);

    return elementValue;
}


function CustomFields_getElementValueInContext(elementId, contextObject)
{
    var element$ = $("#" + elementId, contextObject);
    if (element$.length < 1)
        return "";

    var elementValue = "";
    var attr = element$.attr("value");
    if (typeof attr !== typeof undefined && attr !== false)
        elementValue = $.trim(element$.val());
    
    return elementValue;
}


function CustomFields_ValidateDataObject(customFieldsDataObject)
{
    var noProblem = true;
    var problemText = "";
    for (var sectionIndex = 0; sectionIndex < customFieldsDataObject.Data.length; sectionIndex++)
    {
        var sectionData = customFieldsDataObject.Data[sectionIndex];

        for (var groupIndex = 0; groupIndex < sectionData.Groups.length; groupIndex++)
        {
            var group = sectionData.Groups[groupIndex];
            for (var fieldIndex = 0; fieldIndex < group.Fields.length; fieldIndex++)
            {
                var field = group.Fields[fieldIndex];
                var elementId = field.Id;

                var fieldType = field.Type.toLowerCase();
                if (fieldType.toLowerCase() == _CF_DATE_TYPE)
                    elementId = PREFIX_DATE_TYPE_ELEMENT + elementId;

                var element$ = $("#" + elementId, _p_Custom_Fields_Properties.contextDocument);
                if (element$.length < 1) // if element with the Id exists
                    continue;

                removeProblemFromInput(element$);

                var elementValue = CustomFields_getElementValueInContext(elementId, _p_Custom_Fields_Properties.contextDocument);

                if (field.Required)
                {
                    if (elementValue == "")
                    {
                        noProblem = false;
                        problemText = "Required field should not be empty.";
                        addErrorForInput(element$, problemText);
                        element$.focus();
                        continue;
                    }
                }

                if (CustomFields_isNumericType(fieldType))
                {
                    var numberValue = new Number(elementValue.replace(/,/g, ""));
                    if (isNaN(numberValue))
                    {
                        noProblem = false;
                        var problemText = "Numeric value is expected for the field.";
                        addErrorForInput(element$, problemText);
                        element$.focus();
                        continue;
                    }
                }

                var elementSize = field.Size;
                if (elementSize > 1)
                {
                    if (elementValue.length > elementSize)
                    {
                        noProblem = false;
                        problemText = "Text cannot exceed " + elementSize + " characters in the field.";
                        addErrorForInput(element$, problemText);
                        continue;
                    }
                }
            }

        } // end group
    } // end section

    return noProblem;
}


function CustomFields_WasDataObjectChanged(customFieldsDataObject)
{
    var wasDataObjectChanged = false;
    for (var sectionIndex = 0; sectionIndex < customFieldsDataObject.Data.length; sectionIndex++)
    {
        var sectionData = customFieldsDataObject.Data[sectionIndex];

        for (var groupIndex = 0; groupIndex < sectionData.Groups.length; groupIndex++)
        {
            var group = sectionData.Groups[groupIndex];
            for (var fieldIndex = 0; fieldIndex < group.Fields.length; fieldIndex++)
            {
                var field = group.Fields[fieldIndex];
                var elementId = field.Id;

                var fieldType = field.Type.toLowerCase();
                if (fieldType.toLowerCase() == _CF_DATE_TYPE)
                    elementId = PREFIX_DATE_TYPE_ELEMENT + elementId;

                var element$ = $("#" + elementId, _p_Custom_Fields_Properties.contextDocument);
                if (element$.length < 1) // if element with the Id exists
                    continue;

                var elementValue = CustomFields_getElementValueInContext(elementId, _p_Custom_Fields_Properties.contextDocument);
                var elementInitialValue = CustomFields_getElementInitialValueInContext(elementId, _p_Custom_Fields_Properties.contextDocument);

                if (CustomFields_isNumericType(fieldType))
                    elementInitialValue = elementInitialValue.replace(/,/g, "");

                if (elementInitialValue != elementValue)
                    wasDataObjectChanged = true;

                if (CustomFields_isMoneyType(fieldType)) 
                    wasDataObjectChanged = !CustomFields_MoneyValueEqual(elementInitialValue, elementValue);

                if (wasDataObjectChanged == true)
                    return true;

            } // end field

        } // end group
    } // end section

    return wasDataObjectChanged;
}


function saveCfDataObject(requestObject)
{
    return makeSyncPostJsonRequest("/portal/net/webapi/customFields/SaveFieldsBulk", requestObject);
}

function getCfDataObject(requestObject)
{
    return makeSyncPostJsonRequest("/portal/net/webapi/customFields/GetFieldsBulk", requestObject);
}


function makeSyncPostJsonRequest(requestUrl, requestObject)
{
    var requestObjectString = JSON.stringify(requestObject);
    var responseData = null;
    try
    {
        $.ajax({
            type: "POST"
            , url: requestUrl
            , data: requestObjectString
            , contentType: "application/json; charset=utf-8"
            , dataType: "json"
            , async: false
            , success: function (data, textStatus, xhr) { responseData = data; }
            , error: onWebApiRequestError

        });
    }
    catch (e)
    {
        alert("Error info [" + e.message + "]; [" + e.name + "].");
    }

    return responseData;
}


function onWebApiRequestError(requestObject, textStatus, errorThrown)
{
    alert("Web API Server Request Error: [" + requestObject.responseText + "]; Status: [" + textStatus + "];  Error Thrown: [" + errorThrown + "].");
}


// === BUILDER 
function CustomFieldsBuilder()
{
    this.createLabel$ = function (elementId, elementText, isMandatory)
    {
        var span$ = $("<span>", { "id": _CUSTOM_FIELDS_LABEL_PREFIX + elementId });
        if (typeof elementText != "undefined")
            span$.text(elementText + ": ");

        if (isMandatory)
        {
            var mandatory$ = this.createMandatorySymbol$(elementId, " * ");
            span$.prepend(mandatory$);
        }

        span$.addClass("Dynamically_Generated_Label");
        return span$;
    }


    this.createSpan$ = function (elementId, elementText)
    {
        var span$ = $("<span>", { "id": _g_DISP_TEXT_PREFIX + elementId });
        if (typeof elementText != "undefined")
            span$.text(elementText);
        span$.addClass("Dynamically_Generated_Span");

        return span$;
    }


    this.createLastModifiedSpan$ = function (elementId, elementText)
    {
        var span$ = $("<span>", { "id": _g_DISP_TEXT_PREFIX + "_LastModified_" + elementId });
        if (typeof elementText != "undefined")
            span$.text(elementText);
        span$.addClass("small");

        return span$;
    }


    this.createInputText$ = function (elementId, elementText)
    {
        var element$ = $("<input>").attr({ "type": "text", "id": elementId, "name": elementId });
        if (typeof elementText != "undefined")
            element$.val(elementText);
        element$.addClass("Dynamically_Generated_Input_Text");
        return element$;
    }


    this.createTextArea$ = function (elementId, elementText)
    {
        var element$ = $("<textarea>", { "id": elementId, "name": elementId });
        element$.addClass("Dynamically_Generated_Textarea");
        if (typeof elementText != "undefined")
            element$.text(elementText);
        element$.attr("rows", "4");
        element$.css('width', 600);
        return element$;
    }


    this.createSelect$ = function (elementId)
    {
        var element$ = $("<select>", { "id": elementId, "name": elementId });
        element$.addClass("Dynamically_Generated_Select");
        return element$;
    }


    this.createTr$ = function (rowIndex)
    {
        var element$ = $("<tr>", { "id": "row" + rowIndex });
        element$.addClass("Dynamically_Generated_Tr");
        return element$;
    }


    this.createTd$ = function (elementId, rowIndex)
    {
        var element$ = $("<td>", { "id": "" + elementId + rowIndex });
        element$.addClass("Dynamically_Generated_Td");
        return element$;
    }


    this.createTable$ = function (elementId)
    {
        var element$ = $("<table>", { "id": ("table_" + elementId), "border": "0", "cellspacing": "6" });
        element$.addClass("Dynamically_Generated_Table");
        return element$;
    }


    this.createDiv$ = function (elementId, headerText)
    {
        var element$ = $("<div>", { "id": "Div" + elementId });
        // replaced with Caption of the Div's table
        //var header$ = $("<h3>");
        //header$.text(headerText);
        //element$.append(header$);
        //element$.append($("<br>"));

        element$.addClass("Dynamically_Generated_Div");
        return element$;
    }


    this.createMandatorySymbol$ = function (elementId, elementText)
    {
        var element$ = $("<span>", { "id": "b_" + elementId });
        element$.html(elementText);
        element$.css("color:red;");
        return element$;
    }


    this.createOptionsForSelectText = function (optionsForSelect)
    {
        var elementText = "";
        var arrayLength = optionsForSelect.length;
        for (var optionIndex = 0; optionIndex < arrayLength; optionIndex++)
        {
            var optionItem = optionsForSelect[optionIndex];
            elementText += "<option value=\"" + optionItem["value"] + "\" >" + optionItem["text"] + "</option>";
        }
        return elementText;
    }

    this.renderItemTr = function (table$, index, Name, Value, Label, Type, ReadOnly, Required, Type, isSelect, Size, Dictionary, LastModified, numberOfColumns, customFieldsPlaceholderManager, labelPosition)
    {
        var isDateType = false;
        if (Type == _CF_DATE_TYPE)
            isDateType = true;

        var label$ = this.createLabel$(Name, Label, Required);
        var elem$;

        if (_p_Custom_Fields_Properties.readOnly)
            ReadOnly = true;

        var elementId = Name;

        if (isSelect)
        {
            if (ReadOnly)
            {
                var Value = CustomFields_convertDictionatyIdToName(Dictionary, Value)
                elem$ = this.createSpan$(Name, Value);
                //elem$.attr('disabled', 'disabled');
            }
            else
            {
                elem$ = this.createSelect$(elementId);
                var optionText = this.createOptionsForSelectText(Dictionary);
                elem$.html(optionText);
                elem$.val(Value);
            }
        }
        else
        {
            if (ReadOnly)
            {
                if (Value == "")
                    Value = _CF_EMPTY_READ_ONLY_STRING;
                elem$ = this.createSpan$(Name, Value);
            }
            else
            {
                if (isDateType)
                {
                    elementId = PREFIX_DATE_TYPE_ELEMENT + Name;
                    Size = 40;
                    elem$ = this.createInputText$(elementId, Value);
                    elem$.attr("size", Size);
                    elem$.attr("maxlength", Size);
                }
                if (Type == _CF_STRING_TYPE)
                {
                    if (Size > 0 && Size < 301)
                    {
                        elem$ = this.createInputText$(elementId, Value);
                        elem$.attr("size", Size);
                        elem$.attr("maxlength", Size);
                    }
                    else
                    {
                        elem$ = this.createTextArea$(elementId, Value);
                        if (Size > 0)
                        {
                            elem$.attr(_CF_ATTR_DATA_MAX_LENGTH, Size);
                            elem$.on("change", CustomFields_checkSize);
                        }
                    }

                } // end (Type == "string")
                else if (CustomFields_isNumericType(Type))
                {
                    Size = 13;
                    if (CustomFields_isMoneyType(Type))
                    {
                        Value = CustomFields_MoneyFormat(Value, 2);
                        Size += 2;
                    }

                    elem$ = this.createInputText$(elementId, Value);
                    elem$.attr("size", Size);
                    elem$.attr("maxlength", Size);
                    elem$.on("change", CustomFields_checkForNumeric);
                }
            }
        }

        elem$.attr(_CF_ATTR_DATA_LABEL, Label);
        elem$.attr(_CF_INITIAL_VALUE_ATTRIBUTE, Value);

        var trIndex = customFieldsPlaceholderManager.getLabelTrIndex(index, labelPosition, numberOfColumns);
        var tdIndex = customFieldsPlaceholderManager.getLabelTdIndex(index, labelPosition, numberOfColumns);
        var tdId = CustomFields_CreatePlaceholderTdId(table$.attr("Id"), trIndex, tdIndex);
        var labelTd$ = $("#" + tdId, table$);
        labelTd$.text("");
        labelTd$.css("font-weight", "bold");
        //labelTd$.text("");
        //labelTd$.text(Label);
        labelTd$.append(label$);
        //labelTd$.html(Label);

        trIndex = customFieldsPlaceholderManager.getValueTrIndex(index, labelPosition, numberOfColumns);
        tdIndex = customFieldsPlaceholderManager.getValueTdIndex(index, labelPosition, numberOfColumns);
        var tdId = CustomFields_CreatePlaceholderTdId(table$.attr("Id"), trIndex, tdIndex);
        var valueTd$ = $("#" + tdId, table$);

        //valueTd$.text(Value);
        valueTd$.text("");
        valueTd$.append(elem$);
        if (LastModified != "")
        {
            var lastModifiedSpan$ = this.createLastModifiedSpan$(elementId, "Last updated by: " + LastModified);
            valueTd$.append($("<br>"));
            valueTd$.append(lastModifiedSpan$);
        }

    }

//    this.renderItemTr2 = function (table$, index, Name, Value, Label, Type, ReadOnly, Required, Type, isSelect, Size, Dictionary, LastModified, numberOfColumns)
//    {
//        var isDateType = false;
//        if (Type == _CF_DATE_TYPE)
//            isDateType = true;

//        var tr1$ = this.createTr$(1000 + index);
//        var tr2$ = this.createTr$(index);

//        var td1$ = this.createTd$(Name + "1", index);
//        tr1$.css("font-weight", "bold");
//        var label$ = this.createLabel$(Name, Label, Required);
//        td1$.append(label$);
//        tr1$.append(td1$);

//        var td2$ = this.createTd$(Name + "2", index);
//        var elem$;

//        if (_p_Custom_Fields_Properties.readOnly)
//            ReadOnly = true;

//        var elementId = Name;

//        if (isSelect)
//        {
//            if (ReadOnly)
//            {
//                var Value = CustomFields_convertDictionatyIdToName(Dictionary, Value)
//                elem$ = this.createSpan$(Name, Value);
//                //elem$.attr('disabled', 'disabled');
//            }
//            else
//            {
//                elem$ = this.createSelect$(elementId);
//                var optionText = this.createOptionsForSelectText(Dictionary);
//                elem$.html(optionText);
//                elem$.val(Value);
//            }
//        }
//        else
//        {
//            if (ReadOnly)
//            {
//                if (Value == "")
//                    Value = _CF_EMPTY_READ_ONLY_STRING;
//                elem$ = this.createSpan$(Name, Value);
//            }
//            else
//            {
//                if (isDateType)
//                {
//                    elementId = PREFIX_DATE_TYPE_ELEMENT + Name;
//                    Size = 40;
//                    elem$ = this.createInputText$(elementId, Value);
//                    elem$.attr("size", Size);
//                    elem$.attr("maxlength", Size);
//                }
//                if (Type == _CF_STRING_TYPE)
//                {
//                    if (Size > 0 && Size < 301)
//                    {
//                        elem$ = this.createInputText$(elementId, Value);
//                        elem$.attr("size", Size);
//                        elem$.attr("maxlength", Size);
//                    }
//                    else
//                    {
//                        elem$ = this.createTextArea$(elementId, Value);
//                        if (Size > 0)
//                        {
//                            elem$.attr(_CF_ATTR_DATA_MAX_LENGTH, Size);
//                            elem$.on("change", CustomFields_checkSize);
//                        }
//                    }

//                } // end (Type == "string")
//                else if (CustomFields_isNumericType(Type))
//                {
//                    Size = 13;
//                    if (CustomFields_isMoneyType(Type))
//                    {
//                        Value = CustomFields_MoneyFormat(Value, 2);
//                        Size += 2;
//                    }

//                    elem$ = this.createInputText$(elementId, Value);
//                    elem$.attr("size", Size);
//                    elem$.attr("maxlength", Size);
//                    elem$.on("change", CustomFields_checkForNumeric);
//                }
//            }
//        }

//        elem$.attr(_CF_ATTR_DATA_LABEL, Label);
//        elem$.attr(_CF_INITIAL_VALUE_ATTRIBUTE, Value);

//        td2$.append(elem$);
//        if (LastModified != "")
//        {
//            var lastModifiedSpan$ = this.createLastModifiedSpan$(elementId, "Last updated by: " + LastModified);
//            td2$.append($("<br>"));
//            td2$.append(lastModifiedSpan$);
//        }
//        tr2$.append(td2$);
//        table$.append(tr1$);
//        table$.append(tr2$);
//    }


    this.createGroupDiv = function (container$, containerId, groupIndex, headerText, fieldData, captionText, numberOfColumns, labelPosition)
    {
        headerText = "";
        var div$ = this.createDiv$(containerId + groupIndex, headerText);
        container$.append(div$);
        var table$ = this.createTable$(containerId);
        if (typeof captionText != "undefined" && captionText != "")
        {
            var captionElement = table$[0].createCaption();
            captionElement.style.textAlign = "left";
            captionElement.style.fontWeight = "bold"
            captionElement.innerHTML = captionText;
            table$.attr("border", "1");
            table$.attr("rules", "none");
        }
    
        var numberOfRecords = fieldData.length;
        var customFieldsPlaceholderManager = new CustomFieldsPlaceholderManager();
        var numberOfTrs = customFieldsPlaceholderManager.calculateNumberOfTrs(numberOfRecords, numberOfColumns, labelPosition);
        var numberOfTds = customFieldsPlaceholderManager.calculateNumberOfTds(numberOfColumns, labelPosition);
        this.createPlaceholderTable(table$, numberOfTrs, numberOfTds);
        div$.append(table$);
        for (var index = 0; index < numberOfRecords; index++)
        {
            var fieldInfo = fieldData[index];
            var Name = fieldInfo.Id;
            var Type = fieldInfo.Type.toLowerCase();
            var Value = fieldInfo.Value;
            var Required = fieldInfo.Required;

            var isSelect = false;
            var ReadOnly = fieldInfo.ReadOnly;
            var Size = fieldInfo.Size;
            var LastModified = "";
            if (typeof fieldInfo.LastModified != "undefined" || typeof fieldInfo.LastModified != null)
                LastModified = fieldInfo.LastModified;

            if (Type == "bool")
            {
                fieldInfo.Dictionary = _g_dictionaryYesNo;
                isSelect = true;
            }
            else if (Type == "bool2")
            {
                fieldInfo.Dictionary = _g_dictionaryYesNoNa;
                isSelect = true;
            }
            else
            {
                var newDictionary = null;
                if (!fieldInfo.ReadOnly)
                    newDictionary = CustomFields_getDictionary(_p_Custom_Fields_Data_Object, fieldInfo.DictionaryName);

                if (newDictionary != null)
                {
                    fieldInfo.Dictionary = newDictionary;
                    isSelect = true;
                }
            }

            var Label = fieldInfo["Label"];     // Name.replace(/_/g, " ");
              this.renderItemTr(table$, index, Name, Value, Label, Type, ReadOnly, Required, Type, isSelect, Size, fieldInfo.Dictionary, LastModified, numberOfColumns, customFieldsPlaceholderManager, labelPosition);
            //this.renderItemTr2(table$, index, Name, Value, Label, Type, ReadOnly, Required, Type, isSelect, Size, fieldInfo.Dictionary, LastModified, numberOfColumns);
        }

        div$.append($("<br>"));
        div$.append($("<br>"));
        return div$;
    }


    this.createPlaceholderTd$ = function (tableId, rowIndex, columnIndex)
    {
        var element$ = $("<td>", { "id": CustomFields_CreatePlaceholderTdId(tableId, rowIndex, columnIndex) });
        element$.addClass("Dynamically_Generated_Td");
        return element$;
    }


    this.createPlaceholderTable = function (table$, numberOfRows, numberOfColumns)
    {
        for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++)
        {
            var tr$ = this.createTr$(rowIndex);

            for (var columnIndex = 0; columnIndex < numberOfColumns; columnIndex++)
            {
                var td$ = this.createPlaceholderTd$(table$.attr("Id"), rowIndex, columnIndex);
                //td$.text("c_" + rowIndex + "-" + columnIndex + "_PlTdId_" + CustomFields_CreatePlaceholderTdId(table$.attr("Id"), rowIndex, columnIndex)); // !!
                td$.text(" ");
                tr$.append(td$);
            }

            table$.append(tr$);
        }
    }
}


// === Placeholder Table
function CustomFieldsPlaceholderManager()
{
    this.calculateNumberOfTrs = function (numberOfRecords, numberOfColumns, labelPosition)
    {
        if (labelPosition == _CF_LABEL_POSITION_ABOVE)
        {
            return (Math.floor(numberOfRecords / numberOfColumns) + (numberOfRecords % numberOfColumns)) * 2;
        }
        else if (labelPosition == _CF_LABEL_POSITION_LEFT)
        {
            return Math.floor(numberOfRecords / numberOfColumns) + Math.floor(numberOfRecords % numberOfColumns);
        }
    }


    this.calculateNumberOfTds = function (numberOfColumns, labelPosition)
    {
        if (labelPosition == _CF_LABEL_POSITION_ABOVE)
        {
            return numberOfColumns;
        }
        else if (labelPosition == _CF_LABEL_POSITION_LEFT)
        {
            return 2 * numberOfColumns;
        }
    }


    this.getValueTdIndex = function (rowIndex, labelPosition, numberOfColumns)
    {
        if (labelPosition == _CF_LABEL_POSITION_ABOVE)
        {
            return this.getLabelTdIndex(rowIndex, labelPosition, numberOfColumns);
        }
        else if (labelPosition == _CF_LABEL_POSITION_LEFT)
        {
            return this.getLabelTdIndex(rowIndex, labelPosition, numberOfColumns) + 1;
        }
    }


    this.getLabelTdIndex = function (rowIndex, labelPosition, numberOfColumns)
    {
        if (labelPosition == _CF_LABEL_POSITION_ABOVE)
        {
            return rowIndex % numberOfColumns;
        }
        else if (labelPosition == _CF_LABEL_POSITION_LEFT)
        {
            return Math.floor(rowIndex % numberOfColumns) * 2;
        }
    }


    this.getLabelTrIndex = function (rowIndex, labelPosition, numberOfColumns)
    {
        if (labelPosition == _CF_LABEL_POSITION_ABOVE)
        {
            return Math.floor(rowIndex / numberOfColumns) * 2;
        }
        else if (labelPosition == _CF_LABEL_POSITION_LEFT)
        {
            return Math.floor(rowIndex / numberOfColumns);
        }

    }

    this.getValueTrIndex = function (rowIndex, labelPosition, numberOfColumns)
    {
        if (labelPosition == _CF_LABEL_POSITION_ABOVE)
        {
            return this.getLabelTrIndex(rowIndex, labelPosition, numberOfColumns) + 1;
        }
        else if (labelPosition == _CF_LABEL_POSITION_LEFT)
        {
            return this.getLabelTrIndex(rowIndex, labelPosition, numberOfColumns);
        }
    }
}
