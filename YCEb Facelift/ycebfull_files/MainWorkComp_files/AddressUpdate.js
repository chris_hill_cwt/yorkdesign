function Address_Update(    addrTitle,
                            addrAddr1, addrAddr1Len, addrAddr1Required,
                            addrAddr2, addrAddr2Len, addrAddr2Required,
                            addrCity, addrCityLen, addrCityRequired,
                            addrStateID, addrState, addrStateName, addrStateRequired,  // StateCode ?
                            addrPostalCode, addrPostalCodeLen, addrPostalCodeRequired,
                            addrCounty,
                            addrCountry, addrCountryName,
                            addrPhone1, addrPhone1Len, 
                            addrPhone1Type, addrPhone1Ext,  // Phone1Type:    //H - home,  // W - work
                            addrPhone2, addrPhone2Len, 
                            addrPhone2Type, addrPhone2Ext, // Phone1Type:    //H - home,  // W - work
                            addrFax,  addrFaxLen,
                            addrEmail, addrEmailLen,
                            addrContact,
                            addrAddrViewLine,
                            addrValidationLevel // None:0,         CountryIsOptional: 1,    Zip : 2,
                        ) {
    //Address_Update
    var addr = new Address(); // create Address and assign values

    if (addrValidationLevel != null) {
        addr.AddrValidationLevel = addrValidationLevel //
    }

    addr.Title = addrTitle;   //"Claimant Address"

    if(addrAddr1 != null){
        addr.Addr1 = addrAddr1.value;                  // txtClmtAddress.value;
        addr.Addr1Len = addrAddr1Len;            //50;
        addr.Addr1Required = addrAddr1Required  //true;
    }
    
    if(addrAddr2 != null){
        addr.Addr2 = addrAddr2.value;    //txtClmtAddress2.value;
        addr.Addr2Len = addrAddr2Len;    //50;
    }
    
    if(addrCity!=null){
        addr.City = addrCity.value; // txtClmtCity.value;
        addr.CityLen = addrCityLen //18;
    }
    
    
    if (addrStateID != null && addrStateID != '0') {
        addr.StateID = addrStateID.value;   //ClmtStateId.value;
    }
    
    if( addrStateName != null) {
        addr.StateName = addrStateName.value;   //ClmtStateId.value;
        addr.State = addrState.value;  //cboClmtState.value;
    }
    
    if(addrPostalCode!= null) {
        addr.PostalCode = addrPostalCode.value;  //txtClmtZip.value;
        addr.PostalCodeRequired = addrPostalCodeRequired;  //txtClmtZip.value;
    }
    // Form does not have County and Country. If it does   //uncomment 3 lines below

    if(addrCounty!=null){
        addr.County = addrCounty.value;   //County.value;
    }
    
    if(addrCountry!= null){
        addr.Country = addrCountry.value;  //ClmtCountry.value;
    }


    if (addrPhone1 != null) {
         addr.Phone1 = addrPhone1.value;
         addr.Phone1Len = addrPhone1Len;
        addr.Phone1Type = addrPhone1Type;  // 'H';   //H - home
        addr.Phone1Ext = addrPhone1Ext;    //Phone1Ext.value;
    }

    if(addrPhone2!= null) {
        addr.Phone2 = addrPhone2.value;  //txtClmtBusPhone.value;
        addr.Phone2Len = addrPhone2Len;
        addr.Phone2Type = addrPhone2Type;   // W - work
        addr.Phone2Ext = addrPhone2Ext;
    }
    
    if(addrFax != null){
        addr.Fax = addrFax.value;   //Fax.value;
        addr.FaxLen = addrFaxLen;
    }

    if (addrEmail != null) {
        addr.Email = addrEmail.value;       // CEMAIL.value;
        addr.EmailLen = addrEmailLen;
    }
    
    if (addrContact!= null){
        addr.Contact = addrContact.value;  //CCONTACT.value;
    }
    
    if (addr.Edit() == false) return;
        
    try{
        addrAddr1.value = addr.Addr1
    }catch(e){}
    
    try{
        addrAddr2.value = addr.Addr2
    }catch(e){}
    
    try{
        addrCity.value = addr.City;
    }catch(e){}
    
    try{
         addrStateID.value = addr.StateID;   //ClmtStateId.value;
    }catch(e){}
    
    try{
        addrStateName.value = addr.StateName;
        addrState.value = addr.State;
    }catch(e){}
    
     try{
        addrPostalCode.value =  addr.PostalCode;
    }catch(e){}
    // Form does not have County and Country. If it does   //uncomment 3 lines below

    if(addrCounty!=null){
         try{
            addrCounty.value = addr.County;
         }catch(e){}
    }
    
    if(addrCountry!= null){
        try{
            addrCountry.value = addr.Country;
        }catch(e){}
    }

    if (addrCountryName != null) {
        try {
            addrCountryName.value = addr.CountryName;
        } catch (e) { }
    }

    if(addrPhone1 != null){
        try{
            addrPhone1.value =  addr.Phone1;
        }catch(e){}
    }

    if(addrPhone2!= null) {
        try{
            addrPhone2.value = addr.Phone2;
        }catch(e){}
    }
    
    if(addrFax != null){
        try{
            addrFax.value = addr.Fax;   //Fax.value;
       }catch(e){}
    }
    
    if(addrEmail != null) {
       try{
             addrEmail.value = addr.Email;       // CEMAIL.value;
       }catch(e){}         
    }
    
    if (addrContact!= null){
       try{
            addrContact.value =   addr.Contact;  //CCONTACT.value;
       }catch(e){}  
    }

    if (addrAddrViewLine != null) {
        try {

            if (typeof addrAddrViewLine.value != 'undefined') {
                addrAddrViewLine.value = (addrAddr1 != null) ? trim(addr.Addr1) : '';
                addrAddrViewLine.value = (addrAddr2 != null) ? addrAddrViewLine.value + ' ' + trim(addr.Addr2) : addrAddrViewLine.value;
            } else {
                if (typeof addrAddrViewLine.innerHTML != 'undefined') {
                    addrAddrViewLine.innerHTML = (addrAddr1 != null) ? trim(addr.Addr1) : '';
                    addrAddrViewLine.innerHTML = (addrAddr2 != null) ? addrAddrViewLine.innerHTML + ' ' + trim(addr.Addr2) : addrAddrViewLine.innerHTML;
                }
            }
       }catch(e){} 
    }
        
    return;
}

function AddressUpdate_claimant(title) {
    // call address info update:		    
    var addrAddr1Required = true;
    var addrAddr2Required = false;
    var addrCityRequired = false;
    var addrStateRequired = false;
    var addrPostalCodeRequired = false;

    Address_Update(  //Populate_claimant.addrCallback,
                    title,
                    frInfo.document.frmClaim.txtClmtAddress, 
                            30, addrAddr1Required,
                    frInfo.document.frmClaim.txtClmtAddress2,  
                            30, addrAddr2Required,
                    frInfo.document.frmClaim.txtClmtCity,  
                            25, addrCityRequired,
                    frInfo.document.frmClaim.ClmtStateId, 
                    frInfo.document.frmClaim.cboClmtState, 
                    frInfo.document.frmClaim.txtClmtState, 
                    addrStateRequired,
                    frInfo.document.frmClaim.txtClmtZip, 
                             10,
                    addrPostalCodeRequired,
                    null,
                    frInfo.document.frmClaim.ClmtCountryId, 
                    frInfo.document.frmClaim.txtCountry,
                    frInfo.document.frmClaim.txtClmtPhone,
                    25,        // PhoneLen             
                    null, null,
                    frInfo.document.frmClaim.txtClmtBusPhone, 
                    25,
                    "W", null,
                    null,             25,  // FaxLen
                    frInfo.document.frmClaim.CEMAIL, 
                        50,
                    frInfo.document.frmClaim.CCONTACT  ,
                    
                    frInfo.document.frmClaim.AddrViewLine,
                    1   //addrValidationLevel
                    );

    
    return;
}


function AddressUpdate_law(title) {
    // call address info update:		    
    var addrAddr1Required = true;
    var addrAddr2Required = false;
    var addrCityRequired = false;
    var addrStateRequired = false;
    var addrPostalCodeRequired = false;

    Address_Update( title,
                    frInfo.document.frmClaim.txtLawAddress, 
                             50, addrAddr1Required,
                    frInfo.document.frmClaim.txtLawAddress2,
                            50, addrAddr2Required,
                    frInfo.document.frmClaim.txtLawCity, 
                            50, addrCityRequired,
                    frInfo.document.frmClaim.LawStateId, 
                    frInfo.document.frmClaim.cboLawState,
                    frInfo.document.frmClaim.txtLawState,
                    addrStateRequired,
                    frInfo.document.frmClaim.txtLawZip, 
                             20,
                    addrPostalCodeRequired,
                    null,
                    frInfo.document.frmClaim.LawCountryId, 
                    frInfo.document.frmClaim.txtCountry, 
                    frInfo.document.frmClaim.txtLawPhone, 25,
                    null, null,
                    null, 25, //frInfo.document.frmClaim.txtLawBusPhone,  
                    null, //  "W",
                    null,
                    frInfo.document.frmClaim.txtLawFax, 25, 
                    frInfo.document.frmClaim.LEMAIL, 
                    50,
                    frInfo.document.frmClaim.LCONTACT ,

                    frInfo.document.getElementById('AddrViewLine'),
                    1   //addrValidationLevel
                    );
    return;
}


function AddressUpdate_adjuster_independent(title) {
    // call address info update:		    
    var addrAddr1Required = true;
    var addrAddr2Required = false;
    var addrCityRequired = false;
    var addrStateRequired = false;
    var addrPostalCodeRequired = false;
    
    Address_Update( title,
                    frInfo.document.frmClaim.txtIAdjAddr1, 
                            50, addrAddr1Required,
                    frInfo.document.frmClaim.txtIAdjAddr2, 
                            50, addrAddr2Required,
                    frInfo.document.frmClaim.txtIAdjCity,
                            50, addrCityRequired,
                            
                    frInfo.document.frmClaim.IAdjStateId,   
                    frInfo.document.frmClaim.cboIAdjState,
                    frInfo.document.frmClaim.txtIAdjState,  
                    
                    addrStateRequired,
                    frInfo.document.frmClaim.txtIAdjZip,    
                             20,
                    addrPostalCodeRequired,
                    null,
                    frInfo.document.frmClaim.IAdjCountryId, 
                    frInfo.document.frmClaim.txtCountry,    
                    frInfo.document.frmClaim.txtIAdjPhone, 25, 
                    null, null,
                    frInfo.document.frmClaim.txtWorkPhone, 25,
                    null, //"W",
                    null,
                    frInfo.document.frmClaim.txtIAdjFax, 25,
                    frInfo.document.frmClaim.txtIAdjEMail,
                    50,
                    null,  // CONTACT
                    
                    frInfo.document.frmClaim.AddrViewLine,
                     1   //addrValidationLevel
                    );
    return;
}


function AddressUpdate_insureds(title) {
    // call address info update:		    
    var addrAddr1Required = true;
    var addrAddr2Required = false;
    var addrCityRequired = false;
    var addrStateRequired = false;
    var addrPostalCodeRequired = false;

    Address_Update( title,
                    frInfo.document.frmClaim.txtInsAddress, 
                            30, addrAddr1Required,
                    frInfo.document.frmClaim.txtInsAddress2,  
                            30, addrAddr2Required,
                    frInfo.document.frmClaim.txtInsCity,  
                            18, addrCityRequired,
                    frInfo.document.frmClaim.IStateId,   // addrStateID, addrState, addrStateName, addrStateRequired,  // StateCode ?
                    frInfo.document.frmClaim.cboInsState, 
                    frInfo.document.frmClaim.txtIState, 
                    addrStateRequired,
                    frInfo.document.frmClaim.txtInsZip, 
                             10,
                    addrPostalCodeRequired,
                    null,
                    frInfo.document.frmClaim.ICountryId, 
                    frInfo.document.frmClaim.txtCountry, 
                    frInfo.document.frmClaim.txtInsPhone, 25,
                    null, null,
                    frInfo.document.frmClaim.txtWorkPhone, 25,
                    "W",
                    null,
                    null, 25,
                    frInfo.document.frmClaim.txtEmail,
                    50, 
                    null, // CONTACT
                    frInfo.document.frmClaim.AddrViewLine,
                     1   //addrValidationLevel
                    );
    return;
}

function AddressUpdate_rental(title) {
    // call address info update:		    
    var addrAddr1Required = true;
    var addrAddr2Required = false;
    var addrCityRequired = false;
    var addrStateRequired = false;
    var addrPostalCodeRequired = false;

    Address_Update(title,
                    frInfo.document.frmClaim.txtRentAddress,  
                            30, addrAddr1Required,
                    frInfo.document.frmClaim.txtRentAddress2,  
                            30, addrAddr2Required,
                    frInfo.document.frmClaim.txtRentCity,  
                            18, addrCityRequired,
                    frInfo.document.frmClaim.RentStateId,  //  addrStateID, addrState, addrStateName, addrStateRequired,  // StateCode ?
                    frInfo.document.frmClaim.cboRentState,
                    frInfo.document.frmClaim.txtRentState, 
                    addrStateRequired,
                    frInfo.document.frmClaim.txtRentZip, 
                             10,
                    addrPostalCodeRequired,
                    null,
                    frInfo.document.frmClaim.RentCountryId,
                    frInfo.document.frmClaim.cboRentCountry, 
                    frInfo.document.frmClaim.txtRentPhone, 25,
                    null, null,
                    null , 25,
                    null, //"W",
                    null,
                    null, 25,  //FaxLen
                    frInfo.document.frmClaim.txtEmail,
                    50,
                    null,  // CONTACT
                    frInfo.document.frmClaim.AddrViewLine,
                     1   //addrValidationLevel
                    );
    return;
}


function AddressUpdate_Rec_ResponsibleParty(title) {
    // call address info update:		    
    var addrAddr1Required = true;
    var addrAddr2Required = false;
    var addrCityRequired = false;
    var addrStateRequired = false;
    var addrPostalCodeRequired = false;

    Address_Update(title,
                    frInfo.document.frmClaim.txtSAddress1,
                            50, addrAddr1Required,
                    frInfo.document.frmClaim.txtSAddress2,
                            50, addrAddr2Required,
                    frInfo.document.frmClaim.txtSCity,
                            50, addrCityRequired,
                    frInfo.document.frmClaim.SStateId,  //  addrStateID, addrState, addrStateName, addrStateRequired,  // StateCode ?
                    frInfo.document.frmClaim.cboSState,
                    frInfo.document.frmClaim.SStateDesc,
                    addrStateRequired,
                    frInfo.document.frmClaim.txtSZip,
                             20,
                    addrPostalCodeRequired,
                    null,
                    frInfo.document.frmClaim.SCountryId,
                    frInfo.document.frmClaim.SCountry,
                    frInfo.document.frmClaim.txtSPhone, 25,
                    null, null,
                    null, 25,
                    null, //"W",
                    null,
                    frInfo.document.frmClaim.txtSFax, 25,  //FaxLen
                    frInfo.document.frmClaim.txtSEmail,
                    50,
                    null,  // CONTACT
                    frInfo.document.getElementById('SAddrViewLine'),
                    1   //addrValidationLevel
                    );
    return;
}




function AddressUpdate_Rec_AdverseCarrier(title) {
    // call address info update:		    
    var addrAddr1Required = true;
    var addrAddr2Required = false;
    var addrCityRequired = false;
    var addrStateRequired = false;
    var addrPostalCodeRequired = false;

    Address_Update(title,
                    frInfo.document.frmClaim.txtAAddress1,
                            50, addrAddr1Required,
                    frInfo.document.frmClaim.txtAAddress2,
                            50, addrAddr2Required,
                    frInfo.document.frmClaim.txtACity,
                            50, addrCityRequired,
                    frInfo.document.frmClaim.ACStateID,  // var ACStateID = 0; var ACCountryId = ''; var ACCountry = ''; var ACStateDesc = '';
                    frInfo.document.frmClaim.cboAState,
                    frInfo.document.frmClaim.ACStateDesc,
                    addrStateRequired,
                    frInfo.document.frmClaim.txtAZip,
                             20,
                    addrPostalCodeRequired,
                    null,
                    frInfo.document.frmClaim.ACCountryId,  
                    frInfo.document.frmClaim.ACCountry,
                    frInfo.document.frmClaim.txtAPhone, 25,
                    null, null,
                    null, 25,
                    null, //"W",
                    null,
                    frInfo.document.frmClaim.txtAFax, 25,  //FaxLen
                    frInfo.document.frmClaim.txtAEmail,
                    50,
                    null,  // CONTACT
                    frInfo.document.getElementById('AddrViewLine') ,
                    1   //addrValidationLevel
                    );
    return;
}


function AddressUpdate_producer(title) {
    // call address info update:		    
    var addrAddr1Required = false;
    var addrAddr2Required = false;
    var addrCityRequired = false;
    var addrStateRequired = false;
    var addrPostalCodeRequired = false;

    Address_Update(title,
                    frInfo.document.frmClaim.txtPrAddr1,
                            50, addrAddr1Required,
                    frInfo.document.frmClaim.txtPrAddr2,
                            50, addrAddr2Required,
                    frInfo.document.frmClaim.txtPrCity,
                            50, addrCityRequired,
                    frInfo.document.frmClaim.PrstateId, 
                    frInfo.document.frmClaim.cboPrState,
                    frInfo.document.frmClaim.PrstateDesc,
                    addrStateRequired,
                    frInfo.document.frmClaim.txtPrZip,
                             20,
                    addrPostalCodeRequired,
                    null,
                    frInfo.document.frmClaim.PrCountryId,
                    frInfo.document.frmClaim.txtCountry,
                    frInfo.document.frmClaim.txtPrPhone, 25,
                    null, null,
                    null, 25,
                    null, //"W",
                    null,
                    frInfo.document.frmClaim.txtPrFax, 25,  //FaxLen
                    frInfo.document.frmClaim.txtAEmail,
                    50,
                    null,  // CONTACT
                    frInfo.document.frmClaim.AddrViewLine,
                     1   //addrValidationLevel
                    );
    return;
}


function AddressUpdate_loss(title) {
    // call address info update:		    
    var addrAddr1Required = false;
    var addrAddr2Required = false;
    var addrCityRequired = false;
    var addrStateRequired = false;
    var addrPostalCodeRequired = false;

    Address_Update(  //Populate_claimant.addrCallback,
                    title,
                    frInfo.document.frmClaim.txtLossAddr1,
                            50, addrAddr1Required,
                    frInfo.document.frmClaim.txtLossAddr2,
                            50, addrAddr2Required,
                    frInfo.document.frmClaim.txtLossCity,
                            25, addrCityRequired,
                    frInfo.document.frmClaim.txtLossStateId,
                    frInfo.document.frmClaim.txtLossState,
                    frInfo.document.frmClaim.txtLossStateDesc,
                    addrStateRequired,
                    frInfo.document.frmClaim.txtLossZip,
                             10,
                    addrPostalCodeRequired,
                    null,
                    frInfo.document.frmClaim.txtLossCountryId,
                    frInfo.document.frmClaim.txtLossCountry,
                    null,
                    null,        // PhoneLen             
                    null, null,
                    null,
                    null,
                    null, null,
                    null, null,  // FaxLen
                    null,
                    null,
                    null,

                    frInfo.document.frmClaim.AddrViewLine,
                    1   //addrValidationLevel
                    );

    return;
}
 