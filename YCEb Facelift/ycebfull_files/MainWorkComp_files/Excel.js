			
			/*
				tblHeader - if null then header row it taken from row #1 tblData
			*/
function ExportToExcel(tblData,
                        tblHeader, /* null  is same table */
                        HeaderText, SubTitle,
                        rowSelectionFunction  /* null is none */
                        , listOfColumns    /* comma separated , zero base list of columns##*/
                        ) {
					 var a //As Excel.Application
					 var b //As Excel.Workbook
					 var s //As Excel.Worksheet
					 var i , j
					 var nStartingRow=3;
					 var cell, txt, pos;
					 var filterDefined = false, listOfColumnsDefined = false;
					 filterDefined = (rowSelectionFunction != null)
					 listOfColumnsDefined = (listOfColumns != null) // comma separated
					 if (listOfColumnsDefined) listOfColumns = "," + listOfColumns + ","
				
					 window.status="Exporting data. Please wait........."
					 try {
							 a = new ActiveXObject("Excel.Application")
					 }
					 catch (e) {
						alert(  "Cannot start MS Excel!\n" 
							+"Excel is not installed on your PC or "    
							+"browser security disables the run of ActiveX components. \n" 
							+"Make sure that browser's  Internet Options-->Security-->Custom level-->Run ActiveX  control set to Enable");
						window.status="";
						return false;
					 }
					
					 a.Workbooks.Add();
					 a.Workbooks.Item(1).Activate();
					 s = a.Workbooks.Item(1).Worksheets.Item(1);
					 s.Activate();
					 
					
					//make header
					var tblHdr=  tblHeader==null ? tblData : tblHeader;
					var startRow= tblHeader==null ? 1 : 0;
					var nCols;
					
					if  (tblData.rows.length >0 ) {
						nCols = tblData.rows(0).cells.length;
					 }
					 else {
						nCols = (tblHeader==null) ? 0: tblHeader.rows(0).cells.length;
					 }
                   
             var k=1;
             for (j = 0; j < nCols; j++) {
                 if (!listOfColumnsDefined || listOfColumns.indexOf("," + j + ",") > -1) {
                     if (tblHdr.cells(j).clientWidth > 0) {
                         s.columns(k).ColumnWidth = tblHdr.cells(j).clientWidth / 6;
                         s.columns(k).font.size = 8;
                         cell = s.cells(nStartingRow + 1, k);
                         cell.font.bold = true;
                         cell.font.color = 100;
                         cell.HorizontalAlignment = -4108;

                         s.cells(nStartingRow + 1, k) = tblHdr.rows(0).cells(j).innerText;
                         k++;
                     }
                 }
			    }
                    
            var str, row, rowNuber;
            nStartingRow += 2;
            rowNuber = nStartingRow ;

			for (i = startRow; i < tblData.rows.length; i++) {
                window.status = "Exporting data. Row:" + i;
                if (!filterDefined || rowSelectionFunction(i)) {
                        row = tblData.rows(i);
						k=1;
						for (j = 0; j < nCols; j++) {
						    if (!listOfColumnsDefined || listOfColumns.indexOf("," + j + ",") > -1) {
						            if (tblHdr.cells(j).clientWidth > 0) {
						                str = row.cells(j).innerText;
						                if (str.length > 1 && str.substr(0, 1) == '0' && str.indexOf('.') == -1 && str.indexOf('/') == -1) {
						   						 str = '="' + str + '"';
						                }
						                s.cells(rowNuber, k) = str;
						                k++;
						            }
						    }// list

						} // next j
						rowNuber++;
                } //expr
			}  // for..i

					// report header
			s.cells(1,2).font.bold=false;
			s.cells(1,2).font.size=14;
			s.cells(1,2)= HeaderText;

			s.cells(2,1).font.bold=false;
			s.cells(2,1).font.size=9;
			s.cells(2,1)= SubTitle;
					
			s.cells(1,8).font.bold=false;
			s.cells(1,8).font.size=9;
			s.cells(1,8)= Date().toString();
					
			a.windows(1).Caption=HeaderText;
			
			a.visible=true;
			window.status="";
		} //	end function	
		

