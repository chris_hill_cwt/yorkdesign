

String.prototype.trim = function() {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}


var mm_errMsg;
var _LastVisitUrl = new Array();
setErrorHandler();
window.focus();

function mmBrowserVersion(compatibleVerion) {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    var wind = ua.indexOf()
    var ver = 0; //If not IE , return 0

    if (ua.indexOf("Trident/7.0") > 0 && ua.indexOf("rv:11.0") > 0 && ua.indexOf("Mozilla/5.0") >= 0) {
        ver = 11;
    }
    if (msie > 0) {  // If IE, return version number
        var ver = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
        if (ver == 7) {
            // compat mode
            if (compatibleVerion == false) {
                if (ua.indexOf('Trident/4.0') > 0) return 8;
                if (ua.indexOf('Trident/5.0') > 0) return 9;
                if (ua.indexOf('Trident/6.0') > 0) return 10;
                if (ua.indexOf('Trident/7.0') > 0) return 11;
            }
        }

    }

    return ver;

}

function setErrorHandler() {
    var errorHanlderDisabled = false;
    try {
        errorHanlderDisabled = top.mmErrorHanlderDisabled;
    }
    catch (ex) {
    }
      
    if (errorHanlderDisabled) return;

    window.onerror = function(msg, url, linenumber) {
        mm_errMsg = 'ClaimsConnect has detected JavaScript error:\n '
            + '\nPage: ' + url + '\nLine:  ' + linenumber + '\nError:' + msg;

        if (mmIsProdEnvironment(url)) {
            _mmErrLog(1);
        }

        else {
            if (errorHanlderDisabled) return true;
            var html = "<html><header><title>Error Report</title></header><body>"
            + mm_errMsg.replace(/\n/g, '<br>')
            + "<BR><BR><CENTER><button onclick='opener._mmErrLog(0);window.close()'>Close</button>"
             + "&nbsp;&nbsp;<button onclick='opener._mmErrLog(1);window.close()'>Log Error</button>"
             + "&nbsp;&nbsp;<button onclick='opener._mmErrLog(2);window.close()'>Disable this popup</button>"
             + "</body></html>";

            var wnd = window.open("", "mmErr", "height=350,width=500, left=300, top=200, status=0");
            wnd.document.write(html);
            wnd.document.close();
            wnd.focus();

        }
        return true;
    }

}


function _mmErrLog(mode) {
    if (mode == 0) {
       
    } 
    else if (mode == 1) {
    mmWriteToLog("E", "Error in JavaScript", mm_errMsg);
        
    }

    else if (mode == 2) {
        top.mmErrorHanlderDisabled = true;
        window.onerror = null;
    }
 

    return;
}


function mmIsProdEnvironment(url) {
    url = url.toLowerCase();
    
    return url.indexOf("//yorkint") > -1 || url.indexOf("//yorkwebbeta") > -1 || url.indexOf(".com") > -1;
}




var oMM=null;
var _runInPopup=false;
try {
	oMM=top.mm;
}
    catch (e) {}
 
if (!oMM)  {
    if (opener) {
        try {
            oMM = opener.top.mm;
            _runInPopup = true;
        } catch (ex) {
        }

	}
}


//======== Application  Menu==============
function mmAddAppMenu(sTitle) {
	parent.addMenuIToolBarItem(sTitle)
}

function mmAddAppMenuItem(sTitle,sURL) {
	with  (parent) {
		addMenuItem(aMenuToolBar.length, sTitle , sURL )
	}
}

function mmAddAppShortcut(sTitle, sURL) {
  parent.addMenuIToolBarItem("!" + sTitle , sURL)
}  

function mmShowAppMenu() {
	
	try {		
		writeAppMenus();
		createMenu(parent.document.all.mmMenuBar);
  } catch(e) {}
}


function mmClearAppMenu() {
  try {	
  with  (parent) {
	aMenuToolBar.length=nPortalMenuLength;
	aMenus.length=nPortalMenuLength+1;
 }
 } catch(e) {}
}

function _mmClearAppMenu() {       // to call from portal itself
  if (_runInPopup) return;
  with(top) {
    if  (aMenuToolBar.length==nPortalMenuLength) return;
			aMenuToolBar.length=nPortalMenuLength;
			aMenus.length=nPortalMenuLength+1;
			createMenu(document.all.mmMenuBar); 
   } 
}


function mmSetNotifyHandler(funcPtr) {
	try {
		 oMM.contentChanged=funcPtr;
	 }
	 catch (e) {
		//	document.close();
	 }
}


function mmSetAppTitle(sAppTitle) {
	if (!oMM) return;
	
	if  (_runInPopup) {
		document.title=sAppTitle;
		return;
	}
	oMM.appTitle=sAppTitle;
	var oTitle;
	try {
	    if (arguments.length>1) oTitle=document.all.mmAppTitle;
	    else oTitle =	top.document.all.mmAppTitle;
	    oTitle.innerHTML=sAppTitle;
	 }
    catch (e) {}
	}


function mmGetTarget() {
	return oMM.target;
}

function  __mmMakeQueryString4App(url) { 
    var qryStr="";
    if (!oMM) return;
    if (mmGetClaimNo().length > 0) {
        if (url.toLowerCase().indexOf("&claim_no=") == -1) { //if query string has it then use it
            qryStr = "claim_no=" + escape(oMM.claimNo);
        }
    }   
    
    if  ( mmGetClientNo().length>0) {
		if (qryStr.length>0) qryStr +="&"
		qryStr +="client_no=" +escape(oMM.clientNo) 	
    }
    
     if  (mmGetAdjusterNo.length>0) {
		if (qryStr.length>0) qryStr +="&"
		qryStr +="adjuster_no=" +escape(oMM.adjusterNo) 	
    }
        return qryStr;
    
}

function mmRerunCurrentApp(hWnd) {
    if (top.mmCurrentApp_sURL) {
        mmRunApp(top.mmCurrentApp_sURL, hWnd, false);  //,  window.self, mmCurrentApp_hWndTarget
    }
}




function _mmAddToHistory(sURL,  AppTitle) {

    if (sURL.length == 0 || (!AppTitle)) return;
    
     for (var i=0; i<_LastVisitUrl.length; i++) {
		if (_LastVisitUrl[i][0]==sURL) {
			_LastVisitUrl.splice(i,1);
		}
	}
	_LastVisitUrl.splice(0,0, new Array(sURL,  AppTitle))
	_LastVisitUrl[0]=new Array(sURL,  AppTitle);
	 
	
    try {
		_mmPopulateHistory(_LastVisitUrl);
    }
    catch(e){};
}


var mmPopupWnd=null;
function mmSetFocus2Popup() {
    try { mmPopupWnd.focus(); }
    catch (e) { window.setTimeout("mmSetFocus2Popup()", 300); }
}


function mmRunApp(sURL, hWndTarget, bSetAppDefaults, AppTitle) {
   if  (sURL=="") return;
   
	var bRunOnTop=false;
	var bAddParams = true;
	var bFullWindow = false;
	var bNewWindow = false;
		
	if (sURL.indexOf("+") == 0 ) {
		sURL=sURL.substr(1)
	}
	else {	
		_mmAddToHistory(sURL, AppTitle);
	}

  if (sURL.indexOf("!!!") == 0) {
      sURL = sURL.substr(3)
     bNewWindow=  bRunOnTop = true;
   }
	if (sURL.indexOf("!!") == 0 ) {
		sURL=sURL.substr(2)
		bAddParams=false;
		bFullWindow=true;
	}
	if (sURL.indexOf("!") == 0 ) {
		sURL=sURL.substr(1)
		bRunOnTop=true;
	}
	
	
	if (sURL.indexOf("-") == 0 ) {
		sURL=sURL.substr(1)
		bAddParams=false;
	}

	bSetAppDefaults =(arguments.length>2) ?  bSetAppDefaults : true
	hWndTarget=(arguments.length>1) ?  hWndTarget : null
	if (bSetAppDefaults)   _mmSetAppDefaults();      // this is default
	//if (AppTitle) 		mmSetAppTitle(AppTitle);  // set title (called from menu)
    top.mmCurrentApp_sURL=sURL;
    var sChar= (sURL.indexOf("?") < 0)  ? "?" : "&";
    
    if (sURL.substr(0,1) == "/" && (sURL.toUpperCase().indexOf("/PORTAL") !=0) ) {    // URLs starting with '/' are considered as located at root of site
      sURL=oMM.rootURL+sURL.substr(1);
    }
    
    var sURLAndParams;
    if (bAddParams) sURLAndParams=sURL+sChar+__mmMakeQueryString4App(sURL);
    else sURLAndParams=sURL;
     
	if ( sURL.indexOf("EmptyPage.asp" == -1)    )   mmWriteToLog("R","RunApp",    sURLAndParams);

	// showdocument.asp  - do not show in app area - obstruct menu
    if (bRunOnTop == false && bFullWindow == false && sURL.toLowerCase().indexOf("showdocument.asp") > -1) {
        mmPopupWnd = window.open(sURLAndParams, "doc", 
        "Width=1000,height=" + (screen.availHeight - 100) + ",left=20,top=20,status=no,toolbar=0,scrollbars=1,resizable=1")
        window.setTimeout("mmSetFocus2Popup()", 300);
        return;
    }
    
    if (bFullWindow) {
	window.open(sURLAndParams, "wnd1",  "status=1,menubar=1,location=1,resizable,scrollbars=1;", false, "" )	
	return;
    }
if (bRunOnTop) {
        
	    mmOpenWindowInCenter(sURLAndParams, (bNewWindow? "_blank": "wnd1"), 900, 800, "alwaysRaised,dependent=yes,resizable,scrollbars", false, AppTitle? AppTitle : "Claims Connect" )
	    return;
    } 
    if (hWndTarget) 	{
		hWndTarget.document.location=sURLAndParams;
	}
	else {
	    try {
	        top.window.frames[mmGetTarget()].location = sURLAndParams;
	    }
	    catch (e) { }
		
	}
		
}



function mmCloseApp() {
	mmRunApp("/startUp/EmptyPage.asp",  (window.top==window.self)  ? null : window.self )
}


function  mmSetHelp(sURL) {
   	oMM.helpURL=sURL;
}



 //--------- adjuster  FUNCTIONS -------------------

function mmSetAdjusterNo(sAdjusterNo, sAdjusterName) {
	sAdjusterNo=sAdjusterNo.trim(); 
	if (oMM.adjusterNo !=sAdjusterNo) {
		oMM.adjusterNo=sAdjusterNo;
		oMM.adjusterName=sAdjusterName;
		parent.document.all.mmAdjusterNo.innerHTML="<u>Adjuster:" + sAdjusterNo+" " +sAdjusterName+ "</U>";
		parent.document.all.mmAdjuster.style.display="inline";
		oMM.contentChanged("ADJUSTER",sAdjusterNo);
	 }	
}
function mmGetAdjusterNo() {
	return	oMM.adjusterNo;
}


function mmShowAdjusterInfo(sAdjusterNo) {
	mmRunApp("/startup/search/searchAdjuster/AdjusterInfo.asp")
}

//--------- client  FUNCTIONS -------------------
function mmSetClientNo(sClientNo,sClientName) {
	sClientNo=sClientNo.trim();              //replace(/\ /g ,"");   // takes all spaces out
	if (oMM.clientNo !=sClientNo) {
		oMM.clientNo=sClientNo;
		oMM.clientName  = sClientName
		parent.document.all.mmClientNo.innerHTML="<u>Client:" + sClientNo+ " " + sClientName + "</U>";
		parent.document.all.mmClient.style.display="inline";
		oMM.contentChanged("CLIENT",sClientNo);
	 }	
}

function mmGetClientNo() {
	return	oMM.clientNo;
}

function mmShowClientInfo(bClaimMode) {
  if (bClaimMode)
	mmRunApp("!/startup/search/searchClient/ClientInfo.asp?byClaim=Y"  )
  else
   mmRunApp("/startup/search/searchClient/ClientInfo.asp"  )
   
}


// - - - - CLAIM list  functions ----
function mmClearClaimList() {
	 top.document.all.mmClaimNo.options.length=1;
	 top.document.all.mmClaimNo.options[0]=new Option("","",false,false);
	 
}

function mmShowClaimList() {
	with(top.document.all.mmClaimNo) {
			if (selectedIndex== -1) return;
			mmSetClaimNo(options[selectedIndex].value,options[selectedIndex].text, true);
			
	 }
}

function mmAddToClaimList(sClaimNo, sClaimName, takeFromTop, makeItCurrent) {
	var fromTop = (arguments.length<3)  ?  false:takeFromTop;
	var start = fromTop ? 1 : 0;
	var selected = (arguments.length < 4) ? false : makeItCurrent;

	 sClaimNo = sClaimNo.trim();
	 with (top.document.all.mmClaimNo) {
	     var newItem = (sClaimNo + ": " + sClaimName.trim()).replace(/\n\n/g, '\n').replace(/\n/g, '\r\n');
	     var newOption = fixWorkQueueItem(new Option(newItem, sClaimNo, false, false), i);
         for (var i = start; i < options.length; i++) {
	        if (options[i].value == sClaimNo) {
	            if (options[i].text == newOption.text) {
	                if (selected) {
	                    options[i].selected = true;
	                    onchange();
	                }
		            oMM.workQueueModified = true;// it was in Q already. True is to make it current
	                return;
		          } 
	        }
	    }
	    options[i] = newOption;
	    if (selected) {
	        options[i].selected = true;
	        onchange();
	    }
	    window.status = "Items in work queue: " + (options.length - 1);
	    oMM.workQueueModified = true;
	}
	
}

function mmEditClaimList() {   // clicked DEL
var KEY_DELETE=46
		with(parent.document.all.mmClaimNo) {
		    if (event.keyCode == KEY_DELETE && selectedIndex >= 0) {
		        var index = selectedIndex;
		        if (confirm("Do you want to remove " + options[selectedIndex].text + " from the queue?")) {
		            options[selectedIndex] = null;
		            selectedIndex = index;
		            oMM.workQueueModified = true;
		            oMM.contentChanged("CLAIM", options[selectedIndex].value);
		            mmShowClaimList();
		            
		        }
		}
	}
}		
// - - - - CLAIM list related functions END----

function mmClaimQueueModified() {
    return oMM.workQueueModified;
}



function fixWorkQueueItem(item, index) {
    item.title = item.text.replace(/\r\n/g, '\n').replace(/\n\n/g, '\n'); // for tooltip. issue is the \n in option present as \r\n
    item.text = item.text.replace(/\r\n/g, '\n').replace(/\n/g, '  ');
    return item;
}

function mmSetClaimNo(sClaimNo, sClaimName, bFromList, bShowClaimInfo) {
    var sName = (arguments.length<2)  ?  sClaimNo :  sClaimNo+ ": " + sClaimName;
	sClaimNo=sClaimNo.trim();   
    if (sClaimNo != "" ) { //! when queue set
		oMM.claimNo=sClaimNo;
		oMM.claimName = sClaimName;
		if (arguments.length < 3)
		    top.document.all.mmClaimNo.options[0] = fixWorkQueueItem(new Option(sName, sClaimNo, true, true), 0);
			try {
				oMM.contentChanged("CLAIM",sClaimNo);
				
			} catch (e) { //freed script  fix
				oMM.contentChanged=top._mmChagedContent;
				oMM.contentChanged("CLAIM", sClaimNo);
			
			}

   }
	 top.document.all.mmClaim.style.display="inline";
	 top.document.all.mmClaimIcons.style.display = top.document.all.mmClaimNo.options[top.document.all.mmClaimNo.selectedIndex].value == "" ? "none": "";
	 top.document.all.mmClaim.blur();
}


function mmGetClaimNo() {
    if (oMM.claimNo.length > 0) {
        return oMM.claimNo;
    }
    else {
        //fix for direct link
        var ddlClaimsQueue = top.document.all.mmClaimNo;
        var claimNo = "";
        if (ddlClaimsQueue.options.length > 0) {
            claimNo = ddlClaimsQueue.options[0].value;
            oMM.claimNo = claimNo;
            return oMM.claimNo;
        }
        else return "";
    }
}

function mmShowClaimInfo(sURL) {

	if (mmGetClaimNo()=="") return;
	if (sURL) 
		mmRunApp(sURL)
	else 
		mmRunApp("/startup/search/searchClaim/ClaimInfo.asp"  )
	window.setTimeout( mmShowClaimInfo_2,1000)
	
}

function mmShowClaimSummary(claimNo) {
    var url = "/portal/Startup/Search/SearchClaim/ClaimInfo.asp?mode=fullview&claim_no=" + claimNo
    var width = 700;
    var height = screen.availHeight * 0.9
    mmOpenWindowInCenter(url, "", 700, 900, "alwaysRaised,dependent=yes,resizable,scrollbars", false, "")
}

function mmShowClaimDocuments(claimNo, viewOnly) {
    var url = "/portal/AttachedDocuments/ClaimDocuments.asp?claim_no=" + claimNo
    if (viewOnly) url += "&view=viewonly";
    var width = 700;
    var height = screen.availHeight * 0.9
    mmOpenWindowInCenter(url, "", 900, 700, "alwaysRaised,dependent=yes,resizable,scrollbars", false, "")
    }


function mmShowClaimInfo_2() {
	mmShowHotList('CLAIM',mmGetClaimNo(),"Claim related Records")              // show related items
}

function mmShowDocument(DocID) {
	var params;
	var claimNo;
	var arr=DocID.split(" ");
	
	if (arr.length==1) {
		params = "ID=" +DocID;
	}
	else {
		params = "ID="+arr[0]+"&Claim_no="+arr[1];
	}
	mmRunApp("/AttachedDocuments/ShowDocument.asp?" +params);
}

// ------------- working list related functions --------------------
function mmAdd2HotList(sCategory, sDefaultCommentary) {
var sValue="";
var sCommentary;
switch (sCategory) {
case "CLAIM":
	sValue=mmGetClaimNo();	
	sCommentary= sDefaultCommentary
	break ;
case "CLIENT":
	sValue=mmGetClientNo();	
	sCommentary= sDefaultCommentary
	break ;
case "ADJUSTER":
	sValue=mmGetAdjusterNo();	
	sCommentary= sDefaultCommentary
	break;	
case "URL":
	sValue= document.location.toString(); 
	sCommentary= sDefaultCommentary
	break ;
default:
	alert("Category: " + sCategory + " is not supported by HotList");
	return;
	break;	
}	
if (sValue.length==0) {
	alert("Please select a " + sCategory.toLowerCase() + " first!"); 
	return;
}
var arrArgs=new Array(sCategory, sValue, sCommentary)
var nRez =window.showModalDialog("../StartUp/HotList/AddToHotList.Asp?Category=" + sCategory + "&value=" + sValue,
							arrArgs,
							"DialogWidth:900px;DialogHeight:500px;help:no;status:no");
	if (nRez =="OK" && wndHotList !=null ) {
		wndHotList.close() // just to refresh;
		
	}
}


//---------------------------

var wndHotList=null;
function _mmRefreshHotList() {
	if (wndHotList ==null)  return;
	wndHotList.reloadHotList();
	
}

function mmShowHotList(sCategory,sValue,sTitle) {
// sCategory - optional ,  sValue - optional, sTitle- optional
	
   var sOptions="";
   var Url = oMM.hotListUrl;  
   var winOptions="top=" + (screen.availHeight-500)/2 
				+ "px, left=" + (screen.availWidth-900)/2 
				+"px, Width=900px,Height=500px;help=no;status=no,toolbar=no,resizable=1"                                                          
   
    if (arguments.length>1) 	{  
		//  related records
		sOptions="?CATEGORY="+sCategory+ "&VALUE="+sValue  +"&MainTitle="+sTitle + "&MODE=NO_EDIT"
		
		//window.showModalDialog(Url+sOptions, self," DialogWidth:900px;DialogHeight:500px;help:no;status:no");

		window.open(Url+sOptions, "wndHotList2",winOptions);
		return;
    }

   if (wndHotList == null  || wndHotList.closed  )       { //    Full Hot List
		wndHotList=window.open(Url+sOptions,"wndHotList",winOptions);
    }	
    mmSetWndFocus(wndHotList, "Hot List");
	
}

function mmSetWndFocus(hwnd,  sWndTitle) {
	try	{									
   		hwnd.focus();
 	 }
   	catch (e) {
   		alert("Could not open " +  sWndTitle + " window!\n\nYou may have active popup blocker");
   		
   	}	
}

function mmHotListClose() {
	wndHotList=null;
}

//-------------------
function mmShowHotListNotify(ansver) {             // called from HotList.Asp
    
   var sOptions="";
   	if (ansver.length>0 ) {
	    var sCategory= ansver[0];
	    var sValue=ansver[1];
	    var sTitle ="" + ansver[2];
		var bRunApp=(ansver.length>3) ? ansver[3] : false;
		var UrlToRun = "" + (ansver.length > 4 ? ansver[4] : "");

//		alert("Debbugger : ansver = 0 :" + ansver[0] + " 1:" + ansver[1] + " 2:" + ansver[2] + " 3:" + ansver[3]);	
	   	switch (sCategory) {
			case "CLAIM":
			    if (bRunApp) {
				       
						 mmAddToClaimList(sValue, sTitle);
						 mmShowClaimList();
						 
						 if (UrlToRun.length == 0) {
						     mmSetClaimNo(sValue, sTitle);
						 }
						 else {
						     mmSetClaimNo(sValue, sTitle);
						     mmRunApp(UrlToRun);
						 }

			}  
				
				else  {
				    // top.spanAddTopToQueue.style.visibility="visible";
				     mmAddToClaimList(sValue, sTitle);
					 mmSetClaimNo(sValue, sTitle);
				  
				}
				break;
				
			case "CLIENT":
				 mmSetClientNo(sValue, sTitle);
				break;	
			case "ADJUSTER":
				mmSetAdjusterNo(sValue, sTitle);
				break;	
			case "URL":
				window.top.frames[mmGetTarget()].location = sValue;
				break;
			case "DOCUMENT":
				mmShowDocument(sValue);
				break;
			default:
				alert("Category '" + sCategory + "' is not supported by HotList");
				return;
				
		}	
	}
}

//=============EMAIL======================
var EMailWnd;
var EMailHTMLToSend;
var EMailFilePath;

// sFilePath [ optional] format: "fullpath1;fileName1;....fullpathN;fileNameN"
//sFileTitle - optional 
// if sHTMLToSend is not null or empty then attaches as file


    

function mmSendEMail(sTo, sSubject, sHTMLToSend, sBodyText,sFilePath,sFileTitle, additionToBody, relatedTo, relationType) {
   
	sTo= (arguments.length>0) ?  sTo : "";
	sSubject= (arguments.length>1) ?  sSubject  : "";
	sBodyText = (arguments.length>3) ? sBodyText : "";      // body of the message 
	sFilePath =(arguments.length>4) ? sFilePath : "";        // file path on server to send
	sFileTitle = (arguments.length > 5 && sFileTitle != null) ? sFileTitle : sFilePath;     // file name to dispaly
	
	EMailHTMLToSend =  (arguments.length>2) ?  sHTMLToSend : null;
	 EMailFilePath= (arguments.length>4) ? sFilePath : null;
	 
	 
	var theType = (arguments.length > 8) ? relationType : "claim_no";
	var clmNoPar="";
	if (arguments.length >7) {
	    clmNoPar="&" + theType + "=" + relatedTo
	}
	
	var url = "/portal/StartUp/EMail/EMail.Asp?TO=" + sTo
				+ "&Subject=" + escape(sSubject)
				+ (sFilePath ? "&File=" + escape(sFilePath) : "")
				+ (sFilePath ? "&FileTitle=" + escape(sFileTitle) : "")
				+ "&Body=" + escape(sBodyText)
                + clmNoPar;
	if (arguments.length > 6 && clmNoPar.length==0) {
	       // quite submission
	        var wndParams="alwaysRaised=1,dependent=yes,help=no,status=no,resizable=yes,scrollbars,menubar=no";
	        top.frmHidden.value1.value = additionToBody;
	        top.frmHidden.action = url;
	        top.frmHidden.target = "MailWnd";
	        EMailWnd = mmOpenWindowInCenter("", "MailWnd",  650,450, wndParams,null,"E-mail");
			top.frmHidden.method="POST";
			top.frmHidden.submit();
	 }
	 else {
		       EMailWnd = mmOpenWindowInCenter(url, "MailWnd", 650, 450, wndParams,null,"E-mail"); 
	 }
}
 
 function mmSendEMail_notify(sMsgType, sContext) {
	if (sMsgType =="LOADED") {
		if ( EMailHTMLToSend != null || EMailFilePath != null)  {
			EMailWnd.setHTMLToSend(EMailHTMLToSend,EMailFilePath)

		}	
			
	}
 }
 

//--- functions internally used by mmLibrary  
function _mmSetAppDefaults() {
	
	mmSetAppTitle("");
	_mmClearAppMenu();
	if (!oMM) return;
	oMM.helpURL="";
	oMM.contentChanged=top._mmChagedContent;
}


function mmWriteToLog(sEventType, sEventCategory, sEventDescription) {

	if (oMM==null) return;    
	try {   
		if (Config_LogEvents==null) return;
	} catch(e) {
	   return;
}

if (Config_LogEvents.indexOf(sEventType + ";") == -1) return;
 var sURL= oMM.rootURL+"StartUp/Utils/WriteToLog.asp?TYPE=" + sEventType
		+ "&CATEGORY="+ sEventCategory + "&DESCRIPTION="+ escape(sEventDescription);
     mmSendText(sURL, null,true);
}


function mmSetPortalMsg(sMsg) {
	if  (window.self==window.top)
 		document.all.mmPortalMsg.innerHTML=sMsg;
 	else {
 		parent.document.all.mmPortalMsg.innerHTML=sMsg;
 	}
}

function mmSendText(sURL, sText, bAsync){
	var xmlhttp;
	var async = bAsync ? bAsync: false;
	try {
	    if (window.XMLHttpRequest) {
	        xmlhttp = new window.XMLHttpRequest();
	    }
	    else {
	        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	    }
     }
    catch(e) {
       	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.open("POST", sURL, async);
    if (sText != null && sText.length > 0) {
       xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xmlhttp.setRequestHeader("Content-Length", sText.length);
    }
	
	xmlhttp.send(sText);
	if  (async) return "";

	var sResp=xmlhttp.responseText;
	xmlhttp = null;
	return sResp;
}



function mmOpenWindowInCenter(URL, WindName, width, height, options, bMakeModal, title ) {

    if (WindName == null || WindName.length == 0) WindName = "PopupWnd";
    var baseWnd = window.top;
  
    if (window == window.top) {
        if (window.opener)
            baseWnd = window.opener;
        else  if (window.parent)
            baseWnd = window.parent;
    }
   
    
    var left=0;
    try {
        left = baseWnd.screenLeft + (baseWnd.document.body.clientWidth - width) / 2;
       } 
    catch (ex) { // when https
          left = (screen.availWidth - width) / 2;
    }    
        var opt = "width=" + width + ",height=" + height + ",left=" +left+  ",top=" + (screen.availHeight-height)/2  + ",status=0," + options;

	var wndInCenter= window.open(URL, WindName, opt);
	var theTitle = (title)? title :  "this";
	mmSetWndFocus(wndInCenter,  theTitle);
	
	if( bMakeModal){
		window.onfocus=function() {	
			try {
				if (wndInCenter) 	{if (!wndInCenter.closed ) wndInCenter.focus();}
				} catch (e) {}
		}
	}	
	return wndInCenter;			
}

/* --  helpSpot --*/
/* programatically create help spot for Page level help */
function mmCreateHelpSpot(helpID, description, spanElement, onClick) {
    //see example of call in openers/CommonJSFunc.js
    try {
        var helpSpot = document.createElement('img');
        helpSpot.id = helpID; 
        helpSpot.src = contextHelpIcon;
        helpSpot.style.cursor = 'hand';
        helpSpot.style.zIndex = "200";
        helpSpot.height = "11";
        helpSpot.width = "8";
        helpSpot.callback = onClick;
        helpSpot.onclick = _mmHelpSpotClicked;
        helpSpot.helpID=helpID;
        helpSpot.title = description; 
        
        if (spanElement) {
            spanElement.appendChild(helpSpot);
        }
        else {
            alert("Call to mmCreateHelpSpot() error: spanElement is null \nPage:\n" + document.location)
        }
    }
    catch (err) {
        alert("mmCreateHelpSpot() error: " + err + '\nPage:\n' + document.location);
        retrurn;
    }


}

function mmOpenHelp(helpId, paramsObj) {
    var url="/portal/startup/Help/Help.asp?helpID=" + helpId;
    for(var name in paramsObj) {
        url +="&" +name + "=" + escape(paramsObj[name]);
    }
    mmOpenWindowInCenter(url, "wndHelp",  640,700, "toolbar=0,resizable=1,status=0" ,null, false,"");
}



function _mmHelpSpotClicked() {
    var helpSpot = this; 
    helpSpot.callback(helpSpot.helpID);
}


/* -- end  helpSpot --*/

/* -----------------context help-------------------------*/

var contextHelpService = "/portal/startup/help/ContextHelp/GetHelpContent.asp";
var contextHelpIcon="/portal/startup/help/ContextHelp/helpHotSpot.gif";
var contextHelpEditor = "/portal/net/contextHelp/editor.aspx";
var contextHelpParams = "";

var contextFirstTime = true;
var contextWnd=null

function ContextObject() {
    this.Init = function () {
        if (typeof window.top.document.all.iframeHlp == "undefined") {
            var div = window.top.document.createElement('div');
            div.zIndex = "200";
            div.innerHTML = hlpGetHTML();
            document.body.appendChild(div)
            contextHelpParams = getParamas();
        }
        else {
            hlpHide(true);
        }
        return true;
    }

    this.Load = function (theDocument, url) {
        var elem;
        var forCurrentDoc = arguments.length > 0;
        var doc = theDocument ? theDocument : document
        var str = mmSendText(contextHelpService + "?mode=getHotSpots&url=" + document.location.pathname, null, false);
        
        if (str.length == 0) return;
        var id = str.split("|");
        if (id[0] == 'y') 
            doc.oncontextmenu = contextMenu;
        
        var elemId;
        var hotSpotProp;
        
        for (var i = 1; i < id.length; i++) {
            hotSpotProp = id[i].split(":");          
            try {
                elem = doc.getElementsByName(hotSpotProp[0]);
            }
            catch (e) { //happens on google maps load
                return;
            }

            if (elem != null) {
                for (var j = 0; j < elem.length; j++) {
                    var id1 = elem[j].id1;
                    if (id1 == "") id1 = elem[j].name;
                    if (id1 != "") addHotSpot(elem[j], false, hotSpotProp[1]);
                }
            }
            else {
                elem = doc.getElementById(hotSpotProp[0]);
                if (elem != null) {
                    addHotSpot(elem, false, hotSpotProp[1]);
                }
            }
        }
        return true;
    }

    function getParamas() {
        var pars = "";
        var query = document.location.search;
        if (query.length == 0) return "";
        query = query.substr(1);
        var arr = query.split('&');
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].toUpperCase().indexOf("CLAIM_NO") == 0)
                pars += "&" + arr[i];
        }
        return pars;
    }
}

function contextHelpViewDoc(ID) {
    window.open("/portal/startup/Help/HelpAdmin/ShowHelpDocument.asp?catID=1&ID=" + ID,
	 "doc", "Width=800,height=" + (screen.availHeight - 100) + ",left=20,top=20,status=no,toolbar=0,scrollbars=1,resizable=1")
    
}

var hlpTimer = null;

function hlpStartTimer(fromhlpShow) {
    if (fromhlpShow) window.top.IsHelpActive = true;
    if (hlpTimer != null)
        window.clearTimeout(hlpTimer);

    hlpTimer= window.setTimeout(hlpUpdateContent, 2000)
}


function hlpUpdateContent() {
    var value = hlpGetFieldValue(window.top.currentHelpId);
    if (value != window.top.CurrentScreenValue) {
        window.top.CurrentScreenValue= value;
        if (window.top.IsHelpActive) 
           hlpShow(window.top.currentHelpId, window.top.contextCurrentObj, true, true)

   }
   else hlpStartTimer(false);
    
}

function hlpShow(help_ID, theObj, refresh, isCallBack) {
  
    isCallBack = arguments.length > 3 ? isCallBack : false;
    if (!isCallBack) {
        window.top.CurrentUrl = document.location.pathname;
        window.top.CurrentLocation=document.location;
        window.top.CurrentQueryString = document.location.search;
        window.top.CurrentQueryString = window.top.CurrentQueryString.replace("?", "");
    }
    
    var obj = (arguments.length > 1) ? theObj : window.event.srcElement;
    if (obj == window.top.contextCurrentObj && refresh != true) {
        hlpHide(true);
        window.top.contextCurrentObj = null;
        return;
    }
    
    window.top.contextCurrentObj = obj;
    if (obj == null)         return; // just in case
    
   window.top.currentHelpId = (arguments.length > 0) ? help_ID : helpGetId(window.top.contextCurrentObj);
   window.top.CurrentScreenValue= hlpGetFieldValue(window.top.currentHelpId);
   var divHelp = window.top.document.all.iframeHlp; 
   
    var helpObj = hlpGetContent(window.top.currentHelpId);
    var x = obj.offsetLeft;
    var y = obj.offsetTop;

        while (obj.parentElement) {
            obj = obj.parentElement;
            if (obj.tagName != 'TBODY' && obj.tagName != 'TR' && obj.tagName != 'CENTER') {
                x += obj.offsetLeft;
                y += obj.offsetTop;
            }
        }
    
    window.top.contextCurrentObj = obj;

    //---- x/y taken from event:  works fine if hlpShow called from event
    var evt = window.event;
    if (evt != null) {
        /*
        var clickX, clickY;
        if ((evt.clientX || evt.clientY) && document.body && document.body.scrollLeft != null) {
            clickX = evt.clientX + document.body.scrollLeft;
            clickY = evt.clientY + document.body.scrollTop;
        }
        if ((evt.clientX || evt.clientY) && document.compatMode == 'CSS1Compat' && document.documentElement && document.documentElement.scrollLeft != null) {
            clickX = evt.clientX + document.documentElement.scrollLeft;
            clickY = evt.clientY + document.documentElement.scrollTop;
        }
        if (evt.pageX || evt.pageY) {
            clickX = evt.pageX; clickY = evt.pageY;
        }
        x = clickX - evt.offsetX; y = clickY - evt.offsetY;
       */

        //NEW

        x = evt.screenX - window.top.screenLeft - evt.offsetX;
        y = evt.screenY - window.top.screenTop - evt.offsetY;

        window.top.contextCurrentObj.hotSpotX = x;
        window.top.contextCurrentObj.hotSpotY = y;
    }
    else {
        if (typeof window.top.contextCurrentObj.hotSpotX != "undefined") {
         
            x = window.top.contextCurrentObj.hotSpotX;
            y = window.top.contextCurrentObj.hotSpotY;
        }
    }

    if (helpObj.posType == helpObj.posTypes.Relative) {//relative
        x += helpObj.x;  y += helpObj.y;
    }

    if (helpObj.posType == helpObj.posTypes.Absolute  ) {//abs
        x = helpObj.x;      y = helpObj.y;
    }

    if (helpObj.posType == helpObj.posTypes.Window) {
        x = helpObj.x;  y = helpObj.y;
    
    }

    divHelp.style.left = x + 15;
    divHelp.style.top = y + 5;
    divHelp.style.display = "";

    //-----iframe -
    divHelp = window.top.document.all.iframeHlp;

    divHelp.style.display = "";
    divHelp.style.width = helpObj.width;
    divHelp.style.height = helpObj.height;

    var frameWnd = window.top.document.frames["iframeHlp"];
    obj = frameWnd.document.body.style;
    obj.backgroundColor = helpObj.color;
    obj.scrollbarShadowColor = helpObj.color;
    obj.scrollbarDarkShadowColor = helpObj.color;
    obj.scrollbarArrowColor = helpObj.color;
    obj.scrollbar3dlightColor = helpObj.color;
    obj.scrollbarTrackColor = helpObj.color;

    if (contextFirstTime) {
        contextFirstTime = false;
        frameWnd.document.body.innerHTML = window.top.document.all.divHelp.innerHTML;
        frameWnd.document.body.style.overflow = "auto";
        frameWnd.document.body.style.margin = 0;
        frameWnd.document.body.style.padding = 0;
    }
    window.top.contextCurrentObj.style.marginLeft = helpObj.hotSpotOffset + "px";
    
    if (helpObj.posType == helpObj.posTypes.Window) {
         hlpHide(false);
         var wndParams = "status=0,menubar=0,location=0,resizable,scrollbars=1,width=" + helpObj.width + ",height=" + helpObj.height + ",top=" + y + ",left=" + x;
          var theContent;

        if (helpObj.contentType == helpObj.contentTypes.Url) {
            theContent = '<iframe id="iframeHlp2" style="height:100%; width:100%;margin:0" frameborder="1" marginheight="0" marginwidth="0" framespacing="0" src="{0}"></iframe>';
            theContent = theContent.replace('{0}', helpObj.content);
        }
        else {
            theContent= helpObj.content;
        }
        window.top.document.all.helpContent.innerHTML = theContent
        var html = "<BODY style='heght:100%;width:100%;margin:0;background-color:" + obj.backgroundColor + "'>" + window.top.document.all.divHelp.innerHTML + "</BODY>";
        window.top.document.all.helpContent.innerHTML = "";
      
        contextWnd = window.open("", "wndContext", wndParams, false, "");
        contextWnd.document.write(html);
        contextWnd.document.close();
        contextWnd.focus();
        hlpStartTimer(true);
    }
    else {            // iframe on the page
        if (helpObj.contentType == helpObj.contentTypes.Url) {
           html = '<iframe id="iframeHlp" style="height:100%; width: 100%;margin:0" frameborder="1" marginheight="0" marginwidth="0" framespacing="0" src="{0}"></iframe>'
           html = html.replace('{0}', helpObj.content);
           frameWnd.document.all.helpContent.innerHTML = html;
        }
        else { //content
            frameWnd.document.all.helpContent.innerHTML = helpObj.content;
        }
        
        hlpStartTimer(true);
    }

    return false;
}


function helpGetId(obj) {
    if (obj == null) return null;
    if (obj.name) return obj.name;
    return obj.id;
}

function hlpHide(cleanUpCurrObject) {
    
    window.top.document.all.iframeHlp.style.display = "none";
    if (cleanUpCurrObject) {
        window.top.contextCurrentObj = null;
    }

    try { contextWnd.close() }
    catch (e) { }

    if (hlpTimer != null) {
        window.clearInterval(hlpTimer);
    }
    window.top.IsHelpActive = false;
}


function hlpGetHTML() {
    return mmSendText(contextHelpService + "?mode=init",null,false);
}

function hlpGetFieldValue(id) {
    var obj = document.all[id];       // what if it's array
    var value;	
    if (typeof obj == "undefined") {
        return '';
      }
    else {
       value =  "SELECT,INPUT".indexOf(obj.tagName) != -1  ? obj.value : obj.innerText; //for hidden it doesnot work
    }
    return  ( value.length>100) ? value.substr(0,100): value;
}

function hlpGetFieldValueAsParam(id) {

    if (id == null) return "";
    var value = window.top.CurrentScreenValue; 
    if  (value==null || value.length == 0) return "";
    if  (value.length>100) value=value.substr(0,100)
    return  "&screen=" + escape(value);
}

function hlpGetContent(helpId) {
    var str = mmSendText(contextHelpService + "?helpId=" + helpId + hlpGetFieldValueAsParam(helpId)
        + "&URL=" + escape(window.top.CurrentUrl)
        + "&QS="+ escape(window.top.CurrentQueryString)
        + contextHelpParams, null, false);

    var arr = str.split("|");
    this.posTypeAuto = parseInt(arr[5], 10);
    this.width = arr[0];
    this.height = arr[1];
    this.color = arr[2];
    this.x = parseInt(arr[3], 10);
    this.y = parseInt(arr[4], 10);
    this.posType = parseInt(arr[5], 10);
    this.hotSpotOffset = arr[6]
    this.contentType=parseInt(arr[7], 10);
    this.content = arr[8].trim();
    this.posTypes = {  Auto:0,   Relative : 1,  Absolute: 2, Window: 3  };
    this.contentTypes = { Html:0,  Url : 1   };
    return this;
}


function hlpEdit() {
    var params = "width=770,height=650,top=100,left=100,resizable=1";
    window.open(contextHelpEditor + 
         "?helpID=" + window.top.currentHelpId + hlpGetFieldValueAsParam(window.top.currentHelpId)
        + "&URL=" + escape(window.top.CurrentUrl)
        + "&QS=" + escape(window.top.CurrentQueryString)
        ,  "editContHelp", params);
}


function hlpEditDoneCallback() {
    hlpShow(window.top.currentHelpId, window.top.contextCurrentObj, true,true)
}


var contextPrevElem;
var contextPrevElemStyle;
var contextDisabled; 

function contextMenu() {
    if (typeof window.top.contextDisabled == "undefined") {
        var ans = window.confirm("You are in Context Help Admin mode.\nClick OK to continue or Cancel to exit from admin mode")
        if (!ans) {
            document.oncontextmenu = null;
            window.top.contextDisabled = true;
            return;
        }
        else 
            window.top.contextDisabled = false;
    }
    else {
        if (window.top.contextDisabled) {
                document.oncontextmenu = null;
                return;
            }
        }
 
    window.event.cancelBubble = true;
    addHotSpot(window.event.srcElement, true,0)
    return false;
}

var hotSpotIcontSuffix = "__hs";

function addHotSpot(elem, highlightElement, xOffset) {
    if (highlightElement) {
        if (contextPrevElem != null) contextPrevElem.style.backgroundColor = contextPrevElemStyle;
        contextPrevElemStyle = elem.style.backgroundColor;
        contextPrevElem = elem;
        elem.style.backgroundColor = "#FFDDCC";
    }
  
    var nextElem = elem.nextSibling;
    if (typeof next != "undefined") {
        if (next != null && next.innerHTML == 's') {
            return false; // hot spot is already here
        }
    }

    var elemId = elem.id;
    if (elemId == "") elemId = elem.name;

    if (elemId == "" || typeof elemId == "undefined") {
        alert("Cannot create help spot: this element has neither ID nor Name.");
        return false;
    }

    if (elemId.indexOf(hotSpotIcontSuffix) > -1) return;  // prevent multipls
    var hotSpotIcontId = elemId + hotSpotIcontSuffix;
        
    var helpElem = document.createElement('img');
    helpElem.src = contextHelpIcon
    helpElem.style.cursor = 'hand';
    helpElem.style.zIndex = "200";
    helpElem.id = hotSpotIcontId;
    helpElem.height="11"
    helpElem.width = "8"
        
    if (xOffset != 0) {
        helpElem.style.marginLeft = xOffset.toString()+"px";
    }
    helpElem.onclick = function () {  hlpShow(elemId) };

   
    if (nextElem) {
        if (nextElem.id != hotSpotIcontId) {
            nextElem = elem.nextSibling;
            if (nextElem.nodeName == 'IMG' && nextElem.nameProp == 'btn_calendar.gif') {
                nextElem = elem.nextSibling;
            
            }
            
            if (nextElem) {
                elem.parentNode.insertBefore(helpElem, nextElem);
            }
            else {
                elem.parentNode.appendChild(helpElem);
            }
        }
    }
    else {
        elem.parentNode.appendChild(helpElem);

    }

    
}


//  boot 

var ContextHelp;
function startContext(currentDoc) {

    var doc = currentDoc == null ? document : currentDoc;
    if (doc.body == null) {
        window.setTimeout(startContext, 70)
        return;
    }
    ContextHelp = new ContextObject();
    ContextHelp.Init();
    ContextHelp.Load(doc);
    try {
        Sys.Application.add_load(ContextHelp.Load);
        return;
     }
    catch (e) { }
    
}
startContext(null)

//- context end 