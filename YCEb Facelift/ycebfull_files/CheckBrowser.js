

function checkBrowser() {
    var ver = mmBrowserVersion(true);
   if (ver != 7 && ver != 6) {
        window.showModalDialog("BrowserSetup.asp",null,"dialogHeight:300px;dialogWidth:600px;resizable=1;center=1;status:0");
    }
}


function mmBrowserVersion(compatibleVerion) {// copy is in PortalLib
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    var wind = ua.indexOf()
    var ver = 0; //If not IE , return 0

    if (ua.indexOf("Trident/7.0") > 0 && ua.indexOf("rv:11.0") > 0 && ua.indexOf("Mozilla/5.0") >= 0) {
        ver = 11;
    }
    if (msie > 0) {  // If IE, return version number
        var ver = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
        if (ver == 7) {
            // compat mode
            if (compatibleVerion == false) {
                if (ua.indexOf('Trident/4.0') > 0) return 8;
                if (ua.indexOf('Trident/5.0') > 0) return 9;
                if (ua.indexOf('Trident/6.0') > 0) return 10;
                if (ua.indexOf('Trident/7.0') > 0) return 11;
            }
        }

    }

    return ver;

}

function msieVersionDescription() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    var calculatedVer = mmBrowserVersion(true);
    
    var ver = 0;  
    if (calculatedVer == 11) {
        return 'IE version 11';
    }
    if (msie <= 0) {
        ver = " Non-IE";
    }
    if (msie > 0) {   
        var ver = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
        if (ver == 7) {
            if (ua.indexOf('Trident/4.0') > 0) return "IE version 8 in compatible with IE7 mode";
            if (ua.indexOf('Trident/5.0') > 0) return "IE version 7 compat with 7";
            if (ua.indexOf('Trident/6.0') > 0) return "IE version 10 compat with 7";
            return "IE v7";
        }
        else {
            return "IE Version " + ver;
        }

    }
    return ver;

}